#!/bin/bash

#Returns the arguments to be used for dumpsnapshot as it uses different directory for snapshot when run in update database

args=$@

#getPath of db_utils. It is in same directory as this file
DB_UTILS_PATH=$(cd "$(dirname "$0")"; pwd)

. $DB_UTILS_PATH/commandLineParser.sh $args

ARGS_WITHOUT_VERSIONNUM_AND_SNAPSHOT=$($DB_UTILS_PATH/getMysqlArguments.sh inc-database-for-arg)

#Get other arguments excluding snapshot

ARGS_WITHOUT_VERSIONNUM_AND_SNAPSHOT=$ARGS_WITHOUT_VERSIONNUM_AND_SNAPSHOT" -e$parsed_DatabaseType"
ARGS_WITHOUT_VERSIONNUM_AND_SNAPSHOT=$ARGS_WITHOUT_VERSIONNUM_AND_SNAPSHOT" -x$parsed_DatabaseType_Password"

echo $ARGS_WITHOUT_VERSIONNUM_AND_SNAPSHOT