#!/bin/bash

if [ $# -lt 1 ]
then
	echo "Invalid arguments;	correct usage: <script> <password>";
	exit 1;
fi

dbTypePassword=$1;
echo -n $dbTypePassword | openssl dgst -sha512 | sed 's/^.* //'

