#!/bin/bash


args=$@

#getPath of db_utils. It is in same directory as this file
DB_UTILS_PATH=$(cd "$(dirname "$0")"; pwd)

#parse the arguments
. $DB_UTILS_PATH/commandLineParser.sh $args

#Get sql arguments
SQL_ARGS=$($DB_UTILS_PATH/getMysqlArguments.sh)

#read current database version
currentType=$(mysql $SQL_ARGS --protocol tcp --execute="drop schema $parsed_DatabaseName" --silent --skip-column-names)
if [ "$?" != "0" ]; then
	echo "Drop database fail"
	exit;
fi

echo "deleted database $parsed_DatabaseName"


