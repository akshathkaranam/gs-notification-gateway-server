def buildComponent = "NotificationGateway"
def buildWarName = "gsngw.war"
def buildDirectory = "gsnotificationgateway"
def ecrName = "076544065911.dkr.ecr.us-east-1.amazonaws.com"
def ecrProject = "notificationgw-stage"
//def oldImage = "076544065911.dkr.ecr.us-east-1.amazonaws.com/notificationgw-stage:v1.34"
def newImage = ""
def testAutomationPath = "notificationgatewaytestautomation"
def DBType = "dev"
def SVN_VERSION=1
//def apache_version

pipeline {

    agent any

    options {
	    //Only keep the 30 most recent builds
	    buildDiscarder(logRotator(numToKeepStr:'30'))
	    disableConcurrentBuilds()
	 }

     stages {

         stage ('Choose your pipeline stages'){

             agent{ 
                label 'testnode_4a'
                }

			steps {
				script {

					env.DEPLOY_STAGE_CHOICE = input message: 'User input required', ok: 'Continue',
							parameters: [choice(name: 'DEPLOY_STAGE_CHOICE', choices: 'execute\ndo not execute', description: 'Do you want to execute the Deploy stage?')]
					env.TESTING_STAGE_CHOICE = input message: 'User input required', ok: 'Continue',
							parameters: [choice(name: 'TESTING_STAGE_CHOICE', choices: 'execute\ndo not execute', description: 'Do you want to execute the Testing stage?')]
				}
				echo "Deploy stage: ${env.DEPLOY_STAGE_CHOICE}"
				echo "Testing stage: ${env.TESTING_STAGE_CHOICE}"
				
			}
		}

         stage ('Initialization & Preparation') {
             agent{ 
                label 'testnode_4a'
            }
            
            steps {   

                    script {

					env.DB_VERSION = input(
                        id: 'userInput', message: 'Enter latest DB version', parameters: [
                        [$class: 'TextParameterDefinition', defaultValue: '', description: 'requesting latest db version', name: 'db_version']])
					
				}       	 
					
                    sh """
                        echo "Starting CI for notification gateway"
                        echo "{ " >> ${WORKSPACE}/${buildDirectory}/src/main/webapp/buildInfo.txt
                        echo "\"type\"":"\"BuildInfo\"," >> ${WORKSPACE}/${buildDirectory}/src/main/webapp/buildInfo.txt
                        echo "\"jenkins_build\"":"${env.BUILD_NUMBER}," >> ${WORKSPACE}/${buildDirectory}/src/main/webapp/buildInfo.txt
                        echo "\"date_time\"":"\"${env.BUILD_ID}\"," >> ${WORKSPACE}/${buildDirectory}/src/main/webapp/buildInfo.txt
                        echo "\"svn_version\"":"${SVN_VERSION}," >> ${WORKSPACE}/${buildDirectory}/src/main/webapp/buildInfo.txt
                        echo "\"db_version\"":"${env.DB_VERSION}," >> ${WORKSPACE}/${buildDirectory}/src/main/webapp/buildInfo.txt
                        echo "\"db_type\"":"\"${DBType}\"" >> ${WORKSPACE}/${buildDirectory}/src/main/webapp/buildInfo.txt
                        echo " }" >> ${WORKSPACE}/${buildDirectory}/src/main/webapp/buildInfo.txt
                        cat ${WORKSPACE}/${buildDirectory}/src/main/webapp/buildInfo.txt
                    """
                 }
        }

        stage ('Build process for Notification Gateway') {
            agent{ 
                label 'testnode_4a'
            }

            steps{
                
                configFileProvider([configFile(fileId: 'nexus-maven-settings', variable: 'NEXUS_SETTINGS')]) {
                        sh "mvn -f ${buildDirectory}/pom.xml -s $NEXUS_SETTINGS -DskipTests -Drevision=1.0.0-${env.BUILD_ID} clean deploy"
                }
                

                sh """
                tar -czvf db_schemas.tar.gz db_schemas 
                tar -czvf db_utils.tar.gz db_utils 
                tar -czvf Tomcat.tar.gz ${WORKSPACE}/configurations/Tomcat 
                tar -czvf db_configurations.tar.gz ${WORKSPACE}/configurations/db_configurations   
                """
            }
            post {

                success {
			      //archive "target/**/*"			      
			      archiveArtifacts artifacts:"${buildDirectory}/target/${buildWarName},${WORKSPACE}/Tomcat.tar.gz,${WORKSPACE}/db_configurations.tar.gz"
                  stash includes: "${buildDirectory}/target/${buildWarName}", name: "${buildComponent}"
                  echo "Maven build successfully completed. Artifacts have been archived"			      
			    }

			    failure {
			    	echo "Maven build failed. Please check logs"
			    }
            }
        }

        /*stage ('Unit Tests') {
                //Placeholder for unit tests stage
        }*/

         /*stage (SonarQube Analysis) {
                //Placeholder for SonarQube stage
        }*/

        stage ('Dockerize Notification Gateway and push to ECR') {

            when {
				expression {
						return env.DEPLOY_STAGE_CHOICE=='execute';
					}
            }
            agent{ 
                label 'testnode_4a'
             }

            steps{
            unstash "${buildComponent}"
            script{
                newImage = "${ecrName}/${ecrProject}:v1.${env.BUILD_ID}"
            }
            sh """
            python3 tomcat_version.py >> version.txt
            apache_version=\$(cat version.txt)
            pwd
            aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin ${ecrName}/${ecrProject}
            docker build --build-arg VERSION=8.0.32 -t ${newImage} .
            docker push ${newImage}
            rm -r version.txt
            """
            }

        }


        stage ('Deploy using Docker Compose') {

            when {
				expression {
						return env.DEPLOY_STAGE_CHOICE=='execute';
					}
            }
            agent{ 
                label 'testnode_4a'
             }

            steps{
            unstash "${buildComponent}"    
            sh """
            aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin ${ecrName}/${ecrProject}
            IMAGE_ID=${newImage} docker-compose up -d
            sleep 60
            docker exec -i ngw-db /var/db-install.sh ${env.DB_VERSION}
            echo "done" > db.txt
            docker cp ./db.txt ngw-service:/db.txt
            sleep 120
            """
            }
        }

        stage ('QA Automation') {
                when {
				expression {
						return env.TESTING_STAGE_CHOICE=='execute';
					}
            }

            agent{ 
                label 'testnode_4a'
             }
                //Placeholder for QA Automation stage


            steps{
                		        
		    	echo "..............................\nExecuting Test NG based testing\n.............................."
		    	unstash "${buildComponent}" 
	   			configFileProvider([configFile(fileId: 'nexus-maven-settings', variable: 'NEXUS_SETTINGS')]) {
		   		
		    		catchError {
		    			sh "mvn -f ${testAutomationPath}/pom.xml -s $NEXUS_SETTINGS  -DSUITE_XML_FILES='suites/AllSuites.xml' -DCONFIG_FILE='local_mac/config_keyvalue.txt' clean test"
		    		}
		
				}
					   
										
		
		    	}		    	 
 
		    
			post {
				always {
                    
                     publishHTML([allowMissing: true, alwaysLinkToLastBuild: true, keepAll: true, reportDir: 'notificationgatewaytestautomation/gsnotificationgateway/target/surefire-reports/html/', reportFiles: 'index.html', reportName: 'HTML Report', reportTitles: ''])
                     step([$class: 'Publisher', reportFilenamePattern: 'notificationgatewaytestautomation/gsnotificationgateway/target/surefire-reports/*.xml'])
                     }

                success {		
                	echo "TestNG Execution Successful"
			    }

			    failure {
			    	echo "Ops! Maven build failed for Test automation"
			        }
			    }	
            }
        }
        


     post {


		cleanup {
  
            echo "cleaning the workspace for this branch to avoid leaving unnecessary directories. It is understood that it will impact the build performance negatively in the next run on this branch"
            cleanWs()
            
    }

	} 
}