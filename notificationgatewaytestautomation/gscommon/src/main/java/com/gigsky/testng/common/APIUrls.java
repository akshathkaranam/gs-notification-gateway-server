/*
 * Copyright (c) 2016. GigSky, Inc.
 * All rights reserved.
 *
 * THIS SOFTWARE IS THE CONFIDENTIAL AND PROPRIETARY
 * INFORMATION OF GIGSKY, INC.
 *
 * UNAUTHORIZED USE OR DISCLOSURE IS PROHIBITED.
 *
 * https://www.gigsky.com/
 */
package com.gigsky.testng.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class APIUrls {
    private static final Logger logger = LoggerFactory.getLogger(APIUrls.class);

    public enum APIServer {
        NOTIFICATIONGATEWAY
    };


    public enum APIName {
        API_INFO,
        GET_CUSTOMERDETAILS,
        ADD_DEVICE,
        GET_DEVICE,
        CREATE_EVENT,
        DELETE_CUSTOMER_DETAILS,
        GET_EVENT,
        APP_FEATURE_INFO,
        UPDATE_DEVICE,
        DELETE_DEVICE,
        NOTIFICATION_STATUS,
        PARTNER_APP_REGISTER,
        PARTNER_APP_GET_DEVICE

    }


    public enum APIPathParams {
        CUSTOMER_UUID,
        EVENT_ID,
        DEVICE_ID,
        NOTIFICATION_ID
    }

}
