#!/usr/bin/env bash
# ============= Deinit +++
echo "Jenkins: Shutting down Tomcat"
sudo /etc/init.d/tomcat8 stop

sudo cp /var/lib/tomcat8/logs/catalina.out $WORKSPACE/

# ===== Delete all db schemas
sudo $WORKSPACE/gsnotificationgateway-artifacts/db_utils/deleteLocalDatabase.sh -dgigskyNotificationGateway -uroot -proot

#=========Delete all artifcats
sudo rm -rf /var/lib/tomcat8/webapps/*

cd $WORKSPACE

sudo cp -a $WORKSPACE/gsnotificationgateway/target/gsNGWTest.jar $WORKSPACE/

#echo "copying jacoco.exec to workspace"
#outputFileName=$JOB_NAME
#suffix="_jacoco.exec"
#outputFileName=$outputFileName$suffix

#echo $SUDO_PASSWORD | sudo -S mv /var/lib/tomcat8/logs/jacoco.exec $outputFileName
#echo $SUDO_PASSWORD | sudo -S sed -i "s/JAVA_OPTS='-Xmx2048m -javaagent:\/home\/jenkins\/org.jacoco.agent-0.7.4.201502262128-runtime.jar=destfile=\/var\/lib\/tomcat8\/logs\/jacoco.exec,append=true'/JAVA_OPTS='-Xmx2048m'/g" /usr/share/tomcat8/bin/catalina.sh

