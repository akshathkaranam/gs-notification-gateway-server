
-- Host: localhost    Database: gigskyNotificationGateway
-- ------------------------------------------------------
-- Server version	5.6.23
SET autocommit=0;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `ConfigurationsKeyValue`
--

LOCK TABLES `ConfigurationsKeyValue` WRITE;
/*!40000 ALTER TABLE `ConfigurationsKeyValue` DISABLE KEYS */;
INSERT INTO `ConfigurationsKeyValue` (`ckey`, `value` ) VALUES ('AUDIENCE_ENABLE_ALL','YES');
INSERT INTO `ConfigurationsKeyValue` (`ckey`, `value`) VALUES ('DEVICE_COUNT','3');
INSERT INTO `ConfigurationsKeyValue` (`ckey`, `value`) VALUES ('EVENT_EXEC_TIME_MS','000000000001000');
INSERT INTO `ConfigurationsKeyValue` (`ckey`, `value`) VALUES ('EVENTS_PROCESSING_COUNT','500');
INSERT INTO `ConfigurationsKeyValue` (`ckey`, `value`) VALUES ('GATEWAY_MODE','TEST');
INSERT INTO `ConfigurationsKeyValue` (`ckey`, `value`) VALUES ('NOTIFICATION_PROCESSING_THREADS','4');
INSERT INTO `ConfigurationsKeyValue` (`ckey`, `value`) VALUES ('PROCESS_EVENTS','YES');
INSERT INTO `ConfigurationsKeyValue` (`ckey`, `value`) VALUES ('SHUTDOWN_WAIT_TIME_MS','000000000005000');
INSERT INTO `ConfigurationsKeyValue` (`ckey`, `value`) VALUES ('UA_FAILURE_MODE','NO');
INSERT INTO `ConfigurationsKeyValue` (`ckey`, `value`) VALUES ('EVENT_FAIL_COUNT','2');
INSERT INTO `ConfigurationsKeyValue` (`ckey`, `value`) VALUES ('RATE_CONTROL_TIME_MS','000000000000001');
INSERT INTO `ConfigurationsKeyValue` (`ckey`, `value`) VALUES ('MOCK_NOTIFICATION_DELAY','000000000000001');
INSERT INTO `ConfigurationsKeyValue` (`ckey`, `value`) VALUES ('NOTIFICATION_EXPIRY','000000000060000');
INSERT INTO `ConfigurationsKeyValue` (`ckey`, `value`) VALUES ('PARTNER_APP_DETAILS_EXPIRY','000001000000000');
INSERT INTO `ConfigurationsKeyValue` (`ckey`, `value`) VALUES ('GATEWAY_UTILITY_EXEC_TIME','000001000000000');
INSERT INTO `ConfigurationsKeyValue` (`ckey`, `value`) VALUES ('DEFAULT_TENANT_ID','1');
INSERT INTO `ConfigurationsKeyValue` (`ckey`,`value`,`createTime`,`updateTime`) VALUES ('EMAIL_FAIL_COUNT','3','2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ConfigurationsKeyValue` (`ckey`,`value`,`createTime`,`updateTime`) VALUES ('EMAIL_HOST','NONE','2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ConfigurationsKeyValue` (`ckey`,`value`,`createTime`,`updateTime`) VALUES ('EMAIL_PASSWORD','aIxqZy5YyWSaz8T/gr/ouNB3VWz10cTh','2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ConfigurationsKeyValue` (`ckey`,`value`,`createTime`,`updateTime`) VALUES ('EMAIL_USER','EjA/vfFj1j1mgFT30z+qpaqFZ7XgEThhd6JAIFCzZL8=','2015-10-19 12:48:16','2015-10-19 12:48:16');

INSERT INTO `ConfigurationsKeyValue` (`ckey`,`value`,`createTime`,`updateTime`) VALUES ('GSB_BASE_URL','http://localhost:8080/api/v4/','2015-10-19 12:48:16','2015-10-19 12:48:16');

INSERT INTO `ConfigurationsKeyValue` (`ckey`,`value`,`createTime`,`updateTime`) VALUES ('REQUEST_TIMEOUT_MS','000000000010000','2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ConfigurationsKeyValue` (`ckey`,`value`,`createTime`,`updateTime`) VALUES ('SMTP_HOST','NONE','2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `gigskyNotificationGateway`.`ConfigurationsKeyValue` (`ckey`, `value`) VALUES ('PURCHASE_PLAN_MODE', 'YES');

/*!40000 ALTER TABLE `ConfigurationsKeyValue` ENABLE KEYS */;
UNLOCK TABLES;


LOCK TABLES `Tenant` WRITE;
/*!40000 ALTER TABLE `Tenant` DISABLE KEYS */;
INSERT INTO `Tenant` (`id`, `name`, `description`, `createTime`, `updateTime` ) VALUES (1,'gigsky', 'gigsky tenant', '2015-10-19 12:48:16', NULL);

/*!40000 ALTER TABLE `Tenant` ENABLE KEYS */;
UNLOCK TABLES;


--
-- Dumping data for table `Customer`
--

LOCK TABLES `Customer` WRITE;
/*!40000 ALTER TABLE `Customer` DISABLE KEYS */;
INSERT INTO `gigskyNotificationGateway`.`Customer` (`id`, `tenant_Id`, `customerUuid`,`customerType`) VALUES ('1', '1', '046b6c7f-0b8a-43b9-b35d-6489e6daee91','CONSUMER');
/*!40000 ALTER TABLE `Customer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CustomerDevice`
--

LOCK TABLES `CustomerDevice` WRITE;
/*!40000 ALTER TABLE `CustomerDevice` DISABLE KEYS */;
/*!40000 ALTER TABLE `CustomerDevice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CustomerEvent`
--

LOCK TABLES `CustomerEvent` WRITE;
/*!40000 ALTER TABLE `CustomerEvent` DISABLE KEYS */;
/*!40000 ALTER TABLE `CustomerEvent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CustomerGroup`
--

LOCK TABLES `CustomerGroup` WRITE;
/*!40000 ALTER TABLE `CustomerGroup` DISABLE KEYS */;
/*!40000 ALTER TABLE `CustomerGroup` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Device`
--

LOCK TABLES `Device` WRITE;
/*!40000 ALTER TABLE `Device` DISABLE KEYS */;
/*!40000 ALTER TABLE `Device` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ErrorString`
--

LOCK TABLES `ErrorString` WRITE;
/*!40000 ALTER TABLE `ErrorString` DISABLE KEYS */;
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (1,11006,'Unknown error occurred',3,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (2,11007,'Resource not found',4,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (3,11005,'Invalid arguments',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (4,11301,'Invalid token',1,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (5,11305,'Token missing',1,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (6,11000,'API not supported',4,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (7,11001,'Method not supported',4,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (8,11002,'Internal server error',3,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (9,11003,'Database is out of reach',5,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (10,11004,'Operation not supported',4,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (11,11100,'Device registration failed',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (12,11101,'Device updation failed',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (13,11102,'Device registration requires valid device type',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (14,11103,'Device registration requires valid channel Id',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (15,11104,'Device registration requires valid locale',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (16,11105,'Device registration requires valid device token',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (17,11106,'Device registration requires valid installation Id',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (18,11107,'Device Id missing',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (19,11200,'Event already exists',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (20,11201,'Event creation failed',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (21,11202,'Event creation requires valid type',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (22,11203,'Event creation requires valid event type',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (23,11204,'Event creation requires valid location',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (24,11205,'Event creation requires valid user id',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (25,11206,'Event creation requires valid expiry',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (26,11207,'Event creation requires valid balance remaining',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (27,11208,'Event Id missing',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (28,11302,'Token expired',1,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (29,11303,'Invalid Customer ID (UUID)',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (30,11304,'Customer doesn\'t exist',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (31,11108,'Device not assigned to user',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (32,11109,'Device already added to user',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (33,11400,'Notification Id missing',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (34,11110,'Device deletion failed',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (35,11209,'Event creation requires valid percentage consumed',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (36,11210,'Event creation requires valid creation time',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (37,11211,'Event creation requires valid iccid',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (38,11111,'Device registration requires valid app version',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (39,11112,'Device registration requires valid os version',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (40,11212,'Event creation requires valid options',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (41,11213,'Event creation requires valid SIM name',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (42,11113,'Device registration requires valid preferences',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (43,11114,'Device registration requires valid location',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (44,11500,'Partner app device registration failed',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (45,11501,'Partner app device registration requires valid device type',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (46,11502,'Partner app device registration requires valid IP address',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (47,11503,'Partner app device registration requires valid locale',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (48,11504,'Partner app device registration requires valid device OS version',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (49,11505,'Partner app device registration requires valid date',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (50,11506,'Partner app device registration requires valid iccid',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (51,11507,'Partner app device registration requires valid location',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (52,11508,'Partner app device registration requires valid partner code',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (53,11509,'Partner app device details already exists',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (54,11510,'Partner app device details not found',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (55,11511,'Partner app device registration requires valid sdk version',2,'2015-10-19 12:48:16','2015-10-19 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (56,11600,'App feature info entry not found',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (57,11601,'App feature info already exist',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (58,11602,'App feature info requires valid client type',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (59,11603,'App feature info requires valid client version',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (60,11604,'App feature info requires valid user id',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (61,11605,'Multiple App feature info entries are found',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (62,11214,'Event creation requires valid language',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (63,11215,'Event creation requires valid sub event type',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (64,11216,'Event creation requires valid customer email',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (65,11217,'Event creation requires valid referral code',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (66,11218,'Event creation requires valid gscustomer id',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (67,11219,'Event creation requires valid advocate amount',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (68,11220,'Event creation requires valid referral amount',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (69,11221,'Event creation requires valid currency',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (70,11222,'Event creation requires valid new customer email',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (71,11223,'Event creation requires valid credit limit',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (72,11224,'Event creation requires valid csn',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (73,11225,'Event creation requires valid credit warning limit',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (74,11008,'Unable to connect to Backend Server',3,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (75,11009,'Invalid credentials',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (76,11700,'Default sim requires valid type',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (77,11701,'Default sim requires valid customer id',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (78,11702,'Default sim requires valid sim nickname',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (79,11703,'Default sim requires valid sim type',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (80,11704,'Default sim requires valid sim ICCID',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (81,11705,'Invalid SIM ID',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (82,11706,'No active subscriptions',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (83,11707,'No SIM ID',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (84,11708,'Purchase plan requires valid type',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (85,11709,'Purchase plan requires valid plan id',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (86,11710,'Purchase plan requires valid country code',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (87,11711,'Purchase plan requires billing info',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (88,11712,'Get plans requires valid country code',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (89,11713,'Get plans requires valid current location',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (90,11714,'Currently your SIM is not connected',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (91,11800,'Customer requires valid type',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (92,11801,'Customer requires valid old email id',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (93,11802,'Customer requires valid anonymous email id',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');
INSERT INTO `ErrorString` (`id`,`errorId`,`errorString`,`httpError_id`,`createTime`,`updateTime`) VALUES (94,11717,'Forbidden country not supported',2,'2016-10-07 12:48:16','2016-10-07 12:48:16');

/*!40000 ALTER TABLE `ErrorString` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Event`
--

LOCK TABLES `Event` WRITE;
/*!40000 ALTER TABLE `Event` DISABLE KEYS */;
/*!40000 ALTER TABLE `Event` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `EventPolicy`
--

LOCK TABLES `EventPolicy` WRITE;
/*!40000 ALTER TABLE `EventPolicy` DISABLE KEYS */;
INSERT INTO `EventPolicy` (`policy_id`,`eventType_id`,`tenant_id`,`enableEvent`,`createTime`,`updateTime`,`groupType_id`) VALUES (1,1,1,NULL,'2015-11-26 12:48:16','2018-06-26 19:46:12',NULL);
INSERT INTO `EventPolicy` (`policy_id`,`eventType_id`,`tenant_id`,`enableEvent`,`createTime`,`updateTime`,`groupType_id`) VALUES (2,2,1,NULL,'2015-11-26 12:48:16','2018-06-26 19:46:12',NULL);
INSERT INTO `EventPolicy` (`policy_id`,`eventType_id`,`tenant_id`,`enableEvent`,`createTime`,`updateTime`,`groupType_id`) VALUES (3,3,1,NULL,'2015-11-26 12:48:16','2018-06-26 19:46:12',NULL);
INSERT INTO `EventPolicy` (`policy_id`,`eventType_id`,`tenant_id`,`enableEvent`,`createTime`,`updateTime`,`groupType_id`) VALUES (4,4,1,NULL,'2015-11-26 12:48:16','2018-06-26 19:46:12',NULL);
INSERT INTO `EventPolicy` (`policy_id`,`eventType_id`,`tenant_id`,`enableEvent`,`createTime`,`updateTime`,`groupType_id`) VALUES (5,5,1,NULL,'2015-11-26 12:48:16','2018-06-26 19:46:12',NULL);
INSERT INTO `EventPolicy` (`policy_id`,`eventType_id`,`tenant_id`,`enableEvent`,`createTime`,`updateTime`,`groupType_id`) VALUES (6,6,1,NULL,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `EventPolicy` (`policy_id`,`eventType_id`,`tenant_id`,`enableEvent`,`createTime`,`updateTime`,`groupType_id`) VALUES (7,7,1,NULL,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `EventPolicy` (`policy_id`,`eventType_id`,`tenant_id`,`enableEvent`,`createTime`,`updateTime`,`groupType_id`) VALUES (8,8,1,NULL,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `EventPolicy` (`policy_id`,`eventType_id`,`tenant_id`,`enableEvent`,`createTime`,`updateTime`,`groupType_id`) VALUES (9,9,1,NULL,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `EventPolicy` (`policy_id`,`eventType_id`,`tenant_id`,`enableEvent`,`createTime`,`updateTime`,`groupType_id`) VALUES (10,10,1,NULL,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `EventPolicy` (`policy_id`,`eventType_id`,`tenant_id`,`enableEvent`,`createTime`,`updateTime`,`groupType_id`) VALUES (11,11,1,NULL,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `EventPolicy` (`policy_id`,`eventType_id`,`tenant_id`,`enableEvent`,`createTime`,`updateTime`,`groupType_id`) VALUES (12,12,1,NULL,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `EventPolicy` (`policy_id`,`eventType_id`,`tenant_id`,`enableEvent`,`createTime`,`updateTime`,`groupType_id`) VALUES (13,13,1,NULL,'2015-11-26 12:48:16','2015-11-26 12:48:16',NULL);
INSERT INTO `EventPolicy` (`policy_id`,`eventType_id`,`tenant_id`,`enableEvent`,`createTime`,`updateTime`,`groupType_id`) VALUES (14,14,1,NULL,'2015-11-26 12:48:16','2015-11-26 12:48:16',NULL);
INSERT INTO `EventPolicy` (`policy_id`,`eventType_id`,`tenant_id`,`enableEvent`,`createTime`,`updateTime`,`groupType_id`) VALUES (15,15,1,NULL,'2015-11-26 12:48:16','2015-11-26 12:48:16',NULL);
INSERT INTO `EventPolicy` (`policy_id`,`eventType_id`,`tenant_id`,`enableEvent`,`createTime`,`updateTime`,`groupType_id`) VALUES (16,16,1,NULL,'2015-11-26 12:48:16','2015-11-26 12:48:16',NULL);


/*!40000 ALTER TABLE `EventPolicy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `EventType`
--

LOCK TABLES `EventType` WRITE;
/*!40000 ALTER TABLE `EventType` DISABLE KEYS */;
INSERT INTO `EventType` (`id`,`type`,`subEventType`,`description`,`enableNotification`,`createTime`,`updateTime`) VALUES (1,'LOW_BALANCE',NULL,'Data Plan Expiring',1,'2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `EventType` (`id`,`type`,`subEventType`,`description`,`enableNotification`,`createTime`,`updateTime`) VALUES (2,'DATA_USED',NULL,'50% of Data Plan Remaining',1,'2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `EventType` (`id`,`type`,`subEventType`,`description`,`enableNotification`,`createTime`,`updateTime`) VALUES (3,'SUBSCRIPTION_NEARING_EXPIRY',NULL,'Data Plan Expires in 24 Hours',1,'2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `EventType` (`id`,`type`,`subEventType`,`description`,`enableNotification`,`createTime`,`updateTime`) VALUES (4,'SUBSCRIPTION_EXPIRED',NULL,'GigSky Data Plan Expired',1,'2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `EventType` (`id`,`type`,`subEventType`,`description`,`enableNotification`,`createTime`,`updateTime`) VALUES (5,'NEW_LOCATION',NULL,'Data Plan Available for Use',1,'2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `EventType` (`id`,`type`,`subEventType`,`description`,`enableNotification`,`createTime`,`updateTime`) VALUES (6,'EMAIL','REFERRAL_INITIATION','Referral Initiation',1,'2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `EventType` (`id`,`type`,`subEventType`,`description`,`enableNotification`,`createTime`,`updateTime`) VALUES (7,'EMAIL','NEW_CUSTOMER_SIGNUP','New Customer SignUp',1,'2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `EventType` (`id`,`type`,`subEventType`,`description`,`enableNotification`,`createTime`,`updateTime`) VALUES (8,'EMAIL','ADVOCATE_CREDIT_ADDED','Advocate Credit Added',1,'2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `EventType` (`id`,`type`,`subEventType`,`description`,`enableNotification`,`createTime`,`updateTime`) VALUES (9,'EMAIL','CREDIT_LIMIT','Credit Limit',1,'2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `EventType` (`id`,`type`,`subEventType`,`description`,`enableNotification`,`createTime`,`updateTime`) VALUES (10,'EMAIL','INTERNAL_CREDIT_ALERT','Internal Credit Alert',1,'2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `EventType` (`id`,`type`,`subEventType`,`description`,`enableNotification`,`createTime`,`updateTime`) VALUES (11,'EMAIL','INTERNAL_CREDIT_WARNING','Internal Credit Warning',1,'2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `EventType` (`id`,`type`,`subEventType`,`description`,`enableNotification`,`createTime`,`updateTime`) VALUES (12,'EMAIL','INTERNAL_CREDIT_DEDUCT','Internal Credit Deduct',1,'2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `EventType` (`id`,`type`,`subEventType`,`description`,`enableNotification`,`createTime`,`updateTime`) VALUES (13,'EMAIL','ICCID_PROMOTION_CREDIT','ICCID promotion Credit',1,'2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `EventType` (`id`,`type`,`subEventType`,`description`,`enableNotification`,`createTime`,`updateTime`) VALUES (14,'EMAIL','RABBIT_MQ_SERVER_UP','Rabbit MQ server up',1,'2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `EventType` (`id`,`type`,`subEventType`,`description`,`enableNotification`,`createTime`,`updateTime`) VALUES (15,'EMAIL','RABBIT_MQ_SERVER_DOWN','Rabbit MQ server down',1,'2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `EventType` (`id`,`type`,`subEventType`,`description`,`enableNotification`,`createTime`,`updateTime`) VALUES (16,'EMAIL','DELETED_ACCOUNTS_THRESHOLD','Deleted accounts thresold',1,'2015-11-26 12:48:16','2015-11-26 12:48:16');
/*!40000 ALTER TABLE `EventType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `GroupEvent`
--

LOCK TABLES `GroupEvent` WRITE;
/*!40000 ALTER TABLE `GroupEvent` DISABLE KEYS */;
/*!40000 ALTER TABLE `GroupEvent` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `GroupEventCustomer`
--

LOCK TABLES `GroupEventCustomer` WRITE;
/*!40000 ALTER TABLE `GroupEventCustomer` DISABLE KEYS */;
/*!40000 ALTER TABLE `GroupEventCustomer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `GroupType`
--

LOCK TABLES `GroupType` WRITE;
/*!40000 ALTER TABLE `GroupType` DISABLE KEYS */;
INSERT INTO `GroupType` (`id`, `name`, `description`) VALUES (1,'ALPHA','Alpha customers verification');
INSERT INTO `GroupType` (`id`, `name`, `description`) VALUES (2,'BETA','Beta customers verification');
/*!40000 ALTER TABLE `GroupType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `HttpError`
--

LOCK TABLES `HttpError` WRITE;
/*!40000 ALTER TABLE `HttpError` DISABLE KEYS */;
INSERT INTO `HttpError` (`id`, `errorCode`, `errorString`) VALUES (1,401,'Unauthorized');
INSERT INTO `HttpError` (`id`, `errorCode`, `errorString`) VALUES (2,400,'Bad Request');
INSERT INTO `HttpError` (`id`, `errorCode`, `errorString`) VALUES (3,500,'Internal Server Error');
INSERT INTO `HttpError` (`id`, `errorCode`, `errorString`) VALUES (4,404,'Resource Not Found');
INSERT INTO `HttpError` (`id`, `errorCode`, `errorString`) VALUES (5,503,'Service Unavailable');
/*!40000 ALTER TABLE `HttpError` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `MockPushNotification`
--

LOCK TABLES `MockPushNotification` WRITE;
/*!40000 ALTER TABLE `MockPushNotification` DISABLE KEYS */;
/*!40000 ALTER TABLE `MockPushNotification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `MockUser`
--

LOCK TABLES `MockUser` WRITE;
/*!40000 ALTER TABLE `MockUser` DISABLE KEYS */;
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (1,'046b6c7f-0b8a-43b9-b35d-6489e6daee91',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (2,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (3,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (4,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (5,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (6,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (7,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (8,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (9,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (10,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (11,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (12,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (13,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (14,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (15,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (16,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (17,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (18,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (19,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (20,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (21,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (22,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (23,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (24,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (25,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (26,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (27,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (28,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (29,'',1);
INSERT INTO `MockUser` (`userId`, `userUuid`, `tenant_id`) VALUES (30,'',1);
/*!40000 ALTER TABLE `MockUser` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `MockUserToken`
--

LOCK TABLES `MockUserToken` WRITE;
/*!40000 ALTER TABLE `MockUserToken` DISABLE KEYS */;
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (1,'d15af80d7d3f401cbf8aa007b28ac7e5',1);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (2,'bbe54d3e117049f0828f72c24d84c7e9',2);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (3,'57e2d3a802c543659ef4458fb37f9bd6',3);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (4,'57e2d3a802c543659ef4458fb37f9bd7',4);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (5,'57e2d3a802c543659ef4458fb37f9bd8',5);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (6,'57e2d3a802c543659ef4458fb37f9bd9',6);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (7,'57e2d3a802c543659ef4458fb37f9be1',7);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (8,'57e2d3a802c543659ef4458fb37f9be2',8);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (9,'57e2d3a802c543659ef4458fb37f9be3',9);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (10,'57e2d3a802c543659ef4458fb37f9be4',10);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (11,'57e2d3a802c543659ef4458fb37f9be5',11);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (12,'57e2d3a802c543659ef4458fb37f9be6',12);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (13,'57e2d3a802c543659ef4458fb37f9be7',13);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (14,'57e2d3a802c543659ef4458fb37f9be8',14);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (15,'57e2d3a802c543659ef4458fb37f9be9',15);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (16,'57e2d3a802c543659ef4458fb37f9bf1',16);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (17,'57e2d3a802c543659ef4458fb37f9bf2',17);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (18,'57e2d3a802c543659ef4458fb37f9bf3',18);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (19,'57e2d3a802c543659ef4458fb37f9bf4',19);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (20,'57e2d3a802c543659ef4458fb37f9bf5',20);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (21,'57e2d3a802c543659ef4458fb37f9bf6',21);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (22,'57e2d3a802c543659ef4458fb37f9bf7',22);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (23,'57e2d3a802c543659ef4458fb37f9bf8',23);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (24,'57e2d3a802c543659ef4458fb37f9bf9',24);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (25,'57e2d3a802c543659ef4458fb37f9bh1',25);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (26,'57e2d3a802c543659ef4458fb37f9bh2',26);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (27,'57e2d3a802c543659ef4458fb37f9bh3',27);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (28,'57e2d3a802c543659ef4458fb37f9bh4',28);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (29,'57e2d3a802c543659ef4458fb37f9bh5',29);
INSERT INTO `MockUserToken` (`id`, `basicToken`, `mockUser_userId`) VALUES (30,'57e2d3a802c543659ef4458fb37f9bh6',30);
/*!40000 ALTER TABLE `MockUserToken` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Notification`
--

LOCK TABLES `Notification` WRITE;
/*!40000 ALTER TABLE `Notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `Notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `NotificationTemplate`
--

LOCK TABLES `NotificationTemplate` WRITE;
/*!40000 ALTER TABLE `NotificationTemplate` DISABLE KEYS */;
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (1,1,1,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (2,1,2,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (3,1,3,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (4,1,4,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (5,1,5,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (6,1,1,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (7,1,2,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (8,1,3,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (9,1,4,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (10,1,5,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (11,1,1,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (12,1,2,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (13,1,3,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (14,1,4,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (15,1,5,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (16,1,1,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (17,1,2,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (18,1,3,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (19,1,4,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (20,1,5,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (21,1,1,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (22,1,2,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (23,1,3,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (24,1,4,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (25,1,5,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (26,1,1,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (27,1,2,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (28,1,3,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (29,1,4,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (30,1,5,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (31,1,1,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (32,1,2,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (33,1,3,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (34,1,4,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (35,1,5,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (36,1,1,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (37,1,2,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (38,1,3,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (39,1,4,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (40,1,5,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (41,1,1,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (42,1,2,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (43,1,3,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (44,1,4,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (45,1,5,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (46,3,6,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (47,3,7,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (48,3,8,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (49,3,9,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (50,3,10,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (51,3,11,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (52,3,12,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (53,3,6,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (54,3,8,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (55,3,6,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (56,3,8,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (57,3,6,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (58,3,8,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (59,3,6,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (60,3,8,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (61,3,6,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (62,3,8,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (63,3,6,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (64,3,8,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (65,3,6,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (66,3,8,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (67,3,6,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (68,3,8,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (69,3,13,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (70,3,13,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (71,3,13,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (72,3,13,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (73,3,13,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (74,3,13,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (75,3,13,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (76,3,13,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (77,3,13,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (78,3,14,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (79,3,15,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `NotificationTemplate` (`template_id`,`notificationType_id`,`eventType_id`,`createTime`,`updateTime`,`tenant_id`) VALUES (80,3,16,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);

/*!40000 ALTER TABLE `NotificationTemplate` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `NotificationType`
--

LOCK TABLES `NotificationType` WRITE;
/*!40000 ALTER TABLE `NotificationType` DISABLE KEYS */;
INSERT INTO `NotificationType` (`id`, `description`, `type`) VALUES (1,'Push Notification','PUSH');
INSERT INTO `NotificationType` (`id`, `description`, `type`) VALUES (2,'SMS','SMS');
INSERT INTO `NotificationType` (`id`, `description`, `type`) VALUES (3,'EMAIL','EMAIL');
/*!40000 ALTER TABLE `NotificationType` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Policy`
--

LOCK TABLES `Policy` WRITE;
/*!40000 ALTER TABLE `Policy` DISABLE KEYS */;
INSERT INTO `Policy` (`id`,`description`,`interval`,`repeatCount`,`createTime`,`updateTime`,`notificationType_id`) VALUES (1,'Low Balance Policy',0,0,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `Policy` (`id`,`description`,`interval`,`repeatCount`,`createTime`,`updateTime`,`notificationType_id`) VALUES (2,'Data Usage Policy',0,0,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `Policy` (`id`,`description`,`interval`,`repeatCount`,`createTime`,`updateTime`,`notificationType_id`) VALUES (3,'Subscription Near Expiry Policy',0,0,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `Policy` (`id`,`description`,`interval`,`repeatCount`,`createTime`,`updateTime`,`notificationType_id`) VALUES (4,'Subscription Expired Policy',0,0,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `Policy` (`id`,`description`,`interval`,`repeatCount`,`createTime`,`updateTime`,`notificationType_id`) VALUES (5,'New Location Policy',0,0,'2015-11-26 12:48:16','2015-11-26 12:48:16',1);
INSERT INTO `Policy` (`id`,`description`,`interval`,`repeatCount`,`createTime`,`updateTime`,`notificationType_id`) VALUES (6,'Referral Invite Policy',0,0,'2015-11-26 12:48:16','2015-11-26 12:48:16',3);
INSERT INTO `Policy` (`id`,`description`,`interval`,`repeatCount`,`createTime`,`updateTime`,`notificationType_id`) VALUES (7,'New Customer SignUp Policy',0,0,'2015-11-26 12:48:16','2015-11-26 12:48:16',3);
INSERT INTO `Policy` (`id`,`description`,`interval`,`repeatCount`,`createTime`,`updateTime`,`notificationType_id`) VALUES (8,'Credit Added Policy',0,0,'2015-11-26 12:48:16','2015-11-26 12:48:16',3);
INSERT INTO `Policy` (`id`,`description`,`interval`,`repeatCount`,`createTime`,`updateTime`,`notificationType_id`) VALUES (9,'Credit Limit Policy',0,0,'2015-11-26 12:48:16','2015-11-26 12:48:16',3);
INSERT INTO `Policy` (`id`,`description`,`interval`,`repeatCount`,`createTime`,`updateTime`,`notificationType_id`) VALUES (10,'Credit Alert Policy',0,0,'2015-11-26 12:48:16','2015-11-26 12:48:16',3);
INSERT INTO `Policy` (`id`,`description`,`interval`,`repeatCount`,`createTime`,`updateTime`,`notificationType_id`) VALUES (11,'Credit Warning Policy',0,0,'2015-11-26 12:48:16','2015-11-26 12:48:16',3);
INSERT INTO `Policy` (`id`,`description`,`interval`,`repeatCount`,`createTime`,`updateTime`,`notificationType_id`) VALUES (12,'Credit Deduct Policy',0,0,'2015-11-26 12:48:16','2015-11-26 12:48:16',3);
INSERT INTO `Policy` (`id`,`description`,`interval`,`repeatCount`,`createTime`,`updateTime`,`notificationType_id`) VALUES (13,'ICCID Promotion Policy',0,0,'2015-11-26 12:48:16','2015-11-26 12:48:16',3);
INSERT INTO `Policy` (`id`,`description`,`interval`,`repeatCount`,`createTime`,`updateTime`,`notificationType_id`) VALUES (14,'Rabbit MQ server up Policy',0,0,'2015-11-26 12:48:16','2015-11-26 12:48:16',3);
INSERT INTO `Policy` (`id`,`description`,`interval`,`repeatCount`,`createTime`,`updateTime`,`notificationType_id`) VALUES (15,'Rabbit MQ server down Policy',0,0,'2015-11-26 12:48:16','2015-11-26 12:48:16',3);
INSERT INTO `Policy` (`id`,`description`,`interval`,`repeatCount`,`createTime`,`updateTime`,`notificationType_id`) VALUES (16,'Deleted accounts threshold policy',0,0,'2015-11-26 12:48:16','2015-11-26 12:48:16',3);
/*!40000 ALTER TABLE `Policy` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `PushNotification`
--

LOCK TABLES `PushNotification` WRITE;
/*!40000 ALTER TABLE `PushNotification` DISABLE KEYS */;
/*!40000 ALTER TABLE `PushNotification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `PushedDevice`
--

LOCK TABLES `PushedDevice` WRITE;
/*!40000 ALTER TABLE `PushedDevice` DISABLE KEYS */;
/*!40000 ALTER TABLE `PushedDevice` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `SupportedClient`
--

LOCK TABLES `SupportedClient` WRITE;
/*!40000 ALTER TABLE `SupportedClient` DISABLE KEYS */;
INSERT INTO `SupportedClient` (`id`, `clientType`, `minClientVersion`, `policy_id`) VALUES (1,'ANDROID','2.6',1);
INSERT INTO `SupportedClient` (`id`, `clientType`, `minClientVersion`, `policy_id`) VALUES (2,'ANDROID','2.6',2);
INSERT INTO `SupportedClient` (`id`, `clientType`, `minClientVersion`, `policy_id`) VALUES (3,'ANDROID','2.6',3);
INSERT INTO `SupportedClient` (`id`, `clientType`, `minClientVersion`, `policy_id`) VALUES (4,'ANDROID','2.6',4);
INSERT INTO `SupportedClient` (`id`, `clientType`, `minClientVersion`, `policy_id`) VALUES (5,'ANDROID','2.6',5);
INSERT INTO `SupportedClient` (`id`, `clientType`, `minClientVersion`, `policy_id`) VALUES (6,'IOS','2.6',1);
INSERT INTO `SupportedClient` (`id`, `clientType`, `minClientVersion`, `policy_id`) VALUES (7,'IOS','2.6',2);
INSERT INTO `SupportedClient` (`id`, `clientType`, `minClientVersion`, `policy_id`) VALUES (8,'IOS','2.6',3);
INSERT INTO `SupportedClient` (`id`, `clientType`, `minClientVersion`, `policy_id`) VALUES (9,'IOS','2.6',4);
INSERT INTO `SupportedClient` (`id`, `clientType`, `minClientVersion`, `policy_id`) VALUES (10,'IOS','2.6',5);
/*!40000 ALTER TABLE `SupportedClient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `Template`
--

LOCK TABLES `Template` WRITE;
/*!40000 ALTER TABLE `Template` DISABLE KEYS */;
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (1,'Push Notification','{\"title\":\"Data Plan Usage\",\"message\":\"You have less than 10% of your data plan remaining. Do you need to add more data?\"}','en','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (2,'Push Notification','{\"title\":\"Data Plan Usage\",\"message\":\"50% or more of the data plan has been used.\"}','en','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (3,'Push Notification','{\"title\":\"Data Plan Time to Use\",\"message\":\"Your data plan will expire in less than 24 hours. Do you need to add more data?\"}','en','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (4,'Push Notification','{\"title\":\"Data Plan Expired\",\"message\":\"The GigSky data plan has expired. Do you need to add a data plan?\"}','en','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (5,'Push Notification','{\"title\":\"Data Plan Available to Use\",\"message\":\"Do you need a GigSky data plan for your current location?\"}','en','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (6,'Push Notification','{\"title\":\"Data Plan Usage\",\"message\":\"Sie haben weniger als 10% Ihres Datenkontingents übrig. Möchten Sie Daten hinzufügen?\"}','de','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (7,'Push Notification','{\"title\":\"Data Plan Usage\",\"message\":\"50% oder mehr des Datentarifs wurde verwendet.\"}','de','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (8,'Push Notification','{\"title\":\"Data Plan Time to Use\",\"message\":\"Weniger als 24 Stunden bleiben zur Nutzung des Datentarifs. Wollen Sie weitere Daten erwerben?\"}','de','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (9,'Push Notification','{\"title\":\"Data Plan Expired\",\"message\":\"Der Gigsky Datentarif ist abgelaufen. Wollen Sie einen Datentarif hinzufügen?\"}','de','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (10,'Push Notification','{\"title\":\"Data Plan Available to Use\",\"message\":\"Benötigen Sie an Ihrem jetzigen Standort einen GigSky Datentarif?\"}','de','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (11,'Push Notification','{\"title\":\"Data Plan Usage\",\"message\":\"Te queda menos del 10% de tu plan de datos. ¿Necesitas añadir más datos?\"}','es','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (12,'Push Notification','{\"title\":\"Data Plan Usage\",\"message\":\"Se ha usado el 50% o más del plan de datos.\"}','es','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (13,'Push Notification','{\"title\":\"Data Plan Time to Use\",\"message\":\"Tu plan de datos vencerá en menos de 24 horas. ¿Necesitas añadir más datos?\"}','es','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (14,'Push Notification','{\"title\":\"Data Plan Expired\",\"message\":\"El plan de datos de GigSky ha vencido. ¿Necesitas añadir un plan de datos?\"}','es','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (15,'Push Notification','{\"title\":\"Data Plan Available to Use\",\"message\":\"¿Necesitas un plan de datos de GigSky para tu ubicación actual?\"}','es','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (16,'Push Notification','{\"title\":\"Data Plan Usage\",\"message\":\"Il ne vous reste que moins de 10 % de votre forfait de données. Voulez-vous ajouter des données ?\"}','fr','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (17,'Push Notification','{\"title\":\"Data Plan Usage\",\"message\":\"Au moins 50 % du forfait de données a été utilisé.\"}','fr','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (18,'Push Notification','{\"title\":\"Data Plan Time to Use\",\"message\":\"Votre forfait de données expire dans moins de 24 heures. Voulez-vous ajouter des données ?\"}','fr','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (19,'Push Notification','{\"title\":\"Data Plan Expired\",\"message\":\"Le forfait de données GigSky a expiré. Voulez-vous ajouter un plan de données ?\"}','fr','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (20,'Push Notification','{\"title\":\"Data Plan Available to Use\",\"message\":\"Voulez-vous un forfait de données GigSky pour votre localisation actuelle ?\"}','fr','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (21,'Push Notification','{\"title\":\"Data Plan Usage\",\"message\":\"È disponibile meno del 10% del piano dati. Vuoi aggiungere più dati?\"}','it','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (22,'Push Notification','{\"title\":\"Data Plan Usage\",\"message\":\"È stato utilizzato il 50% o più del piano dati.\"}','it','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (23,'Push Notification','{\"title\":\"Data Plan Time to Use\",\"message\":\"Il piano dati scade in meno di 24 ore. Vuoi aggiungere più dati?\"}','it','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (24,'Push Notification','{\"title\":\"Data Plan Expired\",\"message\":\"Il piano dati GigSky è scaduto. Vuoi aggiungere un piano dati?\"}','it','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (25,'Push Notification','{\"title\":\"Data Plan Available to Use\",\"message\":\"Hai bisogno di un piano dati GigSky per la tua posizione corrente?\"}','it','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (26,'Push Notification','{\"title\":\"Data Plan Usage\",\"message\":\"データプランの残高が10%を切りました。データプランを追加購入しますか？\"}','ja','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (27,'Push Notification','{\"title\":\"Data Plan Usage\",\"message\":\"50%以上のデータプランがご利用済となりました。\"}','ja','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (28,'Push Notification','{\"title\":\"Data Plan Time to Use\",\"message\":\"24時間以内にデータプランの有効期限が切れます。データプランを追加購入しますか？\"}','ja','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (29,'Push Notification','{\"title\":\"Data Plan Expired\",\"message\":\"GigSkyのデータプランが期限切れとなりました。追加購入しますか？\"}','ja','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (30,'Push Notification','{\"title\":\"Data Plan Available to Use\",\"message\":\"現在の場所で利用可能なGigSkyデータプランを利用しますか？\"}','ja','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (31,'Push Notification','{\"title\":\"Data Plan Usage\",\"message\":\"您有不到10%的数据计划。您需要添加更多的数据吗?\"}','zh','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (32,'Push Notification','{\"title\":\"Data Plan Usage\",\"message\":\"已经使用50％或更多的数据计划。\"}','zh','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (33,'Push Notification','{\"title\":\"Data Plan Time to Use\",\"message\":\"您的数据计划将在不到24小时內到期。您需要添加更多的数据吗?\"}','zh','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (34,'Push Notification','{\"title\":\"Data Plan Expired\",\"message\":\"该GigSky数据计划已过期。您需要添加一个数据计划吗？\"}','zh','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (35,'Push Notification','{\"title\":\"Data Plan Available to Use\",\"message\":\"您需要在您的目前所在位置有一个GigSky数据计划吗?\"}','zh','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (36,'Push Notification','{\"title\":\"Data Plan Usage\",\"message\":\"您的數據計劃尚餘不足10％。您是否需要增加更多數據？\"}','zh-HK','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (37,'Push Notification','{\"title\":\"Data Plan Usage\",\"message\":\"已使用數據計劃50%或以上。\"}','zh-HK','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (38,'Push Notification','{\"title\":\"Data Plan Time to Use\",\"message\":\"您的數據計劃在不足24小時內期滿。您是否需要增加更多數據？\"}','zh-HK','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (39,'Push Notification','{\"title\":\"Data Plan Expired\",\"message\":\"GigSky數據計劃已滿期。您是否需要增加一個數據計劃？\"}','zh-HK','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (40,'Push Notification','{\"title\":\"Data Plan Available to Use\",\"message\":\"您是否需要在您現時的所在地有一個GigSky數據計劃？\"}','zh-HK','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (41,'Push Notification','{\"title\":\"Data Plan Usage\",\"message\":\"您的數據計劃尚餘不足10％。您是否需要增加更多數據？\"}','zh-TW','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (42,'Push Notification','{\"title\":\"Data Plan Usage\",\"message\":\"已使用數據計劃50%或以上。\"}','zh-TW','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (43,'Push Notification','{\"title\":\"Data Plan Time to Use\",\"message\":\"您的數據計劃在不足24小時內期滿。您是否需要增加更多數據？\"}','zh-TW','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (44,'Push Notification','{\"title\":\"Data Plan Expired\",\"message\":\"GigSky數據計劃已滿期。您是否需要增加一個數據計劃？\"}','zh-TW','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (45,'Push Notification','{\"title\":\"Data Plan Available to Use\",\"message\":\"您是否需要在您目前的所在地有一個GigSky數據計劃？\"}','zh-TW','PUSH','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (46,'Email Notification','{\"title\":\"GigSky\'s Referral Program\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>Dear <b>{0}</b></div><br></br><div>You can now refer friends, family, and colleagues to get GigSky Credit to purchase GigSky Data Plans.<br/><br/>Get {1} of GigSky Credit for each friend that signs up using your invitation code and buys a GigSky Data Plan. Your friend will also get {2} of GigSky Credit for signing up, to put towards their first GigSky Data Plan purchase.<br/><br/>Here’s your invitation code:</div><div><b>{3}</b></div><br/><div>Please note that any GigSky Credit earned must be used by the end of the calendar year. Check your GigSky account for your credit balance and expiration status.</div><br><div>To learn more visit the following links:</div><br><div>GigSky Apple SIM : <a href=\\\"https://goo.gl/QTtAaO\\\" target=\\\"_blank\\\">https://goo.gl/QTtAaO</a></div><div>GigSky Data SIM : <a href=\\\"https://goo.gl/0dfKVW\\\" target=\\\"_blank\\\">https://goo.gl/0dfKVW</a></div><br></br><div>Best Regards,<br/>GigSky Support</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">Please do not reply to this message. If you have any questions about this email, please contact<br/>help@gigsky.com or visit the GigSky Help Center.</div><div style=\\\"display: block;\\\">Copyright © 2018 GigSky Inc. All rights reserved | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={4}&e=s\\\" target=\\\"_blank\\\">Unsubscribe</a></div></div></div></body></html>\"}','en','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (47,'Email Notification','{\"title\":\"NEW CUSTOMER SIGNUP\",\"message\":\"Customer Email : {0},  New Customer Email : {1}, Advocate Amount : {2}.\"}','en','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (48,'Email Notification','{\"title\":\"You have just earned {0} of GigSky credit from a friend\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>Dear <b>{0}</b></div><br></br><div>Congratulations! You now have {1} of GigSky Referral credit from {2} who signed up using your invitation code and purchased a GigSky data plan. This credit can be used for your next GigSky Data Plan purchase.<br/><br/>Please note that any GigSky Credit earned must be used by the end of the calendar year. Check your GigSky account for your credit balance and expiration status.</div><br></br><div>Best Regards,<br/>GigSky Support</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">Please do not reply to this message. If you have any questions about this email, please contact<br/>help@gigsky.com or visit the GigSky Help Center.</div><div style=\\\"display: block;\\\">Copyright © 2018 GigSky Inc. All rights reserved | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={3}&e=s\\\" target=\\\"_blank\\\">Unsubscribe</a></div></div></div></body></html>\"}','en','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (49,'Email Notification','{\"title\":\"CREDIT LIMIT\",\"message\":\"Customer Email : {0}, Credit Limit: {1}.\"}','en','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (50,'Email Notification','{\"title\":\"Alert - Referral Credit Limit has been reached\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><br></br><div>Alert email to GigSky internal group to notify that {0} has referral credit balance {1} and no more referral credit will be added beyond the referral credit limit.</div><br></br><div>Best Regards,<br/>GigSky Support</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">Please do not reply to this message. If you have any questions about this email, please contact<br/>help@gigsky.com or visit the GigSky Help Center.</div><div style=\\\"display: block;\\\">Copyright © 2018 GigSky Inc. All rights reserved</div></div></div></body></html>\"}','en','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (51,'Email Notification','{\"title\":\"Alert - Referral Credit Limit has been reached\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><br></br><div>Alert email to GigSky internal group to notify that {0} has reached referral credit warning limit of {1}.</div><br></br><div>Best Regards,<br/>GigSky Support</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">Please do not reply to this message. If you have any questions about this email, please contact<br/>help@gigsky.com or visit the GigSky Help Center.</div><div style=\\\"display: block;\\\">Copyright © 2018 GigSky Inc. All rights reserved</div></div></div></body></html>\"}','en','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (52,'Email Notification','{\"title\":\"Alert - Apple SIM has been linked to another account\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><br></br><div>Alert email to GigSky internal group to notify that {0} is attempting to register a previously registered Apple SIM with CSN: {1} and it is a potential fraud scenario.</div><br></br><div>Best Regards,<br/>GigSky Support</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">Please do not reply to this message. If you have any questions about this email, please contact<br/>help@gigsky.com or visit the GigSky Help Center.</div><div style=\\\"display: block;\\\">Copyright © 2018 GigSky Inc. All rights reserved</div></div></div></body></html>\"}','en','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (53,'Email Notification','{\"title\":\"GigSkys Empfehlungsprogramm\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>Sehr geehrter <b>{0}</b></div><br></br><div>Sie können Jetzt Freunde, Familie und Kollegen einladen, um GigSky-Gutschriften zum Kauf von GigSky-Datenabos zu erhalten.<br/><br/>Erhalten Sie eine {1} GigSky-Gutschrift für jeden Freund, der sich über Ihren Einladungscode anmeldet und ein Datenabo von GigSky kauft. Ihr Freund erhält außerdem eine {2} GigSky-Gutschrift für die Anmeldung, welche für seinen ersten Kauf eines GigSky-Datenabos verwendet werden kann.<br/><br/>Hier ist Ihr Einladungscode:</div><div><b>{3}</b></div><br/><div>Bitte beachten Sie, dass jede Gutschrift die Sie von GigSky erhalten bis zum Ende des laufenden Kalenderjahres verwendet werden muss. Überprüfen Sie Ihr Konto bei GigSky, um Ihre Gutschriftenbilanz und das Ablaufdatum zu sehen.</div><br><div>Besuchen Sie die folgenden Links, um mehr zu erfahren:</div><br><div>GigSky Apple SIM : <a href=\\\"https://goo.gl/QTtAaO\\\" target=\\\"_blank\\\">https://goo.gl/QTtAaO</a></div><div>GigSky Data SIM : <a href=\\\"https://goo.gl/0dfKVW\\\" target=\\\"_blank\\\">https://goo.gl/0dfKVW</a></div><br></br><div>Freundliche Grüße,<br/>GigSky Unterstützung</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">Bitte antworten Sie nicht auf diese Nachricht. Sollten Sie Fragen zu dieser E-Mail haben, so kontaktieren Sie bitte <br/>help@gigsky.com oder besuchen Sie das GigSky Help Center.</div><div style=\\\"display: block;\\\">Copyright ©2018 GigSky Inc. Alle Rechte vorbehalten | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={4}&e=s\\\" target=\\\"_blank\\\">Abo kündigen</a></div></div></div></body></html>\"}','de','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (54,'Email Notification','{\"title\":\"Sie haben gerade eine {0} GigSky-Gutschrift über einen Freund verdient\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>Sehr geehrter <b>{0}</b></div><br></br><div>Herzlichen Glückwunsch! Sie besitzen jetzt eine {1} GigSky-Empfehlungsgutschrift von {2}, welcher sich über Ihren Einladungscode angemeldet hat und ein GigSky-Datenabo gekauft hat. Diese Gutschrift kann für Ihren nächsten Kauf eines GigSky-Datenabos genutzt werden.<br/><br/>Bitte beachten Sie, dass jede Gutschrift die Sie von GigSky erhalten bis zum Ende des laufenden Kalenderjahres verwendet werden muss. Überprüfen Sie Ihr Konto bei GigSky, um Ihre Gutschriftenbilanz und das Ablaufdatum zu sehen.</div><br></br><div>Freundliche Grüße,<br/>GigSky Unterstützung</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">Bitte antworten Sie nicht auf diese Nachricht. Sollten Sie Fragen zu dieser E-Mail haben, so kontaktieren Sie bitte<br/> help@gigsky.com oder besuchen Sie das GigSky Help Center.</div><div style=\\\"display: block;\\\">Copyright © 2018 GigSky Inc. Alle Rechte vorbehalten | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={3}&e=s\\\" target=\\\"_blank\\\">Abo kündigen</a></div></div></div></body></html>\"}','de','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (55,'Email Notification','{\"title\":\"programa de recomendación de GigSky\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>Estimado/a <b>{0}</b></div><br></br><div>Ahora puedes recomendar a tus amigos, familiares y compañeros para que obtengáis crédito de GigSky con el que adquirir planes de datos de GigSky.<br/><br/>GigSky-Gutschrift für die Anmeldung, welche für seinen ersten Kauf eines GigSky-Datenabos verwendet werden kann.<br/><br/>Aquí tienes tu código de invitación:</div><div><b>{3}</b></div><br/><div>Ten en cuenta que cualquier crédito de GigSky ganado debe utilizarse antes del fin del año calendario. Comprueba tu cuenta de GigSky para conocer tu saldo y la fecha de vencimiento.</div><br><div>Para conseguir más información, visita los siguientes enlaces:</div><br><div>GigSky Apple SIM : <a href=\\\"https://goo.gl/QTtAaO\\\" target=\\\"_blank\\\">https://goo.gl/QTtAaO</a></div><div>GigSky Data SIM : <a href=\\\"https://goo.gl/0dfKVW\\\" target=\\\"_blank\\\">https://goo.gl/0dfKVW</a></div><br></br><div>Atentamente,<br/>Soporte GigSky</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">Por favor, no respondas a este mensaje. Si tienes cualquier pregunta sobre el mismo ponte en contacto con </br>help@gigsky.com o visita el Centro de Ayuda de GigSky.</div><div style=\\\"display: block;\\\">Copyright ©2018 GigSky Inc. Todos los derechos reservados | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={4}&e=s\\\" target=\\\"_blank\\\">Cancelar suscripción</a></div></div></div></body></html>\"}','es','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (56,'Email Notification','{\"title\":\"Acabas de ganar {0} de crédito de GigSky gracias a un amigo\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>Estimado/a <b>{0}</b></div><br></br><div>¡Enhorabuena! Ahora tienes {1} de crédito por recomendación de Gigsky gracias a {2} que se registró mediante tu código de invitación y compró un plan de datos de GigSky. Puedes utilizar este crédito en tu próxima compra de plan de datos de GigSky.<br/><br/>Ten en cuenta que cualquier crédito de GigSky ganado debe utilizarse antes del fin del año calendario. Comprueba tu cuenta de GigSky para conocer tu saldo y la fecha de vencimiento.</div><br></br><div>Atentamente,<br/>Soporte GigSky</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">Por favor, no respondas a este mensaje. Si tienes cualquier pregunta sobre el mismo ponte en contacto con </br>help@gigsky.com o visita el Centro de Ayuda de GigSky.</div><div style=\\\"display: block;\\\">Copyright ©2018 GigSky Inc. Todos los derechos reservados | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={3}&e=s\\\" target=\\\"_blank\\\">Cancelar suscripción</a></div></div></div></body></html>\"}','es','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (57,'Email Notification','{\"title\":\"programme de parrainage GigSky\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>Cher/Chère <b>{0}</b></div><br></br><div>Vous pouvez maintenant parrainer vos amis, votre famille et vos collègues pour l\\\'achat d\\\'une formule GigSky Data.<br/><br/>Obtenez {1} de crédits GigSky pour chaque ami qui s\\\'inscrira en utilisant votre code d\\\'invitation et achètera une formule GigSky Data. Votre ami bénéficiera aussi de {2} de crédits GigSky pour son inscription, à utiliser pour l\\\'achat de sa première formule GigSky Data.<br/><br/>Voici votre code d\\\'invitation:</div><div><b>{3}</b></div><br/><div>Veuillez noter que tout crédit GigSky gagné doit être utilisé avant la fin de l\\\'année civile. Allez sur votre compte GigSky pour vérifier le solde de vos crédits et la date d\\\'expiration.</div><br><div>Pour en savoir plus, visitez les liens suivants :</div><br><div>GigSky Apple SIM : <a href=\\\"https://goo.gl/QTtAaO\\\" target=\\\"_blank\\\">https://goo.gl/QTtAaO</a></div><div>GigSky Data SIM : <a href=\\\"https://goo.gl/0dfKVW\\\" target=\\\"_blank\\\">https://goo.gl/0dfKVW</a></div><br></br><div>Cordialement,<br/>Support GigSky</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">Veuillez ne pas répondre à ce message. Si vous avez la moindre question sur cet e-mail, veuillez écrire à help@gigsky.com ou aller sur le Centre daide GigSky.</div><div style=\\\"display: block;\\\">Copyright ©2018 GigSky Inc. Tous droits réservés | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={4}&e=s\\\" target=\\\"_blank\\\">Se désabonner</a></div></div></div></body></html>\"}','fr','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (58,'Email Notification','{\"title\":\"Vous venez de gagner {0} de crédits GigSky grâce à un(e) ami(e)\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>Cher/Chère <b>{0}</b></div><br></br><div>Félicitations&nbsp;! Vous disposez désormais de {1} de crédit de parrainage GigSky grâce à {2} qui s\'est inscrit en utilisant votre code d\'invitation et a acheté une formule GigSky Data. Vous pourrez utiliser ce crédit pour votre prochain achat d\'une formule GigSky Data.<br/><br/>Veuillez noter que tout crédit GigSky gagné doit être utilisé avant la fin de l\'année civile. Allez sur votre compte GigSky pour vérifier le solde de vos crédits et la date d\'expiration.</div><br></br><div>Cordialement,<br/>Support GigSky</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">Veuillez ne pas répondre à ce message. Si vous avez la moindre question sur cet e-mail, veuillez écrire à </br>help@gigsky.com ou aller sur le Centre d\'aide GigSky.</div><div style=\\\"display: block;\\\">Copyright ©2018 GigSky Inc. Tous droits réservés | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={3}&e=s\\\" target=\\\"_blank\\\">Se désabonner</a></div></div></div></body></html>\"}','fr','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (59,'Email Notification','{\"title\":\"referral program di GigSky\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>Caro <b>{0}</b></div><br></br><div>Adesso puoi dire agli amici, ai familiari ed ai colleghi di attivarsi per ottenere un credito GigSky per acquistare un piano di dati GigSky.<br/><br/>Ottieni {1} statunitensi di credito GigSky per ogni amico che si iscrive utilizzando il tuo codice di invito e acquista un piano di dati GigSky. Anche il tuo amico riceverà {2} statunitensi di credito GigSky per essersi iscritto da destinare allacquisto del suo primo piano di dati GigSky.<br/><br/>Questo è il tuo codice di invito:</div><div><b>{3}</b></div><br/><div>Prego, nota che qualsiasi credito GigSky guadagnato deve essere utilizzato entro la fine dellanno solare. Controlla il tuo account GigSky per vedere il saldo contabile e lo stato di scadenza.</div><br><div>Per saperne di più consulta i seguenti link:</div><br><div>GigSky Apple SIM : <a href=\\\"https://goo.gl/QTtAaO\\\" target=\\\"_blank\\\">https://goo.gl/QTtAaO</a></div><div>GigSky Data SIM : <a href=\\\"https://goo.gl/0dfKVW\\\" target=\\\"_blank\\\">https://goo.gl/0dfKVW</a></div><br></br><div>Cordiali saluti,<br/>Supporto GigSky</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">Prego, non rispondere a questo messaggio. Se hai delle domande riguardanti questa e-mail, contatta </br>help@gigsky.com oppure visita il centro di assistenza GigSky.</div><div style=\\\"display: block;\\\">Copyright ©2018 GigSky Inc. Tutti i diritti riservati | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={4}&e=s\\\" target=\\\"_blank\\\">Cancella iscrizione</a></div></div></div></body></html>\"}','it','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (60,'Email Notification','{\"title\":\"Hai appena ottenuto {0} di credito GigSky da parte di un amico\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>Caro <b>{0}</b></div><br></br><div>Congratulazioni! Adesso hai {1} statunitensi di credito referral GigSky da parte di {2} che si è iscritto utilizzando il tuo codice di invito ed ha acquistato un piano di dati GigSky. Questo credito può essere utilizzato per acquistare il tuo prossimo piano dati GigSky.<br/><br/>Prego, nota che qualsiasi credito GigSky guadagnato deve essere utilizzato entro la fine dellanno solare. Controlla il tuo account GigSky per vedere il saldo contabile e lo stato di scadenza.</div><br></br><div>Cordiali saluti,<br/>Supporto GigSky</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">Prego, non rispondere a questo messaggio. Se hai delle domande riguardanti questa e-mail, contatta </br>help@gigsky.com oppure visita il centro di assistenza GigSky.</div><div style=\\\"display: block;\\\">Copyright ©2018 GigSky Inc. Tutti i diritti riservati | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={3}&e=s\\\" target=\\\"_blank\\\">Cancella iscrizione</a></div></div></div></body></html>\"}','it','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (61,'Email Notification','{\"title\":\"GigSky の紹介プログラムのご案内\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>{0} <b> 様</b></div><br></br><div>お友達、ご家族、同僚をご紹介いただくことで GigSky データプランが購入できる GigSky クレジットが獲得できるようになりました。<br/><br/>あなたからの招待コードで登録し、GigSky データプランを購入するお友達1名につき{1}米ドルの GigSky クレジットを獲得しましょう。登録時にお友達にも{2}米ドルの GigSky クレジットが付与され、最初の GigSky データプラン購入に利用できます。<br/><br/>招待コードはこちらです:</div><div><b>{3}</b></div><br/><div>獲得された GigSky クレジットはすべて獲得された年の末日までに必ず使用するようご留意ください。クレジット残高と有効期限は GigSky アカウントから確認することができます。</div><br><div>詳細は次のリンクをご覧ください：</div><br><div>GigSky Apple SIM : <a href=\\\"https://goo.gl/QTtAaO\\\" target=\\\"_blank\\\">https://goo.gl/QTtAaO</a></div><div>GigSky Data SIM : <a href=\\\"https://goo.gl/0dfKVW\\\" target=\\\"_blank\\\">https://goo.gl/0dfKVW</a></div><br></br><div>よろしくお願いいたします。<br/>GigSkyサポート</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">このメッセージには返信しないでください。本メールに関するご質問は、</br>help@gigsky.com までお寄せいただくか GigSky ヘルプセンターをご参照ください。</div><div style=\\\"display: block;\\\">著作権 © 2018 GigSky Inc. 不許複製 | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={4}&e=s\\\" target=\\\"_blank\\\">登録解除</a></div></div></div></body></html>\"}','ja','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (62,'Email Notification','{\"title\":\"{0}米ドルのGigSky クレジットをお友達から獲得しました\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>{0} <b> 様</b></div><br></br><div>おめでとうございます! あなたの招待コードで登録し、GigSky データプランを購入した {1} から{2}米ドルの Gigsky 紹介クレジットを獲得されました。このクレジットは次回の GigSky データプラン購入にお使いになれます。<br/><br/>獲得された GigSky クレジットはすべて獲得された年の末日までに必ず使用するようご留意ください。クレジット残高と有効期限は GigSky アカウントから確認することができます。</div><br></br><div>よろしくお願いいたします。<br/>GigSkyサポート</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">このメッセージには返信しないでください。本メールに関するご質問は、</br>help@gigsky.com までお寄せいただくか GigSky ヘルプセンターをご参照ください。</div><div style=\\\"display: block;\\\">著作権 © 2018 GigSky Inc. 不許複製 | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={3}&e=s\\\" target=\\\"_blank\\\">登録解除</a></div></div></div></body></html>\"}','ja','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (63,'Email Notification','{\"title\":\"GigSky 的推荐计划\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>亲爱的 <b>{0}</b></div><br></br><div>现在您就可以推荐朋友、家人与同事，以获得 GigSky 积分购买 GigSky 数据套餐。<br/><br/>用您的邀请代码注册并购买 GigSky 数据套餐的每一位朋友，均能让您获得 {1} 美元 GigSky 积分。您的朋友也将获得 {2} 美元 GigSky 注册积分，用于其首次购买 GigSky 数据套餐。<br/><br/>这里是您的邀请代码：</div><div><b>{3}</b></div><br/><div>请注意，所获得的任何 GigSky 积分必须在日历年末之前用完。请进入您的 GigSky 帐户查看您的积分余额及过期状态。</div><br><div>如需了解更多信息，请访问下列链接：</div><br><div>GigSky Apple SIM : <a href=\\\"https://goo.gl/QTtAaO\\\" target=\\\"_blank\\\">https://goo.gl/QTtAaO</a></div><div>GigSky Data SIM : <a href=\\\"https://goo.gl/0dfKVW\\\" target=\\\"_blank\\\">https://goo.gl/0dfKVW</a></div><br></br><div>诚挚的问候，<br/>GigSky支持部</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">请勿回复此消息。如果您对此电子邮件有任何问题，请联系 </br>help@gigsky.com 或访问 GigSky 帮助中心。</div><div style=\\\"display: block;\\\">版权 © 2018 GigSky Inc. 保留所有权利 | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={4}&e=s\\\" target=\\\"_blank\\\">取消订阅</a></div></div></div></body></html>\"}','zh','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (64,'Email Notification','{\"title\":\"刚才您从一位朋友那里获得 {0} 美元 GigSky 积分。\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>亲爱的 <b>{0}</b></div><br></br><div>恭喜！{2} 用您的邀请代码注册并购买了 现在 GigSky 数据套餐，现在您有 {1} 美元 GigSky 推荐积分。下次购买 GigSky 数据套餐时，您可以使用该积分。<br/><br/>请注意，所获得的任何 GigSky 积分必须在日历年末之前用完。请进入您的 GigSky 帐户查看您的积分余额及过期状态。</div><br></br><div>诚挚的问候，<br/>GigSky支持部</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">请勿回复此消息。如果您对此电子邮件有任何问题，请联系 </br>help@gigsky.com 或访问 GigSky 帮助中心。</div><div style=\\\"display: block;\\\">版权 © 2018 GigSky Inc. 保留所有权利 | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={3}&e=s\\\" target=\\\"_blank\\\">取消订阅</a></div></div></div></body></html>\"}','zh','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (65,'Email Notification','{\"title\":\"GigSky 推薦計畫\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>親愛的 <b>{0}</b></div><br></br><div>您現在可以透過推薦朋友、家人與同事來獲得 GigSky 積分，並用來購買 GigSky 數據方案。<br/><br/>每位朋友使用您的邀請碼註冊並購買 GigSky 數據方案，您都能獲得 {1} 美金的 GigSky 積分。您的朋友在註冊時也會獲得 {2} 美金的 GigSky 積分，可用於他們第一次的 GigSky 數據方案購買。<br/><br/>以下為您的邀請碼：</div><div><b>{3}</b></div><br/><div>請注意，任何獲得的 GigSky 積分須於日曆年底前用完。請前往您的 GigSky 帳戶查看您的積分額度與有效期限。</div><br><div>欲知詳情，請前往下列連結：</div><br><div>GigSky Apple SIM : <a href=\\\"https://goo.gl/QTtAaO\\\" target=\\\"_blank\\\">https://goo.gl/QTtAaO</a></div><div>GigSky Data SIM : <a href=\\\"https://goo.gl/0dfKVW\\\" target=\\\"_blank\\\">https://goo.gl/0dfKVW</a></div><br></br><div>謹致問候，<br/>GigSky支援部</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">請勿回覆此郵件。如果您對此封電子郵件有任何疑問，請聯繫 </br>help@gigsky.com 或前往 GigSky 幫助中心。</div><div style=\\\"display: block;\\\">著作權 © 2018 GigSky Inc. 版權所有 | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={4}&e=s\\\" target=\\\"_blank\\\">取消訂閱</a></div></div></div></body></html>\"}','zh-TW','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (66,'Email Notification','{\"title\":\"您剛從朋友那獲得了 {0} 美金的 GigSky 積分\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>親愛的 <b>{0}</b></div><br></br><div>恭喜！由於 {2} 使用了您的邀請碼註冊並購買 GigSky 數據方案，您現在擁有 {1} 美金的 Gigsky 推薦積分了。此積分可用於您下一次的 GigSky 數據方案購買。<br/><br/>请注意，所获得的任何 GigSky 积分必须在日历年末之前用完。请进入您的 GigSky 帐户查看您的积分余额及过期状态。</div><br></br><div>謹致問候，<br/>GigSky支援部</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">請勿回覆此郵件。如果您對此封電子郵件有任何疑問，請聯繫 </br>help@gigsky.com 或前往 GigSky 幫助中心。</div><div style=\\\"display: block;\\\">著作權 © 2018 GigSky Inc. 版權所有 | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={3}&e=s\\\" target=\\\"_blank\\\">取消訂閱</a></div></div></div></body></html>\"}','zh-TW','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (67,'Email Notification','{\"title\":\"GigSky 推薦計畫\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>親愛的 <b>{0}</b></div><br></br><div>您現在可以透過推薦朋友、家人與同事來獲得 GigSky 積分，並用來購買 GigSky 數據方案。<br/><br/>每位朋友使用您的邀請碼註冊並購買 GigSky 數據方案，您都能獲得 {1} 美金的 GigSky 積分。您的朋友在註冊時也會獲得 {2} 美金的 GigSky 積分，可用於他們第一次的 GigSky 數據方案購買。<br/><br/>以下為您的邀請碼：</div><div><b>{3}</b></div><br/><div>請注意，任何獲得的 GigSky 積分須於日曆年底前用完。請前往您的 GigSky 帳戶查看您的積分額度與有效期限。</div><br><div>欲知詳情，請前往下列連結：</div><br><div>GigSky Apple SIM : <a href=\\\"https://goo.gl/QTtAaO\\\" target=\\\"_blank\\\">https://goo.gl/QTtAaO</a></div><div>GigSky Data SIM : <a href=\\\"https://goo.gl/0dfKVW\\\" target=\\\"_blank\\\">https://goo.gl/0dfKVW</a></div><br></br><div>謹致問候，<br/>GigSky支援部</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">請勿回覆此郵件。如果您對此封電子郵件有任何疑問，請聯繫 </br>help@gigsky.com 或前往 GigSky 幫助中心。</div><div style=\\\"display: block;\\\">著作權 © 2018 GigSky Inc. 版權所有 | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={4}&e=s\\\" target=\\\"_blank\\\">取消訂閱</a></div></div></div></body></html>\"}','zh-HK','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (68,'Email Notification','{\"title\":\"您剛從朋友那獲得了 {0} 美金的 GigSky 積分\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>親愛的 <b>{0}</b></div><br></br><div>恭喜！由於 {2} 使用了您的邀請碼註冊並購買 GigSky 數據方案，您現在擁有 {1} 美金的 Gigsky 推薦積分了。此積分可用於您下一次的 GigSky 數據方案購買。<br/><br/>请注意，所获得的任何 GigSky 积分必须在日历年末之前用完。请进入您的 GigSky 帐户查看您的积分余额及过期状态。</div><br></br><div>謹致問候，<br/>GigSky支援部</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">請勿回覆此郵件。如果您對此封電子郵件有任何疑問，請聯繫 </br>help@gigsky.com 或前往 GigSky 幫助中心。</div><div style=\\\"display: block;\\\">著作權 © 2018 GigSky Inc. 版權所有 | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={3}&e=s\\\" target=\\\"_blank\\\">取消訂閱</a></div></div></div></body></html>\"}','zh-HK','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (69,'Email Notification','{\"title\":\"We have added {0} credit to your account!\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>Dear <b>{0}</b></div><br></br><div>Congratulations on activating the GigSky SIM! A credit of {1} has been added to your account and can be verified under “Payments -> GigSky credit” in the GigSky App menu.<br/><br/>You may use this credit to purchase any available data plan. A payment method (Credit Card or PayPal) will need to be added to your GigSky account if you would like to purchase any data plans after the initial GigSky credit has been used.</div><br></br><div>Best Regards,<br/>GigSky Support</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">Please do not reply to this message. If you have any questions about this email, please contact<br/>help@gigsky.com or visit the GigSky Help Center.</div><div style=\\\"display: block;\\\">Copyright © 2018 GigSky Inc. All rights reserved | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={2}&e=s\\\" target=\\\"_blank\\\">Unsubscribe</a></div></div></div></body></html>\"}','en','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (70,'Email Notification','{\"title\":\"Wir haben ein Guthaben in Höhe von {0} zu Ihrem Konto hinzugefügt!\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>Sehr geehrter <b>{0}</b></div><br></br><div>Glückwunsch zur Aktivierung der GigSky-SIM! Ein Guthaben von {1} wurde zu Ihrem Konto hinzugefügt und kann im Menü der GigSky-App unter „Payments -&gt; GigSky credit“ (Zahlungen -&gt; GigSky-Guthaben) überprüft werden.<br/><br/>Sie können dieses Guthaben für den Kauf jedes verfügbaren Datentarifs nutzen. Sie müssen eine Zahlungsart (Kreditkarte oder PayPal) zu Ihrem GigSky-Konto hinzufügen, wenn Sie weitere Datentarife kaufen möchten, nachdem Ihr ursprüngliches GigSky-Guthaben aufgebraucht wurde.</div><br></br><div>Freundliche Grüße,<br/>GigSky Unterstützung</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">Bitte antworten Sie nicht auf diese Nachricht. Sollten Sie Fragen zu dieser E-Mail haben, so kontaktieren Sie bitte <br/>help@gigsky.com oder besuchen Sie das GigSky Help Center.</div><div style=\\\"display: block;\\\">Copyright © 2018 GigSky Inc. All rights reserved | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={2}&e=s\\\" target=\\\"_blank\\\">Abo kündigen</a></div></div></div></body></html>\"}','de','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (71,'Email Notification','{\"title\":\"¡Hemos añadido {0} crédito a tu cuenta!\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>Estimado <b>{0}</b></div><br></br><div>¡Felicidades! ¡Has activado la SIM de GigSky! Hemos añadido un crédito de {1} a tu cuenta. Puedes comprobarlo en «Pagos -&gt; Crédito de GigSky» en el menú de la aplicación de GigSky.<br/><br/>Puedes utilizar este crédito para contratar cualquiera de los planes de datos disponibles. Deberás añadir un método de pago (tarjeta de crédito o PayPal) a tu cuenta de GigSky si deseas contratar cualquier plan de datos después de gastar el crédito inicial de GigSky.</div><br></br><div>Atentamente,<br/>Soporte GigSky</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">Por favor, no respondas a este mensaje. Si tienes cualquier pregunta sobre el mismo ponte en contacto con <br/>help@gigsky.com o visita el Centro de Ayuda de GigSky.</div><div style=\\\"display: block;\\\">Copyright © 2018 GigSky Inc. Todos los derechos reservados | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={2}&e=s\\\" target=\\\"_blank\\\">Cancelar suscripción</a></div></div></div></body></html>\"}','es','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (72,'Email Notification','{\"title\":\"Vous avez ajouté {0} crédit à votre compte !\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>Cher/Chère <b>{0}</b></div><br></br><div>Félicitations pour l\\\'\\\'activation de la SIM GigSky ! Un crédit de {1} a été ajouté à votre compte et peut être vérifié sous la section « Paiement -&gt; Crédit GigSky » dans le menu de l\\\'\\\'application GigSky.<br/><br/>Vous pouvez utiliser ce crédit pour acheter n\\\'importe quel forfait de données disponibles. Un moyen de paiement (carte de crédit ou PayPal) devra être ajouté à votre compte GigSky si vous souhaitez acheter un forfait de données après que le crédit GigSky initial ait été utilisé.</div><br></br><div>Cordialement,<br/>Support GigSky</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">Veuillez ne pas répondre à ce message. Si vous avez la moindre question sur cet e-mail, veuillez écrire à <br/>help@gigsky.com ou aller sur le Centre d\'aide GigSky.</div><div style=\\\"display: block;\\\">Copyright © 2018 GigSky Inc. Tous droits réservés | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={2}&e=s\\\" target=\\\"_blank\\\">Se désabonner</a></div></div></div></body></html>\"}','fr','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (73,'Email Notification','{\"title\":\"Abbiamo aggiunto un credito di {0} al tuo account!\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>Caro <b>{0}</b></div><br></br><div>Congratulazioni per aver attivato la SIM GigSky! Abbiamo aggiunto al tuo account un credito di {1}. Puoi verificarlo andando su “Pagamenti -> Credito GigSky” nel menu dell\\\'\\\'app GigSky.<br/><br/>Puoi utilizzare questo credito per acquistare qualunque piano dati disponibile. Sarà necessario aggiungere un metodo di pagamento (carta di credito o PayPal) al tuo account GigSky, se vorrai acquistare piani dati una volta esaurito il credito GigSky iniziale.</div><br></br><div>Cordiali saluti,<br/>Supporto GigSky</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">Prego, non rispondere a questo messaggio. Se hai delle domande riguardanti questa e-mail, contatta <br/>help@gigsky.com oppure visita il centro di assistenza GigSky.</div><div style=\\\"display: block;\\\">Copyright © 2018 GigSky Inc. Tutti i diritti riservati | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={2}&e=s\\\" target=\\\"_blank\\\">Cancella iscrizione</a></div></div></div></body></html>\"}','it','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (74,'Email Notification','{\"title\":\"{0} クレジットがあなたのアカウントに追加されました。\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>様 <b>{0}</b></div><br></br><div>おめでとうございます、GigSky SIMカードがアクティベーションされました。{1} クレジットがあなたのアカウントに追加されました。GigSky アプリのメニューから\\\"Payments-&gt;GigSky credit\\\"で確認して下さい。<br/><br/>利用できるどのデータプランの購入にもこのクレジットが使えます。 もし最初のGigSky(ギグスカイ) クレジットを使った後で何かデータプランを購入したい時はあなたのGigSky アカウントに支払い方法 (クレジットカードまたはPayPal (ペイパル))を追加して下さい。</div><br></br><div>よろしくお願いいたします。<br/>GigSkyサポート</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">このメッセージには返信しないでください。本メールに関するご質問は、<br/>help@gigsky.com までお寄せいただくか GigSky ヘルプセンターをご参照ください。</div><div style=\\\"display: block;\\\">著作権 © 2018 GigSky Inc. 不許複製 | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={2}&e=s\\\" target=\\\"_blank\\\">登録解除</a></div></div></div></body></html>\"}','ja','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (75,'Email Notification','{\"title\":\"我们已将 {0} 信用积分添加至您的账户！\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>亲爱的 <b>{0}</b></div><br></br><div>祝贺 GigSky SIM 激活！ {1} 信用积分已经添加至您的账户，您可以在 GigSky 应用菜单 “支付 -> GigSky 信用积分” 验证。<br/><br/>您可以使用信用积分购买我们提供任一的数据计划。 如果在初始 GigSky 信用积分用完后您希望选购任一数据计划，则需要将付款方式（信用卡或贝宝）添加到您的 GigSky 账户。</div><br></br><div>诚挚的问候，<br/>GigSky支持部</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">请勿回复此消息。如果您对此电子邮件有任何问题，请联系 <br/>help@gigsky.com 或访问 GigSky 帮助中心。</div><div style=\\\"display: block;\\\">版权 © 2018 GigSky Inc. 保留所有权利 | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={2}&e=s\\\" target=\\\"_blank\\\">取消订阅</a></div></div></div></body></html>\"}','zh','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (76,'Email Notification','{\"title\":\"我們已經將 {0} 的額度加入了您的帳戶！\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>親愛的 <b>{0}</b></div><br></br><div>恭喜您，您已經啟用了 GigSky SIM！您的帳戶會新增 {1} 的額度，可以由 GigSky App 選單內的「付款 -> GigSky 額度」進行驗證。<br/><br/>您可以使用此額度購買任何可得的資料方案。 使用首期 GigSky 額度後，如果想購買資料方案，請在您的 GigSky 帳戶中新增付款方法（信用卡或 PayPal）。</div><br></br><div>謹致問候，<br/>GigSky支援部</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">請勿回覆此郵件。如果您對此封電子郵件有任何疑問，請聯繫 <br/>help@gigsky.com 或前往 GigSky 幫助中心。</div><div style=\\\"display: block;\\\">著作權 © 2018 GigSky Inc. 版權所有 | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={2}&e=s\\\" target=\\\"_blank\\\">取消訂閱</a></div></div></div></body></html>\"}','zh-TW','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (77,'Email Notification','{\"title\":\"我們已經將 {0} 的額度加入了您的帳戶！\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><div><br></br>親愛的 <b>{0}</b></div><br></br><div>恭喜您，您已經啟用了 GigSky SIM！您的帳戶會新增 {1} 的額度，可以由 GigSky App 選單內的「付款 -> GigSky 額度」進行驗證。<br/><br/>您可以使用此額度購買任何可得的資料方案。 使用首期 GigSky 額度後，如果想購買資料方案，請在您的 GigSky 帳戶中新增付款方法（信用卡或 PayPal）。</div><br></br><div>謹致問候，<br/>GigSky支援部</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">請勿回覆此郵件。如果您對此封電子郵件有任何疑問，請聯繫 <br/>help@gigsky.com 或前往 GigSky 幫助中心。</div><div style=\\\"display: block;\\\">著作權 © 2018 GigSky Inc. 版權所有 | <a href=\\\"https://www.gigsky.com/referral-unsubscribe?cid={2}&e=s\\\" target=\\\"_blank\\\">取消訂閱</a></div></div></div></body></html>\"}','zh-HK','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (78,'Email Notification','{\"title\":\"UP alert: Referral Engine - Stage - RabbitMQ is UP\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><br></br><div>Alert email to GigSky internal group to notify that Stage Referral Engine server’s RabbitMQ is UP.</div><br></br><div>Best Regards,<br/>GigSky Support</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">Please do not reply to this message. If you have any questions about this email, please contact<br/>help@gigsky.com or visit the GigSky Help Center.</div><div style=\\\"display: block;\\\">Copyright © 2018 GigSky Inc. All rights reserved</div></div></div></body></html>\"}','en','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (79,'Email Notification','{\"title\":\"DOWN alert: Referral Engine - Stage - RabbitMQ is DOWN\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><br></br><div>Alert email to GigSky internal group to notify that Stage Referral Engine server’s RabbitMQ is DOWN.</div><br></br><div>Best Regards,<br/>GigSky Support</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">Please do not reply to this message. If you have any questions about this email, please contact<br/>help@gigsky.com or visit the GigSky Help Center.</div><div style=\\\"display: block;\\\">Copyright © 2018 GigSky Inc. All rights reserved</div></div></div></body></html>\"}','en','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');
INSERT INTO `Template` (`id`,`description`,`content`,`languageCode`,`contentType`,`createTime`,`updateTime`) VALUES (80,'Email Notification','{\"title\":\"Deleted Accounts reached threshold count\",\"message\":\"<html><body><div style=\\\"width:500px;padding:15px;text-align:left;white-space:pre-wrap;font-family: Helvetica neue;color: #000000;font-style: normal;font-size:12px; line-height:1.5em;\\\"><div style=\\\"float:left;width:270px;padding-bottom:30px\\\"><img src=\\\"https://staging-ent.gigsky.com/umbs/api/images/GigSky-Email-Logo.png\\\" alt=\\\"Gigsky\\\" style=\\\"width: 100%;\\\"></div><div style=\\\"clear:both\\\"</div><br></br><div>Alert email to GigSky internal group to notify that Deleted Gigsky Accounts reached Maximum Threshold.</div><br></br><div>Best Regards,<br/>GigSky Support</div><br/><br/><div style=\\\"color: rgb(113, 113, 113); font-family: Helvetica neue; font-size: 11px; font-style: normal; font-weight: normal; text-align: center; white-space: pre-wrap; width: 100%;\\\"><div style=\\\"display: block;\\\">Please do not reply to this message. If you have any questions about this email, please contact<br/>help@gigsky.com or visit the GigSky Help Center.</div><div style=\\\"display: block;\\\">Copyright © 2018 GigSky Inc. All rights reserved</div></div></div></body></html>\"}','en','EMAIL','2015-11-26 12:48:16','2015-11-26 12:48:16');

/*!40000 ALTER TABLE `Template` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `ThirdPartyTool`
--

LOCK TABLES `ThirdPartyTool` WRITE;
/*!40000 ALTER TABLE `ThirdPartyTool` DISABLE KEYS */;
/*!40000 ALTER TABLE `ThirdPartyTool` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `UrlDetail`
--

LOCK TABLES `UrlDetail` WRITE;
/*!40000 ALTER TABLE `UrlDetail` DISABLE KEYS */;
INSERT INTO `UrlDetail` (`id`, `apiBaseUrl`) VALUES (1,'api/v1/');
/*!40000 ALTER TABLE `UrlDetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `VersionInformation`
--

LOCK TABLES `VersionInformation` WRITE;
/*!40000 ALTER TABLE `VersionInformation` DISABLE KEYS */;
INSERT INTO `VersionInformation` (`major`, `minor`, `build`, `buildType`, `id`) VALUES (1,0,0,'dev',1);
/*!40000 ALTER TABLE `VersionInformation` ENABLE KEYS */;
UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 18:16:47
COMMIT;