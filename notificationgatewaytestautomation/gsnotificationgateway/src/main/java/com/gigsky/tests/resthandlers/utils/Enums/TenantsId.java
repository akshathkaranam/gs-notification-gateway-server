package com.gigsky.tests.resthandlers.utils.Enums;

public enum TenantsId {
    GIGSKY("1"),
    KDDI("2");

    public String tenantId;
    TenantsId(String tenantId) {
        this.tenantId = tenantId;
    }
}
