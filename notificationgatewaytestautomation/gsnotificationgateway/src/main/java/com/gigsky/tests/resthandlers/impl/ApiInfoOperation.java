package com.gigsky.tests.resthandlers.impl;

import com.gigsky.testng.common.APIUrls;
import com.gigsky.tests.common.apihandler.beans.APIHandlerConfig;
import com.gigsky.tests.common.apihandler.beans.APIResponse;
import com.gigsky.tests.common.apihandler.factory.APIHandlerFactory;
import com.gigsky.tests.common.apihandler.impl.APIOperations;
import com.gigsky.tests.common.apihandler.interfaces.APIHandler;
import com.gigsky.tests.common.apihandler.interfaces.ResponseBodyValidator;
import com.gigsky.tests.common.utils.ObjectToJsonConverter;
import com.gigsky.tests.resthandlers.beans.ApiInfo;
import com.gigsky.tests.resthandlers.beans.BaseBean;
import com.gigsky.tests.resthandlers.beans.ErrorDetails;

public class ApiInfoOperation extends APIOperations {
    private static final ApiInfoOperation instance = new ApiInfoOperation();
    private APIHandlerFactory apiHandlerFactory = APIHandlerFactory.getInstance();
    public static ApiInfoOperation getInstance() {
        return instance;
    }

    public BaseBean getApi_Info(BaseBean info, Class responseType,String method, Integer responseCode) throws Exception {
        APIHandlerConfig apiHandlerConfig = GatewayAPIConfigurator.getAPIConfig();
        apiHandlerConfig.setApiServer(APIUrls.APIServer.NOTIFICATIONGATEWAY.name());
        apiHandlerConfig.setApiName(APIUrls.APIName.API_INFO.name());
        apiHandlerConfig.addHeader(CONTENT_TYPE_HEADER, JSON_MEDIA_TYPE);
        apiHandlerConfig.setHttpMethod(method);
        apiHandlerConfig.setExpectedResponseCode(responseCode);
        apiHandlerConfig.setExpectedResponseBody(info);
        apiHandlerConfig.setResponseValueType(responseType);
        apiHandlerConfig.setResponseBodyValidator(new APIInfoResponseValidator());

        APIHandler apiHandler = apiHandlerFactory.getAPIHandler();
        APIResponse response = null;

        if(method.equals("GET")) {
            response = apiHandler.get(apiHandlerConfig);
            return (ApiInfo) response.getResponseBody();
        }
        else if(method.equals("POST")){
            response = apiHandler.post(apiHandlerConfig);
            return (ErrorDetails) response.getResponseBody();
        }
        else if(method.equals("PUT")){
            response = apiHandler.put(apiHandlerConfig);
            return (ErrorDetails) response.getResponseBody();
        }
        else if(method.equals("DELETE")){
            response = apiHandler.delete(apiHandlerConfig);
            return (ErrorDetails) response.getResponseBody();
        }

        return null;
    }

    private class APIInfoResponseValidator extends ResponseBodyValidator<BaseBean> {
        @Override
        public boolean validateResponseBody(BaseBean expectedResponseBody,
                                            BaseBean actualResponseBody) throws Exception {

            return validateResponseBodyValues(ObjectToJsonConverter.convert(expectedResponseBody),
                    ObjectToJsonConverter.convert(actualResponseBody));

        }
    }
}
