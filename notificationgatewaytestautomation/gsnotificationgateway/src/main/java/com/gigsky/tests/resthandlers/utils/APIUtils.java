package com.gigsky.tests.resthandlers.utils;

import com.gigsky.tests.common.database.beans.DBConfigInfo;
import com.gigsky.tests.common.database.utils.DBConfigUtils;
import com.gigsky.tests.dbaccess.NotificationGatewayDB;
import com.gigsky.tests.resthandlers.beans.*;
import com.gigsky.tests.resthandlers.impl.GetCustomerDetailsOperation;
import com.gigsky.tests.resthandlers.utils.Enums.AddDeviceData;
import org.apache.http.HttpStatus;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Random;
import java.util.List;

public class APIUtils {

    private static final APIUtils apiUtils = new APIUtils();
    public static APIUtils getInstance() { return apiUtils; }

    DBConfigInfo notificationGatewayDBConfigInfo = DBConfigUtils.getDBAccess("gigskyNotificationGateway");
    NotificationGatewayDB gatewayDB = NotificationGatewayDB.getInstance();

    public static int addDeviceCount = 0;
    public static int customersCount = 0;

    public ErrorDetails getExpectedErrorResponse(int errorInt, String errorString){
        ErrorDetails expectedResponse = new ErrorDetails();
        expectedResponse.setType("error");
        expectedResponse.setErrorInt(errorInt);
        expectedResponse.setErrorStr(errorString);
        return expectedResponse;
    }

    public AddDevice getAddDeviceExpectedResponse(String deviceType, String deviceVersion, String locale, String appVersion, String location){
        Random random = new Random();
        AddDevice addDevice = new AddDevice();
        addDevice.setType("DeviceDetail");
         addDevice.setDeviceType(deviceType);
         addDevice.setDeviceOSVersion(deviceVersion);
        addDevice.setModel("Basic");
        addDevice.setChannelId("012344-8ae0-ab2122"+ random.nextInt(26000));
        if(deviceType.equals("ANDROID"))
            addDevice.setInstallationId("012344-8ae0-ab2122"+ random.nextInt(25000));
        else
            addDevice.setDeviceToken("64123A4512313221"+ random.nextInt(25000));
         addDevice.setLocale(locale);
         addDevice.setAppVersion(appVersion);
        Preferences preferences = new Preferences();
        if(!location.equals("empty"))
         preferences.setLocation(location);
        if(!location.equals("emptyPreference"))
         addDevice.setPreferences(preferences);
        return addDevice;
    }

    public AddDevice getAddDeviceExpectedResponse(AddDeviceData addDeviceData){
        return getAddDeviceExpectedResponse(addDeviceData.deviceType,addDeviceData.deviceOSVersion,addDeviceData.locale,addDeviceData.appVersion,addDeviceData.location);
    }

    public AddDevice getAddDeviceDetails(){
        if ((addDeviceCount % 2) == 0) {
            AddDevice addDevice = getAddDeviceExpectedResponse("ANDROID","9.0.2","en","4.0","ALLOW");
            addDeviceCount ++;
            return addDevice;
        }
        else{
            AddDevice addDevice  = getAddDeviceExpectedResponse("IOS","9.0.2","en","4.0","ALLOW");
            addDeviceCount ++;
            return addDevice;
        }
    }

    public GetCustomerDetails getCustomerDetailsExpectedResponse(){
        GetCustomerDetails customerDetails = new GetCustomerDetails();
        customerDetails.setType("CustomerDetail");
        return customerDetails;
    }

    public GetCustomerDetails getCustomerDetails() throws Exception{
        GetCustomerDetailsOperation getCustomerDetailsOperation = GetCustomerDetailsOperation.getInstance();
        customersCount ++;
        String token = gatewayDB.getMockCustomerToken(notificationGatewayDBConfigInfo, String.valueOf(customersCount));
        GetCustomerDetails customerDetails = (GetCustomerDetails) getCustomerDetailsOperation.getCustomerDetails(getCustomerDetailsExpectedResponse(),token, HttpStatus.SC_OK);
        customerDetails.setAdditionalProperty("token",token);
        return customerDetails;
    }

    public GetDevice getDeviceDetailsExpectedResponse(AddDevice addDevice, String custId) {
        GetDevice getDevice = new GetDevice();
        getDevice.setType("DeviceDetails");
        getDevice.setCount(1);
        getDevice.setStartIndex(0);
        getDevice.setTotalCount(1);
        getDevice.setCustomerId(custId);
        DeviceList deviceList = new DeviceList();
        deviceList.setAppVersion(addDevice.getAppVersion());
        deviceList.setChannelId(addDevice.getChannelId());
        deviceList.setDeviceOSVersion(addDevice.getDeviceOSVersion());
        deviceList.setDeviceToken(addDevice.getDeviceToken());
        deviceList.setInstallationId(addDevice.getInstallationId());
        deviceList.setPreferences(addDevice.getPreferences());
        deviceList.setModel(addDevice.getModel());
        deviceList.setType(addDevice.getType());
        deviceList.setLocale(addDevice.getLocale());
        deviceList.setDeviceType(addDevice.getDeviceType());
        List<DeviceList> lists = new ArrayList<>();
        lists.add(deviceList);
        getDevice.setList(lists);
        return getDevice;
    }

    public GetDevice getDeviceDetailsExpectedResponse(AddDevice addDevice1, AddDevice addDevice2, AddDevice addDevice3, String custId, int count){
        GetDevice getDevice = new GetDevice();
        getDevice.setType("DeviceDetails");
        getDevice.setCount(count);
        getDevice.setStartIndex(0);
        getDevice.setTotalCount(count);
        getDevice.setCustomerId(custId);
        DeviceList deviceList1 =new DeviceList();
        DeviceList deviceList2 =new DeviceList();

        java.util.List<DeviceList> lists = new ArrayList<>();
       if(count!=0) {
           deviceList1.setAppVersion(addDevice1.getAppVersion());
           deviceList1.setChannelId(addDevice1.getChannelId());
           deviceList1.setDeviceOSVersion(addDevice1.getDeviceOSVersion());
           deviceList1.setDeviceToken(addDevice1.getDeviceToken());
           deviceList1.setInstallationId(addDevice1.getInstallationId());
           deviceList1.setPreferences(addDevice1.getPreferences());
           deviceList1.setModel(addDevice1.getModel());
           deviceList1.setType(addDevice1.getType());
           deviceList1.setLocale(addDevice1.getLocale());
           deviceList1.setDeviceType(addDevice1.getDeviceType());


           deviceList2.setAppVersion(addDevice2.getAppVersion());
           deviceList2.setChannelId(addDevice2.getChannelId());
           deviceList2.setDeviceOSVersion(addDevice2.getDeviceOSVersion());
           deviceList2.setDeviceToken(addDevice2.getDeviceToken());
           deviceList2.setInstallationId(addDevice2.getInstallationId());
           deviceList2.setPreferences(addDevice2.getPreferences());
           deviceList2.setModel(addDevice2.getModel());
           deviceList2.setType(addDevice2.getType());
           deviceList2.setLocale(addDevice2.getLocale());
           lists.add(deviceList1);
           lists.add(deviceList2);
       }
       if(addDevice3!=null) {
           DeviceList deviceList3 =new DeviceList();
           deviceList3.setAppVersion(addDevice3.getAppVersion());
           deviceList3.setChannelId(addDevice3.getChannelId());
           deviceList3.setDeviceOSVersion(addDevice3.getDeviceOSVersion());
           deviceList3.setDeviceToken(addDevice3.getDeviceToken());
           deviceList3.setInstallationId(addDevice3.getInstallationId());
           deviceList3.setPreferences(addDevice3.getPreferences());
           deviceList3.setModel(addDevice3.getModel());
           deviceList3.setType(addDevice3.getType());
           deviceList3.setLocale(addDevice3.getLocale());
           deviceList3.setDeviceType(addDevice3.getDeviceType());
           lists.add(deviceList3);
       }

       getDevice.setList(lists);
       return getDevice;

    }

    public BaseBean getEventData(String eventType, String customerId, String emailID){

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        String formattedDate = formatter.format(date);

        if(eventType.contains("CREDIT") || eventType.equals("REFERRAL_INITIATION"))
            return emailEventDetails(eventType, emailID);
        else {
            Event eventData = new Event();
            eventData.setType("EventDetail");
            eventData.setEventType(eventType);
            eventData.setCustomerId(customerId);
            eventData.setUserId(emailID);
            if (eventType.equals("LOW_BALANCE") || eventType.equals("SUBSCRIPTION_NEARING_EXPIRY"))
                eventData.setBalanceRemaining(5);
            else if (eventType.equals("DATA_USED"))
                eventData.setPercentageConsumed(50);
            if (!(eventType.equals("NEW_LOCATION")))
                eventData.setExpiry("2020-12-30 22:22:22");

            eventData.setCreationTime(String.valueOf(formattedDate));
            eventData.setIccId("8910300000000006173");
            eventData.setLocation("US");
            Options options = new Options();
            options.setSimName("TEST");
            eventData.setOptions(options);
            return eventData;
        }
    }

    public BaseBean getEventData(String eventType, String customerId, String emailID,String iccId, String simName){
        Event event = (Event) getEventData(eventType, customerId, emailID);
        if(iccId!=null)
            event.setIccId(iccId);

        if(simName!=null) {
            Options op = new Options();
            op.setSimName(simName);
            event.setOptions(op);
        }

        return event;
    }

    public Event emailEventDetails(String subEventType, String emailId){
        Event emailEvent = new Event();
        emailEvent.setType("EventDetail");
        emailEvent.setEventType("EMAIL");
        emailEvent.setSubEventType(subEventType);
        emailEvent.setLanguage("en");
        emailEvent.setEmailIds(Arrays.asList(emailId));
        Options options = getEmailEventOptions(subEventType, emailId);
        emailEvent.setOptions(options);
        return emailEvent;
    }

    private Options getEmailEventOptions(String eventType, String emailId){

        switch (eventType){
            case "REFERRAL_INITIATION" :   return getReferralInitiationOptions(emailId);
            case "ADVOCATE_CREDIT_ADDED" : return getAdvocateCreditAddedOptions(emailId);
            case "INTERNAL_CREDIT_ALERT" : return getInternalCreditAlertOptions(emailId);
            case "INTERNAL_CREDIT_DEDUCT" : return getInternalCreditDeductOptions(emailId);
            case "INTERNAL_CREDIT_WARNING" : return getInternalCreditWarningOptions(emailId);
            case "ICCID_PROMOTION_CREDIT" : return getPromotionCreditOptions(emailId);
        }
        return new Options();
    }

    private Options getReferralInitiationOptions(String emailId){
        Options emailEventOptions = new Options();
        emailEventOptions.setCustomerName("Test User");
        emailEventOptions.setCustomerEmail(emailId);
        emailEventOptions.setCurrency("USD");
        emailEventOptions.setGsCustomerId(1234);
        emailEventOptions.setReferralCode("TEST100");
        emailEventOptions.setAdvocateAmount("10");
        emailEventOptions.setReferralAmount("10");
        return emailEventOptions;
    }

    private Options getAdvocateCreditAddedOptions(String emailId){
        Options emailEventOptions = new Options();
        emailEventOptions.setCustomerName("Test User");
        emailEventOptions.setCustomerEmail(emailId);
        emailEventOptions.setNewCustomerEmail(emailId);
        emailEventOptions.setGsCustomerId(1234);
        emailEventOptions.setAdvocateAmount("10");
        emailEventOptions.setCurrency("USD");
        return emailEventOptions;
    }

    private Options getInternalCreditAlertOptions(String emailId){
        Options emailEventOptions = new Options();
        emailEventOptions.setCustomerEmail(emailId);
        emailEventOptions.setCreditLimit("900");
        emailEventOptions.setCurrency("USD");
        return emailEventOptions;
    }

    private Options getInternalCreditDeductOptions(String emailId){
        Options emailEventOptions = new Options();
        emailEventOptions.setCustomerEmail(emailId);
        emailEventOptions.setCsn("C8D2E73261DE374CD8254DFF43B304A4");
        return emailEventOptions;
    }

    private Options getInternalCreditWarningOptions(String emailId){
        Options emailEventOptions = new Options();
        emailEventOptions.setCustomerEmail(emailId);
        emailEventOptions.setCurrency("USD");
        emailEventOptions.setCreditWarningLimit("900");
        return emailEventOptions;
    }

    private Options getPromotionCreditOptions(String emailId){
        Options emailEventOptions = new Options();
        emailEventOptions.setCustomerName("Test User");
        emailEventOptions.setCustomerEmail(emailId);
        emailEventOptions.setCurrency("USD");
        emailEventOptions.setGsCustomerId(1234);
        emailEventOptions.setPromoAmount(10);
        emailEventOptions.setCsn("C8D2E73261DE374CD8254DFF43B304A4");
        return emailEventOptions;
    }

    public NotificationStatus getNotificationStatusResponse(String notificationId,String status,String eventID,String eventType,String statusMessage)
    {
        NotificationStatus NotifnStatus=new NotificationStatus();
        NotifnStatus.setType("NotificationDetail");
        NotifnStatus.setNotificationId(notificationId);
        NotifnStatus.setStatus(status);
        NotifnStatus.setEventId(eventID);
        if(!(eventType.equals("EMAIL")))
            NotifnStatus.setNotificationType("PUSH");
        else
            NotifnStatus.setNotificationType("EMAIL");
        NotifnStatus.setStatusMessage(statusMessage);
        return  NotifnStatus;


    }
    public PartnerApp getPartnerAppGetDeviceResponse(String deviceType, String ipAddress, String deviceOSVersion, String locale, String iccId, String location, String partnerCode, String sdkVersion)
    {
        PartnerApp app=new PartnerApp();
        app.setType("PartnerAppDetail");
        app.setDeviceType(deviceType);
        app.setDeviceOSVersion(deviceOSVersion);
        app.setLocale(locale);
        app.setIpAddress(ipAddress);
        app.setIccId(iccId);
        app.setLocation(location);
        app.setPartnerCode(partnerCode);
        app.setSdkVersion(sdkVersion);
        return  app;

    }


}
