package com.gigsky.tests.resthandlers.beans;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "oldEmailId",
        "anonymousEmailId"
})
public class DeleteCustomerDetails extends BaseBean{

    @JsonProperty("type")
    private String type;
    @JsonProperty("oldEmailId")
    private String oldEmailId;
    @JsonProperty("anonymousEmailId")
    private String anonymousEmailId;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("oldEmailId")
    public String getOldEmailId() {
        return oldEmailId;
    }

    @JsonProperty("oldEmailId")
    public void setOldEmailId(String oldEmailId) {
        this.oldEmailId = oldEmailId;
    }

    @JsonProperty("anonymousEmailId")
    public String getAnonymousEmailId() {
        return anonymousEmailId;
    }

    @JsonProperty("anonymousEmailId")
    public void setAnonymousEmailId(String anonymousEmailId) {
        this.anonymousEmailId = anonymousEmailId;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}