package com.gigsky.tests.resthandlers.impl;

import com.gigsky.testng.common.APIUrls;
import com.gigsky.tests.common.apihandler.beans.APIHandlerConfig;
import com.gigsky.tests.common.apihandler.beans.APIResponse;
import com.gigsky.tests.common.apihandler.factory.APIHandlerFactory;
import com.gigsky.tests.common.apihandler.impl.APIOperations;
import com.gigsky.tests.common.apihandler.interfaces.APIHandler;
import com.gigsky.tests.common.apihandler.interfaces.ResponseBodyValidator;
import com.gigsky.tests.common.utils.ObjectToJsonConverter;
import com.gigsky.tests.resthandlers.beans.BaseBean;
import com.gigsky.tests.resthandlers.beans.ErrorDetails;
import com.gigsky.tests.resthandlers.beans.PartnerApp;

import javax.ws.rs.HttpMethod;

public class PartnerAppOperation extends APIOperations {
    private static final PartnerAppOperation instance = new PartnerAppOperation();
    private APIHandlerFactory apiHandlerFactory = APIHandlerFactory.getInstance();

    public static PartnerAppOperation getInstance() {
        return instance;
    }

    public BaseBean registerDevice(String deviceType, String ipAddress, String deviceOSVersion, String locale, String iccId, String location, String partnerCode, String sdkVersion, Integer responseCode, ErrorDetails error) throws Exception {
        APIHandlerConfig apiHandlerConfig = GatewayAPIConfigurator.getAPIConfig();
        apiHandlerConfig.setApiServer(APIUrls.APIServer.NOTIFICATIONGATEWAY.name());
        apiHandlerConfig.addQueryParam("deviceType", deviceType);
        apiHandlerConfig.addQueryParam("ipAddress", ipAddress);
        apiHandlerConfig.addQueryParam("deviceOSVersion", deviceOSVersion);
        apiHandlerConfig.addQueryParam("locale", locale);
        apiHandlerConfig.addQueryParam("iccId", iccId);
        apiHandlerConfig.addQueryParam("location", location);
        apiHandlerConfig.addQueryParam("partnerCode", partnerCode);
        apiHandlerConfig.addQueryParam("sdkVersion", sdkVersion);
        apiHandlerConfig.setApiName(APIUrls.APIName.PARTNER_APP_REGISTER.name());
        apiHandlerConfig.addHeader(CONTENT_TYPE_HEADER, JSON_MEDIA_TYPE);
        apiHandlerConfig.setHttpMethod(HttpMethod.GET);
        apiHandlerConfig.setExpectedResponseCode(responseCode);
        if (error != null) {
            apiHandlerConfig.setResponseValueType(ErrorDetails.class);
            apiHandlerConfig.setResponseBodyValidator(new PartnerAppOperation.PartnerAppValidator());
        }
        APIHandler apiHandler = apiHandlerFactory.getAPIHandler();
        APIResponse response = apiHandler.get(apiHandlerConfig);
        return (BaseBean) response.getResponseBody();

    }

    public BaseBean getDevice(PartnerApp info,String deviceType, String ipAddress, String deviceOSVersion, String locale,Integer responseCode,ErrorDetails error) throws Exception {
        APIHandlerConfig apiHandlerConfig = GatewayAPIConfigurator.getAPIConfig();
        apiHandlerConfig.setApiServer(APIUrls.APIServer.NOTIFICATIONGATEWAY.name());
        apiHandlerConfig.addQueryParam("deviceType", deviceType);
        apiHandlerConfig.addQueryParam("ipAddress", ipAddress);
        apiHandlerConfig.addQueryParam("deviceOSVersion", deviceOSVersion);
        apiHandlerConfig.addQueryParam("locale", locale);
        apiHandlerConfig.setApiName(APIUrls.APIName.PARTNER_APP_GET_DEVICE.name());
        apiHandlerConfig.setHttpMethod(HttpMethod.GET);
        apiHandlerConfig.setExpectedResponseCode(responseCode);
        if (error != null) {
            apiHandlerConfig.setResponseValueType(ErrorDetails.class);
            apiHandlerConfig.setResponseBodyValidator(new PartnerAppOperation.PartnerAppValidator());
        }
        else
        {
            apiHandlerConfig.setResponseValueType(PartnerApp.class);
            apiHandlerConfig.setExpectedResponseBody(info);

        }
        APIHandler apiHandler = apiHandlerFactory.getAPIHandler();
        apiHandlerConfig.setResponseBodyValidator(new PartnerAppOperation.PartnerAppValidator());
        APIResponse response  = apiHandler.get(apiHandlerConfig);
        return (BaseBean) response.getResponseBody();




    }


    private class PartnerAppValidator extends ResponseBodyValidator<BaseBean> {
        @Override
        public boolean validateResponseBody(BaseBean expectedResponseBody,
                                            BaseBean actualResponseBody) throws Exception {

            return validateResponseBodyValues(ObjectToJsonConverter.convert(expectedResponseBody),
                    ObjectToJsonConverter.convert(actualResponseBody));

        }
    }

}
