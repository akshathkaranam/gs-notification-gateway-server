package com.gigsky.tests.resthandlers.impl;

import com.gigsky.testng.common.APIUrls;
import com.gigsky.tests.common.apihandler.beans.APIHandlerConfig;
import com.gigsky.tests.common.apihandler.beans.APIResponse;
import com.gigsky.tests.common.apihandler.factory.APIHandlerFactory;
import com.gigsky.tests.common.apihandler.impl.APIOperations;
import com.gigsky.tests.common.apihandler.interfaces.APIHandler;
import com.gigsky.tests.common.apihandler.interfaces.ResponseBodyValidator;
import com.gigsky.tests.common.utils.ObjectToJsonConverter;
import com.gigsky.tests.resthandlers.beans.BaseBean;
import com.gigsky.tests.resthandlers.beans.ErrorDetails;
import com.gigsky.tests.resthandlers.beans.Event;
import com.gigsky.tests.resthandlers.utils.Enums.RetryEnums;

import javax.ws.rs.HttpMethod;

public class EventOperation extends APIOperations {
    private static final EventOperation instance = new EventOperation();
    private APIHandlerFactory apiHandlerFactory = APIHandlerFactory.getInstance();
    public static EventOperation getInstance() {
        return instance;
    }

    public BaseBean createEvent(BaseBean inputData, ErrorDetails errorDetails, Integer responseCode, String method) throws Exception {
        APIHandlerConfig apiHandlerConfig = GatewayAPIConfigurator.getAPIConfig();
        apiHandlerConfig.setApiServer(APIUrls.APIServer.NOTIFICATIONGATEWAY.name());
        apiHandlerConfig.setApiName(APIUrls.APIName.CREATE_EVENT.name());
        apiHandlerConfig.addHeader(CONTENT_TYPE_HEADER, JSON_MEDIA_TYPE);
        apiHandlerConfig.setHttpMethod(HttpMethod.POST);
        apiHandlerConfig.setRequestBody(inputData);
        if(errorDetails!=null) {
            apiHandlerConfig.setResponseValueType(ErrorDetails.class);
            apiHandlerConfig.setExpectedResponseBody(errorDetails);

        }
        else {
            apiHandlerConfig.setResponseValueType(Event.class);
            apiHandlerConfig.setExpectedResponseBody(inputData);

        }

        apiHandlerConfig.setResponseBodyValidator(new EventOperation.EventResponseValidator());
        apiHandlerConfig.setExpectedResponseCode(responseCode);
        APIHandler apiHandler = apiHandlerFactory.getAPIHandler();
        if(method==HttpMethod.POST)
        {
            APIResponse response = apiHandler.post(apiHandlerConfig);
            return (BaseBean) response.getResponseBody();
        }
       else if(method==HttpMethod.DELETE)
        {
            APIResponse response = apiHandler.delete(apiHandlerConfig);
            return (ErrorDetails) response.getResponseBody();
        }
        else if(method==HttpMethod.PUT)
        {
            APIResponse response = apiHandler.put(apiHandlerConfig);
            return (ErrorDetails) response.getResponseBody();
        }
        else if(method==HttpMethod.GET)
        {
            APIResponse response = apiHandler.get(apiHandlerConfig);
            return (ErrorDetails) response.getResponseBody();
        }

        Thread.sleep(15000);
        return null;
    }

    public BaseBean getEvent(BaseBean expectedRespone, String eventId, Integer responseCode) throws Exception {
        APIHandlerConfig apiHandlerConfig = GatewayAPIConfigurator.getAPIConfig();
        apiHandlerConfig.setApiServer(APIUrls.APIServer.NOTIFICATIONGATEWAY.name());
        apiHandlerConfig.setApiName(APIUrls.APIName.GET_EVENT.name());
        apiHandlerConfig.addHeader(CONTENT_TYPE_HEADER, JSON_MEDIA_TYPE);
        apiHandlerConfig.addPathParam(APIUrls.APIPathParams.EVENT_ID.name(),eventId);
        apiHandlerConfig.setHttpMethod(HttpMethod.GET);
        apiHandlerConfig.setRetryCount(RetryEnums.RETRY_COUNT.getRetryCount());
        apiHandlerConfig.setRetryTimeoutMS(RetryEnums.RETRY_TIME.getRetryCount());
        apiHandlerConfig.setExpectedResponseBody(expectedRespone);
        if(expectedRespone instanceof Event)
            apiHandlerConfig.setResponseValueType(Event.class);
        else
            apiHandlerConfig.setResponseValueType(ErrorDetails.class);

        apiHandlerConfig.setResponseBodyValidator(new EventOperation.EventResponseValidator());
        apiHandlerConfig.setExpectedResponseCode(responseCode);
        APIHandler apiHandler = apiHandlerFactory.getAPIHandler();
        APIResponse response = apiHandler.get(apiHandlerConfig);
        return (BaseBean) response.getResponseBody();
    }

    private class EventResponseValidator extends ResponseBodyValidator<BaseBean> {
        @Override
        public boolean validateResponseBody(BaseBean expectedResponseBody,
                                            BaseBean actualResponseBody) throws Exception {

            return validateResponseBodyValues(ObjectToJsonConverter.convert(expectedResponseBody),
                    ObjectToJsonConverter.convert(actualResponseBody));

        }
    }
}
