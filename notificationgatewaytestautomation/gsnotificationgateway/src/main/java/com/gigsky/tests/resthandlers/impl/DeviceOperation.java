package com.gigsky.tests.resthandlers.impl;

import com.gigsky.testng.common.APIUrls;
import com.gigsky.tests.common.apihandler.beans.APIHandlerConfig;
import com.gigsky.tests.common.apihandler.beans.APIResponse;
import com.gigsky.tests.common.apihandler.factory.APIHandlerFactory;
import com.gigsky.tests.common.apihandler.impl.APIOperations;
import com.gigsky.tests.common.apihandler.interfaces.APIHandler;
import com.gigsky.tests.common.apihandler.interfaces.ResponseBodyValidator;
import com.gigsky.tests.common.utils.ObjectToJsonConverter;
import com.gigsky.tests.resthandlers.beans.AddDevice;
import com.gigsky.tests.resthandlers.beans.BaseBean;
import com.gigsky.tests.resthandlers.beans.ErrorDetails;

public class DeviceOperation extends APIOperations {
    private static final DeviceOperation instance = new DeviceOperation();
    private APIHandlerFactory apiHandlerFactory = APIHandlerFactory.getInstance();
    public static DeviceOperation getInstance() {
        return instance;
    }

    public BaseBean addDevice(BaseBean info, String token, String customerUId, int responseCode, ErrorDetails errorDetails, String method) throws Exception {
        APIHandlerConfig apiHandlerConfig = GatewayAPIConfigurator.getAPIConfig();
        apiHandlerConfig.setApiServer(APIUrls.APIServer.NOTIFICATIONGATEWAY.name());
        apiHandlerConfig.setApiName(APIUrls.APIName.ADD_DEVICE.name());
        apiHandlerConfig.addPathParam(APIUrls.APIPathParams.CUSTOMER_UUID.name(),customerUId);
        apiHandlerConfig.addHeader(CONTENT_TYPE_HEADER, JSON_MEDIA_TYPE);
        apiHandlerConfig.addHeader(AUTHORIZATION_HEADER, "Basic " + token);
        apiHandlerConfig.setHttpMethod(method);
        apiHandlerConfig.setRequestBody(info);
        apiHandlerConfig.setExpectedResponseCode(responseCode);

        if(errorDetails instanceof ErrorDetails)
        {
            apiHandlerConfig.setExpectedResponseBody(errorDetails);
            apiHandlerConfig.setResponseValueType(ErrorDetails.class);
        }
        else
            {
            apiHandlerConfig.setExpectedResponseBody(info);
            apiHandlerConfig.setResponseValueType(AddDevice.class);
         }

        apiHandlerConfig.setResponseBodyValidator(new DeviceOperation.AddDeviceResponseValidator());

        APIHandler apiHandler = apiHandlerFactory.getAPIHandler();
        APIResponse response  = apiHandler.post(apiHandlerConfig);
        return (BaseBean) response.getResponseBody();
    }

    public BaseBean updateDevice(BaseBean requestBody, String token, String customerUId,String deviceID,int responseCode, ErrorDetails errorDetails, String method) throws Exception {
        APIHandlerConfig apiHandlerConfig = GatewayAPIConfigurator.getAPIConfig();
        apiHandlerConfig.setApiServer(APIUrls.APIServer.NOTIFICATIONGATEWAY.name());
        apiHandlerConfig.addPathParam(APIUrls.APIPathParams.CUSTOMER_UUID.name(),customerUId);
        apiHandlerConfig.addPathParam(APIUrls.APIPathParams.DEVICE_ID.name(),deviceID);
        apiHandlerConfig.setApiName(APIUrls.APIName.UPDATE_DEVICE.name());
        apiHandlerConfig.addHeader(CONTENT_TYPE_HEADER, JSON_MEDIA_TYPE);
        if(!(token.equals("empty")))
            apiHandlerConfig.addHeader(AUTHORIZATION_HEADER, "Basic " + token);
        apiHandlerConfig.setHttpMethod(method);
        apiHandlerConfig.setRequestBody(requestBody);
        apiHandlerConfig.setExpectedResponseCode(responseCode);
        if(errorDetails instanceof ErrorDetails)
        {
            apiHandlerConfig.setExpectedResponseBody(errorDetails);
            apiHandlerConfig.setResponseValueType(ErrorDetails.class);
        }
        else {
            apiHandlerConfig.setExpectedResponseBody(requestBody);
            apiHandlerConfig.setResponseValueType(AddDevice.class);
        }
        apiHandlerConfig.setResponseBodyValidator(new DeviceOperation.AddDeviceResponseValidator());

        APIHandler apiHandler = apiHandlerFactory.getAPIHandler();
        APIResponse response  = apiHandler.put(apiHandlerConfig);
        return (BaseBean) response.getResponseBody();
    }


    private class AddDeviceResponseValidator extends ResponseBodyValidator<BaseBean> {
        @Override
        public boolean validateResponseBody(BaseBean expectedResponseBody,
                                            BaseBean actualResponseBody) throws Exception {

            return validateResponseBodyValues(ObjectToJsonConverter.convert(expectedResponseBody),
                    ObjectToJsonConverter.convert(actualResponseBody));

        }
    }
}
