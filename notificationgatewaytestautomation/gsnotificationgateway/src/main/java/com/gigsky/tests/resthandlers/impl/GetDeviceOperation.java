package com.gigsky.tests.resthandlers.impl;

import com.gigsky.testng.common.APIUrls;
import com.gigsky.tests.common.apihandler.beans.APIHandlerConfig;
import com.gigsky.tests.common.apihandler.beans.APIResponse;
import com.gigsky.tests.common.apihandler.factory.APIHandlerFactory;
import com.gigsky.tests.common.apihandler.impl.APIOperations;
import com.gigsky.tests.common.apihandler.interfaces.APIHandler;
import com.gigsky.tests.common.apihandler.interfaces.ResponseBodyValidator;
import com.gigsky.tests.common.utils.ObjectToJsonConverter;
import com.gigsky.tests.resthandlers.beans.BaseBean;
import com.gigsky.tests.resthandlers.beans.ErrorDetails;
import com.gigsky.tests.resthandlers.beans.GetDevice;

import javax.ws.rs.HttpMethod;

public class GetDeviceOperation extends APIOperations {

    private static final GetDeviceOperation instance = new GetDeviceOperation();
    private APIHandlerFactory apiHandlerFactory = APIHandlerFactory.getInstance();
    public static GetDeviceOperation getInstance() {
        return instance;
    }

    public BaseBean getDeviceDetails(BaseBean info, String customerUId, String token, String method, Integer responseCode) throws Exception {
        APIHandlerConfig apiHandlerConfig = GatewayAPIConfigurator.getAPIConfig();
        apiHandlerConfig.setApiServer(APIUrls.APIServer.NOTIFICATIONGATEWAY.name());
        apiHandlerConfig.setApiName(APIUrls.APIName.GET_DEVICE.name());
        apiHandlerConfig.addHeader(CONTENT_TYPE_HEADER, JSON_MEDIA_TYPE);
        if(!(token.equals("empty")))
            apiHandlerConfig.addHeader(AUTHORIZATION_HEADER, "Basic " + token);
        apiHandlerConfig.setHttpMethod(method);
        apiHandlerConfig.setExpectedResponseCode(responseCode);
        apiHandlerConfig.addPathParam(APIUrls.APIPathParams.CUSTOMER_UUID.name(),customerUId);
        apiHandlerConfig.addQueryParam("startIndex","0");
        apiHandlerConfig.addQueryParam("count","10");

        if(info instanceof ErrorDetails)
        {

            apiHandlerConfig.setExpectedResponseBody(info);
            apiHandlerConfig.setResponseValueType(ErrorDetails.class);
        }
        else {
            apiHandlerConfig.setExpectedResponseBody(info);
            apiHandlerConfig.setResponseValueType(GetDevice.class);
        }
        apiHandlerConfig.setResponseBodyValidator(new GetDeviceOperation.GetDeviceInfoResponseValidator());

        APIHandler apiHandler = apiHandlerFactory.getAPIHandler();
        APIResponse response=null;
        if(method.equals(HttpMethod.DELETE)){
            response = apiHandler.delete(apiHandlerConfig);
        }
        else if(method.equals(HttpMethod.PUT))
            response = apiHandler.put(apiHandlerConfig);
        else
            response = apiHandler.get(apiHandlerConfig);

        return (BaseBean) response.getResponseBody();
    }

    private class GetDeviceInfoResponseValidator extends ResponseBodyValidator<BaseBean> {
        @Override
        public boolean validateResponseBody(BaseBean expectedResponseBody,
                                            BaseBean actualResponseBody) throws Exception {

            return validateResponseBodyValues(ObjectToJsonConverter.convert(expectedResponseBody),
                    ObjectToJsonConverter.convert(actualResponseBody));

        }
    }
}
