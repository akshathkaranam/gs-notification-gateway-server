package com.gigsky.tests.resthandlers.impl;


import com.gigsky.testng.common.APIUrls;
import com.gigsky.tests.common.apihandler.beans.APIHandlerConfig;
import com.gigsky.tests.common.apihandler.beans.APIResponse;
import com.gigsky.tests.common.apihandler.factory.APIHandlerFactory;
import com.gigsky.tests.common.apihandler.impl.APIOperations;
import com.gigsky.tests.common.apihandler.interfaces.APIHandler;
import com.gigsky.tests.common.apihandler.interfaces.ResponseBodyValidator;
import com.gigsky.tests.common.utils.ObjectToJsonConverter;
import com.gigsky.tests.resthandlers.beans.AppFeature;
import com.gigsky.tests.resthandlers.beans.BaseBean;
import com.gigsky.tests.resthandlers.beans.ErrorDetails;


import javax.ws.rs.HttpMethod;


public class AppFeatureOperation extends APIOperations {
    private static final AppFeatureOperation instance = new AppFeatureOperation();
    private APIHandlerFactory apiHandlerFactory = APIHandlerFactory.getInstance();

    public static AppFeatureOperation getInstance() {
        return instance;
    }

    public APIHandlerConfig appFeature(String clientType, String version, String userId, Integer responseCode, String method) {
        APIHandlerConfig apiHandlerConfig = GatewayAPIConfigurator.getAPIConfig();
        apiHandlerConfig.setApiServer(APIUrls.APIServer.NOTIFICATIONGATEWAY.name());
        apiHandlerConfig.addQueryParam("clientType", clientType);
        apiHandlerConfig.addQueryParam("clientVersion",version);
        apiHandlerConfig.addQueryParam("userId",userId);

        apiHandlerConfig.setApiName(APIUrls.APIName.APP_FEATURE_INFO.name());
        apiHandlerConfig.addHeader(CONTENT_TYPE_HEADER, JSON_MEDIA_TYPE);

        apiHandlerConfig.setHttpMethod(method);
        apiHandlerConfig.setExpectedResponseCode(responseCode);

        return apiHandlerConfig;

    }

    public BaseBean addAppFeatures(BaseBean appFeatureInfo, String clientType, String version, String userId,Integer responseCode,BaseBean errorDetails) throws  Exception {

        APIHandlerConfig apiHandlerConfig = appFeature(clientType,version,userId,responseCode,HttpMethod.POST);

        apiHandlerConfig.setRequestBody(appFeatureInfo);

        APIHandler apiHandler = apiHandlerFactory.getAPIHandler();

        if(errorDetails!=null) {
            apiHandlerConfig.setResponseValueType(ErrorDetails.class);
            apiHandlerConfig.setResponseBodyValidator(new AppFeatureOperation.AppFeatureResponseValidator());
        }

        APIResponse response  = apiHandler.post(apiHandlerConfig);
        return (BaseBean) response.getResponseBody();


    }
   public BaseBean getAppFeatures(BaseBean appFeatureInfo, String clientType, String version, String userId,Integer responseCode,BaseBean errorDetails) throws  Exception {

       APIHandlerConfig apiHandlerConfig = appFeature(clientType,version,userId,responseCode,HttpMethod.GET);



       if(errorDetails instanceof ErrorDetails) {
           apiHandlerConfig.setResponseValueType(ErrorDetails.class);
           apiHandlerConfig.setExpectedResponseBody(errorDetails);
       }
         else {
           apiHandlerConfig.setResponseValueType(AppFeature.class);
           apiHandlerConfig.setExpectedResponseBody(appFeatureInfo);
       }

       APIHandler apiHandler = apiHandlerFactory.getAPIHandler();

       apiHandlerConfig.setResponseBodyValidator(new AppFeatureOperation.AppFeatureResponseValidator());

       APIResponse response  = apiHandler.get(apiHandlerConfig);
       return (BaseBean) response.getResponseBody();



    }

    public  void deleteAppFeatures(String clientType,String version,String userID,Integer responseCode) throws Exception
    {
        APIHandlerConfig apiHandlerConfig = appFeature(clientType,version,userID,responseCode,HttpMethod.DELETE);
        apiHandlerConfig.setExpectedResponseBody(null);
        APIHandler apiHandler = apiHandlerFactory.getAPIHandler();
        APIResponse response  = apiHandler.delete(apiHandlerConfig);
    }

    public BaseBean updateAppFeatures(BaseBean appFeatureInfo,String clientType,String version,String userId,Integer responseCode,BaseBean errorDetails) throws Exception
    {
        APIHandlerConfig apiHandlerConfig = appFeature(clientType,version,userId,responseCode,HttpMethod.PUT);
        apiHandlerConfig.setRequestBody(appFeatureInfo);


        if(errorDetails instanceof ErrorDetails) {
            apiHandlerConfig.setResponseValueType(ErrorDetails.class);
            apiHandlerConfig.setExpectedResponseBody(errorDetails);
        }
        else {
            apiHandlerConfig.setResponseValueType(AppFeature.class);
            apiHandlerConfig.setExpectedResponseBody(appFeatureInfo);
        }

        APIHandler apiHandler = apiHandlerFactory.getAPIHandler();

        apiHandlerConfig.setResponseBodyValidator(new AppFeatureOperation.AppFeatureResponseValidator());

        APIResponse response  = apiHandler.put(apiHandlerConfig);
        return (BaseBean) response.getResponseBody();


    }

    private class AppFeatureResponseValidator extends ResponseBodyValidator<BaseBean> {
        @Override
        public boolean validateResponseBody(BaseBean expectedResponseBody,
                                            BaseBean actualResponseBody) throws Exception {

            return validateResponseBodyValues(ObjectToJsonConverter.convert(expectedResponseBody),
                    ObjectToJsonConverter.convert(actualResponseBody));

        }


    }
}
