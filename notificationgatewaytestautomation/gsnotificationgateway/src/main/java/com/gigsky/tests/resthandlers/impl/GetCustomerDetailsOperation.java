package com.gigsky.tests.resthandlers.impl;

import com.gigsky.testng.common.APIUrls;
import com.gigsky.tests.common.apihandler.beans.APIHandlerConfig;
import com.gigsky.tests.common.apihandler.beans.APIResponse;
import com.gigsky.tests.common.apihandler.factory.APIHandlerFactory;
import com.gigsky.tests.common.apihandler.impl.APIOperations;
import com.gigsky.tests.common.apihandler.interfaces.APIHandler;
import com.gigsky.tests.common.apihandler.interfaces.ResponseBodyValidator;
import com.gigsky.tests.common.utils.ObjectToJsonConverter;
import com.gigsky.tests.resthandlers.beans.BaseBean;
import com.gigsky.tests.resthandlers.beans.ErrorDetails;
import com.gigsky.tests.resthandlers.beans.GetCustomerDetails;

import javax.ws.rs.HttpMethod;

public class GetCustomerDetailsOperation extends APIOperations {
    private static final GetCustomerDetailsOperation instance = new GetCustomerDetailsOperation();
    private APIHandlerFactory apiHandlerFactory = APIHandlerFactory.getInstance();
    public static GetCustomerDetailsOperation getInstance() {
        return instance;
    }

    public BaseBean getCustomerDetails(BaseBean expectedResponse, String token, int responseCode) throws Exception {
        APIHandlerConfig apiHandlerConfig = GatewayAPIConfigurator.getAPIConfig();
        apiHandlerConfig.setApiServer(APIUrls.APIServer.NOTIFICATIONGATEWAY.name());
        apiHandlerConfig.setApiName(APIUrls.APIName.GET_CUSTOMERDETAILS.name());
        apiHandlerConfig.addHeader(CONTENT_TYPE_HEADER, JSON_MEDIA_TYPE);
        apiHandlerConfig.setHttpMethod(HttpMethod.GET);
        if(token!="empty") {
            apiHandlerConfig.addHeader(AUTHORIZATION_HEADER, "Basic " + token);
        }
        apiHandlerConfig.setExpectedResponseCode(responseCode);
        apiHandlerConfig.setExpectedResponseBody(expectedResponse);
        if(expectedResponse instanceof ErrorDetails) {
            apiHandlerConfig.setResponseValueType(ErrorDetails.class);
        }

        else {
            apiHandlerConfig.setResponseValueType(GetCustomerDetails.class);
            apiHandlerConfig.setResponseBodyValidator(new GetCustomerDetailsOperation.GetCustomerDetailsResponseValidator());
        }



        APIHandler apiHandler = apiHandlerFactory.getAPIHandler();
        APIResponse response = apiHandler.get(apiHandlerConfig);

        return (BaseBean) response.getResponseBody();
    }

    private class GetCustomerDetailsResponseValidator extends ResponseBodyValidator<BaseBean> {
        @Override
        public boolean validateResponseBody(BaseBean expectedResponseBody,
                                            BaseBean actualResponseBody) throws Exception {

            return validateResponseBodyValues(ObjectToJsonConverter.convert(expectedResponseBody),
                    ObjectToJsonConverter.convert(actualResponseBody));

        }
    }
}
