package com.gigsky.tests.resthandlers.beans;

import com.fasterxml.jackson.annotation.*;

import java.util.HashMap;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "type",
        "deviceType",
        "deviceOSVersion",
        "model",
        "channelId",
        "installationId",
        "deviceToken",
        "locale",
        "appVersion",
        "preferences"
})
public class DeviceList {

    @JsonProperty("type")
    private String type;
    @JsonProperty("deviceType")
    private String deviceType;
    @JsonProperty("deviceOSVersion")
    private String deviceOSVersion;
    @JsonProperty("model")
    private String model;
    @JsonProperty("channelId")
    private String channelId;
    @JsonProperty("installationId")
    private String installationId;
    @JsonProperty("deviceToken")
    private String deviceToken;
    @JsonProperty("locale")
    private String locale;
    @JsonProperty("appVersion")
    private String appVersion;
    @JsonProperty("preferences")
    private Preferences preferences;

    @JsonProperty("deviceId")
    private String deviceId;

    @JsonIgnore

    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("type")
    public String getType() {
        return type;
    }

    @JsonProperty("type")
    public void setType(String type) {
        this.type = type;
    }

    @JsonProperty("deviceType")
    public String getDeviceType() {
        return deviceType;
    }

    @JsonProperty("deviceType")
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    @JsonProperty("deviceOSVersion")
    public String getDeviceOSVersion() {
        return deviceOSVersion;
    }

    @JsonProperty("deviceOSVersion")
    public void setDeviceOSVersion(String deviceOSVersion) {
        this.deviceOSVersion = deviceOSVersion;
    }

    @JsonProperty("model")
    public String getModel() {
        return model;
    }

    @JsonProperty("model")
    public void setModel(String model) {
        this.model = model;
    }

    @JsonProperty("channelId")
    public String getChannelId() {
        return channelId;
    }

    @JsonProperty("channelId")
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    @JsonProperty("installationId")
    public String getInstallationId() {
        return installationId;
    }

    @JsonProperty("installationId")
    public void setInstallationId(String installationId) {
        this.installationId = installationId;
    }

    @JsonProperty("deviceToken")
    public String getDeviceToken() {
        return deviceToken;
    }

    @JsonProperty("deviceToken")
    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    @JsonProperty("locale")
    public String getLocale() {
        return locale;
    }

    @JsonProperty("locale")
    public void setLocale(String locale) {
        this.locale = locale;
    }

    @JsonProperty("appVersion")
    public String getAppVersion() {
        return appVersion;
    }

    @JsonProperty("appVersion")
    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    @JsonProperty("preferences")
    public Preferences getPreferences() {
        return preferences;
    }

    @JsonProperty("preferences")
    public void setPreferences(Preferences preferences) {
        this.preferences = preferences;
    }

    @JsonProperty("deviceId")
    public String getDeviceId() {
        return deviceId;
    }
    @JsonProperty("deviceId")
    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
