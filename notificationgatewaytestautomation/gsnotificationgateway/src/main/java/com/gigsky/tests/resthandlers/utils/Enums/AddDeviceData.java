package com.gigsky.tests.resthandlers.utils.Enums;

public enum  AddDeviceData {
    IOS_DEVICE1("IOS", "6.1", "en", "3.0", "ALLOW");

    public String deviceType, deviceOSVersion, locale, appVersion, location;
    AddDeviceData(String deviceType, String deviceOSVersion, String locale, String appVersion, String location) {
     this.deviceType = deviceType;
     this.deviceOSVersion = deviceOSVersion;
     this.locale = locale;
     this.appVersion = appVersion;
     this.location = location;
    }
}
