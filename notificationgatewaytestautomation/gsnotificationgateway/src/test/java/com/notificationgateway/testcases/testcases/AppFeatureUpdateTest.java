package com.notificationgateway.testcases.testcases;

import com.gigsky.tests.common.database.beans.DBConfigInfo;
import com.gigsky.tests.common.database.utils.DBConfigUtils;
import com.gigsky.tests.dbaccess.NotificationGatewayDB;
import com.gigsky.tests.resthandlers.beans.AppFeature;
import com.gigsky.tests.resthandlers.beans.BaseBean;
import com.gigsky.tests.resthandlers.impl.AppFeatureOperation;
import com.gigsky.tests.resthandlers.utils.APIUtils;
import org.apache.http.HttpStatus;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class AppFeatureUpdateTest  extends GSNotificationGatewayBaseTest{
    APIUtils apiUtils = APIUtils.getInstance();
    NotificationGatewayDB gatewayDB = NotificationGatewayDB.getInstance();
    DBConfigInfo gatewayDBConfigInfo = DBConfigUtils.getDBAccess("gigskyNotificationGateway");
    AppFeatureOperation appFeature = AppFeatureOperation.getInstance();

    public AppFeature appFeatureInfo() {
        AppFeature appInfo = new AppFeature();
        List<String> featureList=Arrays.asList("RateUs");
        appInfo.setType("AppFeatureInfo");
        appInfo.setBaseUrl("https://gigsky.com/api/v5/");
        appInfo.setLogoBaseUrl("https://gigsky.com/logoServer");
        appInfo.setTroubleshootingBaseUrl("https://www.gigsky.com/troubleshooting/api/v1/");
        appInfo.setUpdateMandatory("true");
        appInfo.setFeaturesSupported(featureList);
        return appInfo;
    }
    public AppFeature updateAppInfo() {
        AppFeature appInfo = new AppFeature();
        List<String> featureList=Arrays.asList("RateUs","Like On Facebook");
        appInfo.setType("AppFeatureInfo");
        appInfo.setBaseUrl("https://gigsky.com/api/v5/");
        appInfo.setLogoBaseUrl("https://gigsky.com/logoServer");
        appInfo.setTroubleshootingBaseUrl("https://www.gigsky.com/troubleshooting/api/v1/");
        appInfo.setUpdateMandatory("true");
        appInfo.setFeaturesSupported(featureList);
        return appInfo;
    }
    public AppFeature appFeatureInvalidInfo(String featureInfo) {
        List<String> featureList=Arrays.asList("Feature1");
        List<String> emptyFeatureList=new ArrayList<>();

        AppFeature appInfo=new AppFeature();

        if(featureInfo=="noAppFeatureInfo")//When featureInfo is set to null
            appInfo.setType("");
        else
            appInfo.setType("AppFeatureInfo");

        if(featureInfo=="noLogoBaseUrl")
            appInfo.setLogoBaseUrl("");//When LogoBaseUrl is set to null
        else
            appInfo.setLogoBaseUrl("https://gigsky.com/logoServer");

        if(featureInfo=="noBaseURL")
            appInfo.setBaseUrl("");//When BaseURL is set to null
        else
            appInfo.setBaseUrl("https://gigsky.com/api/v5/");

        if(featureInfo=="noTroubleShootingBaseUrl")
            appInfo.setTroubleshootingBaseUrl("");//When TroubleShootingBaseUrl is set to null
        else
            appInfo.setTroubleshootingBaseUrl("https://www.gigsky.com/troubleshooting/api/v1/");

        if(featureInfo=="mandatoryField")
            appInfo.setUpdateMandatory("false");//When UpdateMandatory field is set to false
        else if(featureInfo=="noMandatoryField")
            appInfo.setUpdateMandatory("");//When UpdateMandatory field is set to null
        else if(featureInfo=="invalidMandatoryField")
            appInfo.setUpdateMandatory("invalid");
        else
            appInfo.setUpdateMandatory("true");

        if(featureInfo=="emptyFeatures")
            appInfo.setFeaturesSupported(emptyFeatureList);//When FeaturesSupported List is empty
        else
            appInfo.setFeaturesSupported(featureList);



        return appInfo;

    }

    @Test(dataProvider = "App_Feature_Expected")
    public void updateAppFeature(BaseBean appInfo,String clientType,String version,String userID,Integer responseCode,BaseBean errorDetails,BaseBean getDeviceErrorDetails,BaseBean updateDeviceErrorDetails) throws Exception {
        appFeature.addAppFeatures(appInfo,clientType,version,userID,responseCode,errorDetails);
        appFeature.updateAppFeatures(updateAppInfo(),clientType,version,userID,responseCode,updateDeviceErrorDetails);
        appFeature.getAppFeatures(updateAppInfo(),clientType,version,userID,responseCode,getDeviceErrorDetails);
        appFeature.deleteAppFeatures(clientType,version,userID,responseCode);
    }


    @DataProvider(name = "App_Feature_Expected")
    public Object[][] createData() throws Exception {
        return new Object[][]{
                //Verify single app feature info can be updated for clientType=IOS and clientVersion=3.3 and userId=tester@gigsky.com with all fields
                {appFeatureInfo(), "IOS", "3.3", "tester1@gigsky.com", HttpStatus.SC_OK, null,null,null},
                //Verify single app feature info can be updated for clientType=ANDROID and clientVersion=3.3 and userId=tester@gigsky.com with all fields
                {appFeatureInfo(),"ANDROID","3.3","tester2@gigsky.com",HttpStatus.SC_OK,null,null,null},
                //Verify single app feature info can be updated for clientType=ANDROID and clientVersion=3.0 and userId=ALL with all fields
                {appFeatureInfo(),"ANDROID","3.0","tester3@gigsky.com",HttpStatus.SC_OK,null,null,null},
                //Verify single app feature info can be updated for clientType=ANDROID and clientVersion=3.7&userId=ALL with all fields
                {appFeatureInfo(),"ANDROID","3.7","ALL",HttpStatus.SC_OK,null,null,null},
                //Verify single app feature info can be updated for clientType=ANDROID and clientVersion=3.7&userId=tester@gigsky.com with all fields
                {appFeatureInfo(),"ANDROID","3.3","tester3@gigsky.com",HttpStatus.SC_OK,null,null,null},
                //Verify single app feature info can be updated for clientType=WEB and clientVersion=3.0 and userId=tester@gigsky.com with all fields
                {appFeatureInfo(),"WEB","3.0","tester4@gigsky.com",HttpStatus.SC_OK,null,null,null},
                //Verify single app feature info can be updated for clientType=CAYMAN and clientVersion=3.0 and userId=tester@gigsky.com with all fields
                {appFeatureInfo(),"CAYMAN","3.0","tester5@gigsky.com",HttpStatus.SC_OK,null,null,null},
                //Verify single app feature info can be updated for clientType=CAYMAN and clientVersion=ALL and userId=betatester@gigsky.com  with all fields
                {appFeatureInfo(),"CAYMAN","ALL","tester6@gigsky.com",HttpStatus.SC_OK,null,null,null},
                {appFeatureInfo(),"WEB","ALL","tester12@gigsky.com",HttpStatus.SC_OK,null,null,null},


        };

    }

    //Verify single app feature info can be updated without all fields (with only type)
    @Test
    public void updateAppFeature1() throws  Exception
    {
        AppFeature appInfo = new AppFeature();
        appInfo.setType("AppFeatureInfo");
        appFeature.addAppFeatures(appFeatureInfo(),"IOS","3.3","tester6@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.updateAppFeatures(appInfo,"IOS","3.3","tester6@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.getAppFeatures(appInfo,"IOS","3.3","tester6@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.deleteAppFeatures("IOS","3.3","tester6@gigsky.com",HttpStatus.SC_OK);
    }

    //Verify single app feature info can be updated without 'featuresSupported' field
    @Test
    public void updateAppFeature2() throws Exception
    {
        AppFeature appInfo = new AppFeature();
        appInfo.setType("AppFeatureInfo");
        appInfo.setBaseUrl("https://gigsky.com/api/v5/");
        appInfo.setLogoBaseUrl("https://gigsky.com/logoServer");
        appInfo.setTroubleshootingBaseUrl("https://www.gigsky.com/troubleshooting/api/v1/");
        appInfo.setUpdateMandatory("true");
        appFeature.addAppFeatures(appFeatureInfo(),"IOS","3.3","tester7@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.updateAppFeatures(appInfo,"IOS","3.3","tester7@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.getAppFeatures(appInfo,"IOS","3.3","tester7@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.deleteAppFeatures("IOS","3.3","tester7@gigsky.com",HttpStatus.SC_OK);
    }
    //Verify single app feature info can be updated without 'baseUrl' field
    @Test
    public void updateAppFeature3() throws Exception
    {
        AppFeature appInfo = new AppFeature();
        List<String> featureList=Arrays.asList("Feature1","Feature2");
        appInfo.setType("AppFeatureInfo");
        appInfo.setLogoBaseUrl("https://gigsky.com/logoServer");
        appInfo.setTroubleshootingBaseUrl("https://www.gigsky.com/troubleshooting/api/v1/");
        appInfo.setUpdateMandatory("true");
        appInfo.setFeaturesSupported(featureList);
        appFeature.addAppFeatures(appFeatureInfo(),"IOS","3.3","tester8@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.updateAppFeatures(appInfo,"IOS","3.3","tester8@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.getAppFeatures(appInfo,"IOS","3.3","tester8@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.deleteAppFeatures("IOS","3.3","tester8@gigsky.com",HttpStatus.SC_OK);
    }
    //Verify single app feature info can be updated without 'logoUrl' field
    @Test
    public void updateAppFeature4() throws Exception
    {
        AppFeature appInfo = new AppFeature();
        List<String> featureList=Arrays.asList("Feature1","Feature2");
        appInfo.setType("AppFeatureInfo");
        appInfo.setBaseUrl("https://gigsky.com/api/v5/");
        appInfo.setTroubleshootingBaseUrl("https://www.gigsky.com/troubleshooting/api/v1/");
        appInfo.setUpdateMandatory("true");
        appInfo.setFeaturesSupported(featureList);
        appFeature.addAppFeatures(appFeatureInfo(),"IOS","3.3","tester9@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.updateAppFeatures(appInfo,"IOS","3.3","tester9@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.getAppFeatures(appInfo,"IOS","3.3","tester9@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.deleteAppFeatures("IOS","3.3","tester9@gigsky.com",HttpStatus.SC_OK);
    }
    //Verify single app feature info can be updated without 'troubleshootingBaseUrl' field
    @Test
    public void updateAppFeature5() throws Exception
    {
        AppFeature appInfo = new AppFeature();
        List<String> featureList=Arrays.asList("Feature1","Feature2");
        appInfo.setType("AppFeatureInfo");
        appInfo.setBaseUrl("https://gigsky.com/api/v5/");
        appInfo.setLogoBaseUrl("https://gigsky.com/logoServer");
        appInfo.setUpdateMandatory("true");
        appInfo.setFeaturesSupported(featureList);
        appFeature.addAppFeatures(appFeatureInfo(),"IOS","3.3","tester10@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.updateAppFeatures(appInfo,"IOS","3.3","tester10@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.getAppFeatures(appInfo,"IOS","3.3","tester10@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.deleteAppFeatures("IOS","3.3","tester10@gigsky.com",HttpStatus.SC_OK);
    }
    //Verify single app feature info can be updated without 'update Mandatory' field
    @Test
    public void updateAppFeature6() throws Exception
    {
        AppFeature appInfo = new AppFeature();
        List<String> featureList=Arrays.asList("Feature1","Feature2");
        appInfo.setType("AppFeatureInfo");
        appInfo.setBaseUrl("https://gigsky.com/api/v5/");
        appInfo.setLogoBaseUrl("https://gigsky.com/logoServer");
        appInfo.setTroubleshootingBaseUrl("https://www.gigsky.com/troubleshooting/api/v1/");
        appInfo.setFeaturesSupported(featureList);
        appFeature.addAppFeatures(appFeatureInfo(),"IOS","3.3","tester11@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.updateAppFeatures(appInfo,"IOS","3.3","tester11@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.getAppFeatures(appInfo,"IOS","3.3","tester11@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.deleteAppFeatures("IOS","3.3","tester11@gigsky.com",HttpStatus.SC_OK);
    }

    //Update app feature info for info which is not present
    @Test
    public void updateAppFeature7() throws Exception {
        AppFeature appInfo = new AppFeature();
        List<String> featureList = Arrays.asList("Feature1", "Feature2");
        appInfo.setType("AppFeatureInfo");
        appInfo.setBaseUrl("https://gigsky.com/api/v5/");
        appInfo.setLogoBaseUrl("https://gigsky.com/logoServer");
        appInfo.setTroubleshootingBaseUrl("https://www.gigsky.com/troubleshooting/api/v1/");
        appInfo.setFeaturesSupported(featureList);
        appInfo.setUpdateMandatory("true");
        appFeature.addAppFeatures(appFeatureInfo(),"IOS","3.3","tester12@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.updateAppFeatures(appInfo,"ANDROID","3.3","tester12@gigsky.com",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(11600, "App feature info entry not found"));
        appFeature.deleteAppFeatures("IOS","3.3","tester12@gigsky.com",HttpStatus.SC_OK);
        }


    @Test(dataProvider = "App_Error_Features")
    public void updateAppFeature8(BaseBean appInfo) throws Exception {

        appFeature.addAppFeatures(appFeatureInfo(),"IOS","3.3.1","tester16@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.updateAppFeatures(appInfo,"IOS","3.3.1","tester16@gigsky.com",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(11005, "Invalid arguments"));
        appFeature.deleteAppFeatures("IOS","3.3.1","tester16@gigsky.com",HttpStatus.SC_OK);


    }

    @DataProvider(name = "App_Error_Features")
    public Object[][] createErrorData() throws Exception{
        return new Object[][]{
                    //Update app feature with empty feature list
                    {appFeatureInvalidInfo("emptyFeatures")},
                    //Update app feature with empty baseurl field
                    {appFeatureInvalidInfo("noBaseURL")},
                    //Update feature with empty logobaseurl field
                    {appFeatureInvalidInfo("noLogoBaseUrl")},
                    //Update app feature with empty troubleshoot base url field
                    {appFeatureInvalidInfo("noTroubleShootingBaseUrl")},
                    //Update app feature with empty update mandatory field
                    {appFeatureInvalidInfo("noMandatoryField")},
                    //Update app feature with Invalid update mandatory list
                    {appFeatureInvalidInfo("invalidMandatoryField")}

            };
    }




    @Test(dataProvider = "createInvalidQueryParameterData")
    public void updateAppFeature8(String clientType,String version,String userId,BaseBean errorDetails) throws Exception {

        appFeature.addAppFeatures(appFeatureInfo(),"IOS","3.3.1","tester17@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.updateAppFeatures(updateAppInfo(),clientType,version,userId,HttpStatus.SC_BAD_REQUEST,errorDetails);
        appFeature.deleteAppFeatures("IOS","3.3.1","tester17@gigsky.com",HttpStatus.SC_OK);


    }
    @DataProvider(name = "Invalid_Query_Parameters")
    public Object[][] createInvalidQueryParameterData() throws Exception{
        return new Object[][]{
                //Update app feature with Invalid client type ios
                {"iOS","3.3","tester17@gigsky.com",apiUtils.getExpectedErrorResponse(11602, "App feature info requires valid client type")},
                //Update app feature with Invalid client type android
                {"android","3.3","tester17@gigsky.com",apiUtils.getExpectedErrorResponse(11602, "App feature info requires valid client type")},
                //Update app feature without client type
                {null,"3.3","tester17@gigsky.com",apiUtils.getExpectedErrorResponse(11602, "App feature info requires valid client type")},
                //Update app feature without client version
                {"ANDROID",null,"tester17@gigsky.com",apiUtils.getExpectedErrorResponse(11603, "App feature info requires valid client version")},
                //Update app feature without user id
                {"ANDROID","3.3",null,apiUtils.getExpectedErrorResponse(11604,"App feature info requires valid user id")}
        };
        }


  //Verify featuresSupported list cant be added without providing type = AppFeatureInfo
    @Test
    public void updateAppFeature8() throws Exception {
        AppFeature appInfo = new AppFeature();
        List<String> featureList = Arrays.asList("Feature1", "Feature2");
        appInfo.setBaseUrl("https://gigsky.com/api/v5/");
        appInfo.setLogoBaseUrl("https://gigsky.com/logoServer");
        appInfo.setTroubleshootingBaseUrl("https://www.gigsky.com/troubleshooting/api/v1/");
        appInfo.setFeaturesSupported(featureList);
        appInfo.setUpdateMandatory("true");
        appFeature.addAppFeatures(appFeatureInfo(),"IOS","3.3","tester13@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.updateAppFeatures(appInfo,"ANDROID","3.3","tester13@gigsky.com",HttpStatus.SC_BAD_REQUEST,apiUtils.getExpectedErrorResponse(11005, "Invalid arguments"));
        appFeature.deleteAppFeatures("IOS","3.3","tester13@gigsky.com",HttpStatus.SC_OK);

    }

    //Verify Add App feature info can be updated for clientVersion=0
    @Test
    public void updateAppFeature9() throws Exception {
        appFeature.addAppFeatures(appFeatureInfo(),"CAYMAN","0","tester14@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.updateAppFeatures(updateAppInfo(),"CAYMAN","0","tester14@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.getAppFeatures(updateAppInfo(),"CAYMAN","0","tester14@gigsky.com",HttpStatus.SC_OK,null);
        appFeature.deleteAppFeatures("CAYMAN","0","tester14@gigsky.com",HttpStatus.SC_OK);


    }







}
