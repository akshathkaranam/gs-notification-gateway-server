package com.notificationgateway.testcases.testcases;

import com.gigsky.tests.common.database.beans.DBConfigInfo;
import com.gigsky.tests.common.database.utils.DBConfigUtils;
import com.gigsky.tests.dbaccess.NotificationGatewayDB;
import com.gigsky.tests.resthandlers.beans.ErrorDetails;
import com.gigsky.tests.resthandlers.beans.PartnerApp;
import com.gigsky.tests.resthandlers.impl.PartnerAppOperation;
import com.gigsky.tests.resthandlers.utils.APIUtils;
import org.apache.http.HttpStatus;
import org.junit.After;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertNotEquals;

public class PartnerAppTest extends GSNotificationGatewayBaseTest {
    APIUtils apiUtils = APIUtils.getInstance();
    NotificationGatewayDB gatewayDB = NotificationGatewayDB.getInstance();
    DBConfigInfo gatewayDBConfigInfo = DBConfigUtils.getDBAccess("gigskyNotificationGateway");
    PartnerAppOperation partnerapp=PartnerAppOperation.getInstance();
    NotificationGatewayDB notificationdb = NotificationGatewayDB.getInstance();

    @After
    public void revertPartnerAppExpiry() throws Exception {
        notificationdb.revert_PartnerApp_Expiry_Time(gatewayDBConfigInfo);
    }

    @Test
    //Verify new device can be registered by PartnerAPI
    public  void partnerAppTest1() throws Exception {
        partnerapp.registerDevice("IOS","10.0.1.2","9.1.1","en","8910300000000006173","IN","1234","1.0",HttpStatus.SC_TEMPORARY_REDIRECT,null);
        PartnerApp response=apiUtils.getPartnerAppGetDeviceResponse("IOS","10.0.1.2","9.1.1","en","8910300000000006173","IN","1234","1.0");
        partnerapp.getDevice(response,"IOS","10.0.1.2","9.1.1","en",HttpStatus.SC_OK,null);
    }
    @Test(dataProvider ="invalid_Fields")
    //Without Mandatory Fields
    public  void partnerAppTest2(String deviceType, String ipAddress, String deviceOSVersion, String locale, String iccId, String location, String partnerCode, String sdkVersion, ErrorDetails errorDetails) throws Exception{
        partnerapp.registerDevice(deviceType,ipAddress,deviceOSVersion,locale,iccId,location,partnerCode,sdkVersion,HttpStatus.SC_BAD_REQUEST,errorDetails);
        if(partnerCode!=null && sdkVersion!=null)
        partnerapp.getDevice(null,deviceType,ipAddress,deviceOSVersion,locale,HttpStatus.SC_BAD_REQUEST,errorDetails);
    }

    @DataProvider(name ="invalid_Fields")
    public Object[][] invalidFieldData()
    {
        return new Object[][]{
                //Verify a new device can not be registered without DeviceType
                {null,"10.0.1.2","9.1.1","en","8910300000000006173","IN","1234","1.0",apiUtils.getExpectedErrorResponse(11501,"Partner app device registration requires valid device type")},
                //Verify a new device can not be registered without IP
                {"IOS",null,"9.1.1","en","8910300000000006173","IN","1234","1.0",apiUtils.getExpectedErrorResponse(11502,"Partner app device registration requires valid IP address")},
                //Verify a new device can not be registered without locale
                {"IOS","10.0.1.2","9.1.1",null,"8910300000000006173","IN","1234","1.0",apiUtils.getExpectedErrorResponse(11503,"Partner app device registration requires valid locale")},
                //Verify a new device can not be registered without device OS version
                {"IOS","10.0.1.2",null,"en","8910300000000006173","IN","1234","1.0",apiUtils.getExpectedErrorResponse(11504,"Partner app device registration requires valid device OS version")},
                //Verify a new device can not be registered without partnerCode
                {"IOS","10.0.1.2","9.1.1","en","8910300000000006173","IN",null,"1.0",apiUtils.getExpectedErrorResponse(11508,"Partner app device registration requires valid partner code")},
                //Verify a new device can not be registered without SDK version
                {"IOS","10.0.1.2","9.1.1","en","8910300000000006173","IN","1234",null,apiUtils.getExpectedErrorResponse(11511,"Partner app device registration requires valid sdk version")},
                //Verify a new device can not be registered by providing unsupported Device Type
                {"WIN","10.0.1.2","9.1.1","en","8910300000000006173","IN","1234",null,apiUtils.getExpectedErrorResponse(11501,"Partner app device registration requires valid device type")},


        };
    }
  @Test
  public  void partnerAppTest3() throws Exception {
      //Verify new device can be registered without ICCID
      partnerapp.registerDevice("IOS","10.0.1.2","9.1.1","en",null,"IN","1234","1.0",HttpStatus.SC_TEMPORARY_REDIRECT,null);
      PartnerApp responseWithoutICCId=apiUtils.getPartnerAppGetDeviceResponse("IOS","10.0.1.2","9.1.1","en",null,"IN","1234","1.0");
      partnerapp.getDevice(responseWithoutICCId,"IOS","10.0.1.2","9.1.1","en",HttpStatus.SC_OK,null);

      //Verify new device can be registered without Location
      partnerapp.registerDevice("IOS","10.0.1.2","9.1.1","en","8910300000000006173",null,"1234","1.0",HttpStatus.SC_TEMPORARY_REDIRECT,null);
      PartnerApp responseWithoutLocation=apiUtils.getPartnerAppGetDeviceResponse("IOS","10.0.1.2","9.1.1","en","8910300000000006173",null,"1234","1.0");
      partnerapp.getDevice(responseWithoutLocation,"IOS","10.0.1.2","9.1.1","en",HttpStatus.SC_OK,null);

      //Verify new device can be registered without ICCID and Location
      partnerapp.registerDevice("IOS","10.0.1.2","9.1.1","en",null,null,"1234","1.0",HttpStatus.SC_TEMPORARY_REDIRECT,null);
      PartnerApp response=apiUtils.getPartnerAppGetDeviceResponse("IOS","10.0.1.2","9.1.1","en",null,null,"1234","1.0");
      partnerapp.getDevice(response,"IOS","10.0.1.2","9.1.1","en",HttpStatus.SC_OK,null);
      }

      //Verify new device will be registered when OS version, locale, or IP, is changed
    @Test(dataProvider = "New_Device_Is Created")
    public  void partnerAppTest4(String locale,String ipAddress,String osVersion) throws Exception {
        partnerapp.registerDevice("IOS", "10.0.1.2", "9.1.1", "en", "8910300000000006173","IN" ,"1234","1.0", HttpStatus.SC_TEMPORARY_REDIRECT, null);
        PartnerApp response = apiUtils.getPartnerAppGetDeviceResponse("IOS", "10.0.1.2", "9.1.1", "en", "8910300000000006173","IN" ,"1234", "1.0");
        partnerapp.getDevice(response, "IOS", "10.0.1.2", "9.1.1", "en", HttpStatus.SC_OK, null);

        partnerapp.registerDevice("IOS", ipAddress, osVersion, locale, "8910300000000006173","IN" , "1234", "1.0", HttpStatus.SC_TEMPORARY_REDIRECT, null);
        PartnerApp response1 = apiUtils.getPartnerAppGetDeviceResponse("IOS", ipAddress, osVersion, locale, "8910300000000006173", "IN", "1234", "1.0");
        partnerapp.getDevice(response1, "IOS", ipAddress, osVersion, locale, HttpStatus.SC_OK, null);

    }
    @DataProvider(name = "New_Device_Is Created")
    public Object[][] changedField() {
        return new Object[][]{
                //Add same device with different locale and it should be treated as new device
                {"ja","10.0.1.2","9.1.1"},
                //Add same device with different IP and it should be treated as new device
                {"en","10.0.111.2","9.1.1"},
                //Add same device with different Device OS and it should be treated as new device
                {"en","10.0.1.2","9.3"}

        };
    }
    //Verify same device can not be added twice in a same day
    @Test
    public  void partnerAppTest5() throws Exception {
        partnerapp.registerDevice("ANDROID", "10.0.15.2", "5.5.1", "de", "8910300000000006173","US" ,"1234","1.0", HttpStatus.SC_TEMPORARY_REDIRECT, null);
        PartnerApp response = apiUtils.getPartnerAppGetDeviceResponse("ANDROID", "10.0.15.2", "5.5.1", "de", "8910300000000006173","US" ,"1234","1.0");
        partnerapp.getDevice(response, "ANDROID", "10.0.15.2", "5.5.1", "de", HttpStatus.SC_OK, null);
        //Add the same device second time and error "Device Exists" should be returned
        partnerapp.registerDevice("ANDROID", "10.0.15.2", "5.5.1", "de", "8910300000000006173","US" ,"1234","1.0", HttpStatus.SC_TEMPORARY_REDIRECT, null);


    }
    //Verify existing device will be updated when Location, SDK version, partner Code, and ICCID gets changed
    @Test(dataProvider = "Exisisting_Device")
    public  void partnerAppTest6(String iccid,String location,String sdkVersion,String partnerAppCode) throws Exception {
        partnerapp.registerDevice("IOS", "10.0.1.2", "9.1.1", "en", "8910300000000006173","IN" ,"1234","1.0", HttpStatus.SC_TEMPORARY_REDIRECT, null);
        PartnerApp response = apiUtils.getPartnerAppGetDeviceResponse("IOS", "10.0.1.2", "9.1.1", "en", "8910300000000006173","IN" ,"1234", "1.0");
        partnerapp.getDevice(response, "IOS", "10.0.1.2", "9.1.1", "en", HttpStatus.SC_OK, null);

        partnerapp.registerDevice("IOS", "10.0.1.2", "9.1.1", "en", iccid,location,partnerAppCode,sdkVersion, HttpStatus.SC_TEMPORARY_REDIRECT, null);
        PartnerApp responseAfterAdding = apiUtils.getPartnerAppGetDeviceResponse("IOS", "10.0.1.2", "9.1.1", "en", iccid,location ,partnerAppCode, sdkVersion);
        partnerapp.getDevice(responseAfterAdding, "IOS", "10.0.1.2", "9.1.1", "en", HttpStatus.SC_OK, null);


    }
    @DataProvider(name = "Exisisting_Device")
    public Object[][] Different_Fileds_Update_Exisisting_Device() {
        return new Object[][]{

                //Add same device with different ICCID and different ICCID should not be treated as same device
                {"8910300000000001003", "IN", "1.0", "1234"},
                //Add same device with different Location, different Location should not be treated as same device
                {"8910300000000006173", "JP", "1.0", "1234"},
                //Add same device with different SDK
                {"8910300000000006173", "IN", "2.0.1", "1234"},
                //Add same device with different Partner and different Partner code should not be treated as different device
                {"8910300000000006173", "IN", "1.0", "A1234AB"},
                //Add same device with different ICCID, location, SDK and different Partner code should not be treated as different device
                {"8910300000000001003", "JP", "1.0", "A1234AB"}

        };
    }

    //Verify new iOS device will be deleted after Expiry period
    @Test
public void partnerAppTest7() throws Exception {
        notificationdb.update_PartnerApp_Expiry_Time(gatewayDBConfigInfo);
        partnerapp.registerDevice("IOS", "10.0.1.2", "9.1.1", "en", "8910300000000006173","IN" ,"1234","1.0", HttpStatus.SC_TEMPORARY_REDIRECT, null);
        PartnerApp response = apiUtils.getPartnerAppGetDeviceResponse("IOS", "10.0.1.2", "9.1.1", "en", "8910300000000006173","IN" ,"1234", "1.0");
        PartnerApp device=(PartnerApp) partnerapp.getDevice(response, "IOS", "10.0.1.2", "9.1.1", "en", HttpStatus.SC_OK, null);
        Thread.sleep(60000);
        partnerapp.getDevice(null, "IOS", "10.0.1.2", "9.1.1", "en", HttpStatus.SC_OK, apiUtils.getExpectedErrorResponse(11510,"Partner app device details not found"));
        Thread.sleep(60000);
        //Verify after 60 seconds device is deleted from database
        assertNotEquals(notificationdb.partnerAppDevice(gatewayDBConfigInfo,device.getIpAddress()).equals(device.getIccId()),"Device is Present in Table");
        partnerapp.registerDevice("ANDROID", "10.0.1.2", "9.1.1", "en", "8910300000000006173","IN" ,"1234","1.0", HttpStatus.SC_TEMPORARY_REDIRECT, null);
        PartnerApp responseAndroid = apiUtils.getPartnerAppGetDeviceResponse("ANDROID", "10.0.1.2", "9.1.1", "en", "8910300000000006173","IN" ,"1234", "1.0");
        PartnerApp deviceAndroid=(PartnerApp) partnerapp.getDevice(responseAndroid, "ANDROID", "10.0.1.2", "9.1.1", "en", HttpStatus.SC_OK, null);
        Thread.sleep(60000);
        partnerapp.getDevice(null, "ANDROID", "10.0.1.2", "9.1.1", "en", HttpStatus.SC_OK, apiUtils.getExpectedErrorResponse(11510,"Partner app device details not found"));
        Thread.sleep(60000);
         assertNotEquals(notificationdb.partnerAppDevice(gatewayDBConfigInfo,deviceAndroid.getIpAddress()).equals(deviceAndroid.getIccId()),"Device is Present in Table");



    }





}
