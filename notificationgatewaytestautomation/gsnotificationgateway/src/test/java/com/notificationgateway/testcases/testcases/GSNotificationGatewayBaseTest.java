package com.notificationgateway.testcases.testcases;

import com.gigsky.tests.resthandlers.impl.GatewayAPIConfigurator;
import org.testng.annotations.BeforeClass;

public class GSNotificationGatewayBaseTest {
    @BeforeClass
    public static void setUp(){
        GatewayAPIConfigurator.setTenantID("1");
    }
}
