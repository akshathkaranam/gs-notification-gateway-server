#!/bin/sh
echo `ps -p $$`
ls -l;
pwd;
who am i;


echo catalina home: $CATALINA_HOME;
echo catalina base: $CATALINA_BASE;

echo $PATH;
echo $WORKSPACE;


echo "Jenkins start time=`date`";

sudo sed -i  "s/compression\=\"on\"/ /g" /var/lib/tomcat8/conf/server.xml
sudo sed -i  "s/compressionMinSize=\"2048\"/ /g" /var/lib/tomcat8/conf/server.xml
sudo sed -i  "s/noCompressionUserAgents\=\"gozilla, traviata\"/ /g" /var/lib/tomcat8/conf/server.xml
sudo sed -i  "s/compressableMimeType\=\"text\/html,text\/xml,application\/xml,application\/json\"/ /g" /var/lib/tomcat8/conf/server.xml
sudo sed -i  "s/port\=\"8080\"/port\=\"8080\" compression\=\"on\"  compressionMinSize\=\"2048\"  noCompressionUserAgents\=\"gozilla, traviata\" compressableMimeType\=\"text\/html,text\/xml,application\/xml,application\/json\" /" /var/lib/tomcat8/conf/server.xml

echo
echo "Jenkins: Enabling gzip compression on Tomcat server."


## enabling jacoco agent
#sudo sed -i "s/JAVA_OPTS='-Xmx2048m'/JAVA_OPTS='-Xmx2048m -javaagent:\/home\/jenkins\/org.jacoco.agent-0.7.4.201502262128-runtime.jar=destfile=\/var\/lib\/tomcat8\/logs\/jacoco.exec,append=true'/g" /usr/share/tomcat8/bin/catalina.sh
#cp $WORKSPACE/lib/org.jacoco.agent-0.7.4.201502262128-runtime.jar /home/jenkins

#### TOMCAT
echo "Jenkins: Shutting down Tomcat"
sudo /etc/init.d/tomcat8 stop
sudo rm -f /var/lib/tomcat8/logs/*
sudo rm -rf /var/lib/tomcat8/webapps/*
#To kill tomcat
echo $SUDO_PASSWORD | sudo -S sudo pkill -9 -f tomcat8



#### gsnotificationgateway artifacts
cd $WORKSPACE/gsnotificationgateway-artifacts
sudo rm -rf Tomcat
sudo rm -rf db_schemas
sudo rm -rf db_utils
sudo rm -rf db_configuration
tar -zxf Tomcat.tar.gz
tar -zxf db_schemas.tar.gz
tar -zxf db_utils.tar.gz
tar -zxf db_configurations.tar.gz



#=========== BACKEND DATABASE INIT ===============
echo "Jenkins: Upgrading gsnotification Database schema"
cd $WORKSPACE/gsnotificationgateway-artifacts/db_schemas/
DBVersion=$($WORKSPACE/gsnotificationgateway-artifacts/db_utils/findLatestDBVersion.sh -dgigskyNotificationGateway -uroot -proot)
echo DBVersion:$DBVersion;
export PATH=$PATH:/usr/local/mysql/bin
sudo $WORKSPACE/gsnotificationgateway-artifacts/db_utils/deleteLocalDatabase.sh -dgigskyNotificationGateway -uroot -proot
sudo $WORKSPACE/gsnotificationgateway-artifacts/db_utils/upgradeGigskyDatabase.sh -v$DBVersion -dgigskyNotificationGateway -uroot -proot
sudo rm -rf $WORKSPACE/gsnotificationgateway-artifacts/db_schemas/snapshot
echo "Jenkins: GS Notification gateway Server database upgraded"




#=========== TOMCAT INIT ===============
#echo $SUDO_PASSWORD | sudo -S cp -a $WORKSPACE/gsnotificationgateway-artifacts/Tomcat/lib/* /usr/share/tomcat7/lib/
sudo cp -a $WORKSPACE/gsnotificationgateway-artifacts/db_configurations/dev/notificationgateway_db.properties /var/lib/tomcat8/conf/

sudo cp -a $WORKSPACE/gsnotificationgateway-artifacts/gsngw.war /var/lib/tomcat8/webapps/

#=======Deleting old jars from lib========
sudo rm -rf /usr/share/tomcat8/lib/httpclient-4.2.1.jar
sudo rm -rf /usr/share/tomcat8/lib/httpcore-4.2.1.jar

sudo /etc/init.d/tomcat8 start
echo "Jenkins: Deployed gsngw war"

cd $WORKSPACE/build
echo "listing the contents of build/"
sudo ls -ltra

#sleep 30

echo "Before Init test environment="; date; echo "";

cd $WORKSPACE/
echo "Copying testng common jar file";
sudo rm -rf $WORKSPACE/lib/gstestngcommon*.jar
cp -a $WORKSPACE/gstestngcommon/target/gstestngcommon-1.0-SNAPSHOT.jar $WORKSPACE/lib/
cp -a $WORKSPACE/build/gscommon-1.0-SNAPSHOT-jar-with-dependencies.jar $WORKSPACE/lib/gscommon-1.0-SNAPSHOT.jar
#cp -a $WORKSPACE/build/gscommon-1.0-SNAPSHOT.jar $WORKSPACE/lib/
ls -la

cd $WORKSPACE
echo "listing the contents of lib/"
sudo ls -ltra lib/

cd $WORKSPACE
echo "list contents of workspace"
ls -la

mvn install -DskipTests

echo "Jenkins: Done with mvn clean install of gstestngcommon dependencies ";




