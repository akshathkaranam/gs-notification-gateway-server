package com.gigsky.ngw.gsmeter.sampler;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Random;
import java.util.TimeZone;
import java.util.UUID;

import org.apache.jmeter.protocol.java.sampler.JavaSamplerContext;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.log4j.Logger;

import com.gigsky.abi.GSConfig;
import com.gigsky.abi.GSFactory;
import com.gigsky.abi.GSUtil;
import com.gigsky.abi.LogUtil;
import com.gigsky.ngw.bean.EventResponse;
import com.gigsky.ngw.data.EventInput;
import com.gigsky.ngw.handler.EventHandler;
import com.gigsky.ngw.util.NGWUtil;



public class GSEventsSampler extends org.apache.jmeter.protocol.java.sampler.AbstractJavaSamplerClient
implements java.io.Serializable{
	

    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(GSEventsSampler.class);
    private static final int EXPIRY_PERIOD = 2 * 60 * 60 * 1000; // in msecs
    
    @Override
    public void setupTest(JavaSamplerContext context) {
        String configFilePath =
            context.getParameter("gigsky-loadtest.configFilePath");
        try {
            
        	Properties properties = LogUtil.configureLogging(configFilePath);

            if (properties != null) {
                GSConfig.init(properties);
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
	@Override
	public SampleResult runTest(JavaSamplerContext arg0) {
		
		final SampleResult retVal = new SampleResult();
		log.info("starting test");
		EventHandler eventHandler =
	            GSFactory.get(EventHandler.class, 0);
		log.info("total user count " + GSConfig.getNgwUserCount());
		log.info("NGW URL preix " + GSConfig.getNgwUrlPrefix());
		
		// generate random number and get user code...
		int randomNumber = new Random().nextInt(GSConfig.getNgwUserCount());
		log.info("random number " + randomNumber);
		
		String userRegId = NGWUtil.getUserRegistrationId(randomNumber); 
		String simIccid = NGWUtil.getIccid(randomNumber);
	    // generate random number for event type.
		String eventType = getEventType(randomNumber);
		
		log.info("user id " + userRegId);
		log.info("SIM iccid " + simIccid + " event type : " + eventType);
		log.info(" is UUID valid " + isUUID(userRegId));
		
		// call create event api.
		Map<String, Object> options = new HashMap<String, Object>();
		options.put("simName", "Test SIM");
		
		EventInput eventInput = new EventInput();
		eventInput.setCreationTime(getCreationTime());
		eventInput.setEventType(eventType);
		eventInput.setCustomerId(userRegId);
		
		eventInput.setIccId(simIccid);
		eventInput.setLocation(getLocation(randomNumber));
		eventInput.setOptions(options);
		eventInput.setUserId("test@gigsky.com");
		
		if(eventInput.getEventType().equalsIgnoreCase("LOW_BALANCE") || eventInput.getEventType().equalsIgnoreCase("SUBSCRIPTION_NEARING_EXPIRY"))
		{
			eventInput.setBalanceRemaining(10);
		}
		else if(eventInput.getEventType().equalsIgnoreCase("DATA_USED")){
			eventInput.setPercentageConsumed(50);
		}
		
		if(!eventInput.getEventType().equalsIgnoreCase("NEW_LOCATION")){
			eventInput.setExpiry(getExpiryTime());
		}
		
		
		log.info("Creation time " + eventInput.getCreationTime());
		log.info("Expiry time " + eventInput.getExpiry());
		log.info("Location : " + eventInput.getLocation());
		log.info("Options :" + eventInput.getOptions());
		
		try {
			log.info("calling API: ");
			EventResponse response = eventHandler.createEvent(retVal, eventInput);
			log.info("API call successful. Response : " + response.getEventId());
			retVal.setSuccessful(true);
		} catch (Exception e) {
			log.info("Exception : " +  GSUtil.toString(e));
            retVal.setSuccessful(false);
            retVal.setResponseCode("Error");
            retVal.setResponseMessage("Error "  + GSUtil.toString(e));
        } finally {
            SampleResult[] results = retVal.getSubResults();
            log.info(
                retVal.isSuccessful() + ", " + results.length + " total, " +
                NGWUtil.successfulCount(results) + " succeeded");
        }

		return retVal;
	}

	private boolean isUUID(String string) {
		try {
			UUID.fromString(string);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}
	
	private String getLocation(int randomNumber) {
		String[] locations = {"US","IN", "GB","AU","AR","AM","AT","AZ","BE","FR"};
		int index = randomNumber % locations.length;
		return locations[index];
	}

	private String getExpiryTime() {
		
		// assume 6 hrs for expiry.
		SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
		Date currentTime = new Date();
		currentTime.setTime(currentTime.getTime() + EXPIRY_PERIOD );
		return dateFormatGmt.format(currentTime);
	}

	private String getCreationTime() {
		SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		dateFormatGmt.setTimeZone(TimeZone.getTimeZone("GMT"));
		return dateFormatGmt.format(new Date());
		
	}

	private String getEventType(int randomNumber) {
		String[] eventTypes = {"LOW_BALANCE","SUBSCRIPTION_NEARING_EXPIRY", "NEW_LOCATION","SUBSCRIPTION_EXPIRED","DATA_USED"};
		int index = randomNumber % eventTypes.length;
		return eventTypes[index];

	}    
}
