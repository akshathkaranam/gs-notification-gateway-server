package com.gigsky.abi.handler;

import com.gigsky.abi.GSException;
import com.gigsky.accounts.v4.beans.SimLocationHistoryV4;
import com.gigsky.data.LocationInput;
import org.apache.jmeter.samplers.SampleResult;

/**
 * @author drvijay
 */
public interface LocationHandler extends GSHandler
{

    public SimLocationHistoryV4 getLocationsOfAllSubscribers ( SampleResult locationsOfAllSubscribers, String customerId, String token, LocationInput locationInput ) throws GSException;

    public SimLocationHistoryV4 getLocationsOfASubscriber ( SampleResult locationsOfASubscriber, String customerId, String token, LocationInput locationInput ) throws GSException;

    public SimLocationHistoryV4 getCallDataRecordsOfAllSubscribers ( SampleResult callDataRecordsOfAllSubscribers, String customerId, String token, LocationInput locationInput ) throws GSException;

    public SimLocationHistoryV4 getCallDataRecordsOfASubscriber ( SampleResult callDataRecordsOfASubscriber, String customerId, String token, LocationInput locationInput ) throws GSException;

}
