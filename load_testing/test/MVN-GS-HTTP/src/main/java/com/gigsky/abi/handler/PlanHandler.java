package com.gigsky.abi.handler;

import com.gigsky.abi.GSException;
import com.gigsky.accounts.beans.GlobalPlanSearchResponseBean;
import com.gigsky.accounts.v4.beans.*;
import com.gigsky.data.PlanInput;
import org.apache.jmeter.samplers.SampleResult;

public interface PlanHandler extends GSHandler {
    public GlobalPlanSearchResponseBean searchPlans(SampleResult sampleResult,
                                                    String keyword)
        throws GSException;
    public CountryListV4 getCountries(SampleResult sampleResult)
        throws GSException;
    public ForbiddenCountryListV4 getForbiddenCountries(SampleResult sampleResult)
            throws GSException;
    public CountryListV4 getCountriesWithNetworkGroupInfo(SampleResult sampleResult)
            throws GSException;
    public NetworkGroupSummaryList getNetworkGroupsForCountry(
        SampleResult sampleResult, String countryCode) throws GSException;
    public NetworkGroupDetails getNetworkGroupDetails(SampleResult sampleResult,
                                                      long networkGroupId)
        throws GSException;
    public NetworkGroupPlans getNetworkGroupPlans(SampleResult sampleResult,
                                                  long networkGroupId)
        throws GSException;
    public NetworkGroupPlans getNetworkGroupPlansExt(SampleResult getPlans, long id, String code)
            throws GSException;
    public TransactionV4 buyPlan(SampleResult sampleResult,
                                 String customerId,
                                 String simId,
                                 PlanInput planInput) throws GSException;
}
