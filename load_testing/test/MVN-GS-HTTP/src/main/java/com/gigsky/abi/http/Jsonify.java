package com.gigsky.abi.http;

import java.io.IOException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.DeserializationConfig.Feature;

public class Jsonify {
    public static String toJson(Object obj) throws IOException {
        String retVal = null;

        if (obj != null) {
            if (obj instanceof String) {
                retVal = (String) obj;
            } else {
                retVal = new Serializer().toJson(obj);
            }
        }

        return retVal;
    }
    
    public static <T> T fromJson(String json, Class<T> type)
        throws IOException {
        return createMapper().readValue(json, type);
    }
    
    private static ObjectMapper createMapper() {
        ObjectMapper retVal = new ObjectMapper();

        retVal.configure(Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        return retVal;
    }
}
