package com.gigsky.abi.handler.impl;

import com.gigsky.abi.GSException;
import com.gigsky.abi.handler.EventHandler;
import com.gigsky.abi.http.ServerType;
import com.gigsky.restapi.bean.CallDataRecords;
import com.gigsky.restapi.bean.Locations;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.log4j.Logger;

public class EventHandlerImpl extends HandlerBase implements EventHandler {
    private static final Logger log = Logger.getLogger(EventHandlerImpl.class);

    public EventHandlerImpl(int userNumber) {
        super(userNumber);
    }
    
    @Override
    public Locations getLocations(SampleResult sampleResult)
        throws GSException {
        sampleResult.setSampleLabel(LOCATIONS);
        return sendGet(sampleResult, false, LOCATIONS, Locations.class);
    }
    
    @Override
    public CallDataRecords getCallDataRecords(SampleResult sampleResult)
        throws GSException {
        sampleResult.setSampleLabel(CALL_DATA_RECORDS);
        return sendGet(sampleResult,
                       false,
                       CALL_DATA_RECORDS,
                       CallDataRecords.class);
    }

    @Override
    protected ServerType getServerType() {
        return ServerType.EVENT;
    }
}
