package com.gigsky.abi.handler.impl;

import com.gigsky.abi.GSConstants;
import com.gigsky.abi.GSException;
import com.gigsky.abi.handler.SimHandler;
import com.gigsky.abi.http.NVPair;
import com.gigsky.abi.http.ServerType;
import com.gigsky.accounts.v4.beans.*;
import com.gigsky.data.AddSimInput;
import org.apache.commons.lang3.StringUtils;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.log4j.Logger;

import java.text.MessageFormat;

public class SimHandlerImpl extends HandlerBase implements SimHandler {
    private static final Logger log = Logger.getLogger(SimHandlerImpl.class);

    public SimHandlerImpl(int userNumber) {
        super(userNumber);
    }
    
    @Override
    public SimDetailsV4 getSims(SampleResult sampleResult,
                                String customerId,
                                int startIndex,
                                int count) throws GSException {
        String endpoint = MessageFormat.format(GET_SIMS, customerId);

        sampleResult.setSampleLabel(GET_SIMS);
        return sendGet(sampleResult,
                       false,
                       endpoint,
                       SimDetailsV4.class,
                       NVPair.from(START_INDEX, startIndex),
                       NVPair.from(COUNT, count));
    }
    
    @Override
    public SimV4 getSim(SampleResult sampleResult,
                        String customerId,
                        String simId) throws GSException {
        String endpoint = MessageFormat.format(GET_SIM, customerId, simId);
        sampleResult.setSampleLabel(GET_SIM);

        return sendGet(sampleResult, false, endpoint, SimV4.class);
    }

    @Override
    public void addSim(SampleResult sampleResult,
                       String customerId,
                       AddSimInput input)
        throws GSException {
        String endpoint = MessageFormat.format(ADD_SIM, customerId);
        sampleResult.setSampleLabel(ADD_SIM);
        sendPost(sampleResult, false, endpoint, input, null);
    }

    @Override
    public SubscriptionsList getSubscriptionsOfSim(SampleResult sampleResult,
                                                   String customerId,
                                                   String simId,
                                                   String type) throws GSException {
        sampleResult.setSampleLabel("getSubscriptionsOfSim");
        String subscriptionType = (StringUtils.isNotEmpty(type)) ? type : "ALL";
        String endpoint = MessageFormat.format(GSConstants.GET_SUBSCRIPTIONS, customerId, simId, subscriptionType);
        sampleResult.setSampleLabel(GET_SUBSCRIPTIONS);
        SubscriptionsList subscriptions = sendGet(sampleResult, false, endpoint, SubscriptionsList.class);
        return subscriptions;
    }

    @Override
    public SubscriptionsV4 getSubscriptionsOfSimForNetworkGroup(SampleResult sampleResult,
                                                                String customerId,
                                                                String simId,
                                                                String networkGroupId,
                                                                String type) throws GSException {
        sampleResult.setSampleLabel("getSubscriptionsOfSim");
        String subscriptionType = (StringUtils.isNotEmpty(type)) ? type : "ALL";
        String endpoint = MessageFormat.format(GSConstants.GET_SUBSCRIPTION_BY_NWID, customerId, simId, networkGroupId);
        sampleResult.setSampleLabel(GET_SUBSCRIPTION_BY_NWID);
        SubscriptionsV4 subscription = sendGet(sampleResult, false, endpoint, SubscriptionsV4.class);
        return subscription;
    }

    @Override
    public SubscriptionsV4 getSubscriptionOfSimById(SampleResult sampleResult,
                                                    String customerId,
                                                    String simId,
                                                    long subscriptionId) throws GSException {
        sampleResult.setSampleLabel("getSubscriptionOfSimById");
        String endpoint = MessageFormat.format(GET_SUBSCRIPTION_BY_ID, customerId, simId, String.valueOf(subscriptionId));
        sampleResult.setSampleLabel(GET_SUBSCRIPTION_BY_ID);
        SubscriptionsV4 subscription = sendGet(sampleResult, false, endpoint, SubscriptionsV4.class);
        return subscription;
    }


    @Override
    public SimStatusV4 getSimStatus(SampleResult sampleResult,
                                    String customerId,
                                    String simId)
        throws GSException {
        String endpoint =
            MessageFormat.format(GET_SIM_STATUS, customerId, simId);
        sampleResult.setSampleLabel(GET_SIM_STATUS);
        return
            sendGet(sampleResult,
                    false,
                    endpoint,
                    SimStatusV4.class,
                    NVPair.from("detail", "availableNetworkGroupInfo"));
    }

    @Override
    public AvailableNetworkGroups getAvailableNetworkGroups(
        SampleResult sampleResult, String customerId, String simId)
        throws GSException {
        String endpoint =
            MessageFormat.format(GET_AVAILABLE_NETWORK_GROUPS,
                                 customerId,
                                 simId);
        sampleResult.setSampleLabel(GET_AVAILABLE_NETWORK_GROUPS);
        return
            sendGet(sampleResult,
                    false,
                    endpoint,
                    AvailableNetworkGroups.class);
    }

    @Override
    protected ServerType getServerType() {
        return ServerType.GIGSKY;
    }
}
