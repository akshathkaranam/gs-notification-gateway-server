package com.gigsky.abi;

public interface AuthTokenHandler {
    public void accept(int userNumber, AuthTokenInfo authTokenInfo);
    public AuthTokenInfo get(int userNumber);
    public void clear(int userNumber);
}
