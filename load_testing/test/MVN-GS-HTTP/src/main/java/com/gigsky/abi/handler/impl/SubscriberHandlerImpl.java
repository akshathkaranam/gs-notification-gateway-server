package com.gigsky.abi.handler.impl;

import com.gigsky.abi.GSException;
import com.gigsky.abi.handler.SubscriberHandler;
import com.gigsky.abi.http.ServerType;
import com.gigsky.data.BodyInput;
import com.gigsky.data.LocationEventSchema;
import com.gigsky.data.SubscriberInput;
import com.gigsky.restapi.bean.BalanceCounters;
import com.gigsky.restapi.bean.Subscriber;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.log4j.Logger;

import java.util.List;

public class SubscriberHandlerImpl extends HandlerBase
    implements SubscriberHandler {
    private static final Logger log =
        Logger.getLogger (SubscriberHandlerImpl.class);

    public SubscriberHandlerImpl(int userNumber) {
        super (userNumber);
    }

    @Override
    public void addSubscriber (SampleResult sampleResult,
                               SubscriberInput input) throws GSException {
        //URL: baseURL + /subscriber
        String endpoint = URL_SUBSCRIBER;
        sampleResult.setSampleLabel(URL_SUBSCRIBER);
        sendPost(sampleResult, true, endpoint, input, null);
    }

    @Override
    public Subscriber getSubscriber(SampleResult sampleResult,
                                    String subscriberId) throws GSException {
        String endpoint = URL_SUBSCRIBER + "/" + subscriberId;
        sampleResult.setSampleLabel(URL_SUBSCRIBER+"/subscriberId");
        return sendGet ( sampleResult, true, endpoint, Subscriber.class );
    }

    @Override
    public BalanceCounters getSubscriberMaximumCounters(
        SampleResult sampleResult, String subId ) throws GSException {
        String endpoint = URL_SUBSCRIBER + URL_SUBSCRIBER_MAX_COUNTERS;
        sampleResult.setSampleLabel(endpoint);
        return sendGet ( sampleResult, true, endpoint, BalanceCounters.class );
    }

    @Override
    public void setCaptivePortalBalance(SampleResult setCaptivePortalBalance,
                                        String subId,
                                        BodyInput bodyInput) throws GSException {
        String endpoint =
            URL_SUBSCRIBER + "/" + subId +
            "/captiveCounter/balance";

        setCaptivePortalBalance.setSampleLabel(URL_SUBSCRIBER+"/SubId/captiveCounter/balance");
        sendPut(setCaptivePortalBalance, true, endpoint, bodyInput, null);
    }

    @Override
    public void deleteSubscriber(SampleResult sampleResult,
                                 String subId) throws GSException {
        String endpoint = URL_SUBSCRIBER + "/" + subId;
        sampleResult.setSampleLabel(URL_SUBSCRIBER+"/SubId/");
        sendDelete ( sampleResult, true, endpoint, null );
    }

    @Override
    public void setSubscriberOrTopUpBalance(SampleResult sampleResult,
                                            String subId,
                                            String counterIndex,
                                            BodyInput bodyInput)
        throws GSException {
        String endpoint = URL_SUBSCRIBER + "/" + subId + URL_SUBSCRIBER_COUNTERS + "/" + counterIndex + URL_SUBSCRIBER_BALANCE;

        sampleResult.setSampleLabel(URL_SUBSCRIBER+"/SubId/"+URL_SUBSCRIBER_COUNTERS+"/counterIndex");
        sendPut (sampleResult, true, endpoint, bodyInput, null);
    }
    
    @Override
    public void setSubscriberCaptivePortalBalance (SampleResult sampleResult,
                                                   String customerId,
                                                   String token,
                                                   BodyInput bodyInput,
                                                   SubscriberInput input)
        throws GSException {
        String endpoint =
            URL_SUBSCRIBER + "/" + input.getSubscriberId () +
            URL_SUBSCRIBER_COUNTERS + URL_SUBSCRIBER_CAPTIVE_COUNTER +
            URL_SUBSCRIBER_BALANCE;

        sampleResult.setSampleLabel(URL_SUBSCRIBER+"/SubId/"+URL_SUBSCRIBER_COUNTERS + URL_SUBSCRIBER_CAPTIVE_COUNTER + URL_SUBSCRIBER_BALANCE);
        sendPut(sampleResult, false, endpoint, bodyInput, String.class);
    }
    
    @Override
    public void resetBalanceOfAllCounters(SampleResult sampleResult,
                                          String subId,
                                          BodyInput bodyInput)
        throws GSException {
        String endpoint =
            URL_SUBSCRIBER + "/" + subId + URL_SUBSCRIBER_COUNTERS;

        sampleResult.setSampleLabel("reset"+URL_SUBSCRIBER+"/SubId/"+URL_SUBSCRIBER_COUNTERS);
        sendPut(sampleResult, false, endpoint, bodyInput, null);
    }
    
    @Override
    public void resetBalanceOfACounter(SampleResult reset,
                                       String subId,
                                       String resetCounterIndex,
                                       BodyInput input) throws GSException {
        String endpoint =
            URL_SUBSCRIBER + "/" + subId + URL_SUBSCRIBER_COUNTERS+ "/" +
            resetCounterIndex;

        reset.setSampleLabel(URL_SUBSCRIBER + "/subId" + URL_SUBSCRIBER_COUNTERS);
        sendPut(reset, false, endpoint, input, null);
    }
    
    @Override
    public void addLocation(SampleResult location,
                            List<LocationEventSchema> schema)
        throws GSException {
        String endpoint = "globecomm/event";

        location.setSampleLabel(endpoint);
        sendPost(location, false, endpoint, schema, null);
    }

    @Override
    protected ServerType getServerType() {
        return ServerType.IMSI;
    }
}
