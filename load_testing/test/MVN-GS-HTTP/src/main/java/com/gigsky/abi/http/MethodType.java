package com.gigsky.abi.http;

public enum MethodType implements Comparable<MethodType> {
    GET,
    PUT,
    POST,
    DELETE
}
