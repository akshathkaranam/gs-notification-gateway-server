package com.gigsky.data;

public class LocationInput
{

    private long subscriptionId;
    String startDateStr = "";
    String endDateStr = "";
    int startIndex = 0;
    int count = 0;

    @Override
    public String toString ()
    {
        return "LocationInput{" + "subscriptionId=" + subscriptionId + ", startDateStr=" + startDateStr + ", endDateStr=" + endDateStr + ", startIndex=" + startIndex + ", count=" + count + '}';
    }

    public long getSubscriptionId ()
    {
        return subscriptionId;
    }

    public void setSubscriptionId ( long subscriptionId )
    {
        this.subscriptionId = subscriptionId;
    }

    public String getStartDateStr ()
    {
        return startDateStr;
    }

    public void setStartDateStr ( String startDateStr )
    {
        this.startDateStr = startDateStr;
    }

    public String getEndDateStr ()
    {
        return endDateStr;
    }

    public void setEndDateStr ( String endDateStr )
    {
        this.endDateStr = endDateStr;
    }

    public int getStartIndex ()
    {
        return startIndex;
    }

    public void setStartIndex ( int startIndex )
    {
        this.startIndex = startIndex;
    }

    public int getCount ()
    {
        return count;
    }

    public void setCount ( int count )
    {
        this.count = count;
    }

}
