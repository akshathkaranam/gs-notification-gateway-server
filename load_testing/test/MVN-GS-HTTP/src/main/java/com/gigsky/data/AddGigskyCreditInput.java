package com.gigsky.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class AddGigskyCreditInput {
    private String type;
    private String gigskyCredit;
    private String currency;
    private String description;

    public AddGigskyCreditInput() {
        type = "AddGigskyCredit";
        currency = "USD";
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGigskyCredit() {
        return gigskyCredit;
    }

    public void setGigskyCredit(String gigskyCredit) {
        this.gigskyCredit = gigskyCredit;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "AddGigskyCreditInput{" + "type=" + type + ", gigskyCredit=" + gigskyCredit + ", currency=" + currency + ", description=" + description + '}';
    }
}
