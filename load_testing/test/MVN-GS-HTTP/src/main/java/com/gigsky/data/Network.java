/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gigsky.data;

/**
 *
 * @author veera
 */
public class Network {
 
    private int mcc;
    private int mnc;

    @Override
    public String toString() {
        return "Network{" + "mcc=" + mcc + ", mnc=" + mnc + '}';
    }

    public int getMcc() {
        return mcc;
    }

    public void setMcc(int mcc) {
        this.mcc = mcc;
    }

    public int getMnc() {
        return mnc;
    }

    public void setMnc(int mnc) {
        this.mnc = mnc;
    }
    
    
    
}
