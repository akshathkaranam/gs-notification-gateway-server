package com.gigsky.data;

public class SubscriberInput
{

    public static final String TYPE = "Subscriber";
    private String type = TYPE;
    private long imsi;
    private String subscriberId;

    @Override
    public String toString ()
    {
        return "SubscriberInput{" + "type=" + type + ", imsi=" + imsi + ", subscriberId=" + subscriberId + '}';
    }

    public String getType ()
    {
        return type;
    }

    public void setType ( String type )
    {
        this.type = type;
    }

    public long getImsi ()
    {
        return imsi;
    }

    public void setImsi ( long imsi )
    {
        this.imsi = imsi;
    }

    public String getSubscriberId ()
    {
        return subscriberId;
    }

    public void setSubscriberId ( String subscriberId )
    {
        this.subscriberId = subscriberId;
    }

}
