package com.gigsky.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PlanInput {
    private String type;
    private String transactionPIN;
    private long planId;
    private String handOffToken;

    public PlanInput() {
        type = "TransactionRequest";
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTransactionPIN() {
        return transactionPIN;
    }

    public void setTransactionPIN(String transactionPIN) {
        this.transactionPIN = transactionPIN;
    }

    public long getPlanId() {
        return planId;
    }

    public void setPlanId(long planId) {
        this.planId = planId;
    }

    @Override
    public String toString() {
        return "PlanInput{" + "type=" + type + ", transactionPIN=" + transactionPIN + ", planId=" + planId + '}';
    }

    public String getHandOffToken() {
        return handOffToken;
    }

    public void setHandOffToken(String handOffToken) {
        this.handOffToken = handOffToken;
    }
}
