package com.gigsky.ngw.handler.impl;

import java.text.MessageFormat;

import org.apache.jmeter.samplers.SampleResult;
import org.apache.log4j.Logger;

import com.gigsky.abi.handler.impl.AccountHandlerImpl;
import com.gigsky.abi.handler.impl.HandlerBase;
import com.gigsky.abi.http.ServerType;
import com.gigsky.ngw.bean.EventResponse;
import com.gigsky.ngw.data.EventInput;
import com.gigsky.ngw.handler.EventHandler;
import com.gigsky.abi.GSConfig;
import com.gigsky.abi.GSConstants;
import com.gigsky.abi.GSException;
import com.gigsky.accounts.v4.beans.AccountsV4;

public class EventHandlerImpl extends HandlerBase implements EventHandler {

	private static final Logger log =
	        Logger.getLogger(AccountHandlerImpl.class);
	
	public EventHandlerImpl(int userNumber) {
        super(userNumber);
    }
    
	
	@Override
	public EventResponse createEvent(SampleResult result, EventInput input) throws GSException {
		
		log.info("Create event " + input);
		result.setSampleLabel(GSConstants.URL_NGW_EVENT);
		log.info("calling api with URL " + GSConfig.getNgwUrlPrefix() + GSConstants.URL_NGW_EVENT);
		EventResponse response = sendPost(result, true, GSConfig.getNgwUrlPrefix() + GSConstants.URL_NGW_EVENT, input, EventResponse.class);
		return response;
	}

	@Override
	public EventResponse getEvent(SampleResult result, int eventId) throws GSException {
		// TODO Auto-generated method stub
		log.info("Get event " + eventId);
		
        String endpoint = MessageFormat.format(GSConstants.URL_NGW_EVENT, eventId);
        result.setSampleLabel(GET_ACCOUNT);
        EventResponse response = sendGet(result, true, endpoint, EventResponse.class);
		return response;
	}
	
	@Override
    protected ServerType getServerType() {
        return ServerType.GIGSKY;
    }

}
