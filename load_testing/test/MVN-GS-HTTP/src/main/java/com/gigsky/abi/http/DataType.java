package com.gigsky.abi.http;

import java.io.Serializable;

public enum DataType implements Serializable, Constants {
    STRING (XS_STRING),
    INTEGER (XS_INT),
    BOOLEAN (XS_BOOLEAN);
    
    private String value;
    
    DataType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
    
    public static DataType fromValue(String value) {
        DataType retVal = null;
        
        if (value.equals(XS_INT)) {
            retVal = INTEGER;
        } else if (value.equals(XS_STRING)) {
            retVal = STRING;
        } else if (value.equals(XS_BOOLEAN)) {
            retVal = BOOLEAN;
        }
        
        return retVal;
    }

    static final long serialVersionUID = 0L;
}
