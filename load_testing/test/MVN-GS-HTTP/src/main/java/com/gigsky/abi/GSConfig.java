package com.gigsky.abi;

import org.apache.log4j.Logger;

import java.util.Properties;

public class GSConfig {
    private static final Logger log = Logger.getLogger(GSConfig.class);

    private static String gigskyUrlPrefix;
    private static String imsiUrlPrefix;
    private static String eventUrlPrefix;
    private static boolean needThrottling;
    private static int throttleSeconds;
    private static int userOffset;
    private static int userCount;

    private static int cdrsPerSec = 10;
    private static int locationsPerSec = 1;
    private static int scenario = 1;

    public static int getAcmeMode() {
        return acmeMode;
    }

    private static int acmeMode;

    static String ftpServer ;
    static String ftpUser ;
    static String ftpPassword ;
    static int ftpPort ;
    static int cdrsPerFile ;
    
    private static String ngwURLPrefix;


	public static int getNgwUserCount() {
		return ngwUserCount;
	}

	public static String getRegistrationIdBase() {
		return registrationIdBase;
	}

	public static String getTokenBase() {
		return tokenBase;
	}

	public static String getIccidBase() {
		return iccidBase;
	}

	private static int ngwUserCount = 250000;
    private static String registrationIdBase;
    private static String tokenBase;
    private static String iccidBase;
    

    public static int getLocationsPerMessage() {
        return locationsPerMessage;
    }

    static int locationsPerMessage ;
    static String remotePath ;

    public static synchronized void init(Properties properties) {
        gigskyUrlPrefix = properties.getProperty("gigsky-loadtest.urlPrefix");
        imsiUrlPrefix = properties.getProperty("gigsky-loadtest.imsiUrlPrefix");
        eventUrlPrefix =
            properties.getProperty("gigsky-loadtest.eventUrlPrefix");
        needThrottling =
            properties
                .getProperty("gigsky-loadtest.needThrottling")
                .equalsIgnoreCase("true");
        throttleSeconds =
            Integer.parseInt(
                properties.getProperty("gigsky-loadtest.throttleSeconds"));
        userOffset =
            Integer.parseInt(
                properties.getProperty("gigsky-loadtest.userOffset"));

        String customerCountProperty = properties.getProperty("gigsky-loadtest.userCount");
        if(customerCountProperty != null) {
            userCount = Integer.parseInt(customerCountProperty);
        }

        ftpServer = properties.getProperty("gigsky-loadtest.ftpServer");
        ftpUser = properties.getProperty("gigsky-loadtest.ftpUser");
        ftpPassword = properties.getProperty("gigsky-loadtest.ftpPassword");
        String port = properties.getProperty("gigsky-loadtest.ftpPort");
        if(port != null) {
            ftpPort = Integer.parseInt(port);
        }
        String cdrsPerFileStr = properties.getProperty("gigsky-loadtest.cdrsPerFile");
        if(cdrsPerFileStr != null) {
            cdrsPerFile = Integer.parseInt(cdrsPerFileStr);
        }
        String locationsPerMessageStr = properties.getProperty("gigsky-loadtest.locationsPerMessage");
        if(locationsPerMessageStr != null) {
            locationsPerMessage = Integer.parseInt(locationsPerMessageStr);
        }
        remotePath = properties.getProperty("gigsky-loadtest.remotePath");
        String acmeModeStr = properties.getProperty("gigsky-loadtest.acmeMode");
        if(acmeModeStr != null) {
            acmeMode = Integer.valueOf(acmeModeStr);
        }

        String valueStr = properties.getProperty("gigsky-loadtest.cdrsPerSec");
        if(valueStr != null) {
            cdrsPerSec = Integer.parseInt(valueStr);
        }
        valueStr = properties.getProperty("gigsky-loadtest.locationsPerSec");
        if(valueStr != null) {
            locationsPerSec = Integer.parseInt(valueStr);
        }
        valueStr = properties.getProperty("gigsky-loadtest.scenario");
        if(valueStr != null) {
            scenario = Integer.parseInt(valueStr);
        }

        ngwURLPrefix = properties.getProperty("gigsky-loadtest.ngwUrlPrefix");
        registrationIdBase = properties.getProperty("gigsky-loadtest.regIdBase");
        tokenBase = properties.getProperty("gigsky-loadtest.tokenBase");
        iccidBase = properties.getProperty("gigsky-loadtest.iccidBase");
        String temp = properties.getProperty("gigsky-loadtest.userCount");
        
        if(temp != null)
        {
        	ngwUserCount = Integer.parseInt(temp);
        }
        

        
        log.info(
            "URL prefix is: " + gigskyUrlPrefix + ", IMSI URL prefix is: " +
            imsiUrlPrefix + ", event URL prefix is: " + eventUrlPrefix +
            ", need throttling: " + needThrottling + ", userOffset: " +
            userOffset);
    }
    
    public static String gigskyUrlPrefix() {
        return gigskyUrlPrefix;
    }
    
    public static String imsiUrlPrefix() {
        return imsiUrlPrefix;
    }
    
    public static String eventUrlPrefix() {
        return eventUrlPrefix;
    }
    
    public static boolean needThrottling() {
        return needThrottling;
    }

    public static int throttleSeconds() {
        return throttleSeconds;
    }

    public static int userOffset() {
        return userOffset;
    }

    public static int getUserCount() {
        return userCount;
    }

    public static void setUserCount(int userCount) {
        GSConfig.userCount = userCount;
    }

    public static String getFtpServer() {
        return ftpServer;
    }

    public static String getFtpUser() {
        return ftpUser;
    }

    public static String getFtpPassword() {
        return ftpPassword;
    }

    public static int getFtpPort() {
        return ftpPort;
    }

    public static int getCdrsPerFile() {
        return cdrsPerFile;
    }

    public static String getRemotePath() {
        return remotePath;
    }

    public static int getCdrsPerSec() {
        return cdrsPerSec;
    }

    public static int getLocationsPerSec() {
        return locationsPerSec;
    }

    public static int getScenario() {
        return scenario;
    }

    public static void setScenario(int scenario) {
        GSConfig.scenario = scenario;
    }
    
    public static String getNgwUrlPrefix() {
    	return ngwURLPrefix;
    }
}
