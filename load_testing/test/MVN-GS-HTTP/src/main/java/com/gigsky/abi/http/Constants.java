package com.gigsky.abi.http;

public interface Constants {
    public static final String STYLE_QUERY = "query";
    public static final String STYLE_TEMPLATE = "template";
    public static final String STYLE_HEADER = "header";
    public static final String STYLE_MATRIX = "matrix";
    public static final String STYLE_PLAIN = "plain";
    
    
    public static final String XS_STRING = "xs:string";
    public static final String XS_INT = "xs:int";
    public static final String XS_BOOLEAN = "xs:boolean";
}
