package com.gigsky.abi.handler;

import com.gigsky.abi.GSException;
import com.gigsky.accounts.v4.beans.AccountPaymentStatus;
import com.gigsky.accounts.v4.beans.CreditBalanceDetailsV4;
import com.gigsky.accounts.v4.beans.CreditTransactionsV4;
import com.gigsky.accounts.v4.beans.TransactionV4;
import com.gigsky.common.payment.beans.BillingInfoListBean;
import com.gigsky.common.payment.beans.PaymentMethodListBean;
import com.gigsky.data.AddGigskyCreditInput;
import com.gigsky.data.CreditCardInput;
import org.apache.jmeter.samplers.SampleResult;

public interface BillingHandler extends GSHandler {
    public PaymentMethodListBean getPaymentMethods(SampleResult sampleResult)
        throws GSException;
    public void addCredit(SampleResult sampleResult,
                          String customerId,
                          AddGigskyCreditInput input)
        throws GSException;
    public CreditBalanceDetailsV4 getCredit(SampleResult sampleResult,
                                            String customerId)
        throws GSException;
    public void addCreditCard(SampleResult sampleResult,
                              String customerId,
                              CreditCardInput input)
        throws GSException;
    public CreditTransactionsV4 getTransactions(SampleResult sampleResult,
                                                String customerId)
        throws GSException;
    public TransactionV4 getTransactionById(SampleResult sampleResult,
                                                String customerId, String simId, long transactionId)
            throws GSException;
    public BillingInfoListBean getBillingInfo(SampleResult sampleResult,
                                              String customerId)
        throws GSException;

    public AccountPaymentStatus getPaymentStatus(SampleResult payStatus, String customerId) throws GSException;
}
