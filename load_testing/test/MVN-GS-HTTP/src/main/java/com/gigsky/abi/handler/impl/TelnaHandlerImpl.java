package com.gigsky.abi.handler.impl;

import com.gigsky.abi.GSException;
import com.gigsky.abi.handler.TelnaHandler;
import com.gigsky.abi.http.ServerType;
import com.sun.jersey.core.util.MultivaluedMapImpl;
import javax.ws.rs.core.MultivaluedMap;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.log4j.Logger;

public class TelnaHandlerImpl extends HandlerBase implements TelnaHandler {
    private static final Logger log = Logger.getLogger(TelnaHandlerImpl.class);

    public TelnaHandlerImpl(int userNumber) {
        super(userNumber);
    }
    
    @Override
    public void cdrUpdate(SampleResult sampleResult, String xml)
        throws GSException {
        doSendTelnaEvent(sampleResult, xml);
    }
    
    @Override
    public void locationUpdate(SampleResult sampleResult, String xml)
        throws GSException {
        doSendTelnaEvent(sampleResult, xml);
    }
    
    private void doSendTelnaEvent(SampleResult sampleResult, String xml)
        throws GSException {
        MultivaluedMap formData = new MultivaluedMapImpl();
        formData.add("eventXml", xml);
        sendPost(sampleResult, false, TELNA_EVENT, formData, null);
    }

    @Override
    protected ServerType getServerType() {
        return ServerType.GIGSKY;
    }
}
