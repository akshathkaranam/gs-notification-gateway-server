package com.gigsky.abi.http;

import com.google.gson.Gson;

public class Serializer {
    private final Gson gson;
    
    public Serializer() {
        this.gson = new Gson();
    }
    
    public <T> T fromJson(String json, Class<T> type) {
        return gson.fromJson(json, type);
    }
    
    public String toJson(final Object obj) {
        return gson.toJson(obj);
    }
}
