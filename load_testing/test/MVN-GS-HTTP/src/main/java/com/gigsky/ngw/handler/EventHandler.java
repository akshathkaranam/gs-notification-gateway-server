package com.gigsky.ngw.handler;

import org.apache.jmeter.samplers.SampleResult;

import com.gigsky.abi.GSException;
import com.gigsky.abi.handler.GSHandler;
import com.gigsky.ngw.bean.EventResponse;
import com.gigsky.ngw.data.EventInput;

public interface EventHandler extends GSHandler{
	
	public EventResponse createEvent(SampleResult result, EventInput input) throws GSException;

	public EventResponse getEvent(SampleResult result, int eventId) throws GSException;
}
