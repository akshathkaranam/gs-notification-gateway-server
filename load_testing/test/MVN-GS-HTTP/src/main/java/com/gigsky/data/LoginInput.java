package com.gigsky.data;

public class LoginInput
{

    private String emailId;
    private String loginType;
    private String password;
    private String type;
    private String loginId;

    public String getHandOffToken() {
        return handOffToken;
    }

    public void setHandOffToken(String handOffToken) {
        this.handOffToken = handOffToken;
    }

    private String handOffToken;

    public String getLoginId ()
    {
        return loginId;
    }

    public void setLoginId ( String loginId )
    {
        this.loginId = loginId;
    }

    public String getEmailId ()
    {
        return emailId;
    }

    public void setEmailId ( String emailId )
    {
        this.emailId = emailId;
    }

    public String getLoginType ()
    {
        return loginType;
    }

    public void setLoginType ( String loginType )
    {
        this.loginType = loginType;
    }

    public String getPassword ()
    {
        return password;
    }

    public void setPassword ( String password )
    {
        this.password = password;
    }

    public String getType ()
    {
        return type;
    }

    public void setType ( String type )
    {
        this.type = type;
    }

    @Override
    public String toString ()
    {
        return "LoginInput{" + "emailId=" + emailId + ", loginType=" + loginType + ", password=" + password + ", type=" + type + ", loginId=" + loginId + '}';
    }

}
