package com.gigsky.abi.http;

import com.gigsky.abi.GSConstants;
import com.gigsky.abi.GSUtil;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.UniformInterfaceException;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.config.ClientConfig;
import com.sun.jersey.api.client.config.DefaultClientConfig;
import com.sun.jersey.api.json.JSONConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.AnnotationIntrospector;
import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.xc.JaxbAnnotationIntrospector;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

public class RestClient implements Constants, GSConstants {
    private static final Logger log = Logger.getLogger(RestClient.class);

    private static Client client = null;
    private static int mClientTimeoutMS = 60 * 3 * 1000;
    static {
        ClientConfig clientConfig = new DefaultClientConfig();
        clientConfig.getFeatures().put(JSONConfiguration.FEATURE_POJO_MAPPING, Boolean.TRUE);
        client = Client.create(clientConfig);
        //client.addFilter(new LoggingFilter(System.out));
        client.setConnectTimeout(mClientTimeoutMS);
        client.setReadTimeout(mClientTimeoutMS);
        client.setFollowRedirects(true);
    }


    public RestClient() {
        //client = Client.create();
        //client.setConnectTimeout(60*10*000);
        //client.setReadTimeout(60*10*000);
        //client.setFollowRedirects(true);
    }

    public static void destroy() {
        if(client != null) {
            client.destroy();
        }
    }

    public void close() {
        //client.destroy();
        //client = null;
    }
    
    public <T> T sendGet(SampleResult sampleResult,
                         ServerType serverType,
                         String uri,
                         Class<T> responseType,
                         List<NVPair> nvPairs)
        throws UniformInterfaceException, IOException {
        WebResource.Builder builder =
            createResource(sampleResult,
                           serverType,
                           uri,
                           MediaType.APPLICATION_JSON_TYPE,
                           nvPairs);

        return extractTypedData(sampleResult,
                                builder,
                                MethodType.GET,
                                null,
                                responseType);
    }
    
    public <T> T sendPost(SampleResult sampleResult,
                          ServerType serverType,
                          String uri,
                          Object payload,
                          Class<T> responseType,
                          List<NVPair> nvPairs)
        throws UniformInterfaceException, IOException {

        MediaType mediaType = getMediaType(payload);
        
        WebResource.Builder builder =
            createResource(sampleResult, serverType, uri, mediaType, nvPairs);
        
        return
            extractTypedData(sampleResult,
                             builder,
                             MethodType.POST,
                             payload,
                             responseType);
    }
    
    public <T> T sendPut(SampleResult sampleResult,
                         ServerType serverType,
                         String uri,
                         Object payload,
                         Class<T> responseType,
                         List<NVPair> nvPairs)
        throws UniformInterfaceException, IOException {
        MediaType mediaType = getMediaType(payload);
        
        WebResource.Builder builder =
            createResource(sampleResult, serverType, uri, mediaType, nvPairs);
        
        return extractTypedData(sampleResult,
                                builder,
                                MethodType.PUT,
                                payload,
                                responseType);
    }

    public <T> T sendDelete(SampleResult sampleResult,
                            ServerType serverType,
                            String uri,
                            Class<T> responseType,
                            List<NVPair> nvPairs)
        throws UniformInterfaceException, IOException {
        WebResource.Builder builder =
            createResource(sampleResult,
                           serverType,
                           uri,
                           MediaType.APPLICATION_JSON_TYPE,
                           nvPairs);

        return
            extractTypedData(sampleResult,
                             builder,
                             MethodType.DELETE,
                             null,
                             responseType);
    }

    static ObjectMapper mapper = new ObjectMapper();
    static {
        mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_EMPTY);
        mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        AnnotationIntrospector introspector = new JaxbAnnotationIntrospector();
        mapper.setAnnotationIntrospector(introspector);

    }


    private <T> T extractTypedData(SampleResult sampleResult,
                                   WebResource.Builder builder,
                                   MethodType methodType,
                                   Object payload,
                                   Class<T> responseType) throws IOException {
        T retVal = null;

//        long start1 = System.nanoTime();
        if (payload == null) {
            // Forces content length to be zero
            payload = "";
        } else if (payload instanceof MultivaluedMap) {
            // Do nothing
        } else {
            payload = mapper.writeValueAsString(payload);
            //payload = Jsonify.toJson(payload);
//            log.info(Thread.currentThread().getId()+" "+"requestBody:"+payload);
            sampleResult.setSamplerData((String) payload);
        }

        //ObjectMapper mapper = new ObjectMapper();
        //mapper.serial
//        final String jsonString = mapper.writeValueAsString(payload); //new String(bos.getBytes());
//        System.out.println(payload);

        ClientResponse clientResponse = null;

        long start = System.nanoTime();
        switch (methodType) {
            case GET:
                clientResponse = builder.get(ClientResponse.class);
                break;
            case PUT:
                clientResponse = builder.put(ClientResponse.class, payload);
                break;
            case POST:
                clientResponse = builder.post(ClientResponse.class, payload);
                break;
            case DELETE:
                clientResponse = builder.delete(ClientResponse.class);
                break;
        }
        long end = System.nanoTime();
//        log.info(thId+" Http ms:"+(end-start)/(1000*1000)+" status:"+clientResponse.getStatus());

        if (clientResponse != null) {
            String responseStr = clientResponse.getEntity(String.class);
            if (clientResponse.getStatus() != 200) {
                sampleResult.setSuccessful(false);
                log.info(Thread.currentThread().getId()+" "+"NOT OK response: code:" + clientResponse.getStatus()
                            + " responseBody:"+responseStr
                            +" requestBody:"+sampleResult.getSamplerData()
                            +" time ms:"+((end - start)/(1000 * 1000)));
                throw new IOException("Http Response Status:"+clientResponse.getStatus()+" Response"+clientResponse.getEntity(String.class));
            } else {
                /*
                 log.info(Thread.currentThread().getId()+" "+"OK response: code:" + clientResponse.getStatus()
                            + " responseBody:"+responseStr
                            +" requestBody:"+sampleResult.getSamplerData()
                            +" time ms:"+((end - start)/(1000 * 1000)));
                            */
            }

            sampleResult.setResponseCodeOK();
            //sampleResult.setResponseData(responseStr, "application/json;charset=UTF-8");
            sampleResult.setResponseData(clientResponse.getClientResponseStatus().getReasonPhrase().getBytes());
            sampleResult.setResponseMessage(responseStr);
            sampleResult.setResponseCode("" + clientResponse.getClientResponseStatus().getStatusCode());

            if (responseStr != null && responseStr.length() > 0 && responseType != null) {
                retVal = mapper.readValue(responseStr, responseType);
                //retVal = Jsonify.fromJson(responseStr, responseType);
            }
            sampleResult.setSuccessful(true);

        } else {
            sampleResult.setStopThread(true);
            sampleResult.setSuccessful(false);
            sampleResult.setResponseMessage("No response received; Stop thread");

            throw new IOException("No response received; Stop thread");
        }
//        long end1 = System.nanoTime();
//        log.info(Thread.currentThread().getId()+ " extract ms:"+(end1-start1)/(1000*1000));

        return retVal;
    }



    private MediaType getMediaType(Object payload) {
        MediaType retVal = null;
        
        if (payload instanceof File) {
            File file = (File) payload;
            
            try {
                URL u = file.toURI().toURL();

                URLConnection uc = u.openConnection();
                String contentType = uc.getContentType();
                
                if (contentType.contains("content/unknown")) {
                    String fileName = file.getName();
                    
                    if (fileName.endsWith("mp3")) {
                        contentType = "mpeg/audio";
                    } else if (fileName.endsWith("mp4")) {
                        contentType = "mpeg/video";
                    }
                }
            
                retVal = MediaType.valueOf(contentType);
            } catch (IOException | IllegalArgumentException e) {
                e.printStackTrace();
            }
        } else if (payload instanceof MultivaluedMap) {
            retVal = MediaType.APPLICATION_FORM_URLENCODED_TYPE;
        } else {
            retVal = MediaType.APPLICATION_JSON_TYPE;
        }
        
        return retVal;
    }
    
    private WebResource.Builder createResource(
        SampleResult sampleResult,
        ServerType serverType,
        String uri,
        MediaType mediaType,
        List<NVPair> nvPairs) {

//        long start = System.nanoTime();
        String restUrl = HttpUtil.makeRestUrl(uri, serverType);
        
        StringBuilder uriBuilder = new StringBuilder(restUrl);

        boolean hasParameters = restUrl.contains("?");
        
        String authTokenSpec = null;
        
        if (nvPairs != null) {
            for (int i = 0; i < nvPairs.size(); i++) {
                NVPair nvPair = nvPairs.get(i);
                
                ParamType paramType = nvPair.getParamType();
                
                String value = nvPair.getValueAsString();

                if (paramType == ParamType.AUTH) {
                    authTokenSpec = "Basic "+ value;
//                    log.info(Thread.currentThread().getId()+" "+"authToken:"+authTokenSpec);
                    sampleResult.setRequestHeaders("Authorization: Basic "+value);
                } else  {
                    if (GSUtil.valid(value)) {
                        uriBuilder.append(hasParameters ? "&" : "?");

                        uriBuilder
                            .append(nvPair.getName())
                            .append("=")
                            .append(value);

                        hasParameters = true;
                    }
                }
            }
        }

        String fullUri = uriBuilder.toString();

        try {
            sampleResult.setURL(new URL(fullUri));
        } catch (MalformedURLException e) {
            // Should never happen
        }
//        log.info(Thread.currentThread().getId()+" "+"url:"+fullUri);


        WebResource webResource = client.resource(fullUri);
        
        WebResource.Builder builder = webResource.type(mediaType);
        
        if (GSUtil.valid(authTokenSpec)) {
            builder.header(AUTHORIZATION, authTokenSpec);
        }
//        long end = System.nanoTime();

//        log.info("createRes ms:"+(end-start)/(1000*1000));

        return builder;
    }
}
