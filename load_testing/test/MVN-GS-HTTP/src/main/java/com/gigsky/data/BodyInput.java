package com.gigsky.data;

import java.util.List;

/**
 * @author drvijay
 */

public class BodyInput
{

    private String type;
    private String operationType = "";
    private Integer bytes;
    
    private List<Network> networkList;
    
    private String expiryTime;

    public List<Network> getNetworkList() {
        return networkList;
    }

    public void setNetworkList(List<Network> networkList) {
        this.networkList = networkList;
    }

    public String getExpiryTime() {
        return expiryTime;
    }

    public void setExpiryTime(String expiryTime) {
        this.expiryTime = expiryTime;
    }

    
    @Override
    public String toString ()
    {
        return "BodyInput{" + "type=" + type + ", operationType=" + operationType + ", bytes=" + bytes + '}';
    }

    public String getType ()
    {
        return type;
    }

    public void setType ( String type )
    {
        this.type = type;
    }

    public String getOperationType ()
    {
        return operationType;
    }

    public void setOperationType ( String operationType )
    {
        this.operationType = operationType;
    }

    public Integer getBytes ()
    {
        return bytes;
    }

    public void setBytes ( Integer bytes )
    {
        this.bytes = bytes;
    }

}
