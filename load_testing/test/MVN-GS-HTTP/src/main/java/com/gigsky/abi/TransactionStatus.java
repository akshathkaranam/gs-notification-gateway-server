package com.gigsky.abi;

/**
 * Created on 02/02/15.
 */
public enum TransactionStatus {
    PENDING("PENDING"),
    COMPLETE("COMPLETE"),
    FAILED("FAILED"),
    PENDING_PAYMENT("PENDING-PAYMENT"),
    PENDING_NETWORKUPDATE("PENDING-NETWORKUPDATE"),
    RESERVE_PENDING("RESERVE_PENDING"),
    BIND_PENDING("BIND_PENDING"),
    RESERVE_FAILED("RESERVE_FAILED"),
    BIND_FAILED("BIND_FAILED");

    public String status;

    TransactionStatus(String status) {
        this.status = status;
    }
}
