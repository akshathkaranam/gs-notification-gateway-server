package com.gigsky.data;

public class LocationEventSchema {
    private Params params;
    private Integer id;
    private String method = "push";

    public Params getParams() {
        return params;
    }

    public void setParams(Params params) {
        this.params = params;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    @Override
    public String toString() {
        return "LocationEventSchema{" + "params=" + params + ", id=" + id + ", method=" + method + '}';
    }
}
