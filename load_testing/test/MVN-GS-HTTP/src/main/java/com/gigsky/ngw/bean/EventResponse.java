package com.gigsky.ngw.bean;
import org.codehaus.jackson.map.annotate.JsonSerialize;
/**
 * Created by anant on 18/11/15.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class EventResponse {

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReceivedTime() {
        return receivedTime;
    }

    public void setReceivedTime(String receivedTime) {
        this.receivedTime = receivedTime;
    }

    public long getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(long notificationId) {
        this.notificationId = notificationId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public int getBalanceRemaining() {
        return balanceRemaining;
    }

    public void setBalanceRemaining(int balanceRemaining) {
        this.balanceRemaining = balanceRemaining;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getIccId() {
        return iccId;
    }

    public void setIccId(String iccId) {
        this.iccId = iccId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getPercentageConsumed() {
        return percentageConsumed;
    }

    public void setPercentageConsumed(int percentageConsumed) {
        this.percentageConsumed = percentageConsumed;
    }

    private String type;
    private String eventType;
    private long eventId;
    private String status;
    private String receivedTime;
    private long notificationId;
    private String customerId;
    private String userId;
    private int balanceRemaining;
    private String expiry;
    private String iccId;
    private String location;
    private int percentageConsumed;

}


