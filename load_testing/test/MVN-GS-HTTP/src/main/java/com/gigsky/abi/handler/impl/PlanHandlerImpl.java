package com.gigsky.abi.handler.impl;

import com.gigsky.abi.GSException;
import com.gigsky.abi.handler.PlanHandler;
import com.gigsky.abi.http.NVPair;
import com.gigsky.abi.http.ServerType;
import com.gigsky.accounts.beans.GlobalPlanSearchResponseBean;
import com.gigsky.accounts.v4.beans.*;
import com.gigsky.data.PlanInput;
import org.apache.jmeter.samplers.SampleResult;
import org.apache.log4j.Logger;

import java.text.MessageFormat;

public class PlanHandlerImpl extends HandlerBase implements PlanHandler {
    private static final Logger log = Logger.getLogger(PlanHandlerImpl.class);

    public PlanHandlerImpl(int userNumber) {
        super(userNumber);
    }
    
    @Override
    public GlobalPlanSearchResponseBean searchPlans(SampleResult sampleResult,
                                                    String keyword)
        throws GSException {
        sampleResult.setSampleLabel(PLAN_SEARCH);
        return
            sendGet(sampleResult,
                    false,
                    PLAN_SEARCH,
                    GlobalPlanSearchResponseBean.class,
                    NVPair.from("KEY", keyword));
    }
    
    @Override
    public CountryListV4 getCountriesWithNetworkGroupInfo(SampleResult sampleResult)
        throws GSException {
        sampleResult.setSampleLabel(COUNTRIES);
        return sendGet(sampleResult,
                       false,
                       COUNTRIES,
                       CountryListV4.class,
                       NVPair.from("detail", "NETWORK_GROUP_AND_PLANS"),
                       NVPair.from("currency", "USD"),
                       NVPair.from("sortBy", "cost"));
    }


    @Override
    public CountryListV4 getCountries(SampleResult sampleResult)
            throws GSException {
        sampleResult.setSampleLabel(COUNTRIES_ALL);
        return sendGet(sampleResult,
                false,
                COUNTRIES_ALL,
                CountryListV4.class,
                NVPair.from("startIndex", "0"),
                NVPair.from("count", "1000"));
    }


    @Override
    public ForbiddenCountryListV4 getForbiddenCountries(SampleResult sampleResult)
            throws GSException {
        sampleResult.setSampleLabel(FORBIDDEN_COUNTRIES);
        return sendGet(sampleResult,
                false,
                FORBIDDEN_COUNTRIES,
                ForbiddenCountryListV4.class);
    }

    @Override
    public NetworkGroupSummaryList getNetworkGroupsForCountry(
        SampleResult sampleResult, String countryCode) throws GSException {
        String endpoint =
            MessageFormat.format(COUNTRY_NETWORK_GROUPS, countryCode);
        sampleResult.setSampleLabel(COUNTRY_NETWORK_GROUPS);
        return sendGet(sampleResult,
                       false,
                       endpoint,
                       NetworkGroupSummaryList.class);
    }
    
    @Override
    public NetworkGroupDetails getNetworkGroupDetails(SampleResult sampleResult,
                                                      long networkGroupId)
        throws GSException {
        String endpoint =
            MessageFormat.format(NETWORK_GROUP_DETAILS, ""+networkGroupId);
        sampleResult.setSampleLabel(NETWORK_GROUP_DETAILS);
        return sendGet(sampleResult,
                       false,
                       endpoint,
                       NetworkGroupDetails.class);
    }
    
    @Override
    public NetworkGroupPlans getNetworkGroupPlans(SampleResult sampleResult,
                                                  long networkGroupId)
        throws GSException {
        String endpoint =
            MessageFormat.format(NETWORK_GROUP_PLANS, ""+networkGroupId);
        sampleResult.setSampleLabel(NETWORK_GROUP_PLANS);
        return sendGet(sampleResult, false, endpoint, NetworkGroupPlans.class);
    }

    @Override
    public NetworkGroupPlans getNetworkGroupPlansExt(SampleResult sampleResult, long networkGroupId, String countryCode) throws GSException {
        String endpoint = MessageFormat.format(NETWORK_GROUP_PLANS_EXT, ""+networkGroupId);
        sampleResult.setSampleLabel(NETWORK_GROUP_PLANS_EXT);

        return sendGet(sampleResult,
                false,
                endpoint,
                NetworkGroupPlans.class,
                NVPair.from("includeFreePlan", "false"),
                NVPair.from("countryCode", countryCode));
    }


    @Override
    public TransactionV4 buyPlan(SampleResult sampleResult,
                                 String customerId,
                                 String simId,
                                 PlanInput planInput)
        throws GSException {
        String endpoint = MessageFormat.format(BUY_PLAN, customerId, simId);
        sampleResult.setSampleLabel(BUY_PLAN);
        return sendPost(sampleResult,
                        false,
                        endpoint,
                        planInput,
                        TransactionV4.class);
    }

    @Override
    protected ServerType getServerType() {
        return ServerType.GIGSKY;
    }
}
