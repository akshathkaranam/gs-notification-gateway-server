package com.gigsky.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CreditCardInput {
    private String type;
    private String paymentMethod;
    private String nameOnCard;
    private String address1;
    private String city;
    private String state;
    private String country;
    private String zipCode;
    private String ccv;
    private String ccNumber;
    private String expMonth;
    private String expYear;
    private String firstName;
    private String lastName;
    private String phone;
    private String defaultBilling;
 
    public CreditCardInput() {
        type = "CreditCardBillingInfo";
        paymentMethod = "CreditCard";
        address1 = "address1";
        city = "Bangalore";
        state = "Karnataka";
        country = "IN";
        zipCode = "560000";
        ccv = "081";
        ccNumber = "4500000000000001";
        expMonth = "07";
        expYear = "2014";
        phone = "1234567890";
        defaultBilling = "true";
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getNameOnCard() {
        return nameOnCard;
    }

    public void setNameOnCard(String nameOnCard) {
        this.nameOnCard = nameOnCard;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCcv() {
        return ccv;
    }

    public void setCcv(String ccv) {
        this.ccv = ccv;
    }

    public String getCcNumber() {
        return ccNumber;
    }

    public void setCcNumber(String ccNumber) {
        this.ccNumber = ccNumber;
    }

    public String getExpMonth() {
        return expMonth;
    }

    public void setExpMonth(String expMonth) {
        this.expMonth = expMonth;
    }

    public String getExpYear() {
        return expYear;
    }

    public void setExpYear(String expYear) {
        this.expYear = expYear;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getDefaultBilling() {
        return defaultBilling;
    }

    public void setDefaultBilling(String defaultBilling) {
        this.defaultBilling = defaultBilling;
    }

    @Override
    public String toString() {
        return "CreditCardInput{" + "type=" + type + ", paymentMethod=" + paymentMethod + ", nameOnCard=" + nameOnCard + ", address1=" + address1 + ", city=" + city + ", state=" + state + ", country=" + country + ", zipCode=" + zipCode + ", ccv=" + ccv + ", ccNumber=" + ccNumber + ", expMonth=" + expMonth + ", expYear=" + expYear + ", firstName=" + firstName + ", lastName=" + lastName + ", phone=" + phone + ", defaultBilling=" + defaultBilling + '}';
    }
}
