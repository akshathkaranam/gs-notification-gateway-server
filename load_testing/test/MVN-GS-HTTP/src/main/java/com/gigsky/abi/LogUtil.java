package com.gigsky.abi;

import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class LogUtil {
    private static boolean configured;
    
    public static void printStackTrace(Exception e, Logger log) {
        String stackTrace = GSUtil.toString(e);
        log.error(stackTrace);
    }
    
    public static synchronized Properties configureLogging(
        String configFilePath) throws Exception {
        Properties retVal = null;
        
        if (!configured) {
            configured = true;
            Reader propertyInput = geAsReader(configFilePath);

            retVal = new Properties();

            retVal.load(propertyInput);

            PropertyConfigurator.configure(retVal);
        }
        
        return retVal;
    }
    
    private static Reader geAsReader(String filePath) throws IOException {
        Reader retVal = null;
        
        StringBuilder contentsBuffer = new StringBuilder();
        
        FileReader fr = null;
        
        try {
            fr = new FileReader(filePath);
            
            char[] buffer = new char[2048];

            int nRead = -1;
            
            while ((nRead = fr.read(buffer)) != -1) {
                contentsBuffer.append(buffer, 0, nRead);
            }

            String contents = contentsBuffer.toString();

            retVal = new StringReader(contents);
        } finally {
            if (fr != null) {
                try {
                    fr.close();
                } catch (IOException e) {
                    // Ignore it
                }
            }
        }
        
        return retVal;
    }
}
