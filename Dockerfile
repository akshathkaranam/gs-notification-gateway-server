FROM alpine
ARG VERSION
RUN apk update && apk upgrade && apk add openjdk8 && apk add busybox-extras && \
apk add mysql-client && apk add curl
WORKDIR  /var/lib/
RUN wget -c https://archive.apache.org/dist/tomcat/tomcat-8/v${VERSION}/bin/apache-tomcat-${VERSION}.tar.gz && \
tar xvzf apache-tomcat-${VERSION}.tar.gz && mv apache-tomcat-${VERSION} tomcat8
ADD ./gsnotificationgateway/target/gsngw.war /var/lib/tomcat8/webapps/
ADD ./start-tomcat.sh /var/
EXPOSE 8080