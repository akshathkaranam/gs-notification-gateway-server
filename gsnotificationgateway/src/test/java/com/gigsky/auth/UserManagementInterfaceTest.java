package com.gigsky.auth;

import com.gigsky.encryption.EncryptionFactory;
import com.gigsky.encryption.GigSkyEncryptor;
import com.gigsky.rest.bean.UserInfo;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * Created by anant on 29/10/15.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
public class UserManagementInterfaceTest {

    @Autowired
    private UserManagementInterface userManagementInterface;


    @Autowired
    EncryptionFactory encryptionFactory;

    private Map<String,Integer> userList = new HashMap<String,Integer>();

    private String getValidToken()
    {
        return "ABCDEFGH00001";
    }

    private String getInvalidToken()
    {
        return "XYZSDSDSDF0001";
    }

    @Before
    public void init() {

        userList.clear();
        userList.put("ABCDEFGH00001",1);
        userList.put("ABCDEFGH00002",2);
        userList.put("ABCDEFGH00003",3);
        userList.put("ABCDEFGH00004",4);
        userList.put("ABCDEFGH00005",5);
        userList.put("ABCDEFGH00006",6);
        userList.put("ABCDEFGH00007",7);
        userList.put("ABCDEFGH00008",8);

        userManagementInterface.initUserInfo(userList);




    }

    @After
    public void tearDown() throws Exception
    {
        userList.clear();
        userManagementInterface.clearUserInfo();
    }

    @Test
    public void testInvalidTokenForRegister() throws Exception{


        GigSkyEncryptor encryptor = encryptionFactory.getEncryptor(EncryptionFactory.LATEST_ENCRYPTION_VERSION);
        String email = encryptor.decryptString("iSo5Lwyl3t3hYIHlcX3cX9oE53HX09w+dHDLEZsnZvA4tiJ1qRkMjA==");
        String password = encryptor.decryptString("Gk+84ol0R6q126rU7tbGmRZS1SiJmQIB");

        boolean exceptionThrown = false;
        try{
            userManagementInterface.getUserInfo(getInvalidToken(), 1);
        }
        catch (Exception e)
        {
            if(e instanceof BackendRequestException)
            {
                exceptionThrown = true;
            }
        }

        Assert.assertTrue(exceptionThrown);
    }

    @Test
    public void testUnregisteredUser() throws Exception{
        UserInfo info = userManagementInterface.getUserInfo(getValidToken(), 1);
        Assert.assertNull(info.getNotificationId());

    }

    @Test
    public void testRegisterUser() throws Exception{
        UserInfo info = userManagementInterface.getUserInfo(getValidToken(), 1);
        Assert.assertNull(info.getNotificationId());

        // register new UUID
        info.setNotificationId(UUID.randomUUID().toString());
        userManagementInterface.updateUserInfo(info,getValidToken(), 1);

        // fetch again and see if it is set.
        info = userManagementInterface.getUserInfo(getValidToken(), 1);
        String regId = info.getNotificationId();
        Assert.assertNotNull(regId);
        Assert.assertTrue(regId.length() == 36);

    }

    @Test
    public void testRegisterUserWithInvalidToken() throws Exception{
        boolean exceptionThrown = false;

        try {
            UserInfo info = userManagementInterface.getUserInfo(getValidToken(), 1);
            info.setNotificationId(UUID.randomUUID().toString());
            userManagementInterface.updateUserInfo(info, getInvalidToken(), 1);
        }
        catch (Exception e)
        {
            if(e instanceof BackendRequestException)
            {
                exceptionThrown = true;
            }
        }
        Assert.assertTrue(exceptionThrown);
  }

}
