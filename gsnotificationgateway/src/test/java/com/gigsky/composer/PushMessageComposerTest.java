package com.gigsky.composer;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.*;
import com.gigsky.database.dao.EventDao;
import com.gigsky.database.dao.NotificationDao;
import com.gigsky.database.dao.TemplateDao;
import com.gigsky.event.BaseEvent;
import com.gigsky.event.CustomerEvent;
import com.gigsky.message.NotificationMessage;
import com.gigsky.notification.BaseNotification;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testSpringBeansConfig.xml" })
public class PushMessageComposerTest {

    @Autowired
    private TemplateDao templateDao;

    @Autowired
    private EventDao eventDao;

    @Autowired
    private NotificationDao notificationDao;

    @Autowired
    MessageComposer messageComposer;

    protected int testEventTypeId;
    protected int testNotificationTypeId;
    protected int testTemplateId;

    private BaseNotification baseNotification;
    private CustomerEvent customerEvent;

    public void initClass() throws Exception
    {
        //Create the Event Type required for running the test suite
        EventTypeDBBean testEventType = new EventTypeDBBean();
        testEventType.setDescription("Subscription Low balance");
        testEventType.setType(Configurations.EventType.TYPE_NEW_LOCATION);
        testEventType.setEnableNotification((byte) 1);
        testEventTypeId = eventDao.createEventType(testEventType);
        Assert.assertNotNull(testEventTypeId);

        customerEvent = new CustomerEvent();
        customerEvent.setEventType(testEventType.getType());
        customerEvent.setEventTypeId(testEventTypeId);

        JSONObject eventData = new JSONObject();
        eventData.put("balanceRemaining","1024");
        eventData.put("location","US");
        customerEvent.setEventData(eventData);
        customerEvent.setEventCategory(Configurations.CategoryType.CATEGORY_CUSTOMER);

        //Create Notification type for running the test suite
        NotificationTypeDBBean testNotificationType = new NotificationTypeDBBean();
        testNotificationType.setDescription("Push notification");
        testNotificationType.setType(BaseNotification.TYPE_PUSH);
        testNotificationTypeId = notificationDao.createNotificationType(testNotificationType);
        Assert.assertNotNull(testNotificationTypeId);

        baseNotification = new BaseNotification();
        baseNotification.setNotificationType(testNotificationType.getType());
        baseNotification.setNotificationTypeId(testNotificationTypeId);

        //Create Template
        TemplateDBBean templateDBBean = new TemplateDBBean();
        templateDBBean.setDescription("Push notification");
        templateDBBean.setContentType("PUSH");
        templateDBBean.setContent("{\"title\":\"Low Balance\",\"message\":\"Data balance is %s for location %s\"}");
        //templateDBBean.setContent("{\"title\":\"Nearing Expiry\",\"message\":\"Data balance is %s for location %s\"}");
        templateDBBean.setContent("{\"title\":\"New Location\",\"message\":\"Welcome to location %s\"}");

        templateDBBean.setLanguageCode("en");

        testTemplateId = templateDao.createTemplate(templateDBBean);
        Assert.assertNotNull(testTemplateId);

        //Create notification template entry
        templateDao.createTemplate(testEventTypeId, testNotificationTypeId, testTemplateId);
    }

    public void tearDownClass() throws  Exception {

        //Delete the event type created
        EventTypeDBBean testEventType = eventDao.getEventType(testEventTypeId);

        if (testEventType != null)
        {
            eventDao.deleteEventType(testEventType);
        }

        //Delete notification type
        NotificationTypeDBBean testNotificationType = notificationDao.getNotificationType(testNotificationTypeId);

        if (testNotificationType != null)
        {
            notificationDao.deleteNotificationType(testNotificationType);
        }

        //Delete template
        TemplateDBBean testTemplate = templateDao.getTemplateDetails(testTemplateId);

        if (testTemplate != null)
        {
            templateDao.deleteTemplate(testTemplate);
        }
    }

    @Before
    public void init() throws Exception{

        initClass();
    }

    @After
    public void tearDown() throws Exception {
        tearDownClass();
    }

    @Test
    public void testComposeMessage() throws MessageComposerException {
        String locale = getLocale();

        NotificationMessage msg= messageComposer.composeMessage(customerEvent, baseNotification, locale);
        Assert.assertNotNull(msg);
    }

    public String getLocale() {
        return "en";
    }
}