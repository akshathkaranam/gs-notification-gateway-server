package com.gigsky.database.dao;

import com.gigsky.database.bean.MockPushNotificationDBBean;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by vinayr on 23/11/15.
 */
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml" })
public class MockPushNotificationTest {

    @Autowired
    private MockPushNotificationDao mockPushNotificationDao;

    protected int testMockPushNotificationId;
    protected int testMockPushNotificationId2;
    protected int eventId = 100;


    @Before
    public void init() {

        MockPushNotificationDBBean mockPushNotificationDBBean = new MockPushNotificationDBBean();
        mockPushNotificationDBBean.setStatus("COMPLETE");
        mockPushNotificationDBBean.setLanguageCode("en");
        mockPushNotificationDBBean.setEventId(eventId);
        mockPushNotificationDBBean.setNotificationId(111);

        mockPushNotificationDao.createMockPushNotification(mockPushNotificationDBBean);
        Assert.assertNotNull(mockPushNotificationDBBean.getId());
        testMockPushNotificationId = mockPushNotificationDBBean.getId();

        MockPushNotificationDBBean mockPushNotificationDBBean2 = new MockPushNotificationDBBean();
        mockPushNotificationDBBean2.setStatus("COMPLETE");
        mockPushNotificationDBBean2.setLanguageCode("en");
        mockPushNotificationDBBean2.setEventId(eventId);
        mockPushNotificationDBBean2.setNotificationId(112);

        mockPushNotificationDao.createMockPushNotification(mockPushNotificationDBBean2);
        Assert.assertNotNull(mockPushNotificationDBBean2.getId());
        testMockPushNotificationId2 = mockPushNotificationDBBean2.getId();

    }

    @After
    public void tearDown() throws Exception {

        MockPushNotificationDBBean mockPushNotification = mockPushNotificationDao.getMockPushNotification(testMockPushNotificationId);

        if (mockPushNotification != null)
        {
            mockPushNotificationDao.deleteMockPushNotification(mockPushNotification);
        }

        MockPushNotificationDBBean mockPushNotification2 = mockPushNotificationDao.getMockPushNotification(testMockPushNotificationId2);

        if (mockPushNotification2 != null)
        {
            mockPushNotificationDao.deleteMockPushNotification(mockPushNotification2);
        }
    }

    @Test
    public void testCreateMockPushNotification() throws Exception {

        MockPushNotificationDBBean mockPushNotificationDBBean = new MockPushNotificationDBBean();
        mockPushNotificationDBBean.setStatus("COMPLETE");
        mockPushNotificationDBBean.setLanguageCode("en");
        mockPushNotificationDBBean.setEventId(123);
        mockPushNotificationDBBean.setNotificationId(111);

        mockPushNotificationDao.createMockPushNotification(mockPushNotificationDBBean);
        Assert.assertNotNull(mockPushNotificationDBBean.getId());
    }

    @Test
    public void testGetMockPushNotification() throws Exception {

        List<MockPushNotificationDBBean> mockPushNotificationDBBeanList = mockPushNotificationDao.getMockPushNotificationForEvent(eventId);
        Assert.assertNotNull(mockPushNotificationDBBeanList);
    }

}
