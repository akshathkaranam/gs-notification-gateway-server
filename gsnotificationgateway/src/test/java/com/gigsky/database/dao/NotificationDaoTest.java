package com.gigsky.database.dao;

import com.gigsky.database.bean.*;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;

/**
 * Created by vinayr on 23/10/15.
 */
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml" })
public class NotificationDaoTest {

    @Autowired
    private NotificationDao notificationDao;

    @Autowired
    private EventDao eventDao;

    @Autowired
    private CustomerDao customerDao;

    protected int testEventTypeId;
    protected int testEventId;
    protected String testCustomerUuid;
    protected int testNotificationTypeId;
    protected int testNotificationId;


    public void initClass() throws Exception
    {
        //Create the Event Type required for running the test suite
        EventTypeDBBean testEventType = new EventTypeDBBean();
        testEventType.setDescription("Subcription Low balance");
        testEventType.setType("LOW_BALANCE");
        testEventType.setEnableNotification((byte) 1);
        testEventTypeId = eventDao.createEventType(testEventType);
        Assert.assertNotNull(testEventTypeId);

        //Create the customer for running the test suite
        CustomerDBBean testCustomerDBBean = new CustomerDBBean();
        testCustomerDBBean.setCustomerType("CONSUMER");
        testCustomerUuid = "ac81d4fae-7dec-11d0-a765-00a0c91e6de1";
        testCustomerDBBean.setCustomerUuid(testCustomerUuid);
        testCustomerDBBean.setPreferences("{\"lang\":\"en\",\"event\":\"Low_Balance\"}");

        customerDao.createCustomerInfo(testCustomerDBBean);
        Assert.assertNotNull(testCustomerDBBean.getId());

        //Create event required for running the test suite
        EventDBBean testEvent = new EventDBBean();
        testEvent.setStatus("NEW");
        testEvent.setEventTypeId(testEventTypeId);
        testEvent.setEventCategory("SINGLE_EVENT");

        java.util.Date date = new java.util.Date();
        Timestamp newTimeStamp = new Timestamp(date.getTime());
        testEvent.setReceivedTime(newTimeStamp);

        String eventData = "{\"userId\":\"xyz@gigsky.com\",\"location\":\"US\"}";

        testEventId = eventDao.createEvent(testEvent, testCustomerUuid, eventData);
        Assert.assertNotNull(testEventId);


        //Create Notification type for running the test suite
        NotificationTypeDBBean testNotificationType = new NotificationTypeDBBean();
        testNotificationType.setDescription("Push notification");
        testNotificationType.setType("PUSH");
        testNotificationTypeId = notificationDao.createNotificationType(testNotificationType);
        Assert.assertNotNull(testNotificationTypeId);
    }

    public void tearDownClass() throws  Exception {
        //Delete the event created
        EventDBBean testEvent = eventDao.getEventDetails(testEventId, 1);

        if (testEvent != null)
        {
            eventDao.deleteEventDetails(testEvent);
        }

        //Delete the event type created
        EventTypeDBBean testEventType = eventDao.getEventType(testEventTypeId);

        if (testEventType != null)
        {
            eventDao.deleteEventType(testEventType);
        }

        //Delete the customer
        CustomerDBBean testCustomer = customerDao.getCustomerInfo(testCustomerUuid);

        if (testCustomer != null)
        {
            customerDao.deleteCustomerInfo(testCustomer);
        }

        //Delete notification type
        NotificationTypeDBBean testNotificationType = notificationDao.getNotificationType(testNotificationTypeId);

        if (testNotificationType != null)
        {
            notificationDao.deleteNotificationType(testNotificationType);
        }
    }


    @Before
    public void init() throws Exception{

        initClass();

        NotificationDBBean notificationDBBean = new NotificationDBBean();
        notificationDBBean.setStatus("NEW");
        notificationDBBean.setNotificationTypeId(testNotificationTypeId);
        notificationDBBean.setEventId(testEventId);


        java.util.Date date = new java.util.Date();
        Timestamp newTimeStamp = new Timestamp(date.getTime());
        notificationDBBean.setExpiry(newTimeStamp);

        testNotificationId = notificationDao.createNotification(notificationDBBean);
        Assert.assertNotNull(testNotificationId);
    }

    @After
    public void tearDown() throws Exception {

        //Delete notification
        NotificationDBBean testNotification = notificationDao.getNotificationDetails(testNotificationId);

        if (testNotification != null)
        {
            notificationDao.deleteNotification(testNotification);
        }

        tearDownClass();
    }

    @Test
    public void testCreateNotification() throws Exception {

        NotificationDBBean testNotification = getNotificationData();
        notificationDao.createNotification(testNotification);
        Assert.assertNotNull(testNotification.getId());

    }

    @Test
    public void testGetNotificationDetails() throws Exception {

        NotificationDBBean notification = notificationDao.getNotificationDetails(testNotificationId);
        Assert.assertNotNull(notification);
    }

    @Test
    public void testUpdateNotification() throws Exception {

        NotificationDBBean notification = notificationDao.getNotificationDetails(testNotificationId);
        Assert.assertNotNull(notification);

        notification.setStatus("IN_PROCESS");
        notificationDao.updateNotification(notification);

        NotificationDBBean notification2 = notificationDao.getNotificationDetails(testNotificationId);
        Assert.assertEquals("IN_PROCESS", notification2.getStatus());
    }

    @Test
    public void testDeleteNotification() throws Exception {

        NotificationDBBean notification = notificationDao.getNotificationDetails(testNotificationId);
        Assert.assertNotNull(notification);

        notificationDao.deleteNotification(notification);

        NotificationDBBean notification2 = notificationDao.getNotificationDetails(testNotificationId);
        Assert.assertNull(notification2);
    }


    public NotificationDBBean getNotificationData() {

        NotificationDBBean notificationDBBean = new NotificationDBBean();
        notificationDBBean.setStatus("NEW");
        notificationDBBean.setNotificationTypeId(testNotificationTypeId);
        notificationDBBean.setEventId(testEventId);

        java.util.Date date = new java.util.Date();
        Timestamp newTimeStamp = new Timestamp(date.getTime());
        notificationDBBean.setExpiry(newTimeStamp);

        return notificationDBBean;
    }
}
