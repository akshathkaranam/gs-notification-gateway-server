package com.gigsky.database.dao;

import com.gigsky.database.bean.EventTypeDBBean;
import com.gigsky.database.bean.NotificationTypeDBBean;
import com.gigsky.database.bean.TemplateDBBean;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by vinayr on 23/10/15.
 */
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml" })
public class TemplateDaoTest {

    @Autowired
    private TemplateDao templateDao;

    @Autowired
    private EventDao eventDao;

    @Autowired
    private NotificationDao notificationDao;

    protected int testEventTypeId;
    protected int testNotificationTypeId;
    protected int testTemplateId;

    public void initClass()
    {
        //Create the Event Type required for running the test suite
        EventTypeDBBean testEventType = new EventTypeDBBean();
        testEventType.setDescription("Subcription Low balance");
        testEventType.setType("LOW_BALANCE");
        testEventType.setEnableNotification((byte) 1);
        testEventTypeId = eventDao.createEventType(testEventType);
        Assert.assertNotNull(testEventTypeId);

        //Create Notification type for running the test suite
        NotificationTypeDBBean testNotificationType = new NotificationTypeDBBean();
        testNotificationType.setDescription("Push notification");
        testNotificationType.setType("PUSH");
        testNotificationTypeId = notificationDao.createNotificationType(testNotificationType);
        Assert.assertNotNull(testNotificationTypeId);

        //Create Template
        TemplateDBBean templateDBBean = new TemplateDBBean();
        templateDBBean.setDescription("Push notification");
        templateDBBean.setContentType("PUSH");
        templateDBBean.setContent("{\"title\":\"xyz@gigsky.com\",\"message\":\"US\"}");
        templateDBBean.setLanguageCode("en");

        testTemplateId = templateDao.createTemplate(templateDBBean);
        Assert.assertNotNull(testTemplateId);

        //Create notification template entry
        templateDao.createTemplate(testEventTypeId, testNotificationTypeId, testTemplateId);
    }

    public void tearDownClass() throws  Exception {

        //Delete the event type created
        EventTypeDBBean testEventType = eventDao.getEventType(testEventTypeId);

        if (testEventType != null)
        {
            eventDao.deleteEventType(testEventType);
        }

        //Delete notification type
        NotificationTypeDBBean testNotificationType = notificationDao.getNotificationType(testNotificationTypeId);

        if (testNotificationType != null)
        {
            notificationDao.deleteNotificationType(testNotificationType);
        }

        //Delete template
        TemplateDBBean testTemplate = templateDao.getTemplateDetails(testTemplateId);

        if (testTemplate != null)
        {
            templateDao.deleteTemplate(testTemplate);
        }
    }


    @Before
    public void init() {

        initClass();
    }

    @After
    public void tearDown() throws Exception {
        tearDownClass();
    }

    @Test
    public void testGetTemplate() throws Exception {

        String locale = getLocale();

        TemplateDBBean testTemplate = templateDao.getTemplate(testEventTypeId, testNotificationTypeId, 1, locale);
        Assert.assertNotNull(testTemplate);
    }

    public String getLocale() {
        return "en";
    }
}
