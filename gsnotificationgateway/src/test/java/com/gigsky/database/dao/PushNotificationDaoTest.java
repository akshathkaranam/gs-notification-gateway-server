package com.gigsky.database.dao;

import com.gigsky.database.bean.*;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;

/**
 * Created by vinayr on 23/10/15.
 */
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath*:testDatabaseConfig.xml" })
public class PushNotificationDaoTest {

    @Autowired
    private PushNotificationDao pushNotificationDao;

    @Autowired
    private EventDao eventDao;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private NotificationDao notificationDao;

    @Autowired
    private TemplateDao templateDao;

    protected int testEventTypeId;
    protected int testEventId;
    protected String testCustomerUuid;
    protected int testNotificationTypeId;
    protected int testNotificationId;
    protected int testTemplateId;
    protected int testPushNotificationId;


    public void initClass() throws Exception
    {
        //Create the Event Type required for running the test suite
        EventTypeDBBean testEventType = new EventTypeDBBean();
        testEventType.setDescription("Subcription Low balance");
        testEventType.setType("LOW_BALANCE");
        testEventType.setEnableNotification((byte) 1);
        testEventTypeId = eventDao.createEventType(testEventType);
        Assert.assertNotNull(testEventTypeId);

        //Create the customer for running the test suite
        CustomerDBBean testCustomerDBBean = new CustomerDBBean();
        testCustomerDBBean.setCustomerType("CONSUMER");
        testCustomerUuid = "ac81d4fae-7dec-11d0-a765-00a0c91e6de1";
        testCustomerDBBean.setCustomerUuid(testCustomerUuid);
        testCustomerDBBean.setPreferences("{\"lang\":\"en\",\"event\":\"Low_Balance\"}");

        customerDao.createCustomerInfo(testCustomerDBBean);
        Assert.assertNotNull(testCustomerDBBean.getId());

        //Create event required for running the test suite
        EventDBBean testEvent = new EventDBBean();
        testEvent.setStatus("NEW");
        testEvent.setEventTypeId(testEventTypeId);
        testEvent.setEventCategory("SINGLE_EVENT");

        java.util.Date date = new java.util.Date();
        Timestamp newTimeStamp = new Timestamp(date.getTime());
        testEvent.setReceivedTime(newTimeStamp);

        String eventData = "{\"userId\":\"xyz@gigsky.com\",\"location\":\"US\"}";

        testEventId = eventDao.createEvent(testEvent, testCustomerUuid, eventData);
        Assert.assertNotNull(testEventId);


        //Create Notification type for running the test suite
        NotificationTypeDBBean testNotificationType = new NotificationTypeDBBean();
        testNotificationType.setDescription("Push notification");
        testNotificationType.setType("PUSH");
        testNotificationTypeId = notificationDao.createNotificationType(testNotificationType);
        Assert.assertNotNull(testNotificationTypeId);

        //Create Notification
        NotificationDBBean notificationDBBean = new NotificationDBBean();
        notificationDBBean.setStatus("NEW");
        notificationDBBean.setNotificationTypeId(testNotificationTypeId);
        notificationDBBean.setEventId(testEventId);
        notificationDBBean.setExpiry(newTimeStamp);

        testNotificationId = notificationDao.createNotification(notificationDBBean);
        Assert.assertNotNull(testNotificationId);

        //Create Template
        TemplateDBBean templateDBBean = new TemplateDBBean();
        templateDBBean.setDescription("Push notification");
        templateDBBean.setContentType("PUSH");
        templateDBBean.setContent("{\"title\":\"xyz@gigsky.com\",\"message\":\"US\"}");
        templateDBBean.setLanguageCode("en");

        testTemplateId = templateDao.createTemplate(templateDBBean);
        Assert.assertNotNull(testTemplateId);
    }

    public void tearDownClass() throws  Exception {
        //Delete the event created
        EventDBBean testEvent = eventDao.getEventDetails(testEventId, 1);

        if (testEvent != null)
        {
            eventDao.deleteEventDetails(testEvent);
        }

        //Delete the event type created
        EventTypeDBBean testEventType = eventDao.getEventType(testEventTypeId);

        if (testEventType != null)
        {
            eventDao.deleteEventType(testEventType);
        }

        //Delete the customer
        CustomerDBBean testCustomer = customerDao.getCustomerInfo(testCustomerUuid);

        if (testCustomer != null)
        {
            customerDao.deleteCustomerInfo(testCustomer);
        }

        //Delete notification type
        NotificationTypeDBBean testNotificationType = notificationDao.getNotificationType(testNotificationTypeId);

        if (testNotificationType != null)
        {
            notificationDao.deleteNotificationType(testNotificationType);
        }

        //Delete notification
        NotificationDBBean testNotification = notificationDao.getNotificationDetails(testNotificationId);

        if (testNotification != null)
        {
            notificationDao.deleteNotification(testNotification);
        }

        //Delete template
        TemplateDBBean testTemplate = templateDao.getTemplateDetails(testTemplateId);

        if (testTemplate != null)
        {
            templateDao.deleteTemplate(testTemplate);
        }

    }

    @Before
    public void init() throws Exception{

        initClass();

        PushNotificationDBBean testPushNotification = new PushNotificationDBBean();
        testPushNotification.setPushId("9d78a53b-b16a-c58f-b78d-181d5e242078");
        testPushNotification.setStatus("NEW");

        java.util.Date date = new java.util.Date();
        Timestamp newTimeStamp = new Timestamp(date.getTime());
        testPushNotification.setPushTime(newTimeStamp);

        testPushNotification.setIccId("8910300000000000000");
        testPushNotification.setLocation("US");
        testPushNotification.setLanguageCode("en");
        testPushNotification.setNotificationId(testNotificationId);
        testPushNotification.setTemplateId(testTemplateId); //Id should be already in DB
        testPushNotification.setNotificationCategory("SINGLE_NOTIFICATION");
        testPushNotification.setLastAttemptStatus("SUCCESS");
        testPushNotification.setLastAttemptTime(newTimeStamp);
        testPushNotification.setStatusMessage("New notification");
        testPushNotification.setOpenCount(1);
        testPushNotification.setAttemptCount(1);
        testPushNotification.setSendCount(1);

        testPushNotificationId = pushNotificationDao.createPushNotification(testPushNotification);
        Assert.assertNotNull(testPushNotificationId);
    }

    @After
    public void tearDown() throws Exception {

        //Delete push notification
        PushNotificationDBBean testPushNotification = pushNotificationDao.getPushNotificationDetails(testPushNotificationId);

        if (testPushNotification != null)
        {
            pushNotificationDao.deletePushNotification(testPushNotification);
        }

        tearDownClass();
    }

    @Test
    public void testCreatePushNotification() throws Exception {

        PushNotificationDBBean testPushNotification = getPushNotificationData();
        pushNotificationDao.createPushNotification(testPushNotification);
        Assert.assertNotNull(testPushNotification.getId());
    }

    @Test
    public void testGetPushNotification() throws Exception {

        PushNotificationDBBean pushNotification = pushNotificationDao.getPushNotificationDetails(testPushNotificationId);
        Assert.assertNotNull(pushNotification);
    }

    @Test
    public void testUpdatePushNotification() throws Exception {

        PushNotificationDBBean pushNotification = pushNotificationDao.getPushNotificationDetails(testPushNotificationId);
        Assert.assertNotNull(pushNotification);

        pushNotification.setStatus("IN_PROCESS");
        pushNotificationDao.updatePushNotification(pushNotification);

        PushNotificationDBBean pushNotification2 = pushNotificationDao.getPushNotificationDetails(testPushNotificationId);
        Assert.assertEquals("IN_PROCESS", pushNotification2.getStatus());
    }

    public PushNotificationDBBean getPushNotificationData() {

        PushNotificationDBBean testPushNotification = new PushNotificationDBBean();
        testPushNotification.setPushId("9d78a53b-b16a-c58f-b78d-181d5e242078");
        testPushNotification.setStatus("NEW");

        java.util.Date date = new java.util.Date();
        Timestamp newTimeStamp = new Timestamp(date.getTime());
        testPushNotification.setPushTime(newTimeStamp);

        testPushNotification.setIccId("8910300000000000000");
        testPushNotification.setLocation("US");
        testPushNotification.setLanguageCode("en");
        testPushNotification.setNotificationId(testNotificationId);
        testPushNotification.setTemplateId(testTemplateId);
        testPushNotification.setNotificationCategory("SINGLE_NOTIFICATION");
        testPushNotification.setLastAttemptStatus("SUCCESS");
        testPushNotification.setLastAttemptTime(newTimeStamp);
        testPushNotification.setStatusMessage("New notification");
        testPushNotification.setOpenCount(1);
        testPushNotification.setAttemptCount(1);
        testPushNotification.setSendCount(1);

        return testPushNotification;
    }
}
