package com.gigsky.policy;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.EventDBBean;
import com.gigsky.database.bean.EventTypeDBBean;
import com.gigsky.database.dao.EventDao;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Iterator;
import java.util.List;

/**
 * Created by vinayr on 21/12/15.
 */
public class OutOfOrderCheckPolicy extends BasePolicy {

    private static final Logger logger = LoggerFactory.getLogger(OutOfOrderCheckPolicy.class);

    public static final int MAX_EVENT_COUNT = 10;

    @Autowired
    EventDao eventDao;

    public boolean isOutOfOrder(EventDBBean newEvent)
    {
        EventTypeDBBean eventTypeDBBean =  eventDao.getEventType(newEvent.getEventTypeId());
        String eventType = eventTypeDBBean.getType();

        //Check if the event is of type Data_used or Subscription_nearing_expiry or new location. Other events order is not considered
        if (!(Configurations.EventType.TYPE_DATA_USED.equals(eventType)) &&
                !(Configurations.EventType.TYPE_SUBSCRIPTION_NEARING_EXPIRY.equals(eventType)) &&
                !(Configurations.EventType.TYPE_NEW_LOCATION.equals(eventType)))
        {
            return false;
        }

        try
        {
            //Get the events from the DB for the same iccid and Location
            String data = newEvent.getCustomerEvent().getData();
            JSONObject jsonData = new JSONObject(data);
            String location = jsonData.getString("location");
            String iccid = jsonData.getString("iccId");

            EventTypeDBBean queryEventTypeDBBean = null;

            if (Configurations.EventType.TYPE_DATA_USED.equals(eventType))
            {
                //Get the low balance events and check which one was already sent before data used event
                queryEventTypeDBBean =  eventDao.getEventType(Configurations.EventType.TYPE_LOW_BALANCE);
            }

            else if (Configurations.EventType.TYPE_SUBSCRIPTION_NEARING_EXPIRY.equals(eventType))
            {
                //Get the low balance events and check which one was already sent before data used event
                queryEventTypeDBBean =  eventDao.getEventType(Configurations.EventType.TYPE_SUBSCRIPTION_EXPIRED);
            }

            else if (Configurations.EventType.TYPE_NEW_LOCATION.equals(eventType))
            {
                //Get the new location events and check which one was already sent before
                queryEventTypeDBBean =  eventDao.getEventType(Configurations.EventType.TYPE_NEW_LOCATION);
            }

            if (queryEventTypeDBBean == null)
            {
                return false;
            }

            //List<EventDBBean> eventList = eventDao.getEventsForEventType(queryEventTypeDBBean.getId());

            //Get the lastest max count events of an event type for an iccid
            List<EventDBBean> eventList = eventDao.getEventsForEventTypeAndIccid(queryEventTypeDBBean.getId(), iccid, MAX_EVENT_COUNT);


            for(Iterator<EventDBBean> it = eventList.iterator(); it.hasNext(); )
            {
                EventDBBean eventDBBean = it.next();

                if (Configurations.StatusType.STATUS_NEW.equals(eventDBBean.getStatus()))
                {
                    //Skip the events which are marked as NEW
                    continue;
                }

                //Get the events from the DB for the same iccid and Location
                String tempEventData = eventDBBean.getCustomerEvent().getData();

                JSONObject tempJsonData = new JSONObject(tempEventData);

                String tempLocation = tempJsonData.getString("location");
                //String tempIccid = tempJsonData.getString("iccId");

                //Check if it belongs to the same iccid and location
                if (location.equals(tempLocation))
                {
                    //Check the creationTime
                    if (newEvent.getCreationTime().getTime() <= eventDBBean.getCreationTime().getTime())
                    {
                        return true;
                    }
                }
            }
        }
        catch (JSONException e)
        {
            logger.warn("json object data creation  Exception:" + e + " for Event Id: " + newEvent.getId());
        }

        return false;
    }

}
