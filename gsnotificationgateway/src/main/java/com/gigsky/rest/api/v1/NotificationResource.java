package com.gigsky.rest.api.v1;

import com.gigsky.database.dao.ConfigurationKeyValuesDao;
import com.gigsky.database.dao.TenantDao;
import com.gigsky.exception.ResourceValidationException;
import com.gigsky.exception.ServiceValidationException;
import com.gigsky.rest.bean.Notification;
import com.gigsky.service.NotificationManagementService;
import com.gigsky.service.ValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import util.GSUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by anant on 23/11/15.
 */
@Path("/api/v1/notifications")
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Controller
public class NotificationResource {

    @Autowired
    private ValidationService validationService;

    @Autowired
    private NotificationManagementService notificationManagementService;

    @Autowired
    private TenantDao tenantDao;

    @Autowired
    private ConfigurationKeyValuesDao configurationKeyValuesDao;

    @GET
    @Path("/{notificationId}")
    public Response getNotification(@PathParam("notificationId") long notificationId, @Context HttpHeaders httpHeaders) throws ResourceValidationException, ServiceValidationException {

        int tenantId = GSUtil.extractTenantId(httpHeaders, validationService, tenantDao, configurationKeyValuesDao);

        validationService.validateId((int)notificationId);
        Notification notification = notificationManagementService.getNotification(notificationId, tenantId);
        return Response.status(Response.Status.OK).entity(notification).build();
    }

}
