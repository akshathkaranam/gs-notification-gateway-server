package com.gigsky.rest.api.v1;

import com.gigsky.common.Configurations;
import com.gigsky.database.exception.DatabaseException;
import com.gigsky.exception.PartnerAppException;
import com.gigsky.exception.ResourceValidationException;
import com.gigsky.exception.ServiceValidationException;
import com.gigsky.rest.bean.PartnerAppDetails;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.service.PartnerAppService;
import com.gigsky.service.ValidationService;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Created by vinayr on 15/04/16.
 */
@Path("/api/v1/partnerApp")
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Controller
public class PartnerAppResource {

    @Autowired
    private PartnerAppService partnerAppService;

    @Autowired
    private ValidationService validationService;

    @GET
    @Path("/redirect")
    public Response registerDeviceDetails(@QueryParam("deviceType") String deviceType,
                                          @QueryParam("ipAddress") String ipAddress,
                                          @QueryParam("deviceOSVersion") String deviceOSVersion,
                                          @QueryParam("locale") String locale,
                                          @QueryParam("iccId") String iccId,
                                          @QueryParam("location") String location,
                                          @QueryParam("partnerCode") String partnerCode,
                                          @QueryParam("sdkVersion") String sdkVersion) throws PartnerAppException, ServiceValidationException, DatabaseException, JSONException, URISyntaxException {


        PartnerAppDetails partnerAppDetails = new PartnerAppDetails(deviceType, ipAddress, deviceOSVersion,
                locale, iccId, location, partnerCode, sdkVersion);
        validateDeviceDetails(partnerAppDetails, true);

        partnerAppService.registerDevice(partnerAppDetails);

        URI storePath;

        if (Configurations.ANDROID_TYPE.equals(deviceType))
        {
            storePath = new URI("https://play.google.com/store/apps/details?id=com.gigsky.gigsky");
        }
        else
        {
            storePath = new URI("https://itunes.apple.com/WebObjects/MZStore.woa/wa/viewSoftware?id=590458648&mt=8");
        }

        return Response.status(Response.Status.TEMPORARY_REDIRECT).location(storePath).build();
    }

    @GET
    @Path("/deeplink")
    public Response getDeviceDetails(@QueryParam("deviceType") String deviceType,
                                     @QueryParam("ipAddress") String ipAddress,
                                     @QueryParam("deviceOSVersion") String deviceOSVersion,
                                     @QueryParam("locale") String locale) throws ResourceValidationException, ServiceValidationException, DatabaseException, JSONException, PartnerAppException {

        PartnerAppDetails partnerAppDetails = new PartnerAppDetails(deviceType, ipAddress, deviceOSVersion,
                locale, null, null, null, null);
        validateDeviceDetails(partnerAppDetails, false);

        partnerAppDetails = partnerAppService.getDevice(partnerAppDetails);

        return Response.status(Response.Status.OK).entity(partnerAppDetails).build();
    }


    private void validateDeviceDetails(PartnerAppDetails partnerAppDetails, Boolean isRegister) throws PartnerAppException, ServiceValidationException {

        if (partnerAppDetails.getDeviceType() == null ||
                (!(Configurations.ANDROID_TYPE.equals(partnerAppDetails.getDeviceType()) ||
                        Configurations.IOS_TYPE.equals(partnerAppDetails.getDeviceType())))) {
            throw new PartnerAppException(ErrorCode.PARTNER_APP_DEVICE_REGISTRATION_REQUIRES_VALID_DEVICE_TYPE, "Invalid device type");
        }

        validationService.checkStringParam(partnerAppDetails.getIpAddress(),1,Configurations.MAX_IP_ADDRESS_LENGTH, Configurations.CommonType.IP_ADDRESSS_LENGTH);
        validationService.checkStringParam(partnerAppDetails.getDeviceOSVersion(),1,Configurations.MAX_OS_VERSION_LENGTH, Configurations.CommonType.PARTNER_APP_OS_VERSION);
        validationService.checkStringParam(partnerAppDetails.getLocale(),1,Configurations.MAX_LOCALE_LENGTH, Configurations.CommonType.PARTNER_APP_LOCALE);

        if(isRegister)
        {
            if((partnerAppDetails.getIccId() != null) && (!partnerAppDetails.getIccId().isEmpty())){
                validationService.checkStringParam(partnerAppDetails.getIccId(), 1, Configurations.MAX_ICCID_LENGTH, Configurations.CommonType.PARTNER_APP_ICCID_LENGTH);
            }

            if((partnerAppDetails.getLocation() != null) && (!partnerAppDetails.getLocation().isEmpty())) {
                validationService.checkStringParam(partnerAppDetails.getLocation(),1,Configurations.MAX_LOCATION_LENGTH, Configurations.CommonType.PARTNER_APP_LOCATION);
            }

            validationService.checkStringParam(partnerAppDetails.getPartnerCode(), 1, Configurations.MAX_PARTNER_CODE_LENGTH, Configurations.CommonType.PARTNER_APP_CODE);
            validationService.checkStringParam(partnerAppDetails.getSdkVersion(), 1, Configurations.MAX_SDK_VERSION_LENGTH, Configurations.CommonType.PARTNER_APP_SDK);
        }
    }

}


