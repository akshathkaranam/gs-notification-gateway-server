package com.gigsky.rest.api.v1;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.dao.ConfigurationKeyValuesDao;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.database.dao.TenantDao;
import com.gigsky.database.exception.DatabaseException;
import com.gigsky.exception.PartnerAppException;
import com.gigsky.exception.ResourceValidationException;
import com.gigsky.exception.ServiceValidationException;
import com.gigsky.rest.bean.Event;
import com.gigsky.rest.bean.Option;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.service.EventManagementService;
import com.gigsky.service.ValidationService;
import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import util.GSUtil;

import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by anant on 18/11/15.
 */
@Path("/api/v1/events")
@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Consumes(MediaType.APPLICATION_JSON + ";charset=UTF-8")
@Controller
public class EventResource {

    @Autowired
    private EventManagementService eventManagementService;

    @Autowired
    private ValidationService validationService;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private TenantDao tenantDao;

    @Autowired
    private ConfigurationKeyValuesDao configurationKeyValuesDao;

    private String[] supportedCurrencies = {"USD", "GBP", "JPY", "EUR"};

    private static final Logger logger = LoggerFactory.getLogger(EventResource.class);

    @POST
    public Response addEvent(Event event, @Context HttpHeaders httpHeaders) throws ResourceValidationException, ServiceValidationException, DatabaseException, JSONException, PartnerAppException
    {
        // authentication ??

        int tenantId = GSUtil.extractTenantId(httpHeaders, validationService, tenantDao, configurationKeyValuesDao);

        validateEvent(event);
        Event output = eventManagementService.addEvent(event, tenantId);

        return Response.status(Response.Status.OK).entity(output).build();
    }

    @GET
    @Path("/{eventId}")
    public Response getEvent(@PathParam("eventId") long eventId, @Context HttpHeaders httpHeaders) throws ResourceValidationException, ServiceValidationException, DatabaseException, JSONException {

        int tenantId = GSUtil.extractTenantId(httpHeaders, validationService, tenantDao, configurationKeyValuesDao);

        validationService.validateId((int)eventId);
        Event output = eventManagementService.getEvent(eventId, tenantId);

        return Response.status(Response.Status.OK).entity(output).build();
    }


    private void validateEvent(Event event) throws ResourceValidationException, ServiceValidationException, PartnerAppException
    {
        if(event.getType() == null || !(event.getType().equals("EventDetail")))
        {
            logger.error("EventResource validateEvent Invalid event type");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_TYPE, "Invalid event type");
        }

        String eventType = event.getEventType();
        validateEventType(eventType);

        if(Configurations.EventType.TYPE_EMAIL.equals(event.getEventType()))
        {
            String subEventType = event.getSubEventType();
            validateSubEventType(subEventType);
            validateLanguage(event.getLanguage());
            validateEmailIds(event.getEmailIds());

            // Rabbit MQ related emails not required any options
            if(!Configurations.SubEventType.TYPE_RABBIT_MQ_SERVER_UP.equals(subEventType) &&
                    !Configurations.SubEventType.TYPE_RABBIT_MQ_SERVER_DOWN.equals(subEventType) &&
                    !Configurations.SubEventType.TYPE_DELETED_ACCOUNTS_THRESHOLD.equals(subEventType)){

                validateOptions(subEventType, event.getOptions());
            }
        } else
        {
            validateCustomerId(event.getCustomerId());

            validateEmailId(event.getUserId());
            validateLocation(event.getLocation());
            validateOptions(event.getOptions());
            validationService.checkStringParam(event.getIccId(),1, Configurations.MAX_ICCID_LENGTH, Configurations.CommonType.ICCID_LENGTH);
            validationService.checkStringParam(event.getCreationTime(), 1, Configurations.MAX_EXPIRY_LENGTH, Configurations.CommonType.CREATION_LENGTH);


            if (!Configurations.EventType.TYPE_NEW_LOCATION.equals(eventType)) {
                validationService.checkStringParam(event.getExpiry(), 1, Configurations.MAX_EXPIRY_LENGTH, Configurations.CommonType.EXPIRY_LENGTH);
            }
            else
            {
                //Expiry is not passed for new location
                if ((event.getExpiry() != null) && (!event.getExpiry().isEmpty()))
                {
                    logger.error("EventResource validateEvent Invalid type");
                    throw new ResourceValidationException(ErrorCode.INVALID_ARGUMENTS, "Invalid type");
                }
            }

            if(Configurations.EventType.TYPE_LOW_BALANCE.equals(eventType) ||
                    Configurations.EventType.TYPE_SUBSCRIPTION_NEARING_EXPIRY.equals(event.getEventType()))
            {
                validateBalanceRemaining(event.getBalanceRemaining());
            }
            else
            {
                //Balance remaining is passed only for low balance and subscription nearing expiry types
                if (event.getBalanceRemaining() != null)
                {
                    logger.error("EventResource validateEvent Invalid type");
                    throw new ResourceValidationException(ErrorCode.INVALID_ARGUMENTS, "Invalid type");
                }
            }

            if (Configurations.EventType.TYPE_DATA_USED.equals(eventType)) {
                validatePercentageConsumed(event.getPercentageConsumed());
            }
            else
            {
                //Percentage consumed is passed only for Data used type
                if (event.getPercentageConsumed() != null)
                {
                    logger.error("EventResource validateEvent Invalid type");
                    throw new ResourceValidationException(ErrorCode.INVALID_ARGUMENTS, "Invalid type");
                }
            }
        }

    }

    private static void validateLocation(String aLocation) throws ResourceValidationException {
        if (StringUtils.isEmpty(aLocation)) {
            logger.error("EventResource validateLocation Invalid type");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_LOCATION, "Invalid type");
        }
    }

    private static void validateEmailIds(List<String> emailIds) throws ResourceValidationException {

        if(emailIds != null && emailIds.size() > 0)
        {
            for(int i = 0; i < emailIds.size(); i++)
            {
                String emailId = emailIds.get(i);
                if (StringUtils.isEmpty(emailId) || ! isEmailValid(emailId)) {
                    logger.error("EventResource validateEmailId Invalid type");
                    throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_USER_ID, "Invalid type");
                }
            }
        } else
        {
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_USER_ID, "Invalid type");
        }

    }

    //TODO: Add Email Id validation
    private static void validateEmailId(String aEmailId) throws ResourceValidationException {
        if (StringUtils.isEmpty(aEmailId)) {
            logger.error("EventResource validateEmailId Invalid type");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_USER_ID, "Invalid type");
        }
    }

    private static boolean isEmailValid(String emailId)
    {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(emailId);
        return matcher.matches();
    }

    private static void validateLanguage(String language) throws ResourceValidationException {
        if (StringUtils.isEmpty(language)) {
            logger.error("EventResource validateLanguage Invalid type");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_LANGUAGE, "Invalid language");
        }
    }

    private static void validateEventType(String eventType) throws ResourceValidationException {

        if (!(Configurations.EventType.TYPE_LOW_BALANCE.equals(eventType) ||
                Configurations.EventType.TYPE_DATA_USED.equals(eventType) ||
                Configurations.EventType.TYPE_NEW_LOCATION.equals(eventType) ||
                Configurations.EventType.TYPE_SUBSCRIPTION_EXPIRED.equals(eventType) ||
                Configurations.EventType.TYPE_SUBSCRIPTION_NEARING_EXPIRY.equals(eventType) ||
                Configurations.EventType.TYPE_EMAIL.equals(eventType)))
        {
            logger.error("EventResource validateEventType  Invalid type");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_EVENT_TYPE, "Invalid type");
        }

    }

    private static void validateSubEventType(String subEventType) throws ResourceValidationException {

        if (!(Configurations.SubEventType.TYPE_REFERRAL_INVITE.equals(subEventType) ||
                Configurations.SubEventType.TYPE_NEW_CUSTOMER_SIGNUP.equals(subEventType) ||
                Configurations.SubEventType.TYPE_CREDIT_ADDED.equals(subEventType) ||
                Configurations.SubEventType.TYPE_CREDIT_ALERT.equals(subEventType) ||
                Configurations.SubEventType.TYPE_CREDIT_LIMIT.equals(subEventType) ||
                Configurations.SubEventType.TYPE_CREDIT_WARNING_INTERNAL.equals(subEventType) ||
                Configurations.SubEventType.TYPE_CREDIT_DEDUCT.equals(subEventType) ||
                Configurations.SubEventType.TYPE_ICCID_PROMOTION_CREDIT.equals(subEventType) ||
                Configurations.SubEventType.TYPE_RABBIT_MQ_SERVER_UP.equals(subEventType) ||
                Configurations.SubEventType.TYPE_RABBIT_MQ_SERVER_DOWN.equals(subEventType) ||
                Configurations.SubEventType.TYPE_DELETED_ACCOUNTS_THRESHOLD.equals(subEventType)))
        {
            logger.error("EventResource validateSubEventType  Invalid sub event type");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_SUB_EVENT_TYPE, "Invalid sub event type");
        }

    }

    private static void validateBalanceRemaining(Integer balanceRemaining) throws ResourceValidationException {

        if (balanceRemaining == null || balanceRemaining <= 0)
        {
            logger.error("EventResource validateBalanceRemaining Invalid type");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_BALANCE_REMAINING, "Invalid type");
        }
    }

    private static void validatePercentageConsumed(Integer percentageConsumed) throws ResourceValidationException {

        if (percentageConsumed == null || percentageConsumed <= 0 || percentageConsumed > 100)
        {
            logger.error("EventResource validatePercentageConsumed Invalid type");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_PERCENTAGE_CONSUMED, "Invalid type");
        }
    }

    private void validateCustomerId(String customerId) throws ResourceValidationException, ServiceValidationException {
        validationService.validateRegistrationId(customerId);

        //Check if the customer UUID is present in the DB
        CustomerDBBean customerDBBean = customerDao.getCustomerInfo(customerId);

        if (customerDBBean == null)
        {
            logger.error("EventResource validateCustomerId Invalid customer Uuid");
            throw new ResourceValidationException(ErrorCode.INVALID_CUSTOMER_UUID, "Invalid customer Uuid");
        }
    }

    private void validateOptions(Option options) throws ResourceValidationException
    {
        if(options == null)
        {
            logger.error("EventResource validateOptions Invalid options");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_OPTIONS, "Invalid options");
        } else
        {
            validateSIMName(options);
        }
    }

    private void validateSIMName(Option options) throws ResourceValidationException
    {
         if(options.getSimName() != null)
         {
            String simName = options.getSimName();
            if(simName.length() > Configurations.MAX_SIM_NAME_LENGTH)
            {
                logger.error("EventResource validateSIMName Invalid SIM name");
                throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_SIM_NAME, "Invalid SIM name");
            }
         }
    }

    private void validateOptions(String subEventType, Option options) throws ResourceValidationException
    {
        if(options == null || options.getCustomerEmail() == null )
        {
            logger.error("EventResource validateOptions Invalid options");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_OPTIONS, "Invalid options");
        } else
        {

            validateCustomerEmail(options.getCustomerEmail());

            if(Configurations.SubEventType.TYPE_REFERRAL_INVITE.equals(subEventType)){
                validateReferralInviteOptions(options);
            } else if(Configurations.SubEventType.TYPE_CREDIT_ADDED.equals(subEventType)) {
                validateAdvocateCreditAddOptions(options);
            } else if(Configurations.SubEventType.TYPE_CREDIT_ALERT.equals(subEventType)) {
                validateCreditAlertOptions(options);
            } else if(Configurations.SubEventType.TYPE_CREDIT_DEDUCT.equals(subEventType)) {
                validateCreditDeductOptions(options);
            } else if(Configurations.SubEventType.TYPE_CREDIT_WARNING_INTERNAL.equals(subEventType)) {
                validateCreditWarningLimitOptions(options);
            } else if(Configurations.SubEventType.TYPE_ICCID_PROMOTION_CREDIT.equals(subEventType)) {
                validateICCIDPromotionOptions(options);
            }
        }
    }


    // Validate Referral Invite Options
    private void validateReferralInviteOptions(Option options) throws ResourceValidationException {

        if(options.getReferralCode() == null || options.getGsCustomerId() == null ||
                options.getReferralAmount() == null || options.getAdvocateAmount() == null || options.getCurrency() == null) {

            logger.error("EventResource validateOptions Invalid options");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_OPTIONS, "Invalid options");
        } else {

            validateReferralCode(options.getReferralCode());
            validateGSCustomerId(options.getGsCustomerId());
            validateAdvocateAmount(options.getAdvocateAmount());
            validateReferralAmount(options.getReferralAmount());
            validateCurrency(options.getCurrency());
        }
    }


    // Validate Advocate Credit Amount Options
    private void validateAdvocateCreditAddOptions(Option options) throws ResourceValidationException {

        if(options.getNewCustomerEmail() == null || options.getAdvocateAmount() == null || options.getCurrency() == null) {

            logger.error("EventResource validateOptions Invalid options");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_OPTIONS, "Invalid options");
        } else {

            validateNewCustomerEmail(options.getNewCustomerEmail());
            validateAdvocateAmount(options.getAdvocateAmount());
            validateCurrency(options.getCurrency());
            validateGSCustomerId(options.getGsCustomerId());
        }
    }

    // Validate Credit Alert Options
    private void validateCreditAlertOptions(Option options) throws ResourceValidationException {
        if(options.getCreditLimit() == null || options.getCurrency() == null) {

            logger.error("EventResource validateOptions Invalid options");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_OPTIONS, "Invalid options");
        } else {

            validateCreditLimit(options.getCreditLimit());
            validateCurrency(options.getCurrency());
        }
    }

    // Validate Credit Deduct Options
    private void validateCreditDeductOptions(Option options) throws ResourceValidationException {
        if(options.getCsn() == null) {

            logger.error("EventResource validateOptions Invalid options");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_OPTIONS, "Invalid options");
        } else {
            validateCSN(options.getCsn());
        }
    }

    // Validate Credit Warning limit Options
    private void validateCreditWarningLimitOptions(Option options) throws ResourceValidationException {
        if(options.getCreditWarningLimit() == null || options.getCurrency() == null) {

            logger.error("EventResource validateOptions Invalid options");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_OPTIONS, "Invalid options");
        } else {
            validateCreditWarningLimit(options.getCreditWarningLimit());
            validateCurrency(options.getCurrency());
        }
    }

    private void validateICCIDPromotionOptions(Option options) throws ResourceValidationException {
        if(options.getGsCustomerId() == null || options.getCurrency() == null
                || options.getPromoAmount() == null ||options.getCsn() == null){
            logger.error("EventResource validateOptions Invalid options");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_OPTIONS, "Invalid options");
        } else {
            validateGSCustomerId(options.getGsCustomerId());
            validateCurrency(options.getCurrency());
            validateCSN(options.getCsn());
            validatePromoAmount(options.getPromoAmount());
        }
    }


    private void validateCustomerEmail(String email) throws ResourceValidationException {
        if(StringUtils.isEmpty(email) || !isEmailValid(email))
        {
            logger.error("EventResource validateOptions Invalid Customer Email");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_CUSTOMER_EMAIL, "Invalid Customer Email");
        }
    }

    private void validateNewCustomerEmail(String email) throws ResourceValidationException {
        if(StringUtils.isEmpty(email) || !isEmailValid(email))
        {
            logger.error("EventResource validateOptions Invalid New Customer Email");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_NEW_CUSTOMER_EMAIL, "Invalid New Customer Email");
        }
    }

    private void validateGSCustomerId(Integer id) throws ResourceValidationException {

        if(id <= 0)
        {
            logger.error("EventResource validateOptions Invalid GS Customer Id");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_GS_CUSTOMER_ID, "Invalid GS Customer Id");
        }
    }

    private void validateAdvocateAmount(int amount) throws ResourceValidationException {

        if(amount <= 0)
        {
            logger.error("EventResource validateOptions Invalid Advocate Amount");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_ADVOCATE_AMOUNT, "Invalid Advocate Amount");
        }
    }

    private void validateReferralAmount(int amount) throws ResourceValidationException {

        if(amount <= 0)
        {
            logger.error("EventResource validateOptions Invalid Referral Amount");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_REFERRAL_AMOUNT, "Invalid Referral Amount");
        }
    }

    private void validatePromoAmount(int amount) throws ResourceValidationException {

        if(amount <= 0)
        {
            logger.error("EventResource validateOptions Invalid Promo Amount");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_PROMO_AMOUNT, "Invalid Promo Amount");
        }
    }

    private void validateCurrency(String currency) throws ResourceValidationException {

        if(StringUtils.isEmpty(currency) || !Arrays.asList(supportedCurrencies).contains(currency))
        {
            logger.error("EventResource validateOptions Invalid Currency");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_CURRENCY, "Invalid Currency");
        }
    }

    private void validateCSN(String csn) throws ResourceValidationException {
        if(StringUtils.isEmpty(csn))
        {
            logger.error("EventResource validateOptions Invalid CSN");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_CSN, "Invalid CSN");
        }
    }

    private void validateCreditLimit(int creditLimit) throws ResourceValidationException {

        if(creditLimit <= 0)
        {
            logger.error("EventResource validateOptions Invalid Credit Limit");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_CREDIT_LIMIT, "Invalid Credit Limit");
        }
    }

    private void validateCreditWarningLimit(int warningLimit) throws ResourceValidationException {

        if(warningLimit <= 0)
        {
            logger.error("EventResource validateOptions Invalid GS Credit Warning Limit");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_CREDIT_WARNING_LIMIT, "Invalid Credit Warning Limit");
        }
    }

    private void validateReferralCode(String referralCode) throws ResourceValidationException {

        if ( referralCode.length() <= 3 ||  !validateInviteCode(referralCode))
        {
            logger.error("EventResource validatePercentageConsumed Invalid Referral Code");
            throw new ResourceValidationException(ErrorCode.EVENT_CREATION_REQUIRES_VALID_REFERRAL_CODE, "Invalid Referral Code");
        }
    }

    private boolean validateInviteCode(String input) {
        Pattern p = Pattern.compile("[^A-Za-z0-9]");
        Matcher m = p.matcher(input);
        boolean b = m.find();
        return !b;
    }


}
