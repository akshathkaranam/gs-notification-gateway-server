package com.gigsky.rest.api.v1;

import com.gigsky.rest.bean.BuildInfo;
import com.gigsky.service.BuildInfoManagementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/api/v1/info")
@Controller
public class BuildInfoManagementAPI {
	@Autowired
	private BuildInfoManagementService buildInfoService;
	@Context
	private ServletContext servletContext;

	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=UTF-8")
	public Response getAPIInformation() {
		String contextPath = servletContext.getRealPath("");
		BuildInfo buildInformation = buildInfoService.getBuildInformation(contextPath);
		return Response.status(Response.Status.OK).entity(buildInformation).build();
	}
}