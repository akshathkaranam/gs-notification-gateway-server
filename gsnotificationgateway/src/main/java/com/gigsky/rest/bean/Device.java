package com.gigsky.rest.bean;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.gigsky.database.bean.DeviceDBBean;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.annotation.XmlElement;

/**
 * Created by anant on 18/11/15.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Device {

    private static final Logger logger = LoggerFactory.getLogger(Device.class);

    public Device()
    {

    }

    public Device(DeviceDBBean deviceDBBean)
    {
        type = "DeviceDetail";
        deviceType = deviceDBBean.getDeviceType();
        deviceOSVersion = deviceDBBean.getDeviceOsVersion();
        model = deviceDBBean.getModel();
        channelId = deviceDBBean.getChannelId();
        installationId = deviceDBBean.getInstallationId();
        deviceToken = deviceDBBean.getDeviceToken();
        locale = deviceDBBean.getLocale();
        appVersion = deviceDBBean.getAppVersion();
        deviceId = deviceDBBean.getId();

        preferences = createPreferences(deviceDBBean);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    public String getDeviceOSVersion() {
        return deviceOSVersion;
    }

    public void setDeviceOSVersion(String deviceOSVersion) {
        this.deviceOSVersion = deviceOSVersion;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getChannelId() {
        return channelId;
    }

    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    public String getInstallationId() {
        return installationId;
    }

    public void setInstallationId(String installationId) {
        this.installationId = installationId;
    }

    public String getDeviceToken() {
        return deviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        this.deviceToken = deviceToken;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(String appVersion) {
        this.appVersion = appVersion;
    }

    public long getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(long deviceId) {
        this.deviceId = deviceId;
    }

    public Preference getPreferences() {
        return preferences;
    }

    public void setPreferences(Preference preferences) {
        this.preferences = preferences;
    }


//    public String getLastAccessTime() {
//        return lastAccessTime;
//    }
//
//    public void setLastAccessTime(String lastAccessTime) {
//        this.lastAccessTime = lastAccessTime;
//    }
//
//    public String getCustomerUuid() {
//        return customerUuid;
//    }
//
//    public void setCustomerUuid(String customerUuid) {
//        this.customerUuid = customerUuid;
//    }
//
//    public boolean isDeleted() {
//        return isDeleted;
//    }
//
//    public void setIsDeleted(boolean isDeleted) {
//        this.isDeleted = isDeleted;
//    }

    private long deviceId;
    @XmlElement(defaultValue = "null")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    private String type;
    @XmlElement(defaultValue = "null")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    private String deviceType;
    private String deviceOSVersion;
    private String model;
    private String channelId;
    @XmlElement(defaultValue = "null")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    private String installationId;
    @XmlElement(defaultValue = "null")
    @JsonSerialize(include = JsonSerialize.Inclusion.NON_DEFAULT)
    private String deviceToken;
    private String locale;
    private String appVersion;
    private Preference preferences;
    //private String lastAccessTime;
    //private String customerUuid;
    //private boolean isDeleted;

    private Preference createPreferences(DeviceDBBean deviceDBBean)
    {
        String preferences = deviceDBBean.getPreferences();

        if (!preferences.isEmpty())
        {
            try
            {
                JSONObject preferenceData = new JSONObject(preferences);

                Preference tempPreferences = new Preference();

                if (preferenceData.has("location"))
                {
                    tempPreferences.setLocation(preferenceData.get("location").toString());
                }

                return tempPreferences;

            }
            catch (JSONException e)
            {
                logger.error("Device json parsing preferences error:"+ e);
            }
        }

        return null;
    }


}
