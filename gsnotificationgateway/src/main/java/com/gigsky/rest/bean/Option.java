package com.gigsky.rest.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.xml.bind.annotation.XmlElement;

/**
 * Created by prsubbareddy on 28/01/16.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Option {

    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    // options for Push notification
    private String simName;

    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    // Options for Email notification referral
    private String customerName;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private String customerEmail;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private String newCustomerName;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private String newCustomerEmail;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private Integer advocateAmount;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private Integer referralAmount;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private String referralCode;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private String currency;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private Integer creditLimit;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private Integer creditWarningLimit;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private Integer gsCustomerId;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private Integer creditDeduct;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private String csn;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private Integer promoAmount;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerEmail() {
        return customerEmail;
    }

    public void setCustomerEmail(String customerEmail) {
        this.customerEmail = customerEmail;
    }

    public String getNewCustomerName() {
        return newCustomerName;
    }

    public void setNewCustomerName(String newCustomerName) {
        this.newCustomerName = newCustomerName;
    }

    public String getNewCustomerEmail() {
        return newCustomerEmail;
    }

    public void setNewCustomerEmail(String newCustomerEmail) {
        this.newCustomerEmail = newCustomerEmail;
    }

    public Integer getAdvocateAmount() {
        return advocateAmount;
    }

    public void setAdvocateAmount(Integer advocateAmount) {
        this.advocateAmount = advocateAmount;
    }

    public Integer getReferralAmount() {
        return referralAmount;
    }

    public void setReferralAmount(Integer referralAmount) {
        this.referralAmount = referralAmount;
    }

    public Integer getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(Integer creditLimit) {
        this.creditLimit = creditLimit;
    }

    public Integer getCreditWarningLimit() {
        return creditWarningLimit;
    }

    public void setCreditWarningLimit(Integer creditWarningLimit) {
        this.creditWarningLimit = creditWarningLimit;
    }

    public Integer getGsCustomerId() {
        return gsCustomerId;
    }

    public void setGsCustomerId(Integer gsCustomerId) {
        this.gsCustomerId = gsCustomerId;
    }

    public Integer getCreditDeduct() {
        return creditDeduct;
    }

    public void setCreditDeduct(Integer creditDeduct) {
        this.creditDeduct = creditDeduct;
    }

    public String getReferralCode() {
        return referralCode;
    }

    public void setReferralCode(String referralCode) {
        this.referralCode = referralCode;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getSimName() {
        return simName;
    }

    public void setSimName(String simName) {
        this.simName = simName;
    }

    public String getCsn() {
        return csn;
    }

    public void setCsn(String csn) {
        this.csn = csn;
    }

    public Integer getPromoAmount() {
        return promoAmount;
    }

    public void setPromoAmount(Integer promoAmount) {
        this.promoAmount = promoAmount;
    }
}
