package com.gigsky.rest.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import javax.xml.bind.annotation.XmlElement;
import java.util.List;

/**
 * Created by anant on 18/11/15.
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class Event {

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public String getSubEventType() {
        return subEventType;
    }

    public void setSubEventType(String subEventType) {
        this.subEventType = subEventType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReceivedTime() {
        return receivedTime;
    }

    public void setReceivedTime(String receivedTime) {
        this.receivedTime = receivedTime;
    }

    public List<Integer> getNotificationIds() {
        return notificationIds;
    }

    public void setNotificationIds(List<Integer> notificationIds) {
        this.notificationIds = notificationIds;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Integer getBalanceRemaining() {
        return balanceRemaining;
    }

    public void setBalanceRemaining(Integer balanceRemaining) {
        this.balanceRemaining = balanceRemaining;
    }

    public String getExpiry() {
        return expiry;
    }

    public void setExpiry(String expiry) {
        this.expiry = expiry;
    }

    public String getIccId() {
        return iccId;
    }

    public void setIccId(String iccId) {
        this.iccId = iccId;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Integer getPercentageConsumed() {
        return percentageConsumed;
    }

    public void setPercentageConsumed(Integer percentageConsumed) {
        this.percentageConsumed = percentageConsumed;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public Option getOptions() {
        return options;
    }

    public void setOptions(Option options) {
        this.options = options;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public List<String> getEmailIds() {
        return emailIds;
    };

    public void setEmailIds(List<String> emailIds)
    {
        this.emailIds = emailIds;
    }


    private String type;
    private String eventType;
    private long eventId;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private String subEventType;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private String status;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private String receivedTime;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private List<Integer> notificationIds;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private String customerId;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private String userId;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private Integer balanceRemaining;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private String expiry;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private String iccId;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private String location;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private Integer percentageConsumed;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private String statusMessage;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private String creationTime;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private Option options;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private String language;
    @XmlElement(defaultValue = "null")
    @JsonInclude( JsonInclude.Include.NON_DEFAULT)
    private List<String> emailIds;
}
