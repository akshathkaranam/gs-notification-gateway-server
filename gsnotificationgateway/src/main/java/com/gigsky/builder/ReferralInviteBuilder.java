package com.gigsky.builder;

import com.gigsky.composer.MessageComposerException;
import com.gigsky.database.bean.TemplateDBBean;
import com.gigsky.event.BaseEvent;
import com.gigsky.event.EmailEvent;
import com.gigsky.message.EmailMessage;
import com.gigsky.notification.BaseNotification;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import util.GSUtil;

import java.text.MessageFormat;

/**
 * Created by prsubbareddy on 03/01/17.
 */
public class ReferralInviteBuilder implements EmailBuilder {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ReferralInviteBuilder.class);

    public EmailMessage buildMessage(BaseEvent event, BaseNotification notification, TemplateDBBean templateDBBean) throws MessageComposerException
    {
        EmailMessage emailMessage = new EmailMessage();
        emailMessage.setEventType(event.getEventType());
        emailMessage.setSubEventType(event.getSubEventType());
        emailMessage.setNotificationType(notification.getNotificationType());

        try
        {
            JSONObject object = new JSONObject(templateDBBean.getContent());

            String title  = object.getString("title");
            String message = object.getString("message");

            String optionsData = ((EmailEvent) event).getEventData().getString("options");

            JSONObject optionsObj = new JSONObject(optionsData);
            int referralAmount = optionsObj.optInt("referralAmount");
            int advocateAmount = optionsObj.optInt("advocateAmount");
            String referralCode = optionsObj.optString("referralCode");
            String currency = optionsObj.optString("currency");
            int gsCustomerId = optionsObj.optInt("gsCustomerId");
            int creditLimit = 100;

            String referralCredit = GSUtil.GSSDK_formatPrice(String.valueOf(referralAmount), currency);
            String advocateCredit = GSUtil.GSSDK_formatPrice(String.valueOf(advocateAmount), currency);
            String maxCreditLimit = GSUtil.GSSDK_formatPrice(String.valueOf(creditLimit),currency);


            message = MessageFormat.format(message,advocateCredit, referralCredit, maxCreditLimit, referralCode,  String.valueOf(gsCustomerId));
            //Re-iterate check for missed params
            message = MessageFormat.format(message,advocateCredit, referralCredit, maxCreditLimit, referralCode,  String.valueOf(gsCustomerId));

            emailMessage.setEmailTitle(title);
            emailMessage.setEmailMessage(message);
            return emailMessage;

        } catch (Exception e)
        {
            logger.error("EmailMessageComposer messageForType_Referral_Invite JSON Exception while creating message");
            throw new MessageComposerException("Event Id-"+event.getEventId()+"has JSON Exception while creating message for Referral Invite", e);
        }

    }

}
