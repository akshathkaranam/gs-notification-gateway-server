package com.gigsky.builder;

import com.gigsky.composer.MessageComposerException;
import com.gigsky.database.bean.TemplateDBBean;
import com.gigsky.event.BaseEvent;
import com.gigsky.event.EmailEvent;
import com.gigsky.message.EmailMessage;
import com.gigsky.notification.BaseNotification;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import util.GSUtil;

import java.text.MessageFormat;

/**
 * Created by prsubbareddy on 03/01/17.
 */
public class CreditAlertBuilder implements EmailBuilder {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CreditLimitBuilder.class);

    public EmailMessage buildMessage(BaseEvent event, BaseNotification notification, TemplateDBBean templateDBBean) throws MessageComposerException
    {
        EmailMessage emailMessage = new EmailMessage();
        emailMessage.setEventType(event.getEventType());
        emailMessage.setSubEventType(event.getSubEventType());
        emailMessage.setNotificationType(notification.getNotificationType());

        try
        {
            JSONObject object = new JSONObject(templateDBBean.getContent());

            String title  = object.getString("title");
            String message = object.getString("message");

            String optionsData = ((EmailEvent) event).getEventData().getString("options");
            JSONObject optionsObj = new JSONObject(optionsData);

            String customerEmail = optionsObj.optString("customerEmail");
            int creditLimit = optionsObj.optInt("creditLimit");
            String currency = optionsObj.optString("currency");

            String creditBalance = GSUtil.GSSDK_formatPrice(String.valueOf(creditLimit), currency);

            message = MessageFormat.format(message, customerEmail, creditBalance);

            emailMessage.setEmailTitle(title);
            emailMessage.setEmailMessage(message);
            return emailMessage;

        } catch (Exception e)
        {
            logger.error("EmailMessageComposer messageForType_CreditAlertBuilder JSON Exception while creating message");
            throw new MessageComposerException("Event Id-"+event.getEventId()+"has JSON Exception while creating message for Credit Alert Builder", e);
        }
    }

}
