package com.gigsky.composer;

import com.gigsky.event.BaseEvent;
import com.gigsky.message.NotificationMessage;
import com.gigsky.notification.BaseNotification;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by anant on 28/10/15.
 */
public class MessageComposer {

   @Autowired
   PushMessageComposer pushMessageComposer;

   @Autowired
   EmailMessageComposer emailMessageComposer;

    public NotificationMessage composeMessage(BaseEvent event, BaseNotification notification, String locale) throws MessageComposerException{

        if(BaseNotification.TYPE_PUSH.equals(notification.getNotificationType()))
        {
            NotificationMessage pushNotificationMessage = pushMessageComposer.composeMessage(event, notification, locale);
            return pushNotificationMessage;
        }
        else if(BaseNotification.TYPE_EMAIL.equals(notification.getNotificationType())) {
            NotificationMessage emailNotificationMessage = emailMessageComposer.composeMessage(event, notification, locale);
            return emailNotificationMessage;
        }
        return null;
    }
}
