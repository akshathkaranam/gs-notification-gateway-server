package com.gigsky.composer;

import com.gigsky.exception.NotificationException;

/**
 * Created by prsubbareddy on 29/10/15.
 */
public class MessageComposerException extends NotificationException {

    public MessageComposerException()
    {
        super("Message Composer Exception");
    }

    public MessageComposerException(String message)
    {
        super(message);
    }

    public MessageComposerException(String message, Exception e)
    {
        super(message, e);
    }

    public MessageComposerException(Exception e)
    {
        super(e);
    }

}
