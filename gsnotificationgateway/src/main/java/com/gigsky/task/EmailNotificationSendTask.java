package com.gigsky.task;

import com.gigsky.encryption.EncryptionFactory;
import com.gigsky.handler.EmailServiceHandlerImpl;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by prsubbareddy on 04/01/17.
 */
public class EmailNotificationSendTask implements TaskInterface {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(EmailNotificationSendTask.class);

    @Autowired
    private EncryptionFactory encryptionFactory;

    @Autowired
    EmailServiceHandlerImpl emailServiceHandlerImpl;

    protected int emailNotificationId;

    public int getEmailNotificationId() {
        return emailNotificationId;
    }

    public void setEmailNotificationId(int emailNotificationId) {
        this.emailNotificationId = emailNotificationId;
    }


    @Override
    public int getTaskId() {
        return 0;
    }

    @Override
    public String getTaskDescription() {
        return null;
    }


    @Override
    public void run() {

        emailServiceHandlerImpl.setEncryptor(encryptionFactory.getEncryptor(EncryptionFactory.LATEST_ENCRYPTION_VERSION));
        emailServiceHandlerImpl.handleService(emailNotificationId);
    }

}