package com.gigsky.task;

/**
 * Created by anant on 27/10/15.
 */
public interface TaskInterface extends Runnable {

    public int getTaskId();

    public String getTaskDescription();

}
