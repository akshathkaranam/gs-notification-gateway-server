package com.gigsky.task;

import com.gigsky.common.Configurations;
import com.gigsky.composer.MessageComposer;
import com.gigsky.database.bean.EventDBBean;
import com.gigsky.database.bean.NotificationDBBean;
import com.gigsky.database.dao.*;
import com.gigsky.event.BaseEvent;
import com.gigsky.event.EmailEvent;
import com.gigsky.handler.GSResult;
import com.gigsky.handler.NotificationServiceHandler;
import com.gigsky.message.NotificationMessage;
import com.gigsky.notification.BaseNotification;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Iterator;
import java.util.List;

/**
 * Created by prsubbareddy on 28/12/16.
 */
public class EmailNotificationCreationTask implements TaskInterface {

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(EmailNotificationCreationTask.class);

    @Autowired
    ConfigurationKeyValuesDao configurationKeyValuesDao;

    @Autowired
    MessageComposer messageComposer;

    @Autowired
    NotificationServiceHandler notificationServiceHandler;

    @Autowired
    NotificationDao notificationDao;

    @Autowired
    EventDao eventDao;

    @Autowired
    PolicyDao policyDao;

    @Autowired
    GroupDao groupDao;

    protected BaseEvent baseEvent;
    protected BaseNotification baseNotification;
    protected int policyId;

    public BaseEvent getBaseEvent() {
        return baseEvent;
    }

    public void setBaseEvent(BaseEvent baseEvent) {
        this.baseEvent = baseEvent;
    }

    public BaseNotification getBaseNotification() {
        return baseNotification;
    }

    public void setBaseNotification(BaseNotification baseNotification) {
        this.baseNotification = baseNotification;
    }

    public int getPolicyId() {
        return policyId;
    }

    public void setPolicyId(int policyId) {
        this.policyId = policyId;
    }


    @Override
    public int getTaskId() {
        return 0;
    }

    @Override
    public String getTaskDescription() {
        return null;
    }


    @Override
    public void run() {

        JSONObject eventData = ((EmailEvent)baseEvent).getEventData();

        String language = eventData.optString("languageCode");

        try {
            NotificationMessage msg =  messageComposer.composeMessage(baseEvent, baseNotification, language);
            logger.debug("Email notification task message for lang: " + language + " is : " + msg);

            // call notification service handler
            handleServiceCall(msg, baseEvent, baseNotification);

        } catch (Exception e){
            logger.error("composeMessage got Exception: " +e + " for event_id "+baseEvent.getEventId());
        }
    }

    private void handleServiceCall(NotificationMessage msg, BaseEvent baseEvent, BaseNotification baseNotification)
    {

        boolean allEntriesCreated = true;

        List<String> emails = ((EmailEvent)baseEvent).getEmailIds();
        if(emails != null && emails.size() > 0) {
            for (Iterator<String> it = emails.iterator(); it.hasNext(); ) {

                String emailId = it.next();
                GSResult result = notificationServiceHandler.handleService(msg, baseEvent, baseNotification, null, emailId);
                if(result == null)
                {
                    allEntriesCreated = false;
                }
                logger.debug("Email notification task handler result: " +result);
            }

            // After creation of All Email entries notification status should be update
            NotificationDBBean notificationDBBean = notificationDao.getNotificationDetails(baseNotification.getNotificationId());
            if(allEntriesCreated)
            {
                notificationDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
            } else
            {
                EventDBBean eventDBBean = eventDao.getEventDetails(baseEvent.getEventId(), baseEvent.getTenantId());
                eventDBBean.setStatus(Configurations.StatusType.STATUS_IN_PROCESS);
                eventDao.updateEventDetails(eventDBBean);

                notificationDBBean.setStatus(Configurations.StatusType.STATUS_FAILED);
                notificationDBBean.setStatus(Configurations.StatusType.STATUS_FAILED_EMAIL_CREATION_PROCESS);
            }
            notificationDao.updateNotification(notificationDBBean);
            //todo: implement for retry event and notification for failed
        }
    }
}