package com.gigsky.task;

import com.gigsky.composer.MessageComposer;
import com.gigsky.database.bean.DeviceDBBean;
import com.gigsky.database.dao.DeviceDao;
import com.gigsky.event.BaseEvent;
import com.gigsky.handler.NotificationServiceHandler;
import com.gigsky.message.NotificationMessage;
import com.gigsky.notification.BaseNotification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by prsubbareddy on 09/12/15.
 */
public class RetryPushNotificationTask implements TaskInterface {

    private static final Logger logger = LoggerFactory.getLogger(RetryPushNotificationTask.class);

    @Autowired
    DeviceDao deviceDao;

    @Autowired
    MessageComposer messageComposer;

    @Autowired
    NotificationServiceHandler notificationServiceHandler;

    protected BaseEvent baseEvent;
    protected BaseNotification baseNotification;
    protected DeviceDBBean deviceDBBean;
    protected int pushNotificationId;


    public int getPushNotificationId() {
        return pushNotificationId;
    }

    public void setPushNotificationId(int pushNotificationId) {
        this.pushNotificationId = pushNotificationId;
    }

    public DeviceDBBean getDeviceDBBean() {
        return deviceDBBean;
    }

    public void setDeviceDBBean(DeviceDBBean deviceDBBean) {
        this.deviceDBBean = deviceDBBean;
    }

    public BaseEvent getBaseEvent() {
        return baseEvent;
    }

    public void setBaseEvent(BaseEvent baseEvent) {
        this.baseEvent = baseEvent;
    }

    public BaseNotification getBaseNotification() {
        return baseNotification;
    }

    public void setBaseNotification(BaseNotification baseNotification) {
        this.baseNotification = baseNotification;
    }



    @Override
    public int getTaskId() {
        return 0;
    }

    @Override
    public String getTaskDescription() {
        return null;
    }

    @Override
    public void run() {

        try {

            String lang = deviceDBBean.getLocale();

            NotificationMessage msg =  messageComposer.composeMessage(baseEvent, baseNotification, lang);
            logger.debug("Retry Push notification task message for lang: " + lang + " is : " + msg);

            // create one more override method for retry process
            notificationServiceHandler.handleServiceRetry(pushNotificationId, msg, baseEvent, baseNotification, deviceDBBean);

        } catch (Exception e){
            logger.error("composeMessage got Exception: " +e + " for event Id: "+baseEvent.getEventId());
        }

    }
}
