package com.gigsky.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by vinayr on 25/11/15.
 */
public class CommonUtils {

    public static final int ONE_DAY_MS = 86400000;
    private static final Logger logger = LoggerFactory.getLogger(CommonUtils.class);

    public static Timestamp getCurrentTimestampInUTC() {

        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));

        String currentTimeUTC = dateFormatGmt.format(new Date());
        Timestamp currentTimestamp = Timestamp.valueOf(currentTimeUTC);
        return currentTimestamp;
    }

    public static String convertTimestampToString(Timestamp timestamp) {

        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return dateFormatGmt.format(timestamp);
    }


    public static Timestamp getCurrentTimestampInUTC(String time) {

        SimpleDateFormat dateFormatGmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date currentDate = null;

        try
        {
            currentDate = dateFormatGmt.parse(time);
            dateFormatGmt.setTimeZone(TimeZone.getTimeZone("UTC"));
            String currentTimeStampInUtc = dateFormatGmt.format(currentDate);

            return Timestamp.valueOf(currentTimeStampInUtc);

        } catch (ParseException e) {
            logger.error("getCurrentTimestampInUTC parsing error:"+ e);
        }

        return Timestamp.valueOf(time);
    }
}
