package com.gigsky.exception;

import com.gigsky.exception.NotificationException;
import com.gigsky.rest.exception.ErrorCode;

/**
 * Created by prsubbareddy on 27/03/17.
 */
public class PartnerAppException extends NotificationException {

    public PartnerAppException(ErrorCode code, String msg)
    {
        super(code, msg);
    }

    public PartnerAppException(String message)
    {
        super(message);
    }

    public PartnerAppException(String message, Exception e)
    {
        super(message, e);
    }

    public PartnerAppException(Exception e)
    {
        super(e);
    }
}
