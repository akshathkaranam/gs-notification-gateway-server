package com.gigsky.exception;

import com.gigsky.exception.NotificationException;
import com.gigsky.rest.exception.ErrorCode;

/**
 * Created by anant on 29/10/15.
 */
public class ResourceValidationException extends NotificationException {

    public ResourceValidationException(ErrorCode code, String msg)
    {
        super(code, msg);
    }

    public ResourceValidationException(ErrorCode code)
    {
        super(code);
    }

    public ResourceValidationException(String message)
    {
        super(message);
    }

    public ResourceValidationException(String message, Exception e)
    {
        super(message, e);
    }

    public ResourceValidationException(Exception e)
    {
        super(e);
    }
}
