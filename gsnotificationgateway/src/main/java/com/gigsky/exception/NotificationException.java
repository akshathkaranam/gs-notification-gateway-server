package com.gigsky.exception;

import com.gigsky.rest.bean.Notification;
import com.gigsky.rest.exception.ErrorCode;

/**
 * Created by anant on 29/10/15.
 */
public class NotificationException extends Exception{

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    private ErrorCode errorCode;

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String message) {
        errorMessage = message;
    }

    private String errorMessage;

    public NotificationException()
    {

    }

    public NotificationException(ErrorCode code, String msg)
    {
        errorCode = code;
        errorMessage = msg;
    }

    public NotificationException(ErrorCode code)
    {
        errorCode = code;
    }


    protected NotificationException(String message)
    {
        super(message);
    }

    protected NotificationException(String message, Exception e)
    {
        super(message,e);
    }

    protected NotificationException(Exception e)
    {
        super(e);
    }
}
