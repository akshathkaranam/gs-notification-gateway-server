package com.gigsky.exception;

import com.gigsky.exception.NotificationException;
import com.gigsky.rest.exception.ErrorCode;

/**
 * Created by anant on 29/10/15.
 */
public class ServiceValidationException extends NotificationException {

    public ServiceValidationException(ErrorCode code, String msg)
    {
        super(code, msg);
    }

    public ServiceValidationException(ErrorCode code)
    {
        super(code);
    }

    public ServiceValidationException(String message)
    {
        super(message);
    }

    public ServiceValidationException(String message, Exception e)
    {
        super(message, e);
    }

    public ServiceValidationException(Exception e)
    {
        super(e);
    }
}
