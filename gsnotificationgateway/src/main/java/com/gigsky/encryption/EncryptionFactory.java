package com.gigsky.encryption;

/**
 * Do not modify this file.
 * <p/>
 * Utilities for hashing..sensitive information.
 *
 * @author rchouhan
 */
public interface EncryptionFactory {
    String LATEST_ENCRYPTION_VERSION = "1.0.1";
    /**
     * There can only be only be one encryptor per version for a web app.
     *
     * @param aVersion
     * @return GigSkyEncryptorInterface specified by version
     */
    GigSkyEncryptor getEncryptor(String aVersion);
}
