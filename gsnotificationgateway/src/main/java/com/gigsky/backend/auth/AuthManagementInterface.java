package com.gigsky.backend.auth;

import com.gigsky.backend.beans.LoginResponse;
import com.gigsky.backend.exception.BackendRequestException;
import org.hibernate.service.spi.ServiceException;


/**
 * Created by vinayr on 11/01/17.
 */
public interface AuthManagementInterface {

    public boolean validateCustomerToken(String token, int tenantId) throws ServiceException, BackendRequestException, com.gigsky.auth.BackendRequestException;

    public LoginResponse loginUsingToken(String token, int tenantId) throws ServiceException, BackendRequestException;

}
