package com.gigsky.backend.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by prsubbareddy on 09/11/17.
 */
@JsonInclude( JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PaymentStatusResponse {

    private String type = "AccountPaymentStatus";
    private String paymentMethod;
    private String creditCardNumber;
    private String paypalEmailId;
    private float gigskyCreditAmt;
    private String gigskyCreditCurrency;
    private Integer billingId;
    private int errorInt;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getCreditCardNumber() {
        return creditCardNumber;
    }

    public void setCreditCardNumber(String creditCardNumber) {
        this.creditCardNumber = creditCardNumber;
    }

    public String getPaypalEmailId() {
        return paypalEmailId;
    }

    public void setPaypalEmailId(String paypalEmailId) {
        this.paypalEmailId = paypalEmailId;
    }

    public float getGigskyCreditAmt() {
        return gigskyCreditAmt;
    }

    public void setGigskyCreditAmt(float gigskyCreditAmt) {
        this.gigskyCreditAmt = gigskyCreditAmt;
    }

    public String getGigskyCreditCurrency() {
        return gigskyCreditCurrency;
    }

    public void setGigskyCreditCurrency(String gigskyCreditCurrency) {
        this.gigskyCreditCurrency = gigskyCreditCurrency;
    }

    public Integer getBillingId() {
        return billingId;
    }

    public void setBillingId(Integer billingId) {
        this.billingId = billingId;
    }

    public int getErrorInt() {
        return errorInt;
    }

    public void setErrorInt(int errorInt) {
        this.errorInt = errorInt;
    }

}
