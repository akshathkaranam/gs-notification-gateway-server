package com.gigsky.backend.beans;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by prsubbareddy on 08/11/17.
 */
@JsonInclude( JsonInclude.Include.NON_NULL)
public class Login {

    private String type;
    private String loginType;
    private String emailId;
    private String password;

    public String getLoginType() {
        return loginType;
    }

    public void setLoginType(String loginType) {
        this.loginType = loginType;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
