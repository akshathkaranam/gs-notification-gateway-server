package com.gigsky.backend.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by prsubbareddy on 09/11/17.
 */
@JsonInclude( JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlanResponse {

    private String type = "NetworkGroupPlan";
    private int planId;
    private int networkGroupId;
    private int dataLimitInKB;
    private int validityPeriodInDays;
    private float price;
    private String currency;

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getPlanId() {
        return planId;
    }

    public void setPlanId(int planId) {
        this.planId = planId;
    }

    public int getNetworkGroupId() {
        return networkGroupId;
    }

    public void setNetworkGroupId(int networkGroupId) {
        this.networkGroupId = networkGroupId;
    }

    public int getDataLimitInKB() {
        return dataLimitInKB;
    }

    public void setDataLimitInKB(int dataLimitInKB) {
        this.dataLimitInKB = dataLimitInKB;
    }

    public int getValidityPeriodInDays() {
        return validityPeriodInDays;
    }

    public void setValidityPeriodInDays(int validityPeriodInDays) {
        this.validityPeriodInDays = validityPeriodInDays;
    }

}
