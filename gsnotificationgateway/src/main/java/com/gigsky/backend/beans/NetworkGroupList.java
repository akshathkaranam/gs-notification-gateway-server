package com.gigsky.backend.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

/**
 * Created by prsubbareddy on 09/11/17.
 */
@JsonInclude( JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class NetworkGroupList {

    private String type = "NetworkGroupList";
    private List<NetworkGroupResponse> list;
    private int errorInt;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<NetworkGroupResponse> getList() {
        return list;
    }

    public void setList(List<NetworkGroupResponse> list) {
        this.list = list;
    }

    public int getErrorInt() {
        return errorInt;
    }

    public void setErrorInt(int errorInt) {
        this.errorInt = errorInt;
    }
}
