package com.gigsky.backend.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.Map;

/**
 * Created by pradeepragav on 14/02/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RolesResponse {

    private String type;

    private Map<String, Object> role_self;

    private Map<String, Object> role_group;

    public Map<String, Object> getRole_group() {
        return role_group;
    }

    private int errorInt;

    public void setRole_group(Map<String, Object> role_group) {
        this.role_group = role_group;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Map<String, Object> getRole_self() {
        return role_self;
    }

    public void setRole_self(Map<String, Object> role_self) {
        this.role_self = role_self;
    }

    public int getErrorInt() {
        return errorInt;
    }

    public void setErrorInt(int errorInt) {
        this.errorInt = errorInt;
    }


}
