package com.gigsky.backend.beans;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Created by prsubbareddy on 09/11/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class PlanPurchaseResponse {

    private String type = "Transaction";

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getErrorInt() {
        return errorInt;
    }

    public void setErrorInt(int errorInt) {
        this.errorInt = errorInt;
    }

    private int errorInt;

}
