package com.gigsky.backend.Plan;

import com.gigsky.backend.beans.*;
import com.gigsky.backend.exception.BackendRequestException;
import com.gigsky.database.bean.CustomerContextDBBean;
import com.gigsky.rest.bean.PlanPurchase;
import org.hibernate.service.spi.ServiceException;


/**
 * Created by vinayr on 11/01/17.
 */
public interface PlanManagementInterface {

    public Subscription getSubscriptionDetails(String countryCode, CustomerContextDBBean customerContextDBBean) throws ServiceException, BackendRequestException;

    public NetworkGroupResponse getNetworkGroupForCountry(CustomerContextDBBean customerContextDBBean, String countryCode) throws ServiceException, BackendRequestException;

    public NetworkGroupPlans getPlansForNetwork(CustomerContextDBBean customerContextDBBean, NetworkGroupResponse networkGroupResponse, String countryCode) throws ServiceException, BackendRequestException;

    public PaymentStatusResponse getPaymentStatus(CustomerContextDBBean customerContextDBBean) throws ServiceException, BackendRequestException;

    public PlanPurchaseResponse makePlanPurchase(CustomerContextDBBean customerContextDBBean, PlanPurchase planInfo, PaymentStatusResponse paymentStatusResponse) throws ServiceException, BackendRequestException;

    public SIMStatusResponse getSIMStatus(CustomerContextDBBean customerContextDBBean) throws ServiceException, BackendRequestException;

    public ForbiddenCountryResponse getForbiddenCountries() throws ServiceException, BackendRequestException;

}
