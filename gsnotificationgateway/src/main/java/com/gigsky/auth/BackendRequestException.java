package com.gigsky.auth;

import com.gigsky.exception.NotificationException;
import com.gigsky.rest.exception.ErrorCode;

/**
 * Created by anant on 29/10/15.
 */
public class BackendRequestException extends NotificationException {

    public BackendRequestException(ErrorCode code, String msg)
    {
        super(code, msg);
    }

    public BackendRequestException(String message)
    {
        super(message);
    }

    public BackendRequestException(String message, Exception e)
    {
        super(message, e);
    }

    public BackendRequestException(Exception e)
    {
        super(e);
    }
}
