package com.gigsky.auth;

import com.gigsky.common.Configurations;
import com.gigsky.database.dao.ConfigurationKeyValuesDao;
import com.gigsky.rest.bean.UserInfo;
import com.gigsky.rest.exception.ErrorCode;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import util.GSUtil;

import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.Map;

/**
 * Created by vinayr on 14/12/15.
 */
public class UserManagementImpl implements UserManagementInterface {

    private static final String DEFAULT_BACKEND_BASE_URL = "https://support.gigsky.com/api/v4/customerNotifications";
    private static final Logger logger = LoggerFactory.getLogger(UserManagementImpl.class);

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ConfigurationKeyValuesDao configDao;

    private HttpEntity<String> header;

    @Override
    public void initUserInfo(Map<String, Integer> userList) {

    }

    @Override
    public void clearUserInfo() {

    }

    @Override
    public UserInfo getUserInfo(String token, int tenantId) throws BackendRequestException {

        try
        {
            StringBuilder url = new StringBuilder(configDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.BACKEND_BASE_URL, DEFAULT_BACKEND_BASE_URL));
            url.append("/account?")
                    .append("token=" + token);

            logger.debug("url : " + url);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            //Adding tenant id header
            headers.set(GSUtil.TENANT_ID_HEADER, String.valueOf(tenantId));

            HttpEntity<String> entity = new HttpEntity<String>(headers);

            ResponseEntity<com.gigsky.auth.converter.Customer> customer = restTemplate.exchange(url.toString(), HttpMethod.GET, entity, com.gigsky.auth.converter.Customer.class);

            UserInfo info = new UserInfo();
            info.setNotificationId(customer.getBody().getCustomerUUID());
            info.setStatus(customer.getBody().getStatus());
            return info;

        }
        catch (Exception ex)
        {
            logger.error("Get customer uuid failed for token [" + token + "]", ex);
            handleException(ex);
            throw new BackendRequestException(ErrorCode.INVALID_TOKEN, "Invalid token");
        }
    }


    @Override
    public void updateUserInfo(UserInfo info, String token, int tenantId) throws BackendRequestException {

        try
        {
            StringBuilder url = new StringBuilder(configDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.BACKEND_BASE_URL, DEFAULT_BACKEND_BASE_URL));
            url.append("/account");
            logger.debug("url : " + url);

            JSONObject request = new JSONObject();
            request.put("type", "NotificationGatewayCustomerUUIDMapRequest");
            request.put("token", token);
            request.put("customerUUID", info.getNotificationId());

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            //Adding tenant id header
            headers.set(GSUtil.TENANT_ID_HEADER, String.valueOf(tenantId));

            HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

            ResponseEntity<String>  result = restTemplate.exchange(url.toString(), HttpMethod.POST, entity, String.class);

        }
        catch (Exception ex)
        {
            logger.error("Update customer uuid for token [" + token + "]", ex);
            handleException(ex);
            throw new BackendRequestException(ErrorCode.INVALID_TOKEN, "Invalid token");
        }
    }

    public void configure() {
        //add the converters to convert the json response got from backend
        MappingJacksonHttpMessageConverter jsonConverter = new MappingJacksonHttpMessageConverter();

        restTemplate.getMessageConverters()
                .add(0, new StringHttpMessageConverter(Charset.forName("UTF-8")));
        restTemplate.getMessageConverters().add(jsonConverter);

        //create the header for the requests
        header = new HttpEntity<String>(createHeaders());
    }

    private HttpHeaders createHeaders() {
        return new HttpHeaders() {
            {
                setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
                setContentType(MediaType.APPLICATION_JSON);
            }
        };
    }

    private void handleException(Exception ex) throws BackendRequestException, HttpClientErrorException {
        if (ex instanceof HttpClientErrorException) {
            throw ((HttpClientErrorException)ex);
        }
        else if (ex instanceof BackendRequestException) {
            throw ((BackendRequestException) ex);
        }
    }
}
