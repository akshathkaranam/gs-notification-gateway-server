package com.gigsky.auth;

import com.gigsky.database.bean.MockUserDBBean;
import com.gigsky.database.dao.MockUserDao;
import com.gigsky.rest.bean.UserInfo;
import com.gigsky.rest.exception.ErrorCode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 * Created by anant on 24/11/15.
 */
public class MockUserManagementDBImpl implements UserManagementInterface {

    @Autowired
    MockUserDao mockUserDao;

    private final Logger logger = LoggerFactory.getLogger(MockUserManagementDBImpl.class);

    @Override
    public void initUserInfo(Map<String, Integer> userList) {

    }

    @Override
    public void clearUserInfo() {

    }

    @Override
    public UserInfo getUserInfo(String token, int tenantId) throws BackendRequestException {

        int userId = mockUserDao.getMockUserId(token, tenantId);

        if(userId == -1)
        {
            logger.error("MockUserManagementDBImpl getUserInfo Invalid token");
            throw new BackendRequestException(ErrorCode.INVALID_TOKEN, "Invalid token");
        }

        MockUserDBBean mockUserDBBean = mockUserDao.getMockUser(userId, tenantId);

        UserInfo info = new UserInfo();
        info.setCustomerId(userId);
        info.setNotificationId(mockUserDBBean.getUserUuid());
        return info;
    }

    @Override
    public void updateUserInfo(UserInfo info, String token, int tenantId) throws BackendRequestException {

        int userId = mockUserDao.getMockUserId(token, tenantId);

        if(userId == -1)
        {
            logger.error("MockUserManagementDBImpl updateUserInfo Invalid token");
            throw new BackendRequestException(ErrorCode.INVALID_TOKEN, "Invalid token");
        }

        MockUserDBBean mockUserDBBean = mockUserDao.getMockUser(userId, tenantId);
        mockUserDBBean.setUserUuid(info.getNotificationId());
        mockUserDao.updateMockUser(mockUserDBBean);
    }
}
