package com.gigsky.auth;

import com.gigsky.rest.bean.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by anant on 29/10/15.
 */
public class MockUserManagementImpl implements UserManagementInterface {

    private Map<String,String> userInfo = new HashMap<String, String>();
    private Map<String,Integer> userIdMapping = new HashMap<String, Integer>();

    private final Logger logger = LoggerFactory.getLogger(MockUserManagementImpl.class);


    public void initUserInfo(Map<String,Integer> userList)
    {
        userIdMapping.clear();
        for (Map.Entry<String, Integer> entry : userList.entrySet())
        {
            userIdMapping.put(entry.getKey(),entry.getValue());
        }
    }

    public void clearUserInfo()
    {
        userInfo.clear();
        userIdMapping.clear();
    }

    public UserInfo getUserInfo(String token, int tenantId) throws BackendRequestException {

        UserInfo info = new UserInfo();

        if(userIdMapping.containsKey(token))
        {
            info.setCustomerId(userIdMapping.get(token));
        }
        else
        {
            logger.error("MockUserManagementImpl getUserInfo Invalid token");
            throw new BackendRequestException("Invalid token");
        }

        if(userInfo.containsKey(token))
        {
            info.setNotificationId(userInfo.get(token));
        }

        return info;
    }

    public void updateUserInfo(UserInfo info, String token, int tenantId) throws BackendRequestException{

        if(userIdMapping.containsKey(token))
        {
            userInfo.put(token, info.getNotificationId());
        }
        else
        {
            logger.error("MockUserManagementImpl updateUserInfo Invalid token");
            throw new BackendRequestException("Invalid token");
        }
    }
}
