package com.gigsky.handler;

import com.gigsky.common.Configurations;
import com.gigsky.common.GSResponseKeysFactory;
import com.gigsky.database.bean.*;
import com.gigsky.database.dao.*;
import com.gigsky.event.BaseEvent;
import com.gigsky.event.CustomerEvent;
import com.gigsky.event.EmailEvent;
import com.gigsky.message.EmailMessage;
import com.gigsky.message.NotificationMessage;
import com.gigsky.notification.BaseNotification;
import com.gigsky.notification.PushNotification;
import com.gigsky.template.NotificationTemplateFactory;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by anant on 28/10/15.
 */
public class NotificationServiceHandler {

    @Autowired
    PushServiceHandlerImpl pushServiceHandlerImpl;

    @Autowired
    EmailServiceHandlerImpl emailServiceHandlerImpl;

    @Autowired
    MockPushServiceHandlerImpl mockPushServiceHandlerImpl;

    @Autowired
    MockPushNotificationDao mockPushNotificationDao;

    @Autowired
    NotificationTemplateFactory notificationTemplateFactory;

    @Autowired
    NotificationDao notificationDao;

    @Autowired
    PushNotificationDao pushNotificationDao;

    @Autowired
    EmailNotificationDao emailNotificationDao;

    @Autowired
    ConfigurationKeyValuesDao configurationKeyValuesDao;

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(NotificationServiceHandler.class);

    public GSResult handleService(NotificationMessage message, BaseEvent event, BaseNotification notification, DeviceDBBean deviceDBBean, String emailId){

        GSResult result = null;

        if(BaseNotification.TYPE_PUSH.equals(notification.getNotificationType()))
        {

            String gateWayMode = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.GATEWAY_MODE,"TEST");
            if(gateWayMode != null && (gateWayMode.equals("TEST") || gateWayMode.equals("TEST_BACKEND")))
            {
                Integer mockPushNotificationId = createMockPushNotification(event, notification, deviceDBBean);
                logger.debug("mock push notification creation id: "+ mockPushNotificationId +" for event_id "+event.getEventId());

                result = mockPushServiceHandlerImpl.handleService(mockPushNotificationId, message, event, notification, deviceDBBean);
                if(result != null)
                {
                    updateNotificationStatusWhileUsingMock(result);
                }
            }
            else if(gateWayMode != null && gateWayMode.equals("LIVE"))
            {
                Integer pushNotificationId = createPushNotification(event, notification);
                logger.debug("push_notification_id "+ pushNotificationId+" for event_id "+event.getEventId());

                result = pushServiceHandlerImpl.handleService(pushNotificationId, message, event, notification, deviceDBBean);
                if(result != null)
                {
                    updatePushNotification(result);
                    updateNotificationStatus(result);
                }
            }
        } else if(BaseNotification.TYPE_EMAIL.equals(notification.getNotificationType()))
        {

            // Inserting entries for EmailNotification and PendingEmail
            // handling of Sending Emails processing with another thread.
            Integer emailNotificationId = createEmailNotification(event, notification, message, emailId);

            if(emailNotificationId != null && emailNotificationId != -1) {
                result = new GSResult();
                result.put(GSResponseKeysFactory.EmailNotificationKeys.EMAIL_NOTIFICATION_ID, emailNotificationId);
            }
        }
        return result;
    }

    // handling service for retry events
    public GSResult handleServiceRetry(int pushNotificationId, NotificationMessage message, BaseEvent event, BaseNotification notification, DeviceDBBean deviceDBBean)
    {
        GSResult result = null;

        if(BaseNotification.TYPE_PUSH.equals(notification.getNotificationType()))
        {
            String gateWayMode = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.GATEWAY_MODE,"TEST");
            if(gateWayMode != null && gateWayMode.equals("TEST"))
            {
                logger.debug("mock push notification creation id: "+ pushNotificationId +" for event id:"+event.getEventId());

                result = mockPushServiceHandlerImpl.handleService(pushNotificationId, message, event, notification, deviceDBBean);
                if(result != null)
                {
                    updateNotificationStatusWhileUsingMock(result);
                }
            }
            else if(gateWayMode != null && gateWayMode.equals("LIVE"))
            {
                logger.debug("push notification id: "+ pushNotificationId+" for event id:"+event.getEventId());
                result = pushServiceHandlerImpl.handleService(pushNotificationId, message, event, notification, deviceDBBean);
                if(result != null)
                {
                    updatePushNotification(result);
                    updateNotificationStatus(result);
                }
            }
        }
        return result;
    }



    // creating PushNotification in db
    private Integer createPushNotification(BaseEvent event, BaseNotification notification)
    {
        PushNotificationDBBean pushNotificationDBBean = new PushNotificationDBBean();
        pushNotificationDBBean.setNotificationId(notification.getNotificationId());
        JSONObject eventData = ((CustomerEvent) event).getEventData();

        String locale = eventData.optString("languageCode");

        //Default Locale is en if language code is not set
        if (locale.isEmpty())
        {
            locale = Configurations.LanguageType.ENG_LANG.toString();
        }

        TemplateDBBean templateDBBean = notificationTemplateFactory.getTemplate(event.getEventTypeId(),
                notification.getNotificationTypeId(), event.getTenantId(), locale);

        if(templateDBBean == null && !locale.equals("en"))
        {
            templateDBBean = notificationTemplateFactory.getTemplate(event.getEventTypeId(),
                    notification.getNotificationTypeId(), event.getTenantId(), "en");
            if(templateDBBean != null)
            {
                //Set the template for en language
                pushNotificationDBBean.setTemplateId(templateDBBean.getId());
            }
        } else {
            //Set the right template based on language
            if(templateDBBean != null){
                pushNotificationDBBean.setTemplateId(templateDBBean.getId());
            }
        }

        if(eventData != null)
        {
            pushNotificationDBBean.setStatus(Configurations.StatusType.STATUS_IN_PROCESS);

            String attemptCount = eventData.optString("attemptCount");
            if(!attemptCount.isEmpty())
            {
                pushNotificationDBBean.setAttemptCount(Integer.parseInt(attemptCount));
            }

            pushNotificationDBBean.setLanguageCode(eventData.optString("languageCode"));
            pushNotificationDBBean.setIccId(eventData.optString("iccId"));
            pushNotificationDBBean.setLastAttemptStatus(Configurations.StatusType.STATUS_IN_PROCESS);
            pushNotificationDBBean.setLocation(eventData.optString("location"));
            pushNotificationDBBean.setNotificationCategory(eventData.optString("notificationCategory"));

            String openCount = eventData.optString("openCount");
            if(!openCount.isEmpty())
            {
                pushNotificationDBBean.setOpenCount(Integer.parseInt(openCount));
            }

            String sendCount = eventData.optString("sendCount");
            if(!sendCount.isEmpty())
            {
                pushNotificationDBBean.setSendCount(Integer.parseInt(sendCount));
            }

            pushNotificationDBBean.setStatusMessage("Creation Complete but not sent out");
        }

        int id = pushNotificationDao.createPushNotification(pushNotificationDBBean);
        if(id != -1)
        {
            return  id;
        }
        return null;
    }

    // Updating PushNotification in DB with updated status
    private void updatePushNotification(GSResult result)
    {
        int pushNotificationId = (Integer)result.get(GSResponseKeysFactory.pushNotificationKeys.PUSH_NOTIFICATION_ID);
        PushNotificationDBBean pushNotificationDBBean = pushNotificationDao.getPushNotificationDetails(pushNotificationId);

        String pushId = (String)result.get(GSResponseKeysFactory.pushNotificationKeys.PUSH_ID);
        if(pushId != null && pushId.length() > 0)
        {
            int deviceId = (Integer) result.get(GSResponseKeysFactory.pushNotificationKeys.DEVICE_ID);
            callPushedDevice(pushNotificationId, deviceId);

            pushNotificationDBBean.setPushId((String)result.get(GSResponseKeysFactory.pushNotificationKeys.PUSH_ID));
            pushNotificationDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
            pushNotificationDBBean.setStatusMessage(Configurations.StatusType.STATUS_COMPLETE_SENT);
            //pushNotificationDBBean.setLastAttemptStatus(Configurations.StatusType.STATUS_IN_PROCESS);
            pushNotificationDao.updatePushNotification(pushNotificationDBBean);
            logger.debug("updatePushNotification Status SENT for pushNotificationId-"+pushNotificationId);
        }
    }

    // Updating pushNotificationId and deviceId in PushedDevice DB
    private void callPushedDevice(int pushNotificationId, int deviceId)
    {
        PushedDeviceDBBean pushedDeviceDBBean = pushNotificationDao.getPushedDevice(pushNotificationId);
        if(pushedDeviceDBBean == null)
        {
            pushNotificationDao.createPushedDevice(pushNotificationId, deviceId);
            logger.debug("Pushed device table created for pushNotificationId-"+pushNotificationId);
        }
    }

    private Integer createMockPushNotification(BaseEvent event, BaseNotification notification, DeviceDBBean deviceDBBean)
    {
        MockPushNotificationDBBean mockPushNotificationDBBean = new MockPushNotificationDBBean();
        mockPushNotificationDBBean.setEventId(notification.getEventId());
        mockPushNotificationDBBean.setEventTypeId(String.valueOf(event.getEventTypeId()));
        mockPushNotificationDBBean.setNotificationId(notification.getNotificationId());

        if(Configurations.CategoryType.CATEGORY_CUSTOMER.equals(event.getEventCategory()))
        {
            JSONObject data = ((CustomerEvent)event).getEventData();
            if(data != null)
            {
                mockPushNotificationDBBean.setIccId(data.optString("iccId"));
                mockPushNotificationDBBean.setLocation(data.optString("location"));
            }
            mockPushNotificationDBBean.setNotificationCategory(PushNotification.CATEGORY_SINGLE);
        }
        mockPushNotificationDBBean.setLanguageCode(deviceDBBean.getLocale());
        mockPushNotificationDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
        mockPushNotificationDBBean.setStatusMessage(Configurations.StatusType.STATUS_COMPLETE_SENT);

        int id = mockPushNotificationDao.createMockPushNotification(mockPushNotificationDBBean);
        if(id != -1)
        {
            return  id;
        }
        return null;
    }

    private void updateNotificationStatus(GSResult result)
    {
        int notificationId = (Integer)result.get(GSResponseKeysFactory.pushNotificationKeys.NOTIFICATION_ID);
        NotificationDBBean notificationDBBean =  notificationDao.getNotificationDetails(notificationId);

        // Changing Notification status to COMPLETE_SENT and COMPLETE_SENT
        if(result.get(GSResponseKeysFactory.pushNotificationKeys.PUSH_ID) != null)
        {
            notificationDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
            notificationDBBean.setStatusMessage(Configurations.StatusType.STATUS_COMPLETE_SENT);
            notificationDao.updateNotification(notificationDBBean);
            logger.debug("updateNotificationStatus SENT for notificationId-"+notificationId);
        }
    }

    private void updateNotificationStatusWhileUsingMock(GSResult result)
    {
        int notificationId = (Integer)result.get(GSResponseKeysFactory.pushNotificationKeys.NOTIFICATION_ID);
        NotificationDBBean notificationDBBean =  notificationDao.getNotificationDetails(notificationId);

       if(notificationDBBean != null)
       {
            notificationDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
            notificationDBBean.setStatusMessage(Configurations.StatusType.STATUS_COMPLETE_SENT);
            notificationDao.updateNotification(notificationDBBean);
            logger.debug("updateNotificationStatusWhileUsingMock SENT for notificationId-"+notificationId);
       }
    }


    // creating EmailNotification in db
    private Integer createEmailNotification(BaseEvent event, BaseNotification notification, NotificationMessage message, String emailId)
    {
        EmailNotificationDBBean emailNotificationDBBean = new EmailNotificationDBBean();
        emailNotificationDBBean.setNotificationId(notification.getNotificationId());
        emailNotificationDBBean.setRecipient(emailId);
        emailNotificationDBBean.setSubject(((EmailMessage)message).getEmailTitle());
        emailNotificationDBBean.setContent(((EmailMessage)message).getEmailMessage());
        JSONObject eventData = ((EmailEvent) event).getEventData();

        String locale = eventData.optString("languageCode");

        //Default Locale is en if language code is not set
        if (locale.isEmpty())
        {
            locale = Configurations.LanguageType.ENG_LANG.toString();
        }

        TemplateDBBean templateDBBean = notificationTemplateFactory.getTemplate(event.getEventTypeId(),
                notification.getNotificationTypeId(), event.getTenantId(), locale);
        if(templateDBBean == null && !locale.equals("en")) {
            templateDBBean = notificationTemplateFactory.getTemplate(event.getEventTypeId(),
                    notification.getNotificationTypeId(), event.getTenantId(), "en");
            if(templateDBBean != null)
            {
                //Set the template for en language
                emailNotificationDBBean.setTemplateId(templateDBBean.getId());
            }
        } else {
            //Set the right template based on language
            if(templateDBBean != null){
                emailNotificationDBBean.setTemplateId(templateDBBean.getId());
            }
        }

        if(eventData != null)
        {
            emailNotificationDBBean.setLanguageCode(eventData.optString("languageCode"));
        }

        emailNotificationDBBean.setStatus(Configurations.StatusType.STATUS_NEW);
        emailNotificationDBBean.setStatusMessage(Configurations.StatusType.STATUS_COMPLETE_NOT_SENT);

        int id = emailNotificationDao.createEmailNotification(emailNotificationDBBean);
        if(id != -1)
        {
            return id;
        }
        return null;

    }

}
