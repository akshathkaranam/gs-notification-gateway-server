package com.gigsky.handler;

import com.gigsky.common.Configurations;
import com.gigsky.common.GSResponseKeysFactory;
import com.gigsky.common.GigskyDuration;
import com.gigsky.database.bean.DeviceDBBean;
import com.gigsky.database.bean.EventDBBean;
import com.gigsky.database.bean.MockPushNotificationDBBean;
import com.gigsky.database.bean.NotificationDBBean;
import com.gigsky.database.dao.ConfigurationKeyValuesDao;
import com.gigsky.database.dao.EventDao;
import com.gigsky.database.dao.MockPushNotificationDao;
import com.gigsky.database.dao.NotificationDao;
import com.gigsky.event.BaseEvent;
import com.gigsky.event.CustomerEvent;
import com.gigsky.message.NotificationMessage;
import com.gigsky.message.PushNotificationMessage;
import com.gigsky.notification.BaseNotification;
import org.json.JSONObject;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by anant on 29/10/15.
 */
public class MockPushServiceHandlerImpl implements PushServiceHandlerInterface {


    @Autowired
    MockPushNotificationDao mockPushNotificationDao;

    @Autowired
    ConfigurationKeyValuesDao configurationKeyValuesDao;

    @Autowired
    EventDao eventDao;

    @Autowired
    NotificationDao notificationDao;

    private static final int DEFAULT_NOTIFICATION_DELAY_MS = 10000;

    private static final org.slf4j.Logger logger = LoggerFactory.getLogger(MockPushServiceHandlerImpl.class);

    public GSResult handleService(int mockPushNotificationId, NotificationMessage message, BaseEvent event, BaseNotification notification, DeviceDBBean device)
    {
        int notificationDelayTime;

        //Add delay to mimic the live scenario
        String notificationDelay = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.MOCK_NOTIFICATION_DELAY);
        if (notificationDelay.isEmpty())
        {
            notificationDelayTime = DEFAULT_NOTIFICATION_DELAY_MS;
        }
        else
        {
            notificationDelayTime = (int) new GigskyDuration(notificationDelay).convertTimeInMilliSeconds();
        }

        //Sleep the thread for the time specified in notification delay
        try {
            Thread.sleep(notificationDelayTime);
        } catch (InterruptedException e) {
            logger.error("Mock Push handleService:"+e);
        }

        String failureMode = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.UA_FAILURE_MODE);
        if(failureMode != null && failureMode.equals("YES"))
        {
            handleEventForFailedCase(mockPushNotificationId, notification, device, event, message);
            logger.debug("MockPushServiceHandlerImpl  handleEventForFailedCase for id: "+mockPushNotificationId);
        } else
        {
            String userId = null;
            JSONObject data = ((CustomerEvent)event).getEventData();
            if(data != null) {
                userId = data.optString("userId");
            }

            MockPushNotificationDBBean mockPushNotificationDBBean = mockPushNotificationDao.getMockPushNotification(mockPushNotificationId);
            mockPushNotificationDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
            mockPushNotificationDBBean.setStatusMessage(Configurations.StatusType.STATUS_COMPLETE_SENT);
            mockPushNotificationDBBean.setDeviceId(device.getId());

            if ((userId != null) && (!userId.isEmpty()))
            {
                mockPushNotificationDBBean.setUserId(userId);
            }

            String title = ((PushNotificationMessage) message).getNotificationTitle();
            mockPushNotificationDBBean.setTitle(title);

            String pushMessage = ((PushNotificationMessage) message).getNotificationMessage();
            mockPushNotificationDBBean.setMessage(pushMessage);

            mockPushNotificationDao.updateMockPushNotification(mockPushNotificationDBBean);

            GSResult result = new GSResult();
            result.put(GSResponseKeysFactory.pushNotificationKeys.NOTIFICATION_ID, notification.getNotificationId());
            return result;
        }
        return null;
    }

    // If Push notification sending fails for any event,
    // removing notification and pushNotification entry from db.
    // event status updating as New and failed count will increase.
    private void handleEventForFailedCase(int mockPushNotificationId, BaseNotification baseNotification, DeviceDBBean deviceDBBean, BaseEvent event, NotificationMessage message)
    {
        int maxEventFailedCount = 3;

        String count = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.EVENT_FAIL_COUNT);
        if(count != null)
        {
            maxEventFailedCount =  Integer.parseInt(count);
        }

        EventDBBean eventDBBean = eventDao.getEventDetails(baseNotification.getEventId(), event.getTenantId());
        int eventFailedCount = eventDBBean.getFailCount();
        if(eventFailedCount == maxEventFailedCount)
        {
            // update Event DB status with FAILED
            eventDBBean.setStatus(Configurations.StatusType.STATUS_FAILED);
            eventDBBean.setStatusMessage(Configurations.StatusType.STATUS_FAILED_AFTER_RETRIES);
        } else
        {
            // update Event DB status with IN_PROCESS
            eventDBBean.setStatus(Configurations.StatusType.STATUS_IN_PROCESS);
            eventDBBean.setStatusMessage(Configurations.StatusType.STATUS_FAILED_PUSH_PROCESS);
            eventDBBean.setFailCount(eventFailedCount+1);
        }
        eventDao.updateEventDetails(eventDBBean);

        //TODO Refine code. Repetition of code for every failed attempt till max count is reached
        // updating status failed for notification DB and pushNotification DB.
        NotificationDBBean notificationDBBean = notificationDao.getNotificationDetails(baseNotification.getNotificationId());
        notificationDBBean.setStatus(Configurations.StatusType.STATUS_FAILED);
        notificationDBBean.setStatusMessage(Configurations.StatusType.STATUS_FAILED_PUSH_PROCESS);
        notificationDao.updateNotification(notificationDBBean);

        String userId = null;
        JSONObject data = ((CustomerEvent)event).getEventData();
        if(data != null) {
            userId = data.optString("userId");
        }

        MockPushNotificationDBBean mockPushNotificationDBBean = mockPushNotificationDao.getMockPushNotification(mockPushNotificationId);
        mockPushNotificationDBBean.setStatus(Configurations.StatusType.STATUS_FAILED);
        mockPushNotificationDBBean.setStatusMessage(Configurations.StatusType.STATUS_FAILED_PUSH_PROCESS);
        mockPushNotificationDBBean.setDeviceId(deviceDBBean.getId());

        if ((userId != null) && (!userId.isEmpty()))
        {
            mockPushNotificationDBBean.setUserId(userId);
        }

        String title = ((PushNotificationMessage) message).getNotificationTitle();
        mockPushNotificationDBBean.setTitle(title);

        String pushMessage = ((PushNotificationMessage) message).getNotificationMessage();
        mockPushNotificationDBBean.setMessage(pushMessage);

        mockPushNotificationDao.updateMockPushNotification(mockPushNotificationDBBean);

    }
}
