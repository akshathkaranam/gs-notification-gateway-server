package com.gigsky.handler;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.EmailNotificationDBBean;
import com.gigsky.database.bean.MockEmailNotificationSentDBBean;
import com.gigsky.database.bean.NotificationDBBean;
import com.gigsky.database.dao.ConfigurationKeyValuesDao;
import com.gigsky.database.dao.EmailNotificationDao;
import com.gigsky.database.dao.MockEmailNotificationSentDao;
import com.gigsky.database.dao.NotificationDao;
import com.gigsky.encryption.GigSkyEncryptor;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Created by prsubbareddy on 12/11/15.
 */
public class EmailServiceHandlerImpl implements EmailServiceHandlerInterface {

    private static final Logger logger = LoggerFactory.getLogger(EmailServiceHandlerImpl.class);

    @Autowired
    ConfigurationKeyValuesDao configurationKeyValuesDao;

    @Autowired
    EmailNotificationDao emailNotificationDao;

    @Autowired
    MockEmailNotificationSentDao mockEmailNotificationSentDao;

    @Autowired
    NotificationDao notificationDao;

    private GigSkyEncryptor encryptor;


    public void setEncryptor(GigSkyEncryptor encryptor) {
        this.encryptor = encryptor;
    }

    @Override
    public GSResult handleService(int emailNotificationId) {

        try
        {
            String mailHost = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.EMAIL_HOST);
            String encryptedEmailUser = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.EMAIL_USER);
            String emailSender = StringUtils.isNotEmpty(encryptedEmailUser) ? encryptor.decryptString(encryptedEmailUser) : StringUtils.EMPTY;

            EmailNotificationDBBean emailNotificationDBBean = emailNotificationDao.getEmailNotification(emailNotificationId);
            emailNotificationDBBean.setSender(emailSender);
            emailNotificationDao.updateEmailNotification(emailNotificationDBBean);

            String gateWayMode = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.GATEWAY_MODE,"TEST");

            // Gateway mode should Live and mailHost shouldn't be null or NONE
            if(gateWayMode != null && gateWayMode.equals("LIVE") && mailHost != null && !mailHost.equals("NONE"))
            {
                Session mailSession = createSmtpSession();
                mailSession.setDebug(true);

                MimeMessage message = new MimeMessage(mailSession);
                message.setFrom(new InternetAddress("noreply@gigsky.com"));
                message.setSubject(emailNotificationDBBean.getSubject(), "UTF-8");
                message.setContent(emailNotificationDBBean.getContent(), "text/html; charset=utf-8");
                message.addRecipients(Message.RecipientType.TO, emailNotificationDBBean.getRecipient());

                Transport.send(message);
                logger.info("Email sent for subject :" + emailNotificationDBBean.getSubject());


            } else if(gateWayMode != null && gateWayMode.equals("TEST") ){

                Thread.sleep(2000);

                // Fill required details from notification and create entry in mock email notification
                MockEmailNotificationSentDBBean mockEmailNotification = new MockEmailNotificationSentDBBean();
                mockEmailNotification.setNotificationId(emailNotificationDBBean.getNotificationId());
                mockEmailNotification.setLanguageCode(emailNotificationDBBean.getLanguageCode());
                mockEmailNotification.setSender(emailNotificationDBBean.getSender());
                mockEmailNotification.setRecipient(emailNotificationDBBean.getRecipient());
                mockEmailNotification.setSubject(emailNotificationDBBean.getSubject());
                mockEmailNotification.setContent(emailNotificationDBBean.getContent());
                mockEmailNotification.setStatus(Configurations.StatusType.STATUS_COMPLETE);

                mockEmailNotificationSentDao.createMockEmailNotification(mockEmailNotification);

                logger.info("====================================");
                logger.info("Email sent for subject ::" + emailNotificationDBBean.getSubject());
                logger.info("====================================");
            }

            // updating status for notification complete
            NotificationDBBean notificationDBBean = notificationDao.getNotificationDetails(emailNotificationDBBean.getNotificationId());
            notificationDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
            notificationDBBean.setStatusMessage(Configurations.StatusType.STATUS_COMPLETE_SENT);
            notificationDao.updateNotification(notificationDBBean);

            emailNotificationDBBean.setStatus(Configurations.StatusType.STATUS_COMPLETE);
            emailNotificationDBBean.setStatusMessage(Configurations.StatusType.STATUS_COMPLETE_SENT);
            emailNotificationDao.updateEmailNotification(emailNotificationDBBean);

        }catch (Exception e) {
            handleFailedCase(emailNotificationId);
        }
        return null;
    }

    private void handleFailedCase(int emailNotificationId)
    {
        int maxEmailFailedCount = 3;

        String count = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.EMAIL_FAIL_COUNT);
        if(count != null)
        {
            maxEmailFailedCount =  Integer.parseInt(count);
        }

        EmailNotificationDBBean emailNotificationDBBean = emailNotificationDao.getEmailNotification(emailNotificationId);
        int failedCount = emailNotificationDBBean.getFailedCount();

        // updating status for notification after retries
        NotificationDBBean notificationDBBean = notificationDao.getNotificationDetails(emailNotificationDBBean.getNotificationId());
        notificationDBBean.setStatus(Configurations.StatusType.STATUS_FAILED);
        notificationDBBean.setStatusMessage(Configurations.StatusType.STATUS_FAILED_EMAIL_SEND_PROCESS);
        notificationDao.updateNotification(notificationDBBean);

        if(failedCount == maxEmailFailedCount)
        {
            // update Email Notification DB status with FAILED
            emailNotificationDBBean.setStatus(Configurations.StatusType.STATUS_FAILED_AFTER_RETRIES);
            emailNotificationDBBean.setStatusMessage(Configurations.StatusType.STATUS_FAILED_EMAIL_SEND_PROCESS);

        } else
        {
            // update Event DB status with IN_PROCESS
            emailNotificationDBBean.setStatus(Configurations.StatusType.STATUS_FAILED);
            emailNotificationDBBean.setFailedCount(failedCount+1);
        }
        emailNotificationDao.updateEmailNotification(emailNotificationDBBean);
    }

    public Session createSmtpSession() {
        final Properties props = new Properties();

        props.setProperty("mail.smtp.user", "noreply@gigsky.com");
        props.setProperty("mail.from", "noreply@gigsky.com");
        props.setProperty("mail.smtp.auth", "true");
        props.setProperty("mail.smtp.port", "465");
        props.setProperty("mail.smtp.ssl.enable", "true");
        props.setProperty("mail.transport.protocol", "smtp"); //smtps not supported in this version of javamail. FIXME
        props.setProperty("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory"); // needed until smtps supported.

        String mailHost = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.EMAIL_HOST);
        props.setProperty("mail.host", mailHost);

        String smtpHost = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.SMTP_HOST);
        props.setProperty("mail.smtp.host", smtpHost);

        Authenticator lAuthenticator = new javax.mail.Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {

                String encryptedEmailUser = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.EMAIL_USER);
                String encryptedPassword = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.EMAIL_PWD);
                String emailId = StringUtils.isNotEmpty(encryptedEmailUser) ? encryptor.decryptString(encryptedEmailUser) : StringUtils.EMPTY;
                String emailPassword = StringUtils.isNotEmpty(encryptedPassword) ? encryptor.decryptString(encryptedPassword) : StringUtils.EMPTY;

                return new PasswordAuthentication(emailId, emailPassword);
            }
        };

        return Session.getDefaultInstance(props, lAuthenticator);
    }

}
