package com.gigsky.event;


import org.json.JSONObject;

/**
 * Created by anant on 28/10/15.
 */
public class CustomerEvent extends BaseEvent {

    protected JSONObject eventData;
    protected int customerId;

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public JSONObject getEventData() {
        return eventData;
    }

    public void setEventData(JSONObject eventData) {
        this.eventData = eventData;
    }

}
