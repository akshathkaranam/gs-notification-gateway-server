package com.gigsky.event;

import org.json.JSONObject;
import java.util.List;

/**
 * Created by prsubbareddy on 28/12/16.
 */
public class EmailEvent extends BaseEvent {

    protected JSONObject eventData;
    protected List<String> emailIds;

    public List<String> getEmailIds() {
        return emailIds;
    }

    public void setEmailIds(List<String> emailIds) {
        this.emailIds = emailIds;
    }

    public JSONObject getEventData() {
        return eventData;
    }

    public void setEventData(JSONObject eventData) {
        this.eventData = eventData;
    }

}
