package com.gigsky.message;

/**
 * Created by prsubbareddy on 28/12/16.
 */
public class EmailMessage extends NotificationMessage {

    protected String emailTitle;
    protected String emailMessage;

    public String getEmailMessage() {
        return emailMessage;
    }

    public void setEmailMessage(String emailMessage) {
        this.emailMessage = emailMessage;
    }

    public String getEmailTitle() {
        return emailTitle;
    }

    public void setEmailTitle(String emailTitle) {
        this.emailTitle = emailTitle;
    }

}
