package com.gigsky.message;

/**
 * Created by anant on 28/10/15.
 */
public class NotificationMessage {

    protected String notificationType;
    protected String eventType;
    protected String subEventType;

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getSubEventType() {
        return subEventType;
    }

    public void setSubEventType(String subEventType) {
        this.subEventType = subEventType;
    }

}
