package com.gigsky.message;

/**
 * Created by anant on 28/10/15.
 */
public class PushNotificationMessage extends NotificationMessage {

    protected String notificationTitle;
    protected String notificationMessage;

    public String getNotificationMessage() {
        return notificationMessage;
    }

    public void setNotificationMessage(String notificationMessage) {
        this.notificationMessage = notificationMessage;
    }

    public String getNotificationTitle() {
        return notificationTitle;
    }

    public void setNotificationTitle(String notificationTitle) {
        this.notificationTitle = notificationTitle;
    }

}
