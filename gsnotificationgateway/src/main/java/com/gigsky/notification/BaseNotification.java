package com.gigsky.notification;

import java.sql.Timestamp;

/**
 * Created by anant on 28/10/15.
 */
public class BaseNotification {

    public static final String TYPE_PUSH = "PUSH";
    public static final String TYPE_SMS = "SMS";
    public static final String TYPE_EMAIL = "EMAIL";

    protected int notificationId;
    protected int notificationTypeId;
    protected String notificationType;
    protected int eventId;
    protected Timestamp expiryTime;
    protected String status;
    protected int policyId;

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public int getNotificationTypeId() {
        return notificationTypeId;
    }

    public void setNotificationTypeId(int notificationTypeId) {
        this.notificationTypeId = notificationTypeId;
    }

    public String getNotificationType() {
        return notificationType;
    }

    public void setNotificationType(String notificationType) {
        this.notificationType = notificationType;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public Timestamp getExpiryTime() {
        return expiryTime;
    }

    public void setExpiryTime(Timestamp expiryTime) {
        this.expiryTime = expiryTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getPolicyId() {
        return policyId;
    }

    public void setPolicyId(int policyId) {
        this.policyId = policyId;
    }


}
