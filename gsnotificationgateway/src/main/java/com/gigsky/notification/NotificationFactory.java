package com.gigsky.notification;

import com.gigsky.database.bean.NotificationDBBean;
import com.gigsky.event.BaseEvent;
import com.gigsky.service.NotificationService;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by prsubbareddy on 06/11/15.
 */
public class NotificationFactory {

    @Autowired
    NotificationService notificationService;

    public BaseNotification createNotification(BaseEvent event, String notificationType, NotificationDBBean notificationDBBean)
    {
        if(BaseNotification.TYPE_PUSH.equals(notificationType) || BaseNotification.TYPE_EMAIL.equals(notificationType))
        {
            BaseNotification baseNotification = new BaseNotification();
            baseNotification.setNotificationType(notificationType);
            baseNotification.setEventId(event.getEventId());

            if (notificationDBBean.getExpiry() != null)
            {
                baseNotification.setExpiryTime(notificationDBBean.getExpiry());
            }

            baseNotification.setNotificationTypeId(notificationDBBean.getNotificationTypeId());
            baseNotification.setNotificationId(notificationDBBean.getId());
            baseNotification.setPolicyId(notificationDBBean.getPolicyId());
            return baseNotification;
        }

        return null;
    }
}
