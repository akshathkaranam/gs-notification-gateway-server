package com.gigsky.service;

import com.gigsky.database.bean.CustomerDeviceDBBean;
import com.gigsky.database.bean.DeviceDBBean;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.database.dao.DeviceDao;
import com.gigsky.database.exception.DatabaseException;
import com.gigsky.exception.ServiceValidationException;
import com.gigsky.rest.bean.Device;
import com.gigsky.rest.bean.PageInfo;
import com.gigsky.rest.bean.Preference;
import com.gigsky.rest.exception.ErrorCode;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by anant on 23/11/15.
 */
public class DeviceManagementService {

    private static final Logger logger = LoggerFactory.getLogger(CustomerManagementService.class);

    @Autowired
    CustomerDao customerDao;

    @Autowired
    DeviceDao deviceDao;

    public List<Device> getDevices(String customerId, PageInfo inputPageInfo, PageInfo outputPageInfo)
    {
        List<DeviceDBBean> deviceDBBeanList = deviceDao.getDevicesForCustomer(customerId, inputPageInfo.getStart(), inputPageInfo.getCount());

        List<Device> outputList = new ArrayList<Device>();

        int totalCount = deviceDao.getDeviceCountForCustomer(customerId);

        for(int index = 0; index < deviceDBBeanList.size(); index++ )
        {
            Device deviceInfo = new Device(deviceDBBeanList.get(index));
            outputList.add(deviceInfo);
        }

        if(outputPageInfo != null)
        {
            outputPageInfo.setCount(totalCount);
            outputPageInfo.setStart(inputPageInfo.getStart());
        }
        return outputList;
    }


    public Device addDevice(String customerId, Device device) throws ServiceValidationException, DatabaseException
    {
        // check if this device exists already.
        DeviceDBBean existingDevice = deviceDao.getDevice(device.getInstallationId(), device.getChannelId(), device.getDeviceToken());
        int deviceId;
        if(existingDevice != null)
        {
            deviceId = existingDevice.getId();
            // check if its already associated to same user.
            CustomerDeviceDBBean associatedDevice = deviceDao.getDeviceForCustomerIncludingDeleted(customerId, deviceId);
            if(associatedDevice != null)
            {

                logger.info("Device with id " + existingDevice.getId() + " already exists");
                Byte isDeleted = associatedDevice.getIsDeleted();
                Byte b =  new Byte("1");
                if(b.equals(isDeleted))
                {
                    // add device for deleted device
                    int id = deviceDao.addDeviceForDeletedDevice(associatedDevice);
                    if(DeviceDao.INVALID_INT_VALUE != id )
                    {
                        logger.info("Deleted Device is updated with customer id " + id);
                    }
                } else
                {
                    logger.error("DeviceManagementService addDevice Device is already assigned to user");
                    throw new ServiceValidationException(ErrorCode.DEVICE_ALREADY_ADDED_TO_USER, "Device is already assigned to user");
                }
            }
            // create association.
            else
            {
                deviceDao.addDeviceForCustomer(customerId, existingDevice.getId());
            }

            // update existing device information.
            if(device.getAppVersion() != null && device.getAppVersion().length() > 0) {
                existingDevice.setAppVersion(device.getAppVersion());
            }

            if(device.getDeviceOSVersion() != null && device.getDeviceOSVersion().length() > 0) {
                existingDevice.setDeviceOsVersion(device.getDeviceOSVersion());
            }

            if(device.getLocale() != null && device.getLocale().length() > 0) {
                existingDevice.setLocale(device.getLocale());
            }

            if(device.getInstallationId() != null && device.getInstallationId().length() > 0)
            {
                existingDevice.setInstallationId(device.getInstallationId());
            }

            if(device.getDeviceToken() != null && device.getDeviceToken().length() > 0)
            {
                existingDevice.setDeviceToken(device.getDeviceToken());
            }

            if(device.getChannelId() != null && device.getChannelId().length() > 0)
            {
                existingDevice.setChannelId(device.getChannelId());
            }

            if (device.getPreferences() != null)
            {
                String preferences = createPreferences(device);
                existingDevice.setPreferences(preferences);
            }

            deviceDao.updateDeviceForCustomer(customerId,existingDevice);
        }
        else
        {
            logger.info("Device doesnt exist. Add an entry");
            DeviceDBBean deviceDBBean = new DeviceDBBean(device);
            deviceId = deviceDao.addDeviceForCustomer(customerId, deviceDBBean);
            logger.info("Added device " + deviceId + " for customer " + customerId);
            device.setDeviceId(deviceId);

        }
        Device outputDevice = deviceDao.getDeviceForCustomer(customerId, deviceId);
        outputDevice.setDeviceId(deviceId);

        return outputDevice;
    }


    public Device updateDevice(String customerId, int deviceId, Device device) throws ServiceValidationException, DatabaseException {
        //DeviceDBBean deviceDBBean = new DeviceDBBean(device);

        Device assignedDevice = deviceDao.getDeviceForCustomer(customerId,deviceId);

        if(assignedDevice == null)
        {
            logger.error("DeviceManagementService updateDevice Device not assigned to user");
            throw new ServiceValidationException(ErrorCode.DEVICE_NOT_ASSIGNED_TO_USER, "Device not assigned to user.");
        }

        DeviceDBBean deviceDBBean = new DeviceDBBean(assignedDevice);
        if(device.getAppVersion() != null && device.getAppVersion().length() > 0) {
            deviceDBBean.setAppVersion(device.getAppVersion());
        }

        if(device.getDeviceOSVersion() != null && device.getDeviceOSVersion().length() > 0) {
            deviceDBBean.setDeviceOsVersion(device.getDeviceOSVersion());
        }

        if(device.getLocale() != null && device.getLocale().length() > 0) {
            deviceDBBean.setLocale(device.getLocale());
        }

        if(device.getInstallationId() != null && device.getInstallationId().length() > 0)
        {
            deviceDBBean.setInstallationId(device.getInstallationId());
        }

        if(device.getDeviceToken() != null && device.getDeviceToken().length() > 0)
        {
            deviceDBBean.setDeviceToken(device.getDeviceToken());
        }

        if(device.getChannelId() != null && device.getChannelId().length() > 0)
        {
            deviceDBBean.setChannelId(device.getChannelId());
        }

        if (device.getPreferences() != null)
        {
            String preferences = createPreferences(device);
            deviceDBBean.setPreferences(preferences);
        }

        deviceDBBean.setId(deviceId);

        deviceDao.updateDeviceForCustomer(customerId,deviceDBBean);

        Device outputDevice = deviceDao.getDeviceForCustomer(customerId, deviceId);
        outputDevice.setDeviceId(deviceId);
        return outputDevice;
    }

    public void deleteDevice(String customerId, long deviceId) throws DatabaseException, ServiceValidationException {
        int rowDeleted = deviceDao.deleteDeviceForCustomer(customerId, (int)deviceId);
        if(rowDeleted == 0)
        {
            logger.error("DeviceManagementService deleteDevice Device not assigned to user");
            throw new ServiceValidationException(ErrorCode.DEVICE_NOT_ASSIGNED_TO_USER, "Device not assigned to user.");
        }
    }

    public Device getDevice(String customerId, long deviceId) throws ServiceValidationException {
        Device device = deviceDao.getDeviceForCustomer(customerId, (int)deviceId);
        if(device == null)
        {
            logger.error("DeviceManagementService getDevice Device not found");
            throw new ServiceValidationException(ErrorCode.RESOURCE_NOT_FOUND, "Device not found");
        }
        return device;
    }

    private String createPreferences(Device device)
    {
        Preference preferences = device.getPreferences();

        if (preferences != null)
        {
            JSONObject obj = new JSONObject();
            if(!preferences.getLocation().isEmpty())
            {
                try
                {
                    obj.put("location", preferences.getLocation());
                    return obj.toString();
                }
                catch (JSONException e)
                {
                    logger.error("DeviceDBBean json parsing preferences error:" + e);
                }
            }
        }

        return null;
    }
}
