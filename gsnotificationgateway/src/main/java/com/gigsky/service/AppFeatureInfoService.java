package com.gigsky.service;

import com.gigsky.database.bean.AppFeatureInfoDBBean;
import com.gigsky.database.dao.AppFeatureInfoDao;
import com.gigsky.database.dao.ConfigurationKeyValuesDao;
import com.gigsky.database.exception.DatabaseException;
import com.gigsky.exception.ServiceValidationException;
import com.gigsky.rest.bean.AppFeatureInfoDetails;
import com.gigsky.rest.exception.ErrorCode;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.List;

/**
 * Created by prsubbareddy on 07/10/16.
 */
public class AppFeatureInfoService {

    @Autowired
    private AppFeatureInfoDao appFeatureInfoDao;

    @Autowired
    ConfigurationKeyValuesDao configurationKeyValuesDao;

    private static final Logger logger = LoggerFactory.getLogger(AppFeatureInfoService.class);

    //Add the app feature info entry
    public AppFeatureInfoDetails addAppFeatureInfo(String clientVersion, String clientType, String userId, AppFeatureInfoDetails appFeatureInfoDetails, int tenantId) throws ServiceValidationException, DatabaseException
    {
        //Check if the entry already exists
        List<AppFeatureInfoDBBean> appFeatureInfoDBBeanList = appFeatureInfoDao.getAppFeatureInfo(clientVersion, clientType, userId, tenantId);

        if ((appFeatureInfoDBBeanList != null) && (!appFeatureInfoDBBeanList.isEmpty()))
        {
            //App feature info already exists
            logger.error("App Feature Info entry already exists");
            throw new ServiceValidationException(ErrorCode.APP_FEATURE_INFO_ENTRY_ALREADY_EXIST, "App Feature Info already exists");
        }

        AppFeatureInfoDBBean appFeatureInfoDBBean = new AppFeatureInfoDBBean();

        //Add fields into DBBean
        appFeatureInfoDBBean.setClientVersion(clientVersion);
        appFeatureInfoDBBean.setClientType(clientType);
        appFeatureInfoDBBean.setUserId(userId);
        appFeatureInfoDBBean.setFeatures(Arrays.toString(appFeatureInfoDetails.getFeaturesSupported()));

        //New fields added
        appFeatureInfoDBBean.setBaseUrl(appFeatureInfoDetails.getBaseUrl());
        appFeatureInfoDBBean.setLogoBaseUrl(appFeatureInfoDetails.getLogoBaseUrl());
        appFeatureInfoDBBean.setTroubleshootingBaseUrl(appFeatureInfoDetails.getTroubleshootingBaseUrl());
        appFeatureInfoDBBean.setUpdateMandatory(appFeatureInfoDetails.getUpdateMandatory());
        appFeatureInfoDBBean.setTenantId(tenantId);


        appFeatureInfoDao.addAppFeatureInfo(appFeatureInfoDBBean);
        return prepareAppFeatureInfo(appFeatureInfoDBBean);
    }

    //Update the app feature info entry
    public AppFeatureInfoDetails updateAppFeatureInfo(String clientVersion, String clientType, String userId, AppFeatureInfoDetails appFeatureInfoDetails, int tenantId) throws ServiceValidationException, DatabaseException
    {
        //Get appFeatureInfo entry
        List<AppFeatureInfoDBBean> appFeatureInfoDBBeanList = appFeatureInfoDao.getAppFeatureInfo(clientVersion, clientType, userId, tenantId);

        if(CollectionUtils.isEmpty(appFeatureInfoDBBeanList))
        {
            //App feature info not found
            logger.error("App feature info entry not found");
            throw new ServiceValidationException(ErrorCode.APP_FEATURE_INFO_ENTRY_NOT_FOUND, "App feature info entry not found");
        }

        //As validation is done in the previous layer
        //There should be one and only entry
        AppFeatureInfoDBBean appFeatureInfoDBBean = appFeatureInfoDBBeanList.get(0);

        //FeaturesList update
        if(appFeatureInfoDetails.getFeaturesSupported() != null &&
                appFeatureInfoDetails.getFeaturesSupported().length != 0)
        {
            //FeaturesList will be overwritten
            appFeatureInfoDBBean.setFeatures(Arrays.toString(appFeatureInfoDetails.getFeaturesSupported()));
        }
        //baseUrl update
        if(StringUtils.isNotEmpty(appFeatureInfoDetails.getBaseUrl()))
        {
            appFeatureInfoDBBean.setBaseUrl(appFeatureInfoDetails.getBaseUrl());
        }

        //logoBaseUrl update
        if(StringUtils.isNotEmpty(appFeatureInfoDetails.getLogoBaseUrl()))
        {
            appFeatureInfoDBBean.setLogoBaseUrl(appFeatureInfoDetails.getLogoBaseUrl());
        }

        //troubleshootingBaseUrl update
        if(StringUtils.isNotEmpty(appFeatureInfoDetails.getTroubleshootingBaseUrl()))
        {
            appFeatureInfoDBBean.setTroubleshootingBaseUrl(appFeatureInfoDetails.getTroubleshootingBaseUrl());
        }

        //updateMandatory update
        if(StringUtils.isNotEmpty(appFeatureInfoDetails.getUpdateMandatory()))
        {
            appFeatureInfoDBBean.setUpdateMandatory(appFeatureInfoDetails.getUpdateMandatory());
        }

        //tenant id update
        if(tenantId > 0)
        {
            appFeatureInfoDBBean.setTenantId(tenantId);
        }


        //Update appFeatureInfo entry in db
        appFeatureInfoDao.updateAppFeatureInfo(appFeatureInfoDBBean);

        return prepareAppFeatureInfo(appFeatureInfoDBBean);
    }

    //Delete the app feature info entry. Only one entry will be deleted
    public void deleteAppFeatureInfo(String clientVersion, String clientType, String userId, Integer tenantId) throws ServiceValidationException
    {
        //Get the feature entry to be deleted
        List<AppFeatureInfoDBBean> appFeatureInfoDBBeanList = appFeatureInfoDao.getAppFeatureInfo(clientVersion, clientType, userId, tenantId);

        if ((appFeatureInfoDBBeanList != null) && (!appFeatureInfoDBBeanList.isEmpty()))
        {
            //If more than one entry is present then exception is thrown
            if (appFeatureInfoDBBeanList.size() > 1)
            {
                logger.error("Multiple App feature info entries found. Deletion failed!");
                throw new ServiceValidationException(ErrorCode.APP_FEATURE_INFO_MULTIPLE_ENTRIES_FOUND, "App Feature Info multiple entries found");
            }

            appFeatureInfoDao.deleteAppFeatureInfo(appFeatureInfoDBBeanList.get(0));
            return;
        }

        //Throw not found error
        logger.error("App Feature Info entry Not Found");
        throw new ServiceValidationException(ErrorCode.APP_FEATURE_INFO_ENTRY_NOT_FOUND, "App Feature Info Not Found");
    }

    //Get the app feature info entry
    public AppFeatureInfoDetails getAppFeatureInfo(String clientVersion, String clientType, String userId, Integer tenantId) throws ServiceValidationException
    {
        AppFeatureInfoDBBean appFeatureInfoDBBean;

        //Different combinations are possible for fetching based on the priorities
        //1. Get the feature info based on userId
        List<AppFeatureInfoDBBean> appFeatureInfoDBBeanList = appFeatureInfoDao.getAppFeatureInfo(userId);

        if (CollectionUtils.isNotEmpty(appFeatureInfoDBBeanList))
        {
            for (int index = 0; index < appFeatureInfoDBBeanList.size(); index++)
            {
                appFeatureInfoDBBean = appFeatureInfoDBBeanList.get(index);

                //1.1 Check if the client version and client type match in the entries
                //If both the client version and client type matches then return the app feature info
                if (appFeatureInfoDBBean.getClientType().equals(clientType) &&
                        appFeatureInfoDBBean.getClientVersion().equals(clientVersion)) {
                    return prepareAppFeatureInfo(appFeatureInfoDBBean);
                }
            }

            for (int index = 0; index < appFeatureInfoDBBeanList.size(); index++)
            {
                appFeatureInfoDBBean = appFeatureInfoDBBeanList.get(index);

                //1.2 Check if the client version is set to ALL and client type is input value
                if (appFeatureInfoDBBean.getClientType().equals(clientType) &&
                        appFeatureInfoDBBean.getClientVersion().equals("ALL")) {
                    return prepareAppFeatureInfo(appFeatureInfoDBBean);
                }
            }

            for (int index = 0; index < appFeatureInfoDBBeanList.size(); index++)
            {
                appFeatureInfoDBBean = appFeatureInfoDBBeanList.get(index);

                //1.3 Check if the client version is input value and client type is set to ALL
                if (appFeatureInfoDBBean.getClientType().equals("ALL") &&
                        appFeatureInfoDBBean.getClientVersion().equals(clientVersion)) {
                    return prepareAppFeatureInfo(appFeatureInfoDBBean);
                }
            }

            for (int index = 0; index < appFeatureInfoDBBeanList.size(); index++)
            {
                appFeatureInfoDBBean = appFeatureInfoDBBeanList.get(index);

                //1.4 Check if the client version and client type set to ALL
                if (appFeatureInfoDBBean.getClientType().equals("ALL") &&
                        appFeatureInfoDBBean.getClientVersion().equals("ALL"))
                {
                    return prepareAppFeatureInfo(appFeatureInfoDBBean);
                }
            }
        }

        //2. Production app feature info details
        else
        {
            //For the client version and client type and with user Id set as ALL, check the app feature info
           appFeatureInfoDBBeanList = appFeatureInfoDao.getAppFeatureInfo(clientVersion, clientType, "ALL", tenantId);

           if (CollectionUtils.isNotEmpty(appFeatureInfoDBBeanList))
           {
               //If more than one entry is present then exception is thrown
               if (appFeatureInfoDBBeanList.size() > 1)
               {
                   logger.error("Multiple App feature info entries found. Deletion failed!");
                   throw new ServiceValidationException(ErrorCode.APP_FEATURE_INFO_MULTIPLE_ENTRIES_FOUND, "App Feature Info multiple entries found");
               }

               return prepareAppFeatureInfo(appFeatureInfoDBBeanList.get(0));
           }
        }

        //Throw not found error
        logger.error("App Feature Info entry Not Found");
        throw new ServiceValidationException(ErrorCode.APP_FEATURE_INFO_ENTRY_NOT_FOUND, "App Feature Info Not Found");
    }

    //Prepare the app feature info
    public AppFeatureInfoDetails prepareAppFeatureInfo(AppFeatureInfoDBBean appFeatureInfoDBBean)
    {
        AppFeatureInfoDetails appFeatureInfoDetails = new AppFeatureInfoDetails();
        appFeatureInfoDetails.setType("AppFeatureInfo");

        String features = appFeatureInfoDBBean.getFeatures();

        //Remove the brackets
        String featuresSupported = features.replace("[", "").replace("]", "");

        appFeatureInfoDetails.setFeaturesSupported(featuresSupported.split(", "));

        //TODO : Check for null ?
        //Add baseUrl
        appFeatureInfoDetails.setBaseUrl(appFeatureInfoDBBean.getBaseUrl());

        //Add logoBaseUrl
        appFeatureInfoDetails.setLogoBaseUrl(appFeatureInfoDBBean.getLogoBaseUrl());

        //Add troubleshootingBaseUrl
        appFeatureInfoDetails.setTroubleshootingBaseUrl(appFeatureInfoDBBean.getTroubleshootingBaseUrl());

        //Add updateMandatory
        appFeatureInfoDetails.setUpdateMandatory(appFeatureInfoDBBean.getUpdateMandatory());

        return appFeatureInfoDetails;
    }
}
