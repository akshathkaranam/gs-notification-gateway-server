package com.gigsky.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigsky.database.bean.VersionInformationDBBean;
import com.gigsky.database.dao.VersionInformationDao;
import com.gigsky.rest.bean.BuildInfo;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.rest.exception.GigskyNotificationGatewayServerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class BuildInfoManagementService {
	private static final String BUILD_INFO_FILE_PATH = "buildInfo.txt";
	private static final Logger logger = LoggerFactory.getLogger(BuildInfoManagementService.class);

	@Autowired
	private VersionInformationDao versionInfoDao;

	public BuildInfo getBuildInformation(String contextPath) {
		try {
			//read the file and get build information
			String buildInfoFilePath = contextPath + "/" + BUILD_INFO_FILE_PATH;
            String buildInfoFileContent = readFileAsString(buildInfoFilePath);
			ObjectMapper mapper = new ObjectMapper();
			BuildInfo buildInfo = mapper.readValue(buildInfoFileContent, BuildInfo.class);

			//get version information
			VersionInformationDBBean versionInfo = versionInfoDao.getVersionInformation();
			buildInfo.setDb_type(versionInfo.getBuildType());
			buildInfo.setDb_version(Integer.valueOf("" + versionInfo.getMajor() + versionInfo.getMinor() + versionInfo.getBuild()));
			return buildInfo;
		}
		catch (IOException ex) {
			logger.error("Error occurred while reading build info file", ex);
			throw new GigskyNotificationGatewayServerException(ErrorCode.UNKNOWN_ERROR);
		}
		catch (Exception ex) {
			logger.error("Error occurred during build information", ex);
			throw new GigskyNotificationGatewayServerException(ErrorCode.UNKNOWN_ERROR);
		}
	}

	private String readFileAsString(String filePath) throws java.io.IOException {

		StringBuilder fileData = new StringBuilder(1000);
		InputStreamReader inputStream = null;
		BufferedReader reader = null;

		try {
			inputStream = new InputStreamReader(new FileInputStream(filePath), "UTF-8");
			reader = new BufferedReader(inputStream);

			char[] buf = new char[1024];
			int numRead;
			while ((numRead = reader.read(buf)) != -1) {
				String readData = String.valueOf(buf, 0, numRead);
				fileData.append(readData);
				buf = new char[1024];
			}
		}
		catch (Exception e){
			logger.error("readFileAsString exception", e);
		}
		finally {
			if(inputStream != null && reader != null)
			{
				inputStream.close();
				reader.close();
			}
		}
		return fileData.toString();
	}

}
