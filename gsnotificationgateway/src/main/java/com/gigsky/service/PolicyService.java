package com.gigsky.service;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.*;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.database.dao.GroupDao;
import com.gigsky.database.dao.PolicyDao;
import com.gigsky.event.BaseEvent;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by prsubbareddy on 06/11/15.
 */
public class PolicyService {

    @Autowired
    CustomerDao customerDao;

    @Autowired
    GroupDao groupDao;

    @Autowired
    PolicyDao policyDao;


    public List<PolicyDBBean> getPoliciesForEventType(BaseEvent event, EventDBBean eventDBBean)
    {
        List<PolicyDBBean> policyDBBeanList = new ArrayList<PolicyDBBean>();
        boolean isInGroup;
        boolean minOneGroup = false;

        if(Configurations.EventType.TYPE_EMAIL.equals(event.getEventType()))
        {
            //Get policies
            List<PolicyDBBean> policies = policyDao.getPoliciesByEventType(event.getEventTypeId(), eventDBBean.getTenantId());

            if ((policies == null) || policies.isEmpty())
            {
                return null;
            }

            //Create the list of policies
            for(Iterator<PolicyDBBean> itr = policies.iterator(); itr.hasNext(); )
            {
                PolicyDBBean policyDBBean = itr.next();
                policyDBBeanList.add(policyDBBean);
            }
        } else
        {
            if(Configurations.CategoryType.CATEGORY_CUSTOMER.equals(event.getEventCategory()))
            {
                //Get the customer UUID and check to which group it belongs
                String customerUuid = getCustomerUuid(eventDBBean);

                //Get the groups
                List<GroupTypeDBBean> allGroups = groupDao.getAllGroups();

                for(Iterator<GroupTypeDBBean> it = allGroups.iterator(); it.hasNext(); )
                {
                    GroupTypeDBBean groupTypeDBBean = it.next();

                    isInGroup = checkIfCustomerIsInGroup(customerUuid, groupTypeDBBean);

                    //If the user is in Group, get the policies to create notification
                    if (isInGroup == true)
                    {
                        minOneGroup = true;

                        //Get policies for a group
                        List<PolicyDBBean> policies = policyDao.getPoliciesByEventType(event.getEventTypeId(), groupTypeDBBean.getId(), eventDBBean.getTenantId());

                        //No Policies found so continue to other group
                        if ((policies == null) || policies.isEmpty())
                        {
                            continue;
                        }

                        //Create the list of policies
                        for(Iterator<PolicyDBBean> itr = policies.iterator(); itr.hasNext(); )
                        {
                            PolicyDBBean policyDBBean = itr.next();
                            policyDBBeanList.add(policyDBBean);
                        }
                    }
                }
            }

            //Else add case for Group events

            //Add the policy which enables to all if the customer is not part of any group
            if (!minOneGroup)
            {
                //Get the policies for the event type which has group ID as null
                List<PolicyDBBean> policiesList =  policyDao.getPoliciesByEventTypeForAllCustomers(eventDBBean.getEventTypeId(), eventDBBean.getTenantId());
                return policiesList;
            }
        }
        return policyDBBeanList;
    }

    //Check if Customer is present in the group
    private boolean checkIfCustomerIsInGroup(String customerUuid, GroupTypeDBBean groupTypeDBBean)
    {
        boolean isInGroup = false;

        //Loop through customers and check if the customer UUID is part of the group
        List<CustomerGroupDBBean> customerGroup = groupDao.getCustomersForGroup(groupTypeDBBean.getId());
        for(Iterator<CustomerGroupDBBean> itr = customerGroup.iterator(); itr.hasNext(); )
        {
            CustomerGroupDBBean customerGroupDBBean = itr.next();

            int customerId = customerGroupDBBean.getCustomerId();

            CustomerDBBean customerDBBean = customerDao.getCustomerInfoByCustomerId(customerId);
            String mCustomerUuid = customerDBBean.getCustomerUuid();

            if(mCustomerUuid != null && mCustomerUuid.equals(customerUuid))
            {
                isInGroup = true;
                break;
            }
        }

        return isInGroup;
    }


    // Getting customerUuid using customer id.
    private String getCustomerUuid(EventDBBean eventDBBean)
    {
        String customerUuid = null;
        CustomerEventDBBean customerEventDBBean  = eventDBBean.getCustomerEvent();
        int customerId =  customerEventDBBean.getCustomerId();

        CustomerDBBean customerDBBean = customerDao.getCustomerInfoByCustomerId(customerId);

        if(customerDBBean != null)
        {
            customerUuid = customerDBBean.getCustomerUuid();
            return customerUuid;
        }
        return customerUuid;
    }
}


