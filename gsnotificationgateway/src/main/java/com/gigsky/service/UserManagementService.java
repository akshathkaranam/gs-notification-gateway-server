package com.gigsky.service;

import com.gigsky.auth.UserManagementImpl;
import com.gigsky.auth.UserManagementInterface;
import com.gigsky.common.Configurations;
import com.gigsky.database.dao.ConfigurationKeyValuesDao;
import com.gigsky.auth.BackendRequestException;
import com.gigsky.rest.bean.UserInfo;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Created by anant on 29/10/15.
 */
public class UserManagementService {

    @Autowired
    private UserManagementInterface userManagementInterface;

    @Autowired
    private UserManagementImpl userManagementImpl;

    @Autowired
    ConfigurationKeyValuesDao configurationKeyValuesDao;

    public UserInfo getUserInfo(String token, int tenantId) throws BackendRequestException {

        String gateWayMode = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.GATEWAY_MODE,"TEST");

        if(gateWayMode != null && gateWayMode.equals("TEST"))
        {
            return userManagementInterface.getUserInfo(token, tenantId);
        }

        return userManagementImpl.getUserInfo(token, tenantId);
    }

    public void updateUserInfo(UserInfo info, String token, int tenantId) throws BackendRequestException
    {
        String gateWayMode = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.GATEWAY_MODE,"TEST");

        if(gateWayMode != null && gateWayMode.equals("TEST"))
        {
            userManagementInterface.updateUserInfo(info, token, tenantId);
            return;
        }

        userManagementImpl.updateUserInfo(info, token, tenantId);
    }

}
