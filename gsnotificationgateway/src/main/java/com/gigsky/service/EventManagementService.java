package com.gigsky.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gigsky.common.CommonUtils;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.*;
import com.gigsky.database.dao.CustomerDao;
import com.gigsky.database.dao.EventDao;
import com.gigsky.database.dao.NotificationDao;
import com.gigsky.database.exception.DatabaseException;
import com.gigsky.exception.ServiceValidationException;
import com.gigsky.rest.bean.Event;
import com.gigsky.rest.bean.Option;
import com.gigsky.rest.exception.ErrorCode;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by anant on 23/11/15.
 */
public class EventManagementService {

    private static final Logger logger = LoggerFactory.getLogger(EventManagementService.class);

    @Autowired
    private EventDao eventDao;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private NotificationDao notificationDao;

    public Event addEvent(Event event, int tenantId) throws DatabaseException {

        EventDBBean eventDBBean = createEvent(event, tenantId);
        String eventData = createEventData(event);
        long eventId;

        if(Configurations.EventType.TYPE_EMAIL.equals(event.getEventType()))
        {

            String emailIds = "";

            List<String> listEmails = event.getEmailIds();

            for(int i = 0; i < listEmails.size(); i++)
            {
                String email = listEmails.get(i);

                if(listEmails.size()-1 == i)
                {
                    emailIds = emailIds + email;
                } else
                {
                    emailIds = emailIds + email + ",";
                }
            }

            eventId = eventDao.createEventForEmail(eventDBBean, emailIds, eventData);
        } else
        {
            eventId = eventDao.createEvent(eventDBBean, event.getCustomerId(), eventData);
        }

        event.setEventId(eventId);
        event.setStatus(eventDBBean.getStatus());
        String receivedTime = CommonUtils.convertTimestampToString(eventDBBean.getReceivedTime());
        event.setReceivedTime(receivedTime);
        event.setNotificationIds(new ArrayList<Integer>());
        logger.info("Event add for event_id " + eventId);
        return event;
    }

    public Event getEvent(long eventId, int tenantId) throws ServiceValidationException {

        EventDBBean eventDBBean = eventDao.getEventDetails((int)eventId, tenantId);

        if (eventDBBean != null) {
            Event event = new Event();

            //Create the Event response
            event.setType("EventDetail");
            event.setStatus(eventDBBean.getStatus());

            String receivedTime = CommonUtils.convertTimestampToString(eventDBBean.getReceivedTime());
            event.setReceivedTime(receivedTime);
            event.setEventId(eventDBBean.getId());

            EventTypeDBBean eventTypeDBBean = eventDao.getEventType(eventDBBean.getEventTypeId());
            event.setEventType(eventTypeDBBean.getType());
            event.setStatusMessage(eventDBBean.getStatusMessage());

            if(Configurations.EventType.TYPE_EMAIL.equals(eventTypeDBBean.getType()))
            {
                event.setSubEventType(eventTypeDBBean.getSubEventType());

                EmailEventDBBean emailEvent = eventDao.getEmailEvent((int)eventId);

                String emailIds = emailEvent.getEmailId();
                String[] emails = emailIds.split(",");
                List<String> listEmails = new ArrayList<String>();
                if(emails.length > 0)
                {
                    for(int i = 0; i < emails.length; i++ )
                    {
                        listEmails.add(emails[i]);
                    }
                }

                event.setEmailIds(listEmails);

                List<NotificationDBBean> notifications  = notificationDao.getNotifications(eventDBBean.getId());
                if (CollectionUtils.isNotEmpty(notifications))
                {
                    List<Integer> notificationIds = new ArrayList<Integer>();
                    for(int index = 0; index < notifications.size(); index++ )
                    {
                        NotificationDBBean notificationDBBean = notifications.get(index);
                        notificationIds.add(notificationDBBean.getId());
                    }

                    event.setNotificationIds(notificationIds);
                } else {
                    event.setNotificationIds(new ArrayList<Integer>());
                }

                try
                {
                    if(emailEvent.getData() != null)
                    {
                        JSONObject jsonData = new JSONObject(emailEvent.getData());
                        event.setLanguage(jsonData.getString("languageCode"));

                        // Check email event has options
                        if(emailEvent.getData().contains("options")) {

                            // Fetch options email event data
                            JSONObject optionData = (JSONObject) jsonData.opt("options");

                            boolean isCSNRequired = false;

                            String csnData = optionData.optString("CSN");
                            if(csnData != null && csnData.length() > 0) {
                                optionData.remove("CSN");
                                isCSNRequired = true;
                            }

                            // Convert JSON data to option bean serialization class
                            ObjectMapper mapper = new ObjectMapper();
                            Option option =  mapper.readValue(optionData.toString(), Option.class);
                            if(isCSNRequired) {
                                option.setCsn(csnData);
                            }
                            event.setOptions(option);

                        }
                    }

                } catch (Exception e) {

                    logger.error("Event fetch error for Id:" + eventId, e);
                }

            } else
            {
                CustomerEventDBBean customerEvent = eventDBBean.getCustomerEvent();
                int customerId = customerEvent.getCustomerId();
                CustomerDBBean customerDBBean = customerDao.getCustomerInfoByCustomerId(customerId);
                event.setCustomerId(customerDBBean.getCustomerUuid());

                String creationTime = CommonUtils.convertTimestampToString(eventDBBean.getCreationTime());
                event.setCreationTime(creationTime);

                List<NotificationDBBean> notifications  = notificationDao.getNotifications(eventDBBean.getId());
                if (CollectionUtils.isNotEmpty(notifications))
                {
                    List<Integer> notificationIds = new ArrayList<Integer>();
                    for(int index = 0; index < notifications.size(); index++ )
                    {
                        NotificationDBBean notificationDBBean = notifications.get(index);
                        notificationIds.add(notificationDBBean.getId());
                    }

                    event.setNotificationIds(notificationIds);
                } else {
                    event.setNotificationIds(new ArrayList<Integer>());
                }

                try
                {
                    if(customerEvent.getData() != null)
                    {
                        JSONObject jsonData = new JSONObject(customerEvent.getData());

                        event.setUserId(jsonData.getString("userId"));
                        event.setLocation(jsonData.getString("location"));
                        event.setIccId(jsonData.getString("iccId"));

                        if (jsonData.has("balanceRemaining"))
                        {
                            event.setBalanceRemaining(jsonData.getInt("balanceRemaining"));
                        }

                        if (jsonData.has("percentageConsumed"))
                        {
                            event.setPercentageConsumed(jsonData.getInt("percentageConsumed"));
                        }

                        if (jsonData.has("expireTime"))
                        {
                            String expiry = jsonData.getString("expireTime");
                            if ((expiry != null) && (!expiry.isEmpty())) {
                                event.setExpiry(expiry);
                            }
                        }

                        if (jsonData.has("options"))
                        {
                            JSONObject obj = jsonData.getJSONObject("options");
                            Option option = new Option();
                            if(obj.has("simName"))
                            {
                                option.setSimName((String)obj.opt("simName"));
                            }
                            event.setOptions(option);
                        }
                    }

                } catch (JSONException e) {

                    logger.error("Event fetch error for Id:" + eventId, e);
                }
            }

            return event;
        }
        else
        {
            logger.error("EventManagementService getEvent Invalid type");
            throw new ServiceValidationException(ErrorCode.INVALID_ARGUMENTS, "Invalid type");
        }
    }


    private EventDBBean createEvent(Event event, int tenantId)
    {
        EventDBBean eventDBBean = new EventDBBean();
        eventDBBean.setStatus(Configurations.StatusType.STATUS_NEW);
        Timestamp currentTime = CommonUtils.getCurrentTimestampInUTC();
        eventDBBean.setReceivedTime(currentTime);
        eventDBBean.setTenantId(tenantId);


        if(Configurations.EventType.TYPE_EMAIL.equals(event.getEventType())) {
            EventTypeDBBean eventTypeDBBean = eventDao.getEventTypeBySubEventType(event.getSubEventType());
            eventDBBean.setEventTypeId(eventTypeDBBean.getId());

            eventDBBean.setEventCategory(Configurations.CategoryType.CATEGORY_CUSTOMER);
        }
        else
        {
            EventTypeDBBean eventTypeDBBean = eventDao.getEventType(event.getEventType());
            eventDBBean.setEventTypeId(eventTypeDBBean.getId());

            eventDBBean.setEventCategory(Configurations.CategoryType.CATEGORY_CUSTOMER);

            eventDBBean.setCreationTime(Timestamp.valueOf(event.getCreationTime()));
        }
        return eventDBBean;
    }

    private String createEventData(Event event)
    {
        JSONObject jsonData = new JSONObject();

        if(Configurations.EventType.TYPE_EMAIL.equals(event.getEventType()))
        {
            try
            {
                if(StringUtils.isNotEmpty(event.getLanguage())) {
                    jsonData.put("languageCode", event.getLanguage());
                }

                // Fetch options data from event
                Option options = event.getOptions();

                if(options != null)
                {
                    JSONObject obj = new JSONObject();

                    if(StringUtils.isNotEmpty(options.getCustomerName())) {
                        obj.put("customerName", options.getCustomerName());
                    }

                    if(StringUtils.isNotEmpty(options.getCustomerEmail())) {
                        obj.put("customerEmail", options.getCustomerEmail());
                    }

                    if(StringUtils.isNotEmpty(options.getNewCustomerEmail())) {
                        obj.put("newCustomerEmail", options.getNewCustomerEmail());
                    }

                    if(StringUtils.isNotEmpty(options.getReferralCode())) {
                        obj.put("referralCode", options.getReferralCode());
                    }

                    if(options.getReferralAmount() != null ) {
                        obj.put("referralAmount", options.getReferralAmount());
                    }

                    if(options.getAdvocateAmount() != null) {
                        obj.put("advocateAmount", options.getAdvocateAmount());
                    }

                    if(options.getCreditLimit() != null) {
                        obj.put("creditLimit", options.getCreditLimit());
                    }

                    if(options.getCreditDeduct() != null) {
                        obj.put("creditDeduct", options.getCreditDeduct());
                    }

                    if(options.getCreditWarningLimit() != null) {
                        obj.put("creditWarningLimit", options.getCreditWarningLimit());
                    }

                    if(options.getGsCustomerId() != null) {
                        obj.put("gsCustomerId", options.getGsCustomerId());
                    }

                    if(StringUtils.isNotEmpty(options.getCurrency())) {
                        obj.put("currency", options.getCurrency());
                    }

                    if(StringUtils.isNotEmpty(options.getCsn())){
                        obj.put("CSN", options.getCsn());
                    }

                    if(options.getPromoAmount() != null) {
                        obj.put("promoAmount", options.getPromoAmount());
                    }

                    jsonData.put("options", obj);
                }

            } catch (JSONException e) {
                logger.error("Creating event data Json Exception:" + e);
            }
        }
        else
        {
            try
            {
                if (event.getBalanceRemaining() != null && event.getBalanceRemaining() > 0)
                {
                    jsonData.put("balanceRemaining", event.getBalanceRemaining());
                }

                if (event.getPercentageConsumed() != null && event.getPercentageConsumed() > 0)
                {
                    jsonData.put("percentageConsumed", event.getPercentageConsumed());
                }

                if (event.getExpiry() != null)
                {
                    jsonData.put("expireTime", event.getExpiry());
                }

                jsonData.put("location", event.getLocation());
                jsonData.put("iccId", event.getIccId());
                jsonData.put("userId", event.getUserId());

                Option option = event.getOptions();

                JSONObject obj = new JSONObject();
                if((option.getSimName() != null) && (!option.getSimName().isEmpty()))
                {
                    obj.put("simName", option.getSimName());
                }
                jsonData.put("options", obj);

            } catch (JSONException e) {
                logger.error("Creating event data Json Exception:" + e);
            }
        }


        //String eventData = "{\"balanceRemaining\":\""+event.getBalanceRemaining()+"\",\"percentageConsumed\":\""+event.getPercentageConsumed()+"\",\"location\":\""+event.getLocation()+"\",\"iccId\":\""+event.getIccId()+"\"," +
        //        "\"expireTime\":\""+event.getExpiry()+"\",\"userId\":\""+event.getUserId()+"\"}";

        return jsonData.toString();
    }
}
