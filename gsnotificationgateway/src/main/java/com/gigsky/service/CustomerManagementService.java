package com.gigsky.service;

import com.gigsky.auth.BackendRequestException;
import com.gigsky.common.Configurations;
import com.gigsky.database.bean.CustomerDBBean;
import com.gigsky.database.bean.NotificationDBBean;
import com.gigsky.database.dao.*;
import com.gigsky.exception.ServiceValidationException;
import com.gigsky.rest.bean.Customer;
import com.gigsky.rest.bean.UserInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by anant on 29/10/15.
 */
public class CustomerManagementService {
    private static final Logger logger = LoggerFactory.getLogger(CustomerManagementService.class);

    @Autowired
    private UserManagementService userManagementService;

    @Autowired
    private CustomerDao customerDao;

    @Autowired
    private EventDao eventDao;

    @Autowired
    private NotificationDao notificationDao;

    @Autowired
    private PushNotificationDao pushNotificationDao;

    @Autowired
    private EmailNotificationDao emailNotificationDao;

    @Autowired
    private ConfigurationKeyValuesDao configurationKeyValuesDao;

    @Autowired
    private MockEmailNotificationSentDao mockEmailNotificationSentDao;

    @Autowired
    private MockPushNotificationDao mockPushNotificationDao;


    public Customer getCustomerDetails(String token, int tenantId) throws ServiceValidationException, BackendRequestException {

        Customer customerInfo = null;

        UserInfo userInfo = userManagementService.getUserInfo(token, tenantId);
        String notificationId = userInfo.getNotificationId();
        if (notificationId == null || notificationId.length() == 0) {
            // customer not associated with UUID.
            String registrationID = getNewRegistrationId();
            CustomerDBBean customerBean = new CustomerDBBean();
            customerBean.setCustomerUuid(registrationID);
            customerBean.setCustomerType(CustomerDBBean.CUSTOMER_TYPE_CONSUMER);
            customerBean.setPreferences("");

            // Introduced tenant id support
            customerBean.setTenantId(tenantId);
            customerDao.createCustomerInfo(customerBean);

            logger.info("Creating new UUID customer_id " + registrationID + " for customer ");
            userInfo.setNotificationId(registrationID);
            userManagementService.updateUserInfo(userInfo, token, tenantId);
            customerInfo = new Customer();
            customerInfo.setCustomerId(userInfo.getNotificationId());

        } else {
            customerInfo = new Customer();
            customerInfo.setCustomerId(userInfo.getNotificationId());
            logger.info("returning existing UUID customer_id " + userInfo.getNotificationId() + " for customer ");
        }

        customerInfo.setType("CustomerDetail");
        return customerInfo;
    }


    private String getNewRegistrationId(){

        boolean uniqueUUIDFound = false;
        UUID regId;
        while(true) {
            regId = UUID.randomUUID();

            CustomerDBBean customerInfo = customerDao.getCustomerInfo(regId.toString());

            if(customerInfo == null)
            {
                break;
            }
        }

        return regId.toString();
    }

    public void deleteCustomerDetails(Customer customer, int tenantId) throws ServiceValidationException
    {

        // delete customer events and return list of deleted events
        List<Integer> eventIds = eventDao.deleteCustomerEventDetails(customer.getOldEmailId(), tenantId);

        // prepare list of notification ids which helps to delete push notifications
        List<Integer> notificationIds = new ArrayList<Integer>();

        String gateWayMode = configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.GATEWAY_MODE,"TEST");

        for(Integer eventId : eventIds) {

            List<NotificationDBBean> notificationDBBeanList = notificationDao.getNotifications(eventId);
            for(NotificationDBBean notificationDBBean : notificationDBBeanList) {
                notificationIds.add(notificationDBBean.getId());
            }

            if(gateWayMode != null && gateWayMode.equals("TEST")){

                // delete location from push notification table
                mockPushNotificationDao.deleteMockPushNotification(notificationIds, customer.getAnonymousEmailId());
            } else {
                // delete location from push notification table
                pushNotificationDao.deletePushNotification(notificationIds);
            }

        }

        // delete email id from EmailEvent table and return list of deleted events
        eventIds = eventDao.deleteEmailEventDetails(customer.getOldEmailId(), customer.getAnonymousEmailId(), tenantId);

        // prepare list of notification ids which helps to delete email notifications
        for(Integer eventId : eventIds) {

            List<NotificationDBBean> notificationDBBeanList = notificationDao.getNotifications(eventId);
            for(NotificationDBBean notificationDBBean : notificationDBBeanList) {
                notificationIds.add(notificationDBBean.getId());
            }

            if(gateWayMode != null && gateWayMode.equals("TEST")){

                // delete email id from EmailNotification table
                mockEmailNotificationSentDao.deleteMockEmailNotification(notificationIds, customer.getAnonymousEmailId());

                // delete email id from EmailNotification table
                emailNotificationDao.deleteEmailNotification(notificationIds, customer.getAnonymousEmailId());
            } else {

                // delete email id from EmailNotification table
                emailNotificationDao.deleteEmailNotification(notificationIds, customer.getAnonymousEmailId());
            }
        }

    }
}
