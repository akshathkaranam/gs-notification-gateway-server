package com.gigsky.database.dao;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;

@Repository
public abstract class AbstractDao {
	public static final int INVALID_INT_VALUE = -1;

	@Resource(name = "sessionFactory")
	private SessionFactory sessionFactory;
	private static final Logger logger = LoggerFactory.getLogger(AbstractDao.class);

	public Session openSession() {
		try {
			return sessionFactory.openSession();
		}
		catch (HibernateException ex) {
			logger.error("error occurred while opening hibernate session", ex);
			throw ex;
		}
	}

	public void closeSession(Session session) {
		try {
			if (session != null && session.isOpen()) {
				session.flush();
				session.clear();
				session.disconnect();
			}
		}
		catch (HibernateException ex) {
			logger.error("error occurred while closing hibernate session", ex);
			throw ex;
		}
		finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
			logger.trace("closed connection");
		}
	}
}
