package com.gigsky.database.dao;

import com.gigsky.database.bean.MockUserDBBean;
import com.gigsky.database.bean.MockUserTokenDBBean;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by vinayr on 20/11/15.
 */
public class MockUserDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(MockUserDao.class);


    //Create an entry in the MockUser table
    public int createMockUser(MockUserDBBean mockUser)
    {
        Session session = openSession();

        try
        {
            session.save(mockUser);
            return mockUser.getUserId();
        }

        finally {
            closeSession(session);
        }
    }

    //Get the mock user from MockUser table
    public MockUserDBBean getMockUser(int mockUserId, int tenantId) {

        Session session = openSession();
        MockUserDBBean mockUserDBBean = null;

        try
        {
            mockUserDBBean = (MockUserDBBean)  session.createCriteria(MockUserDBBean.class)
                    .add(Restrictions.eq("userId", mockUserId))
                    .add(Restrictions.eq("tenantId", tenantId))
                    .uniqueResult();

            if (mockUserDBBean == null)
            {
                logger.warn("Mock user entry is missing for " + mockUserId);
            }
        }
        catch (Exception e)
        {
            // handle DB exception and return appropriate response.
        }
        finally {
            closeSession(session);
        }
        return mockUserDBBean;
    }

    //Update the user details in mock user table
    public void updateMockUser(MockUserDBBean mockUser) {
        Session session = openSession();

        try
        {
            session.saveOrUpdate(mockUser);
        }

        finally {
            closeSession(session);
        }
    }

    //Delete Mock User
    public void deleteMockUser(MockUserDBBean mockUser) {
        Session session = openSession();

        try
        {
            session.delete(mockUser);
        }

        finally {
            closeSession(session);
        }
    }

//MockUserToken Table operations

    //Get mock user Id from the MockUserToken table
    public int getMockUserId(String basicToken, int tenantId) {

        Session session = openSession();
        MockUserTokenDBBean mockUserTokenDBBean = null;
        int mockUserId = -1;

        try {
            mockUserTokenDBBean = (MockUserTokenDBBean)  session.createCriteria(MockUserTokenDBBean.class)
                    .add(Restrictions.eq("basicToken", basicToken))
                    .uniqueResult();

            if (mockUserTokenDBBean == null)
            {
                logger.warn("Mock user token entry is missing for " + basicToken);
            }
            else {
                mockUserId = mockUserTokenDBBean.getUserId();
            }
        }
        catch (Exception e)
        {
            // handle DB exception and return appropriate response.
        }
        finally {
            closeSession(session);
        }
        return mockUserId;
    }

    //Create an entry in the MockUserToken Table
    public int createMockUserToken(MockUserTokenDBBean mockUserToken)
    {
        Session session = openSession();

        try
        {
            session.save(mockUserToken);
            return mockUserToken.getId();
        }

        finally {
            closeSession(session);
        }
    }

    //Get Mock User Token
    public MockUserTokenDBBean getMockUserToken(int mockUserTokenId, int tenantId)
    {
        Session session = openSession();
        MockUserTokenDBBean mockUserToken = null;

        try
        {
            mockUserToken = (MockUserTokenDBBean)  session.createCriteria(MockUserTokenDBBean.class)
                    .add(Restrictions.eq("id", mockUserTokenId))
                    .uniqueResult();

            if (mockUserToken == null)
            {
                logger.warn("Mock user token entry is missing for " + mockUserToken);
            }
        }
        catch (Exception e)
        {
            // handle DB exception and return appropriate response.
        }
        finally {
            closeSession(session);
        }
        return mockUserToken;

    }

    //Delete Mock User Token
    public void deleteMockUserToken(MockUserTokenDBBean mockUserToken) {
        Session session = openSession();

        try
        {
            session.delete(mockUserToken);
        }

        finally {
            closeSession(session);
        }
    }

}
