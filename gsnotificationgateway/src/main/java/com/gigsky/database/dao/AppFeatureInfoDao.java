package com.gigsky.database.dao;

import com.gigsky.database.bean.AppFeatureInfoDBBean;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by prsubbareddy on 07/10/16.
 */
public class AppFeatureInfoDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(AppFeatureInfoDao.class);


    // Add app feature info
    public int addAppFeatureInfo(AppFeatureInfoDBBean appFeatureInfoDBBean)
    {

        Session session = openSession();

        try
        {
            session.save(appFeatureInfoDBBean);
            return appFeatureInfoDBBean.getId();
        }

        finally {
            closeSession(session);
        }
    }


    // Update app feature info
    public int updateAppFeatureInfo(AppFeatureInfoDBBean appFeatureInfoDBBean)
    {

        Session session = openSession();

        try
        {
            session.saveOrUpdate(appFeatureInfoDBBean);
            return appFeatureInfoDBBean.getId();
        }

        finally {
            closeSession(session);
        }
    }


    // Get app feature info based on Client Version, Client Type and User Id
    public List<AppFeatureInfoDBBean> getAppFeatureInfo(String clientVersion, String clientType, String userId, Integer tenantId)
    {
        Session session = openSession();

        try
        {
            List<AppFeatureInfoDBBean> appFeatureInfoDBBean =  session.createCriteria(AppFeatureInfoDBBean.class)
                    .add(Restrictions.eq("clientVersion", clientVersion))
                    .add(Restrictions.eq("clientType", clientType))
                    .add(Restrictions.eq("userId", userId))
                    .add(Restrictions.eq("tenantId", tenantId))
                    .list();

            if (appFeatureInfoDBBean != null)
            {
                return appFeatureInfoDBBean;
            }

            logger.warn("appFeatureInfo entry is missing for clientVersion: "+ clientVersion +" clientType: "+ clientType + " userId: "+userId + " tenantId"+ tenantId);
        }
        catch(Exception e)
        {
            logger.warn("Error while getting App Feature Info " + e );
        }

        finally {
            closeSession(session);
        }

        return null;
    }

    // Get app feature info based on User Id only
    public List<AppFeatureInfoDBBean> getAppFeatureInfo(String userId)
    {
        Session session = openSession();

        try
        {
            List<AppFeatureInfoDBBean> appFeatureInfoDBBean =  session.createCriteria(AppFeatureInfoDBBean.class)
                    .add(Restrictions.eq("userId", userId))
                    .list();

            if (appFeatureInfoDBBean != null)
            {
                return appFeatureInfoDBBean;
            }

            logger.warn("appFeatureInfo entry is missing for userId: "+userId);
        }
        catch(Exception e)
        {
            logger.warn("Error while getting App Feature Info " + e );
        }

        finally {
            closeSession(session);
        }

        return null;
    }

    // Delete app feature info
    public void deleteAppFeatureInfo(AppFeatureInfoDBBean appFeatureInfoDBBean)
    {
        Session session = openSession();

        try
        {
            session.delete(appFeatureInfoDBBean);
        }

        finally {
            closeSession(session);
        }
    }

}
