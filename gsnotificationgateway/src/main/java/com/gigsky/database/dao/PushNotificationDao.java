package com.gigsky.database.dao;

import com.gigsky.database.bean.PushNotificationDBBean;
import com.gigsky.database.bean.PushedDeviceDBBean;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by prsubbareddy on 20/10/15.
 */
public class PushNotificationDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(PushNotificationDao.class);

    // Create push notification with data
    public int createPushNotification(PushNotificationDBBean pushNotification)
    {
        Session session = openSession();

        try
        {
            session.save(pushNotification);
            return pushNotification.getId();
        }
        catch(Exception e)
        {
            logger.warn("Push Notification creation Exception: "+e);
        }
        finally {
            closeSession(session);
        }
        return -1;
    }

    // update push notifications for id with data
    public void updatePushNotification(PushNotificationDBBean pushNotification)
    {
        Session session = openSession();

        try
        {
            session.saveOrUpdate(pushNotification);
        }
        finally {
            closeSession(session);
        }
    }

    // Get push notification details for id
    public PushNotificationDBBean getPushNotificationDetails(int pushNotificationId)
    {
        Session session = openSession();

        try
        {
            PushNotificationDBBean pushNotification = (PushNotificationDBBean) session.createCriteria(PushNotificationDBBean.class)
                    .add(Restrictions.eq("id", pushNotificationId))
                    .uniqueResult();

            if (pushNotification != null)
            {
                return pushNotification;
            }

            logger.warn("Push Notification entry is missing for " + pushNotificationId);

            return null;
        }
        finally {
            closeSession(session);
        }
    }

    // Delete push notifications for id
    public void deletePushNotification(PushNotificationDBBean pushNotification)
    {
        Session session = openSession();

        try
        {
            session.delete(pushNotification);
        }
        finally {
            closeSession(session);
        }
    }

    // get push notifications using notificationId
    public List<PushNotificationDBBean> getPushNotifications(int notificationId)
    {
        Session session = openSession();

        try
        {
            List<PushNotificationDBBean> pushNotificationList = session.createQuery(" select P from PushNotificationDBBean P " +
                                          " where P.notificationId = :notificationId ")
                                        .setParameter("notificationId", notificationId)
                                        .list();

            if (CollectionUtils.isNotEmpty(pushNotificationList))
            {
                return pushNotificationList;
            }

            logger.warn("Push Notification List is null for notification Id:" + notificationId);

            return null;
        }
        finally {
            closeSession(session);
        }
    }


    public void createPushedDevice(int pushNotificationId, int deviceId) {

        Session session = openSession();

        try
        {
            SQLQuery sqlQuery = session.createSQLQuery("insert into PushedDevice (pushNotification_id, device_id) values (:pushNotificationId, :deviceId)");
            sqlQuery.setParameter("pushNotificationId", pushNotificationId);
            sqlQuery.setParameter("deviceId", deviceId);
            sqlQuery.executeUpdate();

        }
        catch(Exception e)
        {
            logger.warn("Pushed Device creation Exception: "+e);
        }
        finally {
            closeSession(session);
        }
    }

    public PushedDeviceDBBean getPushedDevice(int pushNotificationId)
    {
        Session session = openSession();

        try
        {
            PushedDeviceDBBean pushNotification = (PushedDeviceDBBean) session.createCriteria(PushedDeviceDBBean.class)
                    .add(Restrictions.eq("pushNotificationId", pushNotificationId))
                    .uniqueResult();

            if (pushNotification != null)
            {
                return pushNotification;
            }

            logger.warn("PushedDeviceDBBean entry is missing for pushNotificationId: " + pushNotificationId);

            return null;
        }
        finally {
            closeSession(session);
        }
    }


    public Long getInProcessPushNotificationsCount(String status)
    {
        Session session = openSession();

        try {
            Long count = (Long) session.createCriteria(PushNotificationDBBean.class)
                    .add(Restrictions.eq("lastAttemptStatus", status))
                    .setProjection(Projections.rowCount())
                    .uniqueResult();

            return count;
        }
        finally {
            closeSession(session);
        }
    }


    public List<PushNotificationDBBean> getAllPushNotifications(int startCount, String status, int maxCount)
    {
        Session session = openSession();

        try {

            List<PushNotificationDBBean> list = session.createCriteria(PushNotificationDBBean.class)
                    .add(Restrictions.eq("lastAttemptStatus", status))
                    .setFirstResult(startCount)
                    .setMaxResults(maxCount)
                    .list();

            if(CollectionUtils.isNotEmpty(list))
            {
                return list;

            }

            return null;
        }
        finally {
            closeSession(session);
        }
    }

    /*
    * ===== Delete push notification ====
    *
    * This method will remove location from push notification
    *
    * */
    public void deletePushNotification(List<Integer> notificationIds)
    {
        Session session = openSession();
        Transaction tx = session.beginTransaction();

        try {

            for(Integer notificationId : notificationIds)
            {
                Query sqlQuery = session.createQuery("update PushNotificationDBBean p set p.location = :location where p.notificationId = :notificationId");
                sqlQuery.setString("location", null);
                sqlQuery.setInteger("notificationId", notificationId);
                sqlQuery.executeUpdate();
            }

            tx.commit();
        }
        finally {
            closeSession(session);
        }

    }

}
