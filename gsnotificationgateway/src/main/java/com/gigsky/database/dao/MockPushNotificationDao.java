package com.gigsky.database.dao;

import com.gigsky.database.bean.MockPushNotificationDBBean;
import com.gigsky.database.bean.PushNotificationDBBean;
import org.apache.commons.collections.CollectionUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by vinayr on 23/11/15.
 */
public class MockPushNotificationDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(MockPushNotificationDao.class);

    //Create an entry in the MockPushNotification table
    public int createMockPushNotification(MockPushNotificationDBBean mockPushNotification)
    {
        Session session = openSession();

        try
        {
            session.save(mockPushNotification);
            return mockPushNotification.getId();
        }

        finally {
            closeSession(session);
        }
    }

    //Update an entry in the MockPushNotification table
    public int updateMockPushNotification(MockPushNotificationDBBean mockPushNotification)
    {
        Session session = openSession();

        try
        {
            session.saveOrUpdate(mockPushNotification);
            return mockPushNotification.getId();
        }

        finally {
            closeSession(session);
        }
    }

    //Get an entry from MockPushNotification table
    public List<MockPushNotificationDBBean> getMockPushNotificationForEvent(int eventId)
    {
        Session session = openSession();

        try
        {
            List<MockPushNotificationDBBean> mockPushNotificationList = session.createCriteria(MockPushNotificationDBBean.class)
                    .add(Restrictions.eq("eventId", eventId))
                    .list();


            if (mockPushNotificationList != null)
            {
                return mockPushNotificationList;
            }

            logger.warn("Mock push notifications are missing for " + eventId);
            return null;
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        finally {
            closeSession(session);
        }

        return null;
    }

    public MockPushNotificationDBBean getMockPushNotification(int mockPushNotificationId)
    {
        Session session = openSession();
        MockPushNotificationDBBean mockPushNotificationDBBean = null;

        try
        {
            mockPushNotificationDBBean = (MockPushNotificationDBBean)  session.createCriteria(MockPushNotificationDBBean.class)
                    .add(Restrictions.eq("id", mockPushNotificationId))
                    .uniqueResult();

            if (mockPushNotificationDBBean == null)
            {
                logger.warn("Mock Push notification entry is missing for " + mockPushNotificationId);
            }
        }
        catch (Exception e)
        {
            // handle DB exception and return appropriate response.
        }
        finally {
            closeSession(session);
        }
        return mockPushNotificationDBBean;

    }


    //Delete from MockPushNotification table
    public void deleteMockPushNotification(MockPushNotificationDBBean mockPushNotification)
    {
        Session session = openSession();

        try
        {
            session.delete(mockPushNotification);
        }

        finally {
            closeSession(session);
        }
    }


    // get push notifications using notificationId
    public List<MockPushNotificationDBBean> getMockPushNotifications(int notificationId)
    {
        Session session = openSession();

        try
        {
            List<MockPushNotificationDBBean> mockPushNotificationList = session.createQuery(" select M from MockPushNotificationDBBean M " +
                    " where M.notificationId = :notificationId ")
                    .setParameter("notificationId", notificationId)
                    .list();

            if (CollectionUtils.isNotEmpty(mockPushNotificationList))
            {
                return mockPushNotificationList;
            }

            logger.warn("Push Notification List is null for notification Id:" + notificationId);

            return null;
        }
        finally {
            closeSession(session);
        }
    }

    /*
    * ===== Delete Mock push notification ====
    *
    * This method will remove location from push notification
    *
    * */
    public void deleteMockPushNotification(List<Integer> notificationIds, String anonymousEmailId)
    {
        Session session = openSession();
        Transaction tx = session.beginTransaction();

        try {

            for(Integer notificationId : notificationIds)
            {
                Query sqlQuery = session.createQuery("update MockPushNotificationDBBean p set p.location = :location, p.userId = :userId where p.notificationId = :notificationId");
                sqlQuery.setString("location", null);
                sqlQuery.setString("userId", anonymousEmailId);
                sqlQuery.setInteger("notificationId", notificationId);
                sqlQuery.executeUpdate();
            }

            tx.commit();
        }
        finally {
            closeSession(session);
        }

    }


}
