package com.gigsky.database.dao;

import com.gigsky.database.bean.MockEmailNotificationSentDBBean;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Created by prsubbareddy on 30/12/16.
 */
public class MockEmailNotificationSentDao extends AbstractDao {

    private static final Logger logger = LoggerFactory.getLogger(MockEmailNotificationSentDao.class);

    // Create Mock Email notification with data
    public int createMockEmailNotification(MockEmailNotificationSentDBBean mockEmailNotification)
    {
        Session session = openSession();

        try
        {
            session.save(mockEmailNotification);
            return mockEmailNotification.getId();
        }
        catch(Exception e)
        {
            logger.warn("Mock Email Notification creation Exception: "+e);
        }
        finally {
            closeSession(session);
        }
        return -1;
    }

    /*
    * ===== Delete Mock Email Notification ====
    *
    * This method will replace recipient from email notification data with anonymous email id and .
    * content from email notification set to null
    * */

    public void deleteMockEmailNotification(List<Integer> notificationIds, String anonymousEmailId) {

        Session session = openSession();
        Transaction tx = session.beginTransaction();

        try {

            for(Integer notificationId : notificationIds)
            {
                Query sqlQuery = session.createQuery("update MockEmailNotificationSentDBBean E set E.recipient = :recipient, E.content = :content where E.notificationId = :notificationId");
                sqlQuery.setString("recipient", anonymousEmailId);
                sqlQuery.setString("content", null);
                sqlQuery.setInteger("notificationId", notificationId);
                sqlQuery.executeUpdate();
            }
            tx.commit();
        }
        finally {
            closeSession(session);
        }
    }


}
