package com.gigsky.database.bean;

import java.sql.Timestamp;

/**
 * Created by vinayr on 19/10/15.
 */
public class EventPolicyDBBean {
    private int policyId;
    private int eventTypeId;
    private Timestamp createTime;
    private Timestamp updateTime;
    private Integer groupId;
    private Byte enableEvent;
    private int tenantId;

    public int getPolicyId() {
        return policyId;
    }

    public void setPolicyId(int policyId) {
        this.policyId = policyId;
    }

    public int getEventTypeId() {
        return eventTypeId;
    }

    public void setEventTypeId(int eventTypeId) {
        this.eventTypeId = eventTypeId;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public Byte getEnableEvent() {
        return enableEvent;
    }

    public void setEnableEvent(Byte enableEvent) {
        this.enableEvent = enableEvent;
    }

    public int getTenantId() {
        return tenantId;
    }

    public void setTenantId(int tenantId) {
        this.tenantId = tenantId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EventPolicyDBBean that = (EventPolicyDBBean) o;

        if (policyId != that.policyId) return false;
        if (eventTypeId != that.eventTypeId) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
        if (groupId != null ? !groupId.equals(that.groupId) : that.groupId != null) return false;
        if (enableEvent != null ? !enableEvent.equals(that.enableEvent) : that.enableEvent != null)
            return false;
        if (tenantId != that.tenantId) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = policyId;
        result = 31 * result + eventTypeId;
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        result = 31 * result + (groupId != null ? groupId.hashCode() : 0);
        result = 31 * result + (enableEvent != null ? enableEvent.hashCode() : 0);
        result = 31 * result + tenantId;
        return result;
    }

}
