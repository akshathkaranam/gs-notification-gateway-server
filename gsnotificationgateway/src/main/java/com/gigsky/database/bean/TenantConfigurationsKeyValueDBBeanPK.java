package com.gigsky.database.bean;

import java.io.Serializable;

/**
 * Created by prsubbareddy on 20/03/19.
 */
public class TenantConfigurationsKeyValueDBBeanPK implements Serializable {

    private String ckey;
    private int tenantId;

    public String getCkey() {
        return ckey;
    }

    public void setCkey(String ckey) {
        this.ckey = ckey;
    }

    public int getTenantId() {
        return tenantId;
    }

    public void setTenantId(int tenantId) {
        this.tenantId = tenantId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TenantConfigurationsKeyValueDBBeanPK that = (TenantConfigurationsKeyValueDBBeanPK) o;

        if (tenantId != that.tenantId) return false;
        if (ckey != null ? !ckey.equals(that.ckey) : that.ckey != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = ckey != null ? ckey.hashCode() : 0;
        result = 31 * result + tenantId;
        return result;
    }
}
