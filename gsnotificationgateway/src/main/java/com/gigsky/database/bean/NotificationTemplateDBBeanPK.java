package com.gigsky.database.bean;

import java.io.Serializable;

/**
 * Created by vinayr on 19/10/15.
 */
public class NotificationTemplateDBBeanPK implements Serializable {
    private int templateId;
    private int notificationTypeId;
    private int eventTypeId;

    public int getTemplateId() {
        return templateId;
    }

    public void setTemplateId(int templateId) {
        this.templateId = templateId;
    }

    public int getNotificationTypeId() {
        return notificationTypeId;
    }

    public void setNotificationTypeId(int notificationTypeId) {
        this.notificationTypeId = notificationTypeId;
    }

    public int getEventTypeId() {
        return eventTypeId;
    }

    public void setEventTypeId(int eventTypeId) {
        this.eventTypeId = eventTypeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        NotificationTemplateDBBeanPK that = (NotificationTemplateDBBeanPK) o;

        if (templateId != that.templateId) return false;
        if (notificationTypeId != that.notificationTypeId) return false;
        if (eventTypeId != that.eventTypeId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = templateId;
        result = 31 * result + notificationTypeId;
        result = 31 * result + eventTypeId;
        return result;
    }
}
