package com.gigsky.database.bean;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * Created by vinayr on 07/10/16.
 */
@Entity
@Table(name = "AppFeatureInfo", schema = "gigskyNotificationGateway")
public class AppFeatureInfoDBBean {
    private int id;
    private String clientType;
    private String clientVersion;
    private String userId;
    private Timestamp createTime;
    private Timestamp updateTime;
    private String config;
    private String features;
    private String baseUrl;
    private String logoBaseUrl;
    private String troubleshootingBaseUrl;
    private String updateMandatory;
    private int tenantId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getClientType() {
        return clientType;
    }

    public void setClientType(String clientType) {
        this.clientType = clientType;
    }

    public String getClientVersion() {
        return clientVersion;
    }

    public void setClientVersion(String clientVersion) {
        this.clientVersion = clientVersion;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp createTime) {
        this.updateTime = updateTime;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public String getFeatures() {
        return features;
    }

    public void setFeatures(String features) {
        this.features = features;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public String getLogoBaseUrl() {
        return logoBaseUrl;
    }

    public void setLogoBaseUrl(String logoBaseUrl) {
        this.logoBaseUrl = logoBaseUrl;
    }

    public String getTroubleshootingBaseUrl() {
        return troubleshootingBaseUrl;
    }

    public void setTroubleshootingBaseUrl(String troubleshootingBaseUrl) {
        this.troubleshootingBaseUrl = troubleshootingBaseUrl;
    }

    public String getUpdateMandatory() {
        return updateMandatory;
    }

    public void setUpdateMandatory(String updateMandatory) {
        this.updateMandatory = updateMandatory;
    }

    public int getTenantId() {
        return tenantId;
    }

    public void setTenantId(int tenantId) {
        this.tenantId = tenantId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AppFeatureInfoDBBean that = (AppFeatureInfoDBBean) o;

        if (id != that.id) return false;
        if (clientType != null ? !clientType.equals(that.clientType) : that.clientType != null) return false;
        if (clientVersion != null ? !clientVersion.equals(that.clientVersion) : that.clientVersion != null)
            return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (config != null ? !config.equals(that.config) : that.config != null) return false;
        if (features != null ? !features.equals(that.features) : that.features != null) return false;
        if (baseUrl != null ? !baseUrl.equals(that.baseUrl) : that.baseUrl != null) return false;
        if (logoBaseUrl != null ? !logoBaseUrl.equals(that.logoBaseUrl) : that.logoBaseUrl != null) return false;
        if (troubleshootingBaseUrl != null ? !troubleshootingBaseUrl.equals(that.troubleshootingBaseUrl)
                : that.troubleshootingBaseUrl != null) return false;
        if (updateMandatory != null ? !updateMandatory.equals(that.updateMandatory) : that.updateMandatory != null)
            return false;
        if (tenantId != that.tenantId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (clientType != null ? clientType.hashCode() : 0);
        result = 31 * result + (clientVersion != null ? clientVersion.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (config != null ? config.hashCode() : 0);
        result = 31 * result + (features != null ? features.hashCode() : 0);
        result = 31 * result + (baseUrl != null ? baseUrl.hashCode() : 0);
        result = 31 * result + (logoBaseUrl != null ? logoBaseUrl.hashCode() : 0);
        result = 31 * result + (troubleshootingBaseUrl != null ? troubleshootingBaseUrl.hashCode() : 0);
        result = 31 * result + (updateMandatory != null ? updateMandatory.hashCode() : 0);
        result = 31 * result + tenantId;
        return result;
    }

}
