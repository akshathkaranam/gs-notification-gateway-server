package com.gigsky.database.bean;

import java.sql.Timestamp;
import java.util.Arrays;

/**
 * Created by vinayr on 19/10/15.
 */
public class CustomerEventDBBean {
    private String data;
    private int customerId;
    private Timestamp createTime;
    private Timestamp updateTime;
    private int eventId;
    private int tenantId;

    public void setData(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public int getEventId() {
        return eventId;
    }

    public void setEventId(int eventId) {
        this.eventId = eventId;
    }

    public int getTenantId() {
        return tenantId;
    }

    public void setTenantId(int tenantId) {
        this.tenantId = tenantId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomerEventDBBean that = (CustomerEventDBBean) o;

        if (customerId != that.customerId) return false;
        if (eventId != that.eventId) return false;
        if (data != null ? !data.equals(that.data) : that.data != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
        if (tenantId != that.tenantId) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = (data != null) ? data.hashCode() : 0;
        result = 31 * result + customerId;
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        result = 31 * result + eventId;
        result = 31 * result + tenantId;
        return result;
    }
}
