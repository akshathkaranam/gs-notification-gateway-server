package com.gigsky.database.bean;

import java.sql.Timestamp;
import java.util.Set;

/**
 * Created by vinayr on 19/10/15.
 */
public class CustomerDBBean {

    public static final String CUSTOMER_TYPE_CONSUMER = "CONSUMER";
    public static final String CUSTOMER_TYPE_ENTERPRISE = "ENTERPRISE";

    private int id;
    private String preferences;
    private Timestamp createTime;
    private Timestamp updateTime;
    private String customerType;
    private Set<DeviceDBBean> customerDevices;
    private String customerUuid;
    private int tenantId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPreferences() {
        return preferences;
    }

    public void setPreferences(String preferences) {
        this.preferences = preferences;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public String getCustomerType() {
        return customerType;
    }

    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    public Set<DeviceDBBean> getCustomerDevices() {
        return customerDevices;
    }

    public void setCustomerDevices(Set<DeviceDBBean> customerDevices) {
        this.customerDevices = customerDevices;
    }

    public String getCustomerUuid() {
        return customerUuid;
    }

    public void setCustomerUuid(String customerUuid) {
        this.customerUuid = customerUuid;
    }

    public int getTenantId() {
        return tenantId;
    }

    public void setTenantId(int tenantId) {
        this.tenantId = tenantId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomerDBBean that = (CustomerDBBean) o;

        if (id != that.id) return false;
        if (preferences != null ? !preferences.equals(that.preferences) : that.preferences != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;
        if (customerUuid != null ? !customerUuid.equals(that.customerUuid) : that.customerUuid != null) return false;
        if (customerType != null ? !customerType.equals(that.customerType) : that.customerType != null) return false;
        if (tenantId != that.tenantId) return false;
        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (preferences != null ? preferences.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        result = 31 * result + (customerUuid != null ? customerUuid.hashCode() : 0);
        result = 31 * result + (customerType != null ? customerType.hashCode() : 0);
        result = 31 * result + tenantId;
        return result;
    }
}
