package com.gigsky.database.bean;

import java.io.Serializable;

/**
 * Created by vinayr on 19/10/15.
 */
public class GroupEventCustomerDBBeanPK implements Serializable {
    private int customerId;
    private int groupEventEventId;

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getGroupEventEventId() {
        return groupEventEventId;
    }

    public void setGroupEventEventId(int groupEventEventId) {
        this.groupEventEventId = groupEventEventId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        GroupEventCustomerDBBeanPK that = (GroupEventCustomerDBBeanPK) o;

        if (customerId != that.customerId) return false;
        if (groupEventEventId != that.groupEventEventId) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = customerId;
        result = 31 * result + groupEventEventId;
        return result;
    }
}
