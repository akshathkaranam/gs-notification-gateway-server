package com.gigsky.database.bean;

import java.sql.Timestamp;

/**
 * Created by prsubbareddy on 07/11/17.
 */
public class CustomerContextDBBean {
    private int id;
    private Long gsCustomerId;
    private String token;
    private String iccid;
    private String simNickname;
    private String simType;
    private String deviceId;
    private String userId;
    private Timestamp createTime;
    private Timestamp updateTime;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Long getGsCustomerId() {
        return gsCustomerId;
    }

    public void setGsCustomerId(Long gsCustomerId) {
        this.gsCustomerId = gsCustomerId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public String getSimNickname() {
        return simNickname;
    }

    public void setSimNickname(String simNickname) {
        this.simNickname = simNickname;
    }

    public String getSimType() {
        return simType;
    }

    public void setSimType(String simType) {
        this.simType = simType;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CustomerContextDBBean that = (CustomerContextDBBean) o;

        if (id != that.id) return false;
        if (gsCustomerId != null ? !gsCustomerId.equals(that.gsCustomerId) : that.gsCustomerId != null) return false;
        if (token != null ? !token.equals(that.token) : that.token != null) return false;
        if (iccid != null ? !iccid.equals(that.iccid) : that.iccid != null) return false;
        if (simNickname != null ? !simNickname.equals(that.simNickname) : that.simNickname != null) return false;
        if (simType != null ? !simType.equals(that.simType) : that.simType != null) return false;
        if (deviceId != null ? !deviceId.equals(that.deviceId) : that.deviceId != null) return false;
        if (userId != null ? !userId.equals(that.userId) : that.userId != null) return false;
        if (createTime != null ? !createTime.equals(that.createTime) : that.createTime != null) return false;
        if (updateTime != null ? !updateTime.equals(that.updateTime) : that.updateTime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (gsCustomerId != null ? gsCustomerId.hashCode() : 0);
        result = 31 * result + (token != null ? token.hashCode() : 0);
        result = 31 * result + (iccid != null ? iccid.hashCode() : 0);
        result = 31 * result + (simNickname != null ? simNickname.hashCode() : 0);
        result = 31 * result + (simType != null ? simType.hashCode() : 0);
        result = 31 * result + (deviceId != null ? deviceId.hashCode() : 0);
        result = 31 * result + (userId != null ? userId.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        result = 31 * result + (updateTime != null ? updateTime.hashCode() : 0);
        return result;
    }
}
