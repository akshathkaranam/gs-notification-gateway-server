package com.gigsky.database.exception;

import com.gigsky.exception.NotificationException;
import com.gigsky.rest.exception.ErrorCode;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

/**
 * Created by vinayr on 21/10/15.
 */
public class DatabaseException extends NotificationException {

    public DatabaseException(ErrorCode code, String msg)
    {
        super(code, msg);
    }

    public DatabaseException(String message)
    {
        super(message);
    }

    public DatabaseException(String message, Exception e)
    {
        super(message, e);
    }

    public DatabaseException(Exception e)
    {
        super(e);
    }
}
