package util;

import com.gigsky.common.Configurations;
import com.gigsky.database.bean.TenantDBBean;
import com.gigsky.database.dao.ConfigurationKeyValuesDao;
import com.gigsky.database.dao.TenantDao;
import com.gigsky.exception.ServiceValidationException;
import com.gigsky.rest.exception.ErrorCode;
import com.gigsky.service.ValidationService;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.HttpHeaders;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

/**
 * Created by anant on 23/11/15.
 */
public class GSUtil {

    private static final String AUTHENTICATION_HEADER = "Authorization";
    public static final String TENANT_ID_HEADER = "tenantsId";

    private static final Logger logger = LoggerFactory.getLogger(GSUtil.class);

    private static String localeStrings = "{\"strings\":\n" +
            "  [\n" +
            "    {\"string\" : \"Customer\", \"localize\":\n" +
            "    [ {\"langCode\" : \"de\",  \"localizedString\" : \"Customer-de\"},\n" +
            "      {\"langCode\" : \"es\",  \"localizedString\" : \"Customer-es\"},\n" +
            "      {\"langCode\" : \"fr\",  \"localizedString\" : \"Customer-fr\"},\n" +
            "      {\"langCode\" : \"it\",  \"localizedString\" : \"Customer-it\"},\n" +
            "      {\"langCode\" : \"ja\",  \"localizedString\" : \"Customer-ja\"},\n" +
            "      {\"langCode\" : \"zh\",  \"localizedString\" : \"Customer-zh\"},\n" +
            "      {\"langCode\" : \"zh-HK\",  \"localizedString\" : \"Customer-zh-HK\"},\n" +
            "      {\"langCode\" : \"zh-TW\",  \"localizedString\" : \"Customer-zh-TW\"}\n" +
            "      ]},\n" +
            "    ]\n" +
            "}";



    public static String getLocalizedString(String engString, String lang) {

        try {
            // Convert locale strings to JSON object
            JSONObject obj = new JSONObject(localeStrings);

            // Fetch Strings array from input json object data
            JSONArray stringArray = obj.getJSONArray("strings");

            for(int i = 0; i < stringArray.length(); i ++) {

                // Fetching objData from string array
                JSONObject objData = stringArray.getJSONObject(i);

                // Check if engString matches with data String
                if(objData.optString("string").equals(engString)) {

                    // Fetch localised string array for eng string
                    JSONArray localisedArray = objData.getJSONArray("localize");
                    for(int j = 0; j < localisedArray.length(); j ++) {

                        JSONObject localizeObjData = localisedArray.getJSONObject(j);

                        // check if lang matches with localised string language
                        // Fetch localised string
                        if(localizeObjData.optString("langCode").equals(lang)) {
                            return localizeObjData.optString("localizedString");
                        }
                    }
                }
            }
        }
        catch(Exception e) {
            logger.error("GSUtil localisation parsing error " + e.toString(),e);
        }

        return engString;
    }


    public static String extractToken(HttpHeaders headers, ValidationService validationService) throws ServiceValidationException {
        String token = null;
        List<String> headerValues = headers.getRequestHeader(AUTHENTICATION_HEADER);

        if(headerValues == null)
        {
            logger.error("GSUtil extractToken Token missing");
            throw new ServiceValidationException(ErrorCode.TOKEN_MISSING, "Token missing");
        }

        if(headerValues.size() > 0)
        {
            token = headerValues.get(0);
        }
        else
        {
            logger.error("GSUtil extractToken Token missing");
            throw new ServiceValidationException(ErrorCode.TOKEN_MISSING, "Token missing");
        }

        validationService.validateToken(token);
        token = token.substring(6);
        return token;
    }

    public static int extractTenantId(HttpHeaders headers, ValidationService validationService, TenantDao tenantDao, ConfigurationKeyValuesDao configurationKeyValuesDao) throws ServiceValidationException {
        List<String> headerValues = headers.getRequestHeader(TENANT_ID_HEADER);
        // Getting default tenant value from config key values
        int tenantId = Integer.parseInt(configurationKeyValuesDao.getConfigurationValue(Configurations.ConfigurationKeyValueType.DEFAULT_TENANT_ID, "1"));

        if(headerValues != null && headerValues.size() > 0) {

            String tId = headerValues.get(0);
            if(StringUtils.isNotEmpty(tId)) {
                tenantId = Integer.parseInt(tId);
                validationService.validateTenantId(tenantId);
            } else {
                logger.error("GSUtil Tenant id is invalid");
                throw new ServiceValidationException(ErrorCode.INVALID_TENANT_ID, "Invalid Tenant id");
            }
        }

        // Validation for tenant id:  Tenant id is present and not in database
        TenantDBBean tenantDBBean = tenantDao.getTenantDetails(tenantId);
        if(tenantDBBean == null)
        {
            logger.error("GSUtil Tenant id is invalid");
            throw new ServiceValidationException(ErrorCode.INVALID_TENANT_ID, "Invalid Tenant id");
        }

        return tenantId;
    }

    public static String extractBase64Token(HttpHeaders headers, ValidationService validationService) throws ServiceValidationException {
        String token = null;
        List<String> headerValues = headers.getRequestHeader(AUTHENTICATION_HEADER);

        if(headerValues == null)
        {
            logger.error("GSUtil extractBase64Token Token missing");
            throw new ServiceValidationException(ErrorCode.TOKEN_MISSING, "Token missing");
        }

        if(headerValues.size() > 0)
        {
            token = headerValues.get(0);
        }
        else
        {
            logger.error("GSUtil extractBase64Token Token missing");
            throw new ServiceValidationException(ErrorCode.TOKEN_MISSING, "Token missing");
        }

        if(token == null || !token.startsWith("Basic "))
        {
            logger.error("GSUtil extractBase64Token Token missing");
            throw new ServiceValidationException(ErrorCode.TOKEN_MISSING, "Token missing");
        }

        token = token.substring(6);
        return token;
    }

    // Getting total price and symbol
    public static String GSSDK_formatPrice(String price, String currency) {
        String num = "0";
        if (price != null) {
            float amount = Float.parseFloat(price);
            float amount1 = (float) Math.round(amount * 100) / 100;

            num = new DecimalFormat("#,###.##", new DecimalFormatSymbols(Locale.US)).format(amount1);
        }
        String curSign = getLocalCurrencySign(currency);

        return curSign + num;
    }

    // Getting currency symbol by passing country code
    public static String getLocalCurrencySign(String currency) {

        if(currency.equals("JPY")) {
            return "¥";
        } else if(currency.equals("GBP")) {
            return "£";
        } else if(currency.equals("EUR")) {
            return "€";
        } else {
            Currency obj = Currency.getInstance(currency);
            if(obj.getSymbol().contains("$"))
            {
                return "US$ ";
            }
            return obj.getSymbol();
        }
    }

    public static String decodeBaseToken(String token){

        // Decode data on other side, by processing encoded data
        byte[] valueDecoded= Base64.decodeBase64(token.getBytes() );
        return new String(valueDecoded);
    }

}
