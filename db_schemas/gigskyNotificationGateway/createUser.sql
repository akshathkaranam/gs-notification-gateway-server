
#In this script grant permissions to all database schema

#create users 

#Since there is no way to check if uses are there using sql command,we have workaround
#A good workaround is to grant a harmless privilege to the user before dropping it.  This will create the user if it doesn't exist, so that it can be dropped safely,
GRANT USAGE ON *.* TO 'gsNGW'@'%' IDENTIFIED BY 'gsnotificationgatewayG1gsky12-s4';
DROP USER 'gsNGW'@'%';
CREATE USER 'gsNGW'@'%' IDENTIFIED BY 'gsnotificationgatewayG1gsky12-s4';

GRANT USAGE ON *.* TO 'gsNGW'@'localhost' IDENTIFIED BY 'gsnotificationgatewayG1gsky12-s4';
DROP USER 'gsNGW'@'localhost';
CREATE USER 'gsNGW'@'localhost' IDENTIFIED BY 'gsnotificationgatewayG1gsky12-s4';


GRANT USAGE ON *.* TO 'gigskyAdmin'@'%' IDENTIFIED BY 'adminG1gsky12-s4';
DROP USER 'gigskyAdmin'@'%';
CREATE USER 'gigskyAdmin'@'%' IDENTIFIED BY 'adminG1gsky12-s4';

GRANT USAGE ON *.* TO 'gigskyAdmin'@'localhost' IDENTIFIED BY 'adminG1gsky12-s4';
DROP USER 'gigskyAdmin'@'localhost';
CREATE USER 'gigskyAdmin'@'localhost' IDENTIFIED BY 'adminG1gsky12-s4';

#Grants previliges. Presenlty for user and admin all preveliges are given. Later needs to be decided based on the table
GRANT ALL PRIVILEGES ON gigskyNotificationGateway.* TO 'gsNGW'@'%' IDENTIFIED BY 'gsnotificationgatewayG1gsky12-s4';
GRANT ALL PRIVILEGES ON gigskyNotificationGateway.* TO 'gsNGW'@'localhost' IDENTIFIED BY 'gsnotificationgatewayG1gsky12-s4';

