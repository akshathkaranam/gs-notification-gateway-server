SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

ALTER TABLE `gigskyNotificationGateway`.`AppFeatureInfo` 
DROP COLUMN `desc`,
ADD COLUMN `updateTime` TIMESTAMP NULL DEFAULT NULL AFTER `createTime`,
ADD COLUMN `baseUrl` TEXT NULL DEFAULT NULL AFTER `features`,
ADD COLUMN `logoBaseUrl` TEXT NULL DEFAULT NULL AFTER `baseUrl`,
ADD COLUMN `troubleshootingBaseUrl` TEXT NULL DEFAULT NULL AFTER `logoBaseUrl`,
ADD COLUMN `updateMandatory` VARCHAR(45) NULL DEFAULT NULL AFTER `troubleshootingBaseUrl`;


DELIMITER $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`AppFeatureInfo_BEFORE_UPDATE` BEFORE UPDATE ON `AppFeatureInfo` FOR EACH ROW
    begin
	set new.updateTime = now();
end$$

DELIMITER ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
