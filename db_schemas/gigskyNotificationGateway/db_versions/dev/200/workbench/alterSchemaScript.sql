SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

CREATE TABLE IF NOT EXISTS `gigskyNotificationGateway`.`MockEmailNotificationSent` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `languageCode` VARCHAR(45) NULL DEFAULT NULL,
  `sender` VARCHAR(100) NULL DEFAULT NULL,
  `recipient` VARCHAR(200) NULL DEFAULT NULL,
  `bccRecipient` VARCHAR(200) NULL DEFAULT NULL,
  `subject` LONGTEXT NULL DEFAULT NULL,
  `content` LONGTEXT NULL DEFAULT NULL,
  `status` VARCHAR(45) NULL DEFAULT NULL,
  `createTime` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
  `updateTime` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_unicode_ci;


DELIMITER $$

USE `gigskyNotificationGateway`$$
CREATE DEFINER = CURRENT_USER TRIGGER `gigskyNotificationGateway`.`MockEmailNotificationSent_BEFORE_UPDATE` BEFORE UPDATE ON `MockEmailNotificationSent` FOR EACH ROW
  begin
    set new.updateTime = now();
  end$$


DELIMITER ;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
