START TRANSACTION;

UPDATE `gigskyNotificationGateway`.`EventPolicy` SET `tenant_id`='1', `enableEvent`='1', `groupType_id`=NULL;
UPDATE `gigskyNotificationGateway`.`MockUser` SET `tenant_id`='1';
UPDATE `gigskyNotificationGateway`.`NotificationTemplate` SET `tenant_id`='1';
UPDATE `gigskyNotificationGateway`.`AppFeatureInfo` SET `tenant_id`='1';
UPDATE `gigskyNotificationGateway`.`EmailEvent` SET `tenant_id`='1';
UPDATE `gigskyNotificationGateway`.`Customer` SET `tenant_id`='1';
UPDATE `gigskyNotificationGateway`.`CustomerEvent` SET `tenant_id`='1';
UPDATE `gigskyNotificationGateway`.`Event` SET `tenant_id`='1';
UPDATE `gigskyNotificationGateway`.`Notification` SET `tenant_id`='1';

INSERT INTO `gigskyNotificationGateway`.`EventPolicy` (`policy_id`, `eventType_id`, `tenant_id`, `enableEvent`, `groupType_id`) VALUES ('1', '1', '2', '1', NULL);
INSERT INTO `gigskyNotificationGateway`.`EventPolicy` (`policy_id`, `eventType_id`, `tenant_id`, `enableEvent`, `groupType_id`) VALUES ('2', '2', '2', '1', NULL);
INSERT INTO `gigskyNotificationGateway`.`EventPolicy` (`policy_id`, `eventType_id`, `tenant_id`, `enableEvent`, `groupType_id`) VALUES ('3', '3', '2', '1', NULL);
INSERT INTO `gigskyNotificationGateway`.`EventPolicy` (`policy_id`, `eventType_id`, `tenant_id`, `enableEvent`, `groupType_id`) VALUES ('4', '4', '2', '1', NULL);
INSERT INTO `gigskyNotificationGateway`.`EventPolicy` (`policy_id`, `eventType_id`, `tenant_id`, `enableEvent`, `groupType_id`) VALUES ('5', '5', '2', '1', NULL);

INSERT INTO `gigskyNotificationGateway`.`Template` (`id`, `description`, `content`, `languageCode`, `contentType`) VALUES ('81', 'Push Notification', '{\"title\":\"Data Plan Usage\",\"message\":\"Less than 10% of the data balance is remaining. Do you need to add more data?\"}', 'en', 'PUSH');
INSERT INTO `gigskyNotificationGateway`.`Template` (`id`, `description`, `content`, `languageCode`, `contentType`) VALUES ('82', 'Push Notification', '{\"title\":\"Data Plan Usage\",\"message\":\"50% or more of the data plan has been used.\"}', 'en', 'PUSH');
INSERT INTO `gigskyNotificationGateway`.`Template` (`id`, `description`, `content`, `languageCode`, `contentType`) VALUES ('83', 'Push Notification', '{\"title\":\"Data Plan Time to Use\",\"message\":\"The data plan will expire in the next 24 hours. Would you like to add more data?\"}', 'en', 'PUSH');
INSERT INTO `gigskyNotificationGateway`.`Template` (`id`, `description`, `content`, `languageCode`, `contentType`) VALUES ('84', 'Push Notification', '{\"title\":\"Data Plan Expired\",\"message\":\"The data plan has expired. Do you need to add a data plan?\"}', 'en', 'PUSH');
INSERT INTO `gigskyNotificationGateway`.`Template` (`id`, `description`, `content`, `languageCode`, `contentType`) VALUES ('85', 'Push Notification', '{\"title\":\"Data Plan Available to Use\",\"message\":\"Do you need a data plan for your current location?\"}', 'en', 'PUSH');
INSERT INTO `gigskyNotificationGateway`.`Template` (`id`, `description`, `content`, `languageCode`, `contentType`) VALUES ('86', 'Push Notification', '{\"title\":\"Data Plan Usage\",\"message\":\"データ残量が10%を切りました。eSIMプランを追加購入しますか？\"}', 'ja', 'PUSH');
INSERT INTO `gigskyNotificationGateway`.`Template` (`id`, `description`, `content`, `languageCode`, `contentType`) VALUES ('87', 'Push Notification', '{\"title\":\"Data Plan Usage\",\"message\":\"50%以上のデータプランがご利用済となりました。\"}', 'ja', 'PUSH');
INSERT INTO `gigskyNotificationGateway`.`Template` (`id`, `description`, `content`, `languageCode`, `contentType`) VALUES ('88', 'Push Notification', '{\"title\":\"Data Plan Time to Use\",\"message\":\"eSIMプランの期限が24時間以内に切れます。プランを追加しますか?\"}', 'ja', 'PUSH');
INSERT INTO `gigskyNotificationGateway`.`Template` (`id`, `description`, `content`, `languageCode`, `contentType`) VALUES ('89', 'Push Notification', '{\"title\":\"Data Plan Expired\",\"message\":\"eSIMプランが期限切れとなりました。追加購入しますか？\"}', 'ja', 'PUSH');
INSERT INTO `gigskyNotificationGateway`.`Template` (`id`, `description`, `content`, `languageCode`, `contentType`) VALUES ('90', 'Push Notification', '{\"title\":\"Data Plan Available to Use\",\"message\":\"現在地で利用可能なeSIMプランを利用しますか？\"}', 'ja', 'PUSH');

INSERT INTO `gigskyNotificationGateway`.`NotificationTemplate` (`template_id`, `notificationType_id`, `eventType_id`, `tenant_id`) VALUES ('81', '1', '1', '2');
INSERT INTO `gigskyNotificationGateway`.`NotificationTemplate` (`template_id`, `notificationType_id`, `eventType_id`, `tenant_id`) VALUES ('82', '1', '2', '2');
INSERT INTO `gigskyNotificationGateway`.`NotificationTemplate` (`template_id`, `notificationType_id`, `eventType_id`, `tenant_id`) VALUES ('83', '1', '3', '2');
INSERT INTO `gigskyNotificationGateway`.`NotificationTemplate` (`template_id`, `notificationType_id`, `eventType_id`, `tenant_id`) VALUES ('84', '1', '4', '2');
INSERT INTO `gigskyNotificationGateway`.`NotificationTemplate` (`template_id`, `notificationType_id`, `eventType_id`, `tenant_id`) VALUES ('85', '1', '5', '2');
INSERT INTO `gigskyNotificationGateway`.`NotificationTemplate` (`template_id`, `notificationType_id`, `eventType_id`, `tenant_id`) VALUES ('86', '1', '1', '2');
INSERT INTO `gigskyNotificationGateway`.`NotificationTemplate` (`template_id`, `notificationType_id`, `eventType_id`, `tenant_id`) VALUES ('87', '1', '2', '2');
INSERT INTO `gigskyNotificationGateway`.`NotificationTemplate` (`template_id`, `notificationType_id`, `eventType_id`, `tenant_id`) VALUES ('88', '1', '3', '2');
INSERT INTO `gigskyNotificationGateway`.`NotificationTemplate` (`template_id`, `notificationType_id`, `eventType_id`, `tenant_id`) VALUES ('89', '1', '4', '2');
INSERT INTO `gigskyNotificationGateway`.`NotificationTemplate` (`template_id`, `notificationType_id`, `eventType_id`, `tenant_id`) VALUES ('90', '1', '5', '2');


COMMIT;