#!/bin/bash

args=$@

UPGRADE_PATH=$(cd "$(dirname "$0")"; pwd)

. $DB_UTILS_PATH/commandLineParser.sh $args

SQL_ARGS=$($DB_UTILS_PATH/getMysqlArguments.sh)" --default-character-set=utf8"
SQL_ARGS_WITH_DATABASE=$($DB_UTILS_PATH/getMysqlArguments.sh inc-database)" --default-character-set=utf8  --protocol=tcp"

#Directory from where to read insert data
INSERT_DIR=$UPGRADE_PATH/tableEntriesForInsert
TEMPARORY_SQL_FILE_NAME=$UPGRADE_PATH/temperary_sql_file_name_1111.sql
rm -f $TEMPARORY_SQL_FILE_NAME

#if database does not exist; Fail
echo "Testing Database connection.."
mysql $SQL_ARGS_WITH_DATABASE --execute="SELECT 1" --silent --skip-column-names > /dev/null 2>&1
if [ "$?" != "0" ]; then
	echo "Database unreachable; exiting.."
	exit 1
fi


#read current database version; and should be 1 less than current version
currentVersion=$($DB_UTILS_PATH/checkLocalDatabaseVersion.sh $args)
if [ "$?" != "0" ]; then
	echo "Local database version check fail; exiting.."
	exit 1
fi
echo Local Database deployed version:$currentVersion



#read the upgrade version
if [ $parsed_Database_Version -le $currentVersion ]
then
	echo "database deployed version is already latest; exiting.."
	exit 1;
fi


#compose SQL Script file for upgrade
echo "set autocommit=0;" >> $TEMPARORY_SQL_FILE_NAME
echo "begin;" >> $TEMPARORY_SQL_FILE_NAME
echo "SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;" >> $TEMPARORY_SQL_FILE_NAME


cat $UPGRADE_PATH/workbench/alterSchemaScript.sql >> $TEMPARORY_SQL_FILE_NAME

#

if [ $currentVersion -eq "390" ] && [ -f "$UPGRADE_PATH/additionalDBChanges.sql" ]
then
cat $UPGRADE_PATH/additionalDBChanges.sql >> $TEMPARORY_SQL_FILE_NAME
fi


#insert (and replace) new table entries
#Load all data from csv files present in insert_data folder
CSV_FILE_LIST=$(ls $INSERT_DIR/*.csv | xargs -n1 basename)
echo "List of csv files" "" ""
echo "" ""
echo $CSV_FILE_LIST
echo "" ""
for CSV_FILE in $CSV_FILE_LIST
do
    TABLE_NAME=${CSV_FILE//.csv/}  # remove .csv from csv file 
    echo "inserting $CSV_FILE into table $TABLE_NAME"

    echo "LOAD DATA LOCAL INFILE '$INSERT_DIR/$CSV_FILE' REPLACE INTO TABLE $TABLE_NAME  CHARACTER SET 'utf8' FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\n' IGNORE 1 LINES;" >> $TEMPARORY_SQL_FILE_NAME
done





echo "SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;" >> $TEMPARORY_SQL_FILE_NAME
echo "commit;" >> $TEMPARORY_SQL_FILE_NAME

mysql $SQL_ARGS_WITH_DATABASE < $TEMPARORY_SQL_FILE_NAME

if [ "$?" != "0" ]; then
	echo "" " "
	echo "****Upgrade fail "

    rm -f $TEMPARORY_SQL_FILE_NAME
	exit 1;
fi

rm -f $TEMPARORY_SQL_FILE_NAME





orphanChildRows=$($DB_UTILS_PATH/checkReferentialIntegrity.sh $args)
if [ "$?" != "0" ] || [ "$orphanChildRows" != "" ]; then
	echo "Referential Integrity Failed, $orphanChildRows, Exiting.."
	exit 1
fi


echo "-Upgraded to version:"$($DB_UTILS_PATH/checkLocalDatabaseVersion.sh $args)

